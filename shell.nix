{ bootpkgs ? (import <nixpkgs> {}) }:

let
  pkgs = import (bootpkgs.fetchgit (import ./z/etc/versions/nixpkgs.nix)) {};
  ghc = pkgs.ghc;
  haskellPackages = pkgs.haskellPackages;
in
  pkgs.haskell.lib.buildStackProject {
    inherit ghc;
    name = "shrine-devenv";
    buildInputs = [
      pkgs.cabal-install
      pkgs.haskellPackages.binary_0_8_5_1
      pkgs.haskellPackages.binary-bits
      pkgs.haskellPackages.bytestring_0_10_8_2
      pkgs.haskellPackages.hprotoc
      pkgs.haskellPackages.snappy
      pkgs.haskellPackages.transformers_0_5_4_0
      pkgs.haskellPackages.tuple
      pkgs.snappy
    ];
  }
