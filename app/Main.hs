{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import Control.Monad.Trans.State (evalState)

import qualified Data.ByteString as B hiding(map, putStrLn)
import qualified Data.ByteString.Lazy as L hiding(map, putStrLn)
import Data.Binary.Get as G
import Data.List
import Data.Maybe
import Debug.Trace
import Numeric
import Data.Char

import Shrine.Replay as SR
import Shrine.Replay.Demo as SRD
import Shrine.Replay.Embed as SRE
import Shrine.Wire.Demo as SWD
import Shrine.Wire.Embed as SWE
import Shrine.Wire.Protobuf.Common.Demo.CDemoPacket as D_P

data Preface = Preface { prefHeader :: B.ByteString
                       , prefOffset1 :: Int
                       , prefOffset2 :: Int
                       } deriving (Show, Read)

getPreface :: G.Get Preface
getPreface = do
  header <- G.getByteString(8)
  offset1 <- G.getWord32le
  offset2 <- G.getWord32le
  return $ Preface { prefHeader=header
                   , prefOffset1=(fromIntegral offset1)
                   , prefOffset2=(fromIntegral offset2)}

readPreface :: L.ByteString -> (L.ByteString, ByteOffset, Preface)
readPreface demo =
  let
    preface = G.runGetOrFail getPreface demo
  in
    case preface of
      Left  (bs, bo, s)       -> error "not a source 2 demo file"
      Right (bs, bo, preface) -> (bs, bo, preface)

onlySyncTick :: SWD.DemoMessage -> Bool
onlySyncTick (SWD.DemoSyncTick _ _) = True
onlySyncTick _                      = False

onlyPackets :: SWD.DemoMessage -> Bool
onlyPackets (SWD.DemoPacket _ _) = True
onlyPackets _                    = False

exceptPacketsAndFullPackets :: SWD.DemoMessage -> Bool
exceptPacketsAndFullPackets (SWD.DemoPacket _ _)     = False
exceptPacketsAndFullPackets (SWD.DemoFullPacket _ _) = False
exceptPacketsAndFullPackets _                        = True

checkSyncTickOrError :: Maybe Int -> Int
checkSyncTickOrError index =
  case index of
    Just a  -> a
    Nothing -> error "demo without a sync tick?"

flattenPacket :: SWD.DemoMessage -> [SWE.EmbedMessage]
flattenPacket (SWD.DemoPacket tick protobuf) =
  case (D_P.data' protobuf) of
    Just data' -> let strictData = L.toStrict data'
                  in evalState (SRE.getEmbeds 0 []) (SR.mkInitState strictData)
    Nothing -> []
flattenPacket _ = error "cannot flatten non-packet message"

tickFromProtobuf :: SWD.DemoMessage -> Int
tickFromProtobuf (SWD.DemoPacket tick pb) = tick
tickFromProtobuf _ = error "whatever"

dataFromProtobuf :: SWD.DemoMessage -> L.ByteString
dataFromProtobuf (SWD.DemoPacket _ pb) = fromJust $ D_P.data' pb
dataFromProtobuf _ = error "whatever"

main :: IO ()
main = do
  demo <- L.readFile "3360438828.dem"
  let (afterPreface, _, preface) = readPreface demo
  let allDemoMessages = readAllDemoMessages afterPreface []
  let syncTickIndex = checkSyncTickOrError $ findIndex onlySyncTick allDemoMessages
  let prologue = take (syncTickIndex - 1) allDemoMessages
  let game = drop syncTickIndex allDemoMessages
  let firstPacket = head (filter onlyPackets game)
  -- putStrLn $ show (map (\x -> showIntAtBase 16 intToDigit x "") bytes)
  putStrLn $ show (flattenPacket firstPacket)

-- messages in prologue:
-- DemoFileHeader
-- DemoPacket (x10)
-- DemoSendTables
-- DemoClassInfo
-- DemoStringTables
