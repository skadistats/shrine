module Shrine.Wire.Embed ( deserializeEmbedMessage
                         , EmbedMessage(..)
                         , ToEmbedMessage
                         , toEmbedMessage
                         ) where

import qualified Data.ByteString.Lazy as L
import Text.ProtocolBuffers

import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_NOP as N_N
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_Disconnect as N_D
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_Tick as N_T
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SetConVar as N_SCV
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SignonState as N_SS
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SpawnGroup_Load as N_SGL
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SpawnGroup_ManifestUpdate as N_SGMU
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SpawnGroup_SetCreationTick as N_SGSCT
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SpawnGroup_Unload as N_SGU
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SpawnGroup_LoadCompleted as N_SGLC

import qualified Shrine.Wire.Protobuf.Common.NetMessages.CCLCMsg_ClientInfo as CLC_CI
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CCLCMsg_Move as CLC_M
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CCLCMsg_BaselineAck as CLC_BA
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CCLCMsg_LoadingProgress as CLC_LP

import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_ServerInfo as S_SI
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_FlattenedSerializer as S_FS
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_ClassInfo as S_CI
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_CreateStringTable as S_CST
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_UpdateStringTable as S_UST
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_VoiceInit as S_VI
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_VoiceData as S_VD
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_Print as S_Print
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_SetView as S_SV
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_ClearAllStringTables as S_CAST
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_PacketEntities as S_PE
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_PeerList as S_PL
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_HLTVStatus as S_HLTVS
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_FullFrameSplit as S_FFS

import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMessageResetHUD as UM_RHUD
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMessageSayText2 as UM_ST2
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMessageTextMsg as UM_TM
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMessageVoiceMask as UM_VM
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMessageSendAudio as UM_SA

import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_GameEventList as S1L_GEL
import qualified Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSource1LegacyListenEvents as S1L_LE
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CSVCMsg_GameEvent as S1L_GE
import qualified Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSosStartSoundEvent as S1L_SStartSE
import qualified Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSosStopSoundEvent as S1L_SStopSE
import qualified Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSosSetSoundEventParams as S1L_SSSEP
import qualified Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSosStopSoundEventHash as S1L_SSSEH

import qualified Shrine.Wire.Protobuf.S2.S2TempEntities.CMsgTEEffectDispatch as TE_ED

import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ChatEvent as DUM_CE
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_CreateLinearProjectile as DUM_CLP
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_DestroyLinearProjectile as DUM_DLP
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_GlobalLightColor as DUM_GLC
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_GlobalLightDirection as DUM_GLD
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_DodgeTrackingProjectiles as DUM_DTP
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_LocationPing as DUM_LP
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_MapLine as DUM_ML
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_MiniKillCamInfo as DUM_MKCI
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_MinimapEvent as DUM_ME
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_NevermoreRequiem as DUM_NR
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_OverheadEvent as DUM_OE
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SharedCooldown as DUM_SC
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SpectatorPlayerClick as DUM_SPC
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_UnitEvent as DUM_UE
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ParticleManager as DUM_PM
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_BotChat as DUM_BC
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_HudError as DUM_HE
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ItemPurchased as DUM_IP
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_WorldLine as DUM_WL
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ChatWheel as DUM_CW
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_GamerulesStateChanged as DUM_GSC
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SendStatPopup as DUM_SSP
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SendRoshanPopup as DUM_SRP
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_TE_Projectile as DUM_TEP
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_TE_ProjectileLoc as DUM_TEPL
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_TE_DotaBloodImpact as DUM_TEDBI
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_TE_UnitAnimation as DUM_TEUA
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_TE_UnitAnimationEnd as DUM_TEUAE
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_AbilitySteal as DUM_AS
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_CourierKilledAlert as DUM_CKA
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_EnemyItemAlert as DUM_EIA
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_PredictionResult as DUM_PR
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_HPManaAlert as DUM_HPMA
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SpectatorPlayerUnitOrders as DUM_SPUO
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ProjectionAbility as DUM_PA
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ProjectionEvent as DUM_PE
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CMsgDOTACombatLogEntry as DUM_CLE
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadataFile as DUM_MMF
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch as DUM_MD
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SelectPenaltyGold as DUM_SPG
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_RollDiceResult as DUM_RDR
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_FlipCoinResult as DUM_FCR
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMessage_TeamCaptainChanged as DUM_TCC
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SendRoshanSpectatorPhase as DUM_SRSP

data EmbedMessage =
    EmbedNetNOP Int N_N.CNETMsg_NOP -- 0
  | EmbedNetDisconnect Int N_D.CNETMsg_Disconnect -- 1
  | EmbedNetTick Int N_T.CNETMsg_Tick -- 4
  | EmbedNetSetConVar Int N_SCV.CNETMsg_SetConVar -- 6
  | EmbedNetSignonState Int N_SS.CNETMsg_SignonState -- 7
  | EmbedNetSpawnGroupLoad Int N_SGL.CNETMsg_SpawnGroup_Load  -- 8
  | EmbedNetSpawnGroupManifestUpdate Int N_SGMU.CNETMsg_SpawnGroup_ManifestUpdate -- 9
  | EmbedNetSpawnGroupSetCreationTick Int N_SGSCT.CNETMsg_SpawnGroup_SetCreationTick -- 11
  | EmbedNetSpawnGroupUnload Int N_SGU.CNETMsg_SpawnGroup_Unload -- 12
  | EmbedNetSpawnGroupLoadCompleted Int N_SGLC.CNETMsg_SpawnGroup_LoadCompleted -- 13

  | EmbedCLCClientInfo Int CLC_CI.CCLCMsg_ClientInfo -- 20
  | EmbedCLCMove Int CLC_M.CCLCMsg_Move -- 21
  | EmbedCLCBaselineAck Int CLC_BA.CCLCMsg_BaselineAck -- 23
  | EmbedCLCLoadingProgress Int CLC_LP.CCLCMsg_LoadingProgress -- 27

  | EmbedSvcServerInfo Int S_SI.CSVCMsg_ServerInfo -- 40
  | EmbedSvcFlattenedSerializer Int S_FS.CSVCMsg_FlattenedSerializer -- 41
  | EmbedSvcClassInfo Int S_CI.CSVCMsg_ClassInfo -- 42
  | EmbedSvcCreateStringTable Int S_CST.CSVCMsg_CreateStringTable -- 44
  | EmbedSvcUpdateStringTable Int S_UST.CSVCMsg_UpdateStringTable -- 45
  | EmbedSvcVoiceInit Int S_VI.CSVCMsg_VoiceInit -- 46
  | EmbedSvcVoiceData Int S_VD.CSVCMsg_VoiceData -- 47
  | EmbedSvcPrint Int S_Print.CSVCMsg_Print -- 48
  | EmbedSvcSetView Int S_SV.CSVCMsg_SetView -- 50
  | EmbedSvcClearAllStringTables Int S_CAST.CSVCMsg_ClearAllStringTables -- 51
  | EmbedSvcPacketEntities Int S_PE.CSVCMsg_PacketEntities -- 55
  | EmbedSvcPeerList Int S_PL.CSVCMsg_PeerList -- 60
  | EmbedSvcHLTVStatus Int S_HLTVS.CSVCMsg_HLTVStatus -- 62
  | EmbedSvcFullFrameSplit Int S_FFS.CSVCMsg_FullFrameSplit -- 70

  | EmbedUserMsgResetHUD Int UM_RHUD.CUserMessageResetHUD -- 115
  | EmbedUserMsgSayText2 Int UM_ST2.CUserMessageSayText2 -- 118
  | EmbedUserMsgTextMsg Int UM_TM.CUserMessageTextMsg -- 124
  | EmbedUserMsgVoiceMask Int UM_VM.CUserMessageVoiceMask -- 128
  | EmbedUserMsgSendAudio Int UM_SA.CUserMessageSendAudio -- 130

  | EmbedS1LGameEventList Int S1L_GEL.CSVCMsg_GameEventList -- 205
  | EmbedS1LListenEvents Int S1L_LE.CMsgSource1LegacyListenEvents -- 206
  | EmbedS1LGameEvent Int S1L_GE.CSVCMsg_GameEvent -- 207
  | EmbedSosStartSoundEvent Int S1L_SStartSE.CMsgSosStartSoundEvent -- 208
  | EmbedSosStopSoundEvent Int S1L_SStopSE.CMsgSosStopSoundEvent -- 209
  | EmbedSetSoundEventParams Int S1L_SSSEP.CMsgSosSetSoundEventParams -- 210
  | EmbedSosStopSoundEventHash Int S1L_SSSEH.CMsgSosStopSoundEventHash -- 212

  | EmbedTEEffectDispatch Int TE_ED.CMsgTEEffectDispatch -- 400

  | EmbedDOTAUserMsgChatEvent Int DUM_CE.CDOTAUserMsg_ChatEvent -- 466
  | EmbedDOTAUserMsgCreateLinearProjectile Int DUM_CLP.CDOTAUserMsg_CreateLinearProjectile -- 471
  | EmbedDOTAUserMsgDestroyLinearProjectile Int DUM_DLP.CDOTAUserMsg_DestroyLinearProjectile -- 472
  | EmbedDOTAUserMsgDodgeTrackingProjectiles Int DUM_DTP.CDOTAUserMsg_DodgeTrackingProjectiles -- 473
  | EmbedDOTAUserMsgGlobalLightColor Int DUM_GLC.CDOTAUserMsg_GlobalLightColor -- 474
  | EmbedDOTAUserMsgGlobalLightDirection Int DUM_GLD.CDOTAUserMsg_GlobalLightDirection -- 475
  | EmbedDOTAUserMsgLocationPing Int DUM_LP.CDOTAUserMsg_LocationPing -- 477
  | EmbedDOTAUserMsgMapLine Int DUM_ML.CDOTAUserMsg_MapLine -- 478
  | EmbedDOTAUserMsgMiniKillCamInfo Int DUM_MKCI.CDOTAUserMsg_MiniKillCamInfo -- 479
  | EmbedDOTAUserMsgMinimapEvent Int DUM_ME.CDOTAUserMsg_MinimapEvent -- 481
  | EmbedDOTAUserMsgNevermoreRequiem Int DUM_NR.CDOTAUserMsg_NevermoreRequiem -- 482
  | EmbedDOTAUserMsgOverheadEvent Int DUM_OE.CDOTAUserMsg_OverheadEvent -- 483
  | EmbedDOTAUserMsgSharedCooldown Int DUM_SC.CDOTAUserMsg_SharedCooldown -- 485
  | EmbedDOTAUserMsgSpectatorPlayerClick Int DUM_SPC.CDOTAUserMsg_SpectatorPlayerClick -- 486
  | EmbedDOTAUserMsgUnitEvent Int DUM_UE.CDOTAUserMsg_UnitEvent -- 488
  | EmbedDOTAUserMsgParticleManager Int DUM_PM.CDOTAUserMsg_ParticleManager -- 489
  | EmbedDOTAUserMsgBotChat Int DUM_BC.CDOTAUserMsg_BotChat -- 490
  | EmbedDOTAUserMsgHudError Int DUM_HE.CDOTAUserMsg_HudError -- 491
  | EmbedDOTAUserMsgItemPurchased Int DUM_IP.CDOTAUserMsg_ItemPurchased -- 492
  | EmbedDOTAUserMsgWorldLine Int DUM_WL.CDOTAUserMsg_WorldLine -- 497
  | EmbedDOTAUserMsgChatWheel Int DUM_CW.CDOTAUserMsg_ChatWheel -- 501
  | EmbedDOTAUserMsgGamerulesStateChanged Int DUM_GSC.CDOTAUserMsg_GamerulesStateChanged -- 506
  | EmbedDOTAUserMsgSendStatPopup Int DUM_SSP.CDOTAUserMsg_SendStatPopup -- 510
  | EmbedDOTAUserMsgSendRoshanPopup Int DUM_SRP.CDOTAUserMsg_SendRoshanPopup -- 512
  | EmbedDOTAUserMsgTEProjectile Int DUM_TEP.CDOTAUserMsg_TE_Projectile -- 518
  | EmbedDOTAUserMsgTEProjectileLoc Int DUM_TEPL.CDOTAUserMsg_TE_ProjectileLoc -- 519
  | EmbedDOTAUserMsgTEDotaBloodImpact Int DUM_TEDBI.CDOTAUserMsg_TE_DotaBloodImpact -- 520
  | EmbedDOTAUserMsgTEUnitAnimation Int DUM_TEUA.CDOTAUserMsg_TE_UnitAnimation -- 521
  | EmbedDOTAUserMsgTEUnitAnimationEnd Int DUM_TEUAE.CDOTAUserMsg_TE_UnitAnimationEnd -- 522
  | EmbedDOTAUserMsgAbilitySteal Int DUM_AS.CDOTAUserMsg_AbilitySteal -- 532
  | EmbedDOTAUserMsgCourierKilledAlert Int DUM_CKA.CDOTAUserMsg_CourierKilledAlert -- 533
  | EmbedDOTAUserMsgEnemyItemAlert Int DUM_EIA.CDOTAUserMsg_EnemyItemAlert -- 534
  | EmbedDOTAUserMsgPredictionResult Int DUM_PR.CDOTAUserMsg_PredictionResult -- 542
  | EmbedDOTAUserMsgHPManaAlert Int DUM_HPMA.CDOTAUserMsg_HPManaAlert -- 544
  | EmbedDOTAUserMsgSpectatorPlayerUnitOrders Int DUM_SPUO.CDOTAUserMsg_SpectatorPlayerUnitOrders -- 547
  | EmbedDOTAUserMsgProjectionAbility Int DUM_PA.CDOTAUserMsg_ProjectionAbility -- 552
  | EmbedDOTAUserMsgProjectionEvent Int DUM_PE.CDOTAUserMsg_ProjectionEvent -- 553
  | EmbedDOTAUserMsgCombatLogEntry Int DUM_CLE.CMsgDOTACombatLogEntry -- 554
  | EmbedDOTAUserMsgMatchMetadataFile Int DUM_MMF.CDOTAMatchMetadataFile -- 557
  | EmbedDOTAUserMsgMatchDetails Int DUM_MD.CMsgDOTAMatch -- 558
  | EmbedDOTAUserMsgSelectPenaltyGold Int DUM_SPG.CDOTAUserMsg_SelectPenaltyGold -- 563
  | EmbedDOTAUserMsgRollDiceResult Int DUM_RDR.CDOTAUserMsg_RollDiceResult -- 564
  | EmbedDOTAUserMsgFlipCoinResult Int DUM_FCR.CDOTAUserMsg_FlipCoinResult -- 565
  | EmbedDOTAUserMsgTeamCaptainChanged Int DUM_TCC.CDOTAUserMessage_TeamCaptainChanged -- 567
  | EmbedDOTAUserMsgSendRoshanSpectatorPhase Int DUM_SRSP.CDOTAUserMsg_SendRoshanSpectatorPhase -- 568
  deriving (Eq, Show)

class ToEmbedMessage a where
  toEmbedMessage :: Int -> a -> EmbedMessage

instance ToEmbedMessage N_N.CNETMsg_NOP where -- 0
  toEmbedMessage tick message = EmbedNetNOP tick message

instance ToEmbedMessage N_D.CNETMsg_Disconnect where -- 1
  toEmbedMessage tick message = EmbedNetDisconnect tick message

instance ToEmbedMessage N_T.CNETMsg_Tick where -- 4
  toEmbedMessage tick message = EmbedNetTick tick message

instance ToEmbedMessage N_SCV.CNETMsg_SetConVar where -- 6
  toEmbedMessage tick message = EmbedNetSetConVar tick message

instance ToEmbedMessage N_SS.CNETMsg_SignonState where -- 7
  toEmbedMessage tick message = EmbedNetSignonState tick message

instance ToEmbedMessage N_SGL.CNETMsg_SpawnGroup_Load where -- 8
  toEmbedMessage tick message = EmbedNetSpawnGroupLoad tick message

instance ToEmbedMessage N_SGMU.CNETMsg_SpawnGroup_ManifestUpdate where -- 9
  toEmbedMessage tick message = EmbedNetSpawnGroupManifestUpdate tick message

instance ToEmbedMessage N_SGSCT.CNETMsg_SpawnGroup_SetCreationTick where -- 11
  toEmbedMessage tick message = EmbedNetSpawnGroupSetCreationTick tick message

instance ToEmbedMessage N_SGU.CNETMsg_SpawnGroup_Unload where -- 12
  toEmbedMessage tick message = EmbedNetSpawnGroupUnload tick message

instance ToEmbedMessage N_SGLC.CNETMsg_SpawnGroup_LoadCompleted where -- 13
  toEmbedMessage tick message = EmbedNetSpawnGroupLoadCompleted tick message

instance ToEmbedMessage CLC_CI.CCLCMsg_ClientInfo where -- 20
  toEmbedMessage tick message = EmbedCLCClientInfo tick message

instance ToEmbedMessage CLC_M.CCLCMsg_Move where -- 21
  toEmbedMessage tick message = EmbedCLCMove tick message

instance ToEmbedMessage CLC_BA.CCLCMsg_BaselineAck where -- 23
  toEmbedMessage tick message = EmbedCLCBaselineAck tick message

instance ToEmbedMessage CLC_LP.CCLCMsg_LoadingProgress where -- 27
  toEmbedMessage tick message = EmbedCLCLoadingProgress tick message

instance ToEmbedMessage S_SI.CSVCMsg_ServerInfo where -- 40
  toEmbedMessage tick message = EmbedSvcServerInfo tick message

instance ToEmbedMessage S_FS.CSVCMsg_FlattenedSerializer where -- 41
  toEmbedMessage tick message = EmbedSvcFlattenedSerializer tick message

instance ToEmbedMessage S_CI.CSVCMsg_ClassInfo where -- 42
  toEmbedMessage tick message = EmbedSvcClassInfo tick message

instance ToEmbedMessage S_CST.CSVCMsg_CreateStringTable where -- 44
  toEmbedMessage tick message = EmbedSvcCreateStringTable tick message

instance ToEmbedMessage S_UST.CSVCMsg_UpdateStringTable where -- 45
  toEmbedMessage tick message = EmbedSvcUpdateStringTable tick message

instance ToEmbedMessage S_VI.CSVCMsg_VoiceInit where -- 46
  toEmbedMessage tick message = EmbedSvcVoiceInit tick message

instance ToEmbedMessage S_VD.CSVCMsg_VoiceData where -- 47
  toEmbedMessage tick message = EmbedSvcVoiceData tick message

instance ToEmbedMessage S_Print.CSVCMsg_Print where -- 48
  toEmbedMessage tick message = EmbedSvcPrint tick message

instance ToEmbedMessage S_SV.CSVCMsg_SetView where -- 50
  toEmbedMessage tick message = EmbedSvcSetView tick message

instance ToEmbedMessage S_CAST.CSVCMsg_ClearAllStringTables where -- 51
  toEmbedMessage tick message = EmbedSvcClearAllStringTables tick message

instance ToEmbedMessage S_PE.CSVCMsg_PacketEntities where -- 55
  toEmbedMessage tick message = EmbedSvcPacketEntities tick message

instance ToEmbedMessage S_PL.CSVCMsg_PeerList where -- 60
  toEmbedMessage tick message = EmbedSvcPeerList tick message

instance ToEmbedMessage S_HLTVS.CSVCMsg_HLTVStatus where -- 62
  toEmbedMessage tick message = EmbedSvcHLTVStatus tick message

instance ToEmbedMessage S_FFS.CSVCMsg_FullFrameSplit where -- 70
  toEmbedMessage tick message = EmbedSvcFullFrameSplit tick message

instance ToEmbedMessage UM_RHUD.CUserMessageResetHUD where -- 115
  toEmbedMessage tick message = EmbedUserMsgResetHUD tick message

instance ToEmbedMessage UM_ST2.CUserMessageSayText2 where -- 118
  toEmbedMessage tick message = EmbedUserMsgSayText2 tick message

instance ToEmbedMessage UM_TM.CUserMessageTextMsg where -- 124
  toEmbedMessage tick message = EmbedUserMsgTextMsg tick message

instance ToEmbedMessage UM_VM.CUserMessageVoiceMask where -- 128
  toEmbedMessage tick message = EmbedUserMsgVoiceMask tick message

instance ToEmbedMessage UM_SA.CUserMessageSendAudio where -- 130
  toEmbedMessage tick message = EmbedUserMsgSendAudio tick message

instance ToEmbedMessage S1L_GEL.CSVCMsg_GameEventList where -- 205
  toEmbedMessage tick message = EmbedS1LGameEventList tick message

instance ToEmbedMessage S1L_LE.CMsgSource1LegacyListenEvents where -- 206
  toEmbedMessage tick message = EmbedS1LListenEvents tick message

instance ToEmbedMessage S1L_GE.CSVCMsg_GameEvent where -- 207
  toEmbedMessage tick message = EmbedS1LGameEvent tick message

instance ToEmbedMessage S1L_SStartSE.CMsgSosStartSoundEvent where -- 208
  toEmbedMessage tick message = EmbedSosStartSoundEvent tick message

instance ToEmbedMessage S1L_SStopSE.CMsgSosStopSoundEvent where -- 209
  toEmbedMessage tick message = EmbedSosStopSoundEvent tick message

instance ToEmbedMessage S1L_SSSEP.CMsgSosSetSoundEventParams where -- 210
  toEmbedMessage tick message = EmbedSetSoundEventParams tick message

instance ToEmbedMessage S1L_SSSEH.CMsgSosStopSoundEventHash where -- 212
  toEmbedMessage tick message = EmbedSosStopSoundEventHash tick message

instance ToEmbedMessage TE_ED.CMsgTEEffectDispatch where -- 400
  toEmbedMessage tick message = EmbedTEEffectDispatch tick message

instance ToEmbedMessage DUM_CE.CDOTAUserMsg_ChatEvent where -- 466
  toEmbedMessage tick message = EmbedDOTAUserMsgChatEvent tick message

instance ToEmbedMessage DUM_CLP.CDOTAUserMsg_CreateLinearProjectile where -- 471
  toEmbedMessage tick message = EmbedDOTAUserMsgCreateLinearProjectile tick message

instance ToEmbedMessage DUM_DLP.CDOTAUserMsg_DestroyLinearProjectile where -- 472
  toEmbedMessage tick message = EmbedDOTAUserMsgDestroyLinearProjectile tick message

instance ToEmbedMessage DUM_DTP.CDOTAUserMsg_DodgeTrackingProjectiles where -- 473
  toEmbedMessage tick message = EmbedDOTAUserMsgDodgeTrackingProjectiles tick message

instance ToEmbedMessage DUM_GLC.CDOTAUserMsg_GlobalLightColor where -- 474
  toEmbedMessage tick message = EmbedDOTAUserMsgGlobalLightColor tick message

instance ToEmbedMessage DUM_GLD.CDOTAUserMsg_GlobalLightDirection where -- 475
  toEmbedMessage tick message = EmbedDOTAUserMsgGlobalLightDirection tick message

instance ToEmbedMessage DUM_LP.CDOTAUserMsg_LocationPing where -- 477
  toEmbedMessage tick message = EmbedDOTAUserMsgLocationPing tick message

instance ToEmbedMessage DUM_ML.CDOTAUserMsg_MapLine where -- 478
  toEmbedMessage tick message = EmbedDOTAUserMsgMapLine tick message

instance ToEmbedMessage DUM_MKCI.CDOTAUserMsg_MiniKillCamInfo where -- 479
  toEmbedMessage tick message = EmbedDOTAUserMsgMiniKillCamInfo tick message

instance ToEmbedMessage DUM_ME.CDOTAUserMsg_MinimapEvent where -- 481
  toEmbedMessage tick message = EmbedDOTAUserMsgMinimapEvent tick message

instance ToEmbedMessage DUM_NR.CDOTAUserMsg_NevermoreRequiem where -- 482
  toEmbedMessage tick message = EmbedDOTAUserMsgNevermoreRequiem tick message

instance ToEmbedMessage DUM_OE.CDOTAUserMsg_OverheadEvent where -- 483
  toEmbedMessage tick message = EmbedDOTAUserMsgOverheadEvent tick message

instance ToEmbedMessage DUM_SC.CDOTAUserMsg_SharedCooldown where -- 485
  toEmbedMessage tick message = EmbedDOTAUserMsgSharedCooldown tick message

instance ToEmbedMessage DUM_SPC.CDOTAUserMsg_SpectatorPlayerClick  where --486
  toEmbedMessage tick message = EmbedDOTAUserMsgSpectatorPlayerClick tick message

instance ToEmbedMessage DUM_UE.CDOTAUserMsg_UnitEvent where -- 488
  toEmbedMessage tick message = EmbedDOTAUserMsgUnitEvent tick message

instance ToEmbedMessage DUM_PM.CDOTAUserMsg_ParticleManager where -- 489
  toEmbedMessage tick message = EmbedDOTAUserMsgParticleManager tick message

instance ToEmbedMessage DUM_BC.CDOTAUserMsg_BotChat where -- 490
  toEmbedMessage tick message = EmbedDOTAUserMsgBotChat tick message

instance ToEmbedMessage DUM_HE.CDOTAUserMsg_HudError where -- 491
  toEmbedMessage tick message = EmbedDOTAUserMsgHudError tick message

instance ToEmbedMessage DUM_IP.CDOTAUserMsg_ItemPurchased where -- 492
  toEmbedMessage tick message = EmbedDOTAUserMsgItemPurchased tick message

instance ToEmbedMessage DUM_WL.CDOTAUserMsg_WorldLine where -- 497
  toEmbedMessage tick message = EmbedDOTAUserMsgWorldLine tick message

instance ToEmbedMessage DUM_CW.CDOTAUserMsg_ChatWheel where -- 501
  toEmbedMessage tick message = EmbedDOTAUserMsgChatWheel tick message

instance ToEmbedMessage DUM_GSC.CDOTAUserMsg_GamerulesStateChanged where -- 506
  toEmbedMessage tick message = EmbedDOTAUserMsgGamerulesStateChanged tick message

instance ToEmbedMessage DUM_SSP.CDOTAUserMsg_SendStatPopup where -- 510
  toEmbedMessage tick message = EmbedDOTAUserMsgSendStatPopup tick message

instance ToEmbedMessage DUM_SRP.CDOTAUserMsg_SendRoshanPopup where -- 512
  toEmbedMessage tick message = EmbedDOTAUserMsgSendRoshanPopup tick message

instance ToEmbedMessage DUM_TEP.CDOTAUserMsg_TE_Projectile where -- 518
  toEmbedMessage tick message = EmbedDOTAUserMsgTEProjectile tick message

instance ToEmbedMessage DUM_TEPL.CDOTAUserMsg_TE_ProjectileLoc where -- 519
  toEmbedMessage tick message = EmbedDOTAUserMsgTEProjectileLoc tick message

instance ToEmbedMessage DUM_TEDBI.CDOTAUserMsg_TE_DotaBloodImpact where -- 520
  toEmbedMessage tick message = EmbedDOTAUserMsgTEDotaBloodImpact tick message

instance ToEmbedMessage DUM_TEUA.CDOTAUserMsg_TE_UnitAnimation where -- 521
  toEmbedMessage tick message = EmbedDOTAUserMsgTEUnitAnimation tick message

instance ToEmbedMessage DUM_TEUAE.CDOTAUserMsg_TE_UnitAnimationEnd where -- 522
  toEmbedMessage tick message = EmbedDOTAUserMsgTEUnitAnimationEnd tick message

instance ToEmbedMessage DUM_AS.CDOTAUserMsg_AbilitySteal where -- 532
  toEmbedMessage tick message = EmbedDOTAUserMsgAbilitySteal tick message

instance ToEmbedMessage DUM_CKA.CDOTAUserMsg_CourierKilledAlert where -- 533
  toEmbedMessage tick message = EmbedDOTAUserMsgCourierKilledAlert tick message

instance ToEmbedMessage DUM_EIA.CDOTAUserMsg_EnemyItemAlert where -- 534
  toEmbedMessage tick message = EmbedDOTAUserMsgEnemyItemAlert tick message

instance ToEmbedMessage DUM_PR.CDOTAUserMsg_PredictionResult where -- 542
  toEmbedMessage tick message = EmbedDOTAUserMsgPredictionResult tick message

instance ToEmbedMessage DUM_HPMA.CDOTAUserMsg_HPManaAlert where -- 544
  toEmbedMessage tick message = EmbedDOTAUserMsgHPManaAlert tick message

instance ToEmbedMessage DUM_SPUO.CDOTAUserMsg_SpectatorPlayerUnitOrders where -- 547
  toEmbedMessage tick message = EmbedDOTAUserMsgSpectatorPlayerUnitOrders tick message

instance ToEmbedMessage DUM_PA.CDOTAUserMsg_ProjectionAbility where -- 552
  toEmbedMessage tick message = EmbedDOTAUserMsgProjectionAbility tick message

instance ToEmbedMessage DUM_PE.CDOTAUserMsg_ProjectionEvent where -- 553
  toEmbedMessage tick message = EmbedDOTAUserMsgProjectionEvent tick message

instance ToEmbedMessage DUM_CLE.CMsgDOTACombatLogEntry where -- 554
  toEmbedMessage tick message = EmbedDOTAUserMsgCombatLogEntry tick message

instance ToEmbedMessage DUM_MMF.CDOTAMatchMetadataFile where -- 557
  toEmbedMessage tick message = EmbedDOTAUserMsgMatchMetadataFile tick message

instance ToEmbedMessage DUM_MD.CMsgDOTAMatch where -- 558
  toEmbedMessage tick message = EmbedDOTAUserMsgMatchDetails tick message

instance ToEmbedMessage DUM_SPG.CDOTAUserMsg_SelectPenaltyGold where -- 563
  toEmbedMessage tick message = EmbedDOTAUserMsgSelectPenaltyGold tick message

instance ToEmbedMessage DUM_RDR.CDOTAUserMsg_RollDiceResult where -- 564
  toEmbedMessage tick message = EmbedDOTAUserMsgRollDiceResult tick message

instance ToEmbedMessage DUM_FCR.CDOTAUserMsg_FlipCoinResult where -- 565
  toEmbedMessage tick message = EmbedDOTAUserMsgFlipCoinResult tick message

instance ToEmbedMessage DUM_TCC.CDOTAUserMessage_TeamCaptainChanged where -- 567
  toEmbedMessage tick message = EmbedDOTAUserMsgTeamCaptainChanged tick message

instance ToEmbedMessage DUM_SRSP.CDOTAUserMsg_SendRoshanSpectatorPhase where -- 568
  toEmbedMessage tick message = EmbedDOTAUserMsgSendRoshanSpectatorPhase tick message

embedMessageOrError :: ToEmbedMessage a => Int -> Either String (a, L.ByteString) -> EmbedMessage
embedMessageOrError tick (Left err) = error (err ++ " at tick " ++ show tick)
embedMessageOrError tick (Right (marshalled, _)) = toEmbedMessage tick marshalled

deserializeEmbedMessage :: Int -> Int -> L.ByteString -> EmbedMessage
deserializeEmbedMessage 0 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_N.CNETMsg_NOP, L.ByteString))
deserializeEmbedMessage 1 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_D.CNETMsg_Disconnect, L.ByteString))
deserializeEmbedMessage 4 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_T.CNETMsg_Tick, L.ByteString))
deserializeEmbedMessage 6 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_SCV.CNETMsg_SetConVar, L.ByteString))
deserializeEmbedMessage 7 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_SS.CNETMsg_SignonState, L.ByteString))
deserializeEmbedMessage 8 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_SGL.CNETMsg_SpawnGroup_Load , L.ByteString))
deserializeEmbedMessage 9 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_SGMU.CNETMsg_SpawnGroup_ManifestUpdate, L.ByteString))
deserializeEmbedMessage 11 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_SGSCT.CNETMsg_SpawnGroup_SetCreationTick, L.ByteString))
deserializeEmbedMessage 12 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_SGU.CNETMsg_SpawnGroup_Unload, L.ByteString))
deserializeEmbedMessage 13 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (N_SGLC.CNETMsg_SpawnGroup_LoadCompleted, L.ByteString))
deserializeEmbedMessage 20 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (CLC_CI.CCLCMsg_ClientInfo, L.ByteString))
deserializeEmbedMessage 21 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (CLC_M.CCLCMsg_Move, L.ByteString))
deserializeEmbedMessage 23 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (CLC_BA.CCLCMsg_BaselineAck, L.ByteString))
deserializeEmbedMessage 27 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (CLC_LP.CCLCMsg_LoadingProgress, L.ByteString))
deserializeEmbedMessage 40 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_SI.CSVCMsg_ServerInfo, L.ByteString))
deserializeEmbedMessage 41 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_FS.CSVCMsg_FlattenedSerializer, L.ByteString))
deserializeEmbedMessage 42 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_CI.CSVCMsg_ClassInfo, L.ByteString))
deserializeEmbedMessage 44 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_CST.CSVCMsg_CreateStringTable, L.ByteString))
deserializeEmbedMessage 45 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_UST.CSVCMsg_UpdateStringTable, L.ByteString))
deserializeEmbedMessage 46 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_VI.CSVCMsg_VoiceInit, L.ByteString))
deserializeEmbedMessage 47 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_VD.CSVCMsg_VoiceData, L.ByteString))
deserializeEmbedMessage 48 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_Print.CSVCMsg_Print, L.ByteString))
deserializeEmbedMessage 50 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_SV.CSVCMsg_SetView, L.ByteString))
deserializeEmbedMessage 51 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_CAST.CSVCMsg_ClearAllStringTables, L.ByteString))
deserializeEmbedMessage 55 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_PE.CSVCMsg_PacketEntities, L.ByteString))
deserializeEmbedMessage 60 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_PL.CSVCMsg_PeerList, L.ByteString))
deserializeEmbedMessage 62 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_HLTVS.CSVCMsg_HLTVStatus, L.ByteString))
deserializeEmbedMessage 70 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S_FFS.CSVCMsg_FullFrameSplit, L.ByteString))
deserializeEmbedMessage 115 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (UM_RHUD.CUserMessageResetHUD, L.ByteString))
deserializeEmbedMessage 118 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (UM_ST2.CUserMessageSayText2, L.ByteString))
deserializeEmbedMessage 124 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (UM_TM.CUserMessageTextMsg, L.ByteString))
deserializeEmbedMessage 128 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (UM_VM.CUserMessageVoiceMask, L.ByteString))
deserializeEmbedMessage 130 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (UM_SA.CUserMessageSendAudio, L.ByteString))
deserializeEmbedMessage 205 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S1L_GEL.CSVCMsg_GameEventList, L.ByteString))
deserializeEmbedMessage 206 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S1L_LE.CMsgSource1LegacyListenEvents, L.ByteString))
deserializeEmbedMessage 207 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S1L_GE.CSVCMsg_GameEvent, L.ByteString))
deserializeEmbedMessage 208 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S1L_SStartSE.CMsgSosStartSoundEvent, L.ByteString))
deserializeEmbedMessage 209 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S1L_SStopSE.CMsgSosStopSoundEvent, L.ByteString))
deserializeEmbedMessage 210 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S1L_SSSEP.CMsgSosSetSoundEventParams, L.ByteString))
deserializeEmbedMessage 212 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (S1L_SSSEH.CMsgSosStopSoundEventHash, L.ByteString))
deserializeEmbedMessage 400 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (TE_ED.CMsgTEEffectDispatch, L.ByteString))
deserializeEmbedMessage 466 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_CE.CDOTAUserMsg_ChatEvent, L.ByteString))
deserializeEmbedMessage 471 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_CLP.CDOTAUserMsg_CreateLinearProjectile, L.ByteString))
deserializeEmbedMessage 472 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_DLP.CDOTAUserMsg_DestroyLinearProjectile, L.ByteString))
deserializeEmbedMessage 473 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_DTP.CDOTAUserMsg_DodgeTrackingProjectiles, L.ByteString))
deserializeEmbedMessage 474 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_GLC.CDOTAUserMsg_GlobalLightColor, L.ByteString))
deserializeEmbedMessage 475 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_GLD.CDOTAUserMsg_GlobalLightDirection, L.ByteString))
deserializeEmbedMessage 477 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_LP.CDOTAUserMsg_LocationPing, L.ByteString))
deserializeEmbedMessage 478 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_ML.CDOTAUserMsg_MapLine, L.ByteString))
deserializeEmbedMessage 479 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_MKCI.CDOTAUserMsg_MiniKillCamInfo, L.ByteString))
deserializeEmbedMessage 481 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_ME.CDOTAUserMsg_MinimapEvent, L.ByteString))
deserializeEmbedMessage 482 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_NR.CDOTAUserMsg_NevermoreRequiem, L.ByteString))
deserializeEmbedMessage 483 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_OE.CDOTAUserMsg_OverheadEvent, L.ByteString))
deserializeEmbedMessage 485 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_SC.CDOTAUserMsg_SharedCooldown, L.ByteString))
deserializeEmbedMessage 486 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_SPC.CDOTAUserMsg_SpectatorPlayerClick, L.ByteString))
deserializeEmbedMessage 488 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_UE.CDOTAUserMsg_UnitEvent, L.ByteString))
deserializeEmbedMessage 489 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_PM.CDOTAUserMsg_ParticleManager, L.ByteString))
deserializeEmbedMessage 490 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_BC.CDOTAUserMsg_BotChat, L.ByteString))
deserializeEmbedMessage 491 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_HE.CDOTAUserMsg_HudError, L.ByteString))
deserializeEmbedMessage 492 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_IP.CDOTAUserMsg_ItemPurchased, L.ByteString))
deserializeEmbedMessage 497 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_WL.CDOTAUserMsg_WorldLine, L.ByteString))
deserializeEmbedMessage 501 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_CW.CDOTAUserMsg_ChatWheel, L.ByteString))
deserializeEmbedMessage 506 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_GSC.CDOTAUserMsg_GamerulesStateChanged, L.ByteString))
deserializeEmbedMessage 510 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_SSP.CDOTAUserMsg_SendStatPopup, L.ByteString))
deserializeEmbedMessage 512 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_SRP.CDOTAUserMsg_SendRoshanPopup, L.ByteString))
deserializeEmbedMessage 518 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_TEP.CDOTAUserMsg_TE_Projectile, L.ByteString))
deserializeEmbedMessage 519 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_TEPL.CDOTAUserMsg_TE_ProjectileLoc, L.ByteString))
deserializeEmbedMessage 520 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_TEDBI.CDOTAUserMsg_TE_DotaBloodImpact, L.ByteString))
deserializeEmbedMessage 521 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_TEUA.CDOTAUserMsg_TE_UnitAnimation, L.ByteString))
deserializeEmbedMessage 522 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_TEUAE.CDOTAUserMsg_TE_UnitAnimationEnd, L.ByteString))
deserializeEmbedMessage 532 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_AS.CDOTAUserMsg_AbilitySteal, L.ByteString))
deserializeEmbedMessage 533 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_CKA.CDOTAUserMsg_CourierKilledAlert, L.ByteString))
deserializeEmbedMessage 534 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_EIA.CDOTAUserMsg_EnemyItemAlert, L.ByteString))
deserializeEmbedMessage 542 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_PR.CDOTAUserMsg_PredictionResult, L.ByteString))
deserializeEmbedMessage 544 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_HPMA.CDOTAUserMsg_HPManaAlert, L.ByteString))
deserializeEmbedMessage 547 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_SPUO.CDOTAUserMsg_SpectatorPlayerUnitOrders, L.ByteString))
deserializeEmbedMessage 552 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_PA.CDOTAUserMsg_ProjectionAbility, L.ByteString))
deserializeEmbedMessage 553 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_PE.CDOTAUserMsg_ProjectionEvent, L.ByteString))
deserializeEmbedMessage 554 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_CLE.CMsgDOTACombatLogEntry, L.ByteString))
deserializeEmbedMessage 557 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_MMF.CDOTAMatchMetadataFile, L.ByteString))
deserializeEmbedMessage 558 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_MD.CMsgDOTAMatch, L.ByteString))
deserializeEmbedMessage 563 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_SPG.CDOTAUserMsg_SelectPenaltyGold, L.ByteString))
deserializeEmbedMessage 564 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_RDR.CDOTAUserMsg_RollDiceResult, L.ByteString))
deserializeEmbedMessage 565 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_FCR.CDOTAUserMsg_FlipCoinResult, L.ByteString))
deserializeEmbedMessage 567 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_TCC.CDOTAUserMessage_TeamCaptainChanged, L.ByteString))
deserializeEmbedMessage 568 tick messageBody =
  embedMessageOrError tick (messageGet messageBody :: Either String (DUM_SRSP.CDOTAUserMsg_SendRoshanSpectatorPhase, L.ByteString))
deserializeEmbedMessage value tick messageBody =
  error $ "encountered unknown embed message: " ++ show value

