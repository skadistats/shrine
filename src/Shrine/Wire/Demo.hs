module Shrine.Wire.Demo ( deserializeDemoMessage
                        , DemoMessage(..)
                        , ToDemoMessage
                        , toDemoMessage
                        ) where

import qualified Data.ByteString.Lazy as L
import Text.ProtocolBuffers

import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoStop as D_S
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoFileHeader as D_FH
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoFileInfo as D_FI
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoSyncTick as D_SyncT
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoSendTables as D_SendT
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoClassInfo as D_CI
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoStringTables as D_StringT
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoPacket as D_P
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoConsoleCmd as D_CC
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoCustomData as D_CD
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoCustomDataCallbacks as D_CDC
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoUserCmd as D_UC
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoFullPacket as D_FP
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoSaveGame as D_SaveG
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoSpawnGroups as D_SpawnG

data DemoMessage =
    DemoStop Int D_S.CDemoStop
  | DemoFileHeader Int D_FH.CDemoFileHeader
  | DemoFileInfo Int D_FI.CDemoFileInfo
  | DemoSyncTick Int D_SyncT.CDemoSyncTick
  | DemoSendTables Int D_SendT.CDemoSendTables
  | DemoClassInfo Int D_CI.CDemoClassInfo
  | DemoStringTables Int D_StringT.CDemoStringTables
  | DemoPacket Int D_P.CDemoPacket
  | DemoConsoleCommand Int D_CC.CDemoConsoleCmd
  | DemoCustomData Int D_CD.CDemoCustomData
  | DemoCustomDataCallbacks Int D_CDC.CDemoCustomDataCallbacks
  | DemoUserCommand Int D_UC.CDemoUserCmd
  | DemoFullPacket Int D_FP.CDemoFullPacket
  | DemoSaveGame Int D_SaveG.CDemoSaveGame
  | DemoSpawnGroups Int D_SpawnG.CDemoSpawnGroups
  deriving (Eq, Show)

class ToDemoMessage a where
  toDemoMessage :: Int -> a -> DemoMessage

instance ToDemoMessage D_S.CDemoStop where
  toDemoMessage tick message = DemoStop tick message

instance ToDemoMessage D_FH.CDemoFileHeader where
  toDemoMessage tick message = DemoFileHeader tick message

instance ToDemoMessage D_FI.CDemoFileInfo where
  toDemoMessage tick message = DemoFileInfo tick message

instance ToDemoMessage D_SyncT.CDemoSyncTick where
  toDemoMessage tick message = DemoSyncTick tick message

instance ToDemoMessage D_SendT.CDemoSendTables where
  toDemoMessage tick message = DemoSendTables tick message

instance ToDemoMessage D_CI.CDemoClassInfo where
  toDemoMessage tick message = DemoClassInfo tick message

instance ToDemoMessage D_StringT.CDemoStringTables where
  toDemoMessage tick message = DemoStringTables tick message

instance ToDemoMessage D_P.CDemoPacket where
  toDemoMessage tick message = DemoPacket tick message

instance ToDemoMessage D_CC.CDemoConsoleCmd where
  toDemoMessage tick message = DemoConsoleCommand tick message

instance ToDemoMessage D_CD.CDemoCustomData where
  toDemoMessage tick message = DemoCustomData tick message

instance ToDemoMessage D_CDC.CDemoCustomDataCallbacks where
  toDemoMessage tick message = DemoCustomDataCallbacks tick message

instance ToDemoMessage D_UC.CDemoUserCmd where
  toDemoMessage tick message = DemoUserCommand tick message

instance ToDemoMessage D_FP.CDemoFullPacket where
  toDemoMessage tick message = DemoFullPacket tick message

instance ToDemoMessage D_SaveG.CDemoSaveGame where
  toDemoMessage tick message = DemoSaveGame tick message

instance ToDemoMessage D_SpawnG.CDemoSpawnGroups where
  toDemoMessage tick message = DemoSpawnGroups tick message

demoMessageOrError :: ToDemoMessage a => Int -> Either String (a, L.ByteString) -> DemoMessage
demoMessageOrError tick (Left err) = error (err ++ " at tick " ++ show tick)
demoMessageOrError tick (Right (marshalled, _)) = toDemoMessage tick marshalled

deserializeDemoMessage :: Int -> Int -> L.ByteString -> DemoMessage
deserializeDemoMessage (-1) tick message = error $ "demo error: " ++ show message
deserializeDemoMessage 0 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_S.CDemoStop, L.ByteString))
deserializeDemoMessage 1 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_FH.CDemoFileHeader, L.ByteString))
deserializeDemoMessage 2 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_FI.CDemoFileInfo, L.ByteString))
deserializeDemoMessage 3 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_SyncT.CDemoSyncTick, L.ByteString))
deserializeDemoMessage 4 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_SendT.CDemoSendTables, L.ByteString))
deserializeDemoMessage 5 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_CI.CDemoClassInfo, L.ByteString))
deserializeDemoMessage 6 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_StringT.CDemoStringTables, L.ByteString))
deserializeDemoMessage 7 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_P.CDemoPacket, L.ByteString))
deserializeDemoMessage 8 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_P.CDemoPacket, L.ByteString))
deserializeDemoMessage 9 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_CC.CDemoConsoleCmd, L.ByteString))
deserializeDemoMessage 10 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_CD.CDemoCustomData, L.ByteString))
deserializeDemoMessage 11 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_CDC.CDemoCustomDataCallbacks, L.ByteString))
deserializeDemoMessage 12 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_UC.CDemoUserCmd, L.ByteString))
deserializeDemoMessage 13 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_FP.CDemoFullPacket, L.ByteString))
deserializeDemoMessage 14 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_SaveG.CDemoSaveGame, L.ByteString))
deserializeDemoMessage 15 tick messageBody =
  demoMessageOrError tick (messageGet messageBody :: Either String (D_SpawnG.CDemoSpawnGroups, L.ByteString))
deserializeDemoMessage value tick messageBody =
  error $ show value

