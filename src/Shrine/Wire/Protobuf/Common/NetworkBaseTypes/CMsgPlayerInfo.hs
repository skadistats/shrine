{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgPlayerInfo (CMsgPlayerInfo(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CMsgPlayerInfo = CMsgPlayerInfo{name :: !(P'.Maybe P'.Utf8), xuid :: !(P'.Maybe P'.Word64), userid :: !(P'.Maybe P'.Int32),
                                     steamid :: !(P'.Maybe P'.Word64), fakeplayer :: !(P'.Maybe P'.Bool),
                                     ishltv :: !(P'.Maybe P'.Bool), customFiles :: !(P'.Seq P'.Word32),
                                     filesDownloaded :: !(P'.Maybe P'.Int32)}
                    deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgPlayerInfo where
  mergeAppend (CMsgPlayerInfo x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8) (CMsgPlayerInfo y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8)
   = CMsgPlayerInfo (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)

instance P'.Default CMsgPlayerInfo where
  defaultValue
   = CMsgPlayerInfo P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue

instance P'.Wire CMsgPlayerInfo where
  wireSize ft' self'@(CMsgPlayerInfo x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 9 x'1 + P'.wireSizeOpt 1 6 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeOpt 1 6 x'4 +
             P'.wireSizeOpt 1 8 x'5
             + P'.wireSizeOpt 1 8 x'6
             + P'.wireSizeRep 1 7 x'7
             + P'.wireSizeOpt 1 5 x'8)
  wirePut ft' self'@(CMsgPlayerInfo x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
             P'.wirePutOpt 17 6 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutOpt 33 6 x'4
             P'.wirePutOpt 40 8 x'5
             P'.wirePutOpt 48 8 x'6
             P'.wirePutRep 61 7 x'7
             P'.wirePutOpt 64 5 x'8
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{name = Prelude'.Just new'Field}) (P'.wireGet 9)
             17 -> Prelude'.fmap (\ !new'Field -> old'Self{xuid = Prelude'.Just new'Field}) (P'.wireGet 6)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{userid = Prelude'.Just new'Field}) (P'.wireGet 5)
             33 -> Prelude'.fmap (\ !new'Field -> old'Self{steamid = Prelude'.Just new'Field}) (P'.wireGet 6)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{fakeplayer = Prelude'.Just new'Field}) (P'.wireGet 8)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{ishltv = Prelude'.Just new'Field}) (P'.wireGet 8)
             61 -> Prelude'.fmap (\ !new'Field -> old'Self{customFiles = P'.append (customFiles old'Self) new'Field}) (P'.wireGet 7)
             58 -> Prelude'.fmap (\ !new'Field -> old'Self{customFiles = P'.mergeAppend (customFiles old'Self) new'Field})
                    (P'.wireGetPacked 7)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{filesDownloaded = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgPlayerInfo) CMsgPlayerInfo where
  getVal m' f' = f' m'

instance P'.GPB CMsgPlayerInfo

instance P'.ReflectDescriptor CMsgPlayerInfo where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 17, 24, 33, 40, 48, 58, 61, 64])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CMsgPlayerInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgPlayerInfo\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CMsgPlayerInfo.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CMsgPlayerInfo.name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CMsgPlayerInfo\"], baseName' = FName \"name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CMsgPlayerInfo.xuid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CMsgPlayerInfo\"], baseName' = FName \"xuid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 17}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CMsgPlayerInfo.userid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CMsgPlayerInfo\"], baseName' = FName \"userid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CMsgPlayerInfo.steamid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CMsgPlayerInfo\"], baseName' = FName \"steamid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 33}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CMsgPlayerInfo.fakeplayer\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CMsgPlayerInfo\"], baseName' = FName \"fakeplayer\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CMsgPlayerInfo.ishltv\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CMsgPlayerInfo\"], baseName' = FName \"ishltv\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CMsgPlayerInfo.customFiles\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CMsgPlayerInfo\"], baseName' = FName \"customFiles\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 61}, packedTag = Just (WireTag {getWireTag = 61},WireTag {getWireTag = 58}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CMsgPlayerInfo.filesDownloaded\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CMsgPlayerInfo\"], baseName' = FName \"filesDownloaded\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgPlayerInfo where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgPlayerInfo where
  textPut msg
   = do
       P'.tellT "name" (name msg)
       P'.tellT "xuid" (xuid msg)
       P'.tellT "userid" (userid msg)
       P'.tellT "steamid" (steamid msg)
       P'.tellT "fakeplayer" (fakeplayer msg)
       P'.tellT "ishltv" (ishltv msg)
       P'.tellT "customFiles" (customFiles msg)
       P'.tellT "filesDownloaded" (filesDownloaded msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'name, parse'xuid, parse'userid, parse'steamid, parse'fakeplayer, parse'ishltv, parse'customFiles,
                   parse'filesDownloaded])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'name
         = P'.try
            (do
               v <- P'.getT "name"
               Prelude'.return (\ o -> o{name = v}))
        parse'xuid
         = P'.try
            (do
               v <- P'.getT "xuid"
               Prelude'.return (\ o -> o{xuid = v}))
        parse'userid
         = P'.try
            (do
               v <- P'.getT "userid"
               Prelude'.return (\ o -> o{userid = v}))
        parse'steamid
         = P'.try
            (do
               v <- P'.getT "steamid"
               Prelude'.return (\ o -> o{steamid = v}))
        parse'fakeplayer
         = P'.try
            (do
               v <- P'.getT "fakeplayer"
               Prelude'.return (\ o -> o{fakeplayer = v}))
        parse'ishltv
         = P'.try
            (do
               v <- P'.getT "ishltv"
               Prelude'.return (\ o -> o{ishltv = v}))
        parse'customFiles
         = P'.try
            (do
               v <- P'.getT "customFiles"
               Prelude'.return (\ o -> o{customFiles = P'.append (customFiles o) v}))
        parse'filesDownloaded
         = P'.try
            (do
               v <- P'.getT "filesDownloaded"
               Prelude'.return (\ o -> o{filesDownloaded = v}))