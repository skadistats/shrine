{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CSVCMsg_GameSessionConfiguration (CSVCMsg_GameSessionConfiguration(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CSVCMsg_GameSessionConfiguration = CSVCMsg_GameSessionConfiguration{is_multiplayer :: !(P'.Maybe P'.Bool),
                                                                         is_loadsavegame :: !(P'.Maybe P'.Bool),
                                                                         is_background_map :: !(P'.Maybe P'.Bool),
                                                                         is_headless :: !(P'.Maybe P'.Bool),
                                                                         min_client_limit :: !(P'.Maybe P'.Word32),
                                                                         max_client_limit :: !(P'.Maybe P'.Word32),
                                                                         max_clients :: !(P'.Maybe P'.Word32),
                                                                         tick_interval :: !(P'.Maybe P'.Word32),
                                                                         hostname :: !(P'.Maybe P'.Utf8),
                                                                         savegamename :: !(P'.Maybe P'.Utf8),
                                                                         s1_mapname :: !(P'.Maybe P'.Utf8),
                                                                         gamemode :: !(P'.Maybe P'.Utf8),
                                                                         server_ip_address :: !(P'.Maybe P'.Utf8),
                                                                         data' :: !(P'.Maybe P'.ByteString),
                                                                         is_localonly :: !(P'.Maybe P'.Bool),
                                                                         is_transition :: !(P'.Maybe P'.Bool),
                                                                         previouslevel :: !(P'.Maybe P'.Utf8),
                                                                         landmarkname :: !(P'.Maybe P'.Utf8)}
                                      deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                Prelude'.Generic)

instance P'.Mergeable CSVCMsg_GameSessionConfiguration where
  mergeAppend (CSVCMsg_GameSessionConfiguration x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18)
   (CSVCMsg_GameSessionConfiguration y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18)
   = CSVCMsg_GameSessionConfiguration (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)
      (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)

instance P'.Default CSVCMsg_GameSessionConfiguration where
  defaultValue
   = CSVCMsg_GameSessionConfiguration P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CSVCMsg_GameSessionConfiguration where
  wireSize ft'
   self'@(CSVCMsg_GameSessionConfiguration x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 8 x'1 + P'.wireSizeOpt 1 8 x'2 + P'.wireSizeOpt 1 8 x'3 + P'.wireSizeOpt 1 8 x'4 +
             P'.wireSizeOpt 1 13 x'5
             + P'.wireSizeOpt 1 13 x'6
             + P'.wireSizeOpt 1 13 x'7
             + P'.wireSizeOpt 1 7 x'8
             + P'.wireSizeOpt 1 9 x'9
             + P'.wireSizeOpt 1 9 x'10
             + P'.wireSizeOpt 1 9 x'11
             + P'.wireSizeOpt 1 9 x'12
             + P'.wireSizeOpt 1 9 x'13
             + P'.wireSizeOpt 1 12 x'14
             + P'.wireSizeOpt 1 8 x'15
             + P'.wireSizeOpt 2 8 x'16
             + P'.wireSizeOpt 2 9 x'17
             + P'.wireSizeOpt 2 9 x'18)
  wirePut ft'
   self'@(CSVCMsg_GameSessionConfiguration x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 8 x'1
             P'.wirePutOpt 16 8 x'2
             P'.wirePutOpt 24 8 x'3
             P'.wirePutOpt 32 8 x'4
             P'.wirePutOpt 40 13 x'5
             P'.wirePutOpt 48 13 x'6
             P'.wirePutOpt 56 13 x'7
             P'.wirePutOpt 69 7 x'8
             P'.wirePutOpt 74 9 x'9
             P'.wirePutOpt 82 9 x'10
             P'.wirePutOpt 90 9 x'11
             P'.wirePutOpt 98 9 x'12
             P'.wirePutOpt 106 9 x'13
             P'.wirePutOpt 114 12 x'14
             P'.wirePutOpt 120 8 x'15
             P'.wirePutOpt 128 8 x'16
             P'.wirePutOpt 138 9 x'17
             P'.wirePutOpt 146 9 x'18
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{is_multiplayer = Prelude'.Just new'Field}) (P'.wireGet 8)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{is_loadsavegame = Prelude'.Just new'Field}) (P'.wireGet 8)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{is_background_map = Prelude'.Just new'Field}) (P'.wireGet 8)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{is_headless = Prelude'.Just new'Field}) (P'.wireGet 8)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{min_client_limit = Prelude'.Just new'Field}) (P'.wireGet 13)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{max_client_limit = Prelude'.Just new'Field}) (P'.wireGet 13)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{max_clients = Prelude'.Just new'Field}) (P'.wireGet 13)
             69 -> Prelude'.fmap (\ !new'Field -> old'Self{tick_interval = Prelude'.Just new'Field}) (P'.wireGet 7)
             74 -> Prelude'.fmap (\ !new'Field -> old'Self{hostname = Prelude'.Just new'Field}) (P'.wireGet 9)
             82 -> Prelude'.fmap (\ !new'Field -> old'Self{savegamename = Prelude'.Just new'Field}) (P'.wireGet 9)
             90 -> Prelude'.fmap (\ !new'Field -> old'Self{s1_mapname = Prelude'.Just new'Field}) (P'.wireGet 9)
             98 -> Prelude'.fmap (\ !new'Field -> old'Self{gamemode = Prelude'.Just new'Field}) (P'.wireGet 9)
             106 -> Prelude'.fmap (\ !new'Field -> old'Self{server_ip_address = Prelude'.Just new'Field}) (P'.wireGet 9)
             114 -> Prelude'.fmap (\ !new'Field -> old'Self{data' = Prelude'.Just new'Field}) (P'.wireGet 12)
             120 -> Prelude'.fmap (\ !new'Field -> old'Self{is_localonly = Prelude'.Just new'Field}) (P'.wireGet 8)
             128 -> Prelude'.fmap (\ !new'Field -> old'Self{is_transition = Prelude'.Just new'Field}) (P'.wireGet 8)
             138 -> Prelude'.fmap (\ !new'Field -> old'Self{previouslevel = Prelude'.Just new'Field}) (P'.wireGet 9)
             146 -> Prelude'.fmap (\ !new'Field -> old'Self{landmarkname = Prelude'.Just new'Field}) (P'.wireGet 9)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_GameSessionConfiguration) CSVCMsg_GameSessionConfiguration where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_GameSessionConfiguration

instance P'.ReflectDescriptor CSVCMsg_GameSessionConfiguration where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList [8, 16, 24, 32, 40, 48, 56, 69, 74, 82, 90, 98, 106, 114, 120, 128, 138, 146])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CSVCMsg_GameSessionConfiguration\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CSVCMsg_GameSessionConfiguration.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.is_multiplayer\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"is_multiplayer\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.is_loadsavegame\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"is_loadsavegame\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.is_background_map\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"is_background_map\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.is_headless\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"is_headless\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.min_client_limit\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"min_client_limit\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.max_client_limit\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"max_client_limit\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.max_clients\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"max_clients\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.tick_interval\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"tick_interval\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 69}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.hostname\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"hostname\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 74}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.savegamename\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"savegamename\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 82}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.s1_mapname\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"s1_mapname\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 90}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.gamemode\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"gamemode\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 98}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.server_ip_address\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"server_ip_address\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 106}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"data'\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 114}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.is_localonly\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"is_localonly\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 120}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.is_transition\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"is_transition\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 128}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.previouslevel\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"previouslevel\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 138}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration.landmarkname\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameSessionConfiguration\"], baseName' = FName \"landmarkname\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 146}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_GameSessionConfiguration where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_GameSessionConfiguration where
  textPut msg
   = do
       P'.tellT "is_multiplayer" (is_multiplayer msg)
       P'.tellT "is_loadsavegame" (is_loadsavegame msg)
       P'.tellT "is_background_map" (is_background_map msg)
       P'.tellT "is_headless" (is_headless msg)
       P'.tellT "min_client_limit" (min_client_limit msg)
       P'.tellT "max_client_limit" (max_client_limit msg)
       P'.tellT "max_clients" (max_clients msg)
       P'.tellT "tick_interval" (tick_interval msg)
       P'.tellT "hostname" (hostname msg)
       P'.tellT "savegamename" (savegamename msg)
       P'.tellT "s1_mapname" (s1_mapname msg)
       P'.tellT "gamemode" (gamemode msg)
       P'.tellT "server_ip_address" (server_ip_address msg)
       P'.tellT "data" (data' msg)
       P'.tellT "is_localonly" (is_localonly msg)
       P'.tellT "is_transition" (is_transition msg)
       P'.tellT "previouslevel" (previouslevel msg)
       P'.tellT "landmarkname" (landmarkname msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'is_multiplayer, parse'is_loadsavegame, parse'is_background_map, parse'is_headless, parse'min_client_limit,
                   parse'max_client_limit, parse'max_clients, parse'tick_interval, parse'hostname, parse'savegamename,
                   parse's1_mapname, parse'gamemode, parse'server_ip_address, parse'data', parse'is_localonly, parse'is_transition,
                   parse'previouslevel, parse'landmarkname])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'is_multiplayer
         = P'.try
            (do
               v <- P'.getT "is_multiplayer"
               Prelude'.return (\ o -> o{is_multiplayer = v}))
        parse'is_loadsavegame
         = P'.try
            (do
               v <- P'.getT "is_loadsavegame"
               Prelude'.return (\ o -> o{is_loadsavegame = v}))
        parse'is_background_map
         = P'.try
            (do
               v <- P'.getT "is_background_map"
               Prelude'.return (\ o -> o{is_background_map = v}))
        parse'is_headless
         = P'.try
            (do
               v <- P'.getT "is_headless"
               Prelude'.return (\ o -> o{is_headless = v}))
        parse'min_client_limit
         = P'.try
            (do
               v <- P'.getT "min_client_limit"
               Prelude'.return (\ o -> o{min_client_limit = v}))
        parse'max_client_limit
         = P'.try
            (do
               v <- P'.getT "max_client_limit"
               Prelude'.return (\ o -> o{max_client_limit = v}))
        parse'max_clients
         = P'.try
            (do
               v <- P'.getT "max_clients"
               Prelude'.return (\ o -> o{max_clients = v}))
        parse'tick_interval
         = P'.try
            (do
               v <- P'.getT "tick_interval"
               Prelude'.return (\ o -> o{tick_interval = v}))
        parse'hostname
         = P'.try
            (do
               v <- P'.getT "hostname"
               Prelude'.return (\ o -> o{hostname = v}))
        parse'savegamename
         = P'.try
            (do
               v <- P'.getT "savegamename"
               Prelude'.return (\ o -> o{savegamename = v}))
        parse's1_mapname
         = P'.try
            (do
               v <- P'.getT "s1_mapname"
               Prelude'.return (\ o -> o{s1_mapname = v}))
        parse'gamemode
         = P'.try
            (do
               v <- P'.getT "gamemode"
               Prelude'.return (\ o -> o{gamemode = v}))
        parse'server_ip_address
         = P'.try
            (do
               v <- P'.getT "server_ip_address"
               Prelude'.return (\ o -> o{server_ip_address = v}))
        parse'data'
         = P'.try
            (do
               v <- P'.getT "data"
               Prelude'.return (\ o -> o{data' = v}))
        parse'is_localonly
         = P'.try
            (do
               v <- P'.getT "is_localonly"
               Prelude'.return (\ o -> o{is_localonly = v}))
        parse'is_transition
         = P'.try
            (do
               v <- P'.getT "is_transition"
               Prelude'.return (\ o -> o{is_transition = v}))
        parse'previouslevel
         = P'.try
            (do
               v <- P'.getT "previouslevel"
               Prelude'.return (\ o -> o{previouslevel = v}))
        parse'landmarkname
         = P'.try
            (do
               v <- P'.getT "landmarkname"
               Prelude'.return (\ o -> o{landmarkname = v}))