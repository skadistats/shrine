{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.SIGNONSTATE (SIGNONSTATE(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data SIGNONSTATE = SIGNONSTATE_NONE
                 | SIGNONSTATE_CHALLENGE
                 | SIGNONSTATE_CONNECTED
                 | SIGNONSTATE_NEW
                 | SIGNONSTATE_PRESPAWN
                 | SIGNONSTATE_SPAWN
                 | SIGNONSTATE_FULL
                 | SIGNONSTATE_CHANGELEVEL
                 deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                           Prelude'.Generic)

instance P'.Mergeable SIGNONSTATE

instance Prelude'.Bounded SIGNONSTATE where
  minBound = SIGNONSTATE_NONE
  maxBound = SIGNONSTATE_CHANGELEVEL

instance P'.Default SIGNONSTATE where
  defaultValue = SIGNONSTATE_NONE

toMaybe'Enum :: Prelude'.Int -> P'.Maybe SIGNONSTATE
toMaybe'Enum 0 = Prelude'.Just SIGNONSTATE_NONE
toMaybe'Enum 1 = Prelude'.Just SIGNONSTATE_CHALLENGE
toMaybe'Enum 2 = Prelude'.Just SIGNONSTATE_CONNECTED
toMaybe'Enum 3 = Prelude'.Just SIGNONSTATE_NEW
toMaybe'Enum 4 = Prelude'.Just SIGNONSTATE_PRESPAWN
toMaybe'Enum 5 = Prelude'.Just SIGNONSTATE_SPAWN
toMaybe'Enum 6 = Prelude'.Just SIGNONSTATE_FULL
toMaybe'Enum 7 = Prelude'.Just SIGNONSTATE_CHANGELEVEL
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum SIGNONSTATE where
  fromEnum SIGNONSTATE_NONE = 0
  fromEnum SIGNONSTATE_CHALLENGE = 1
  fromEnum SIGNONSTATE_CONNECTED = 2
  fromEnum SIGNONSTATE_NEW = 3
  fromEnum SIGNONSTATE_PRESPAWN = 4
  fromEnum SIGNONSTATE_SPAWN = 5
  fromEnum SIGNONSTATE_FULL = 6
  fromEnum SIGNONSTATE_CHANGELEVEL = 7
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.NetworkBaseTypes.SIGNONSTATE")
      . toMaybe'Enum
  succ SIGNONSTATE_NONE = SIGNONSTATE_CHALLENGE
  succ SIGNONSTATE_CHALLENGE = SIGNONSTATE_CONNECTED
  succ SIGNONSTATE_CONNECTED = SIGNONSTATE_NEW
  succ SIGNONSTATE_NEW = SIGNONSTATE_PRESPAWN
  succ SIGNONSTATE_PRESPAWN = SIGNONSTATE_SPAWN
  succ SIGNONSTATE_SPAWN = SIGNONSTATE_FULL
  succ SIGNONSTATE_FULL = SIGNONSTATE_CHANGELEVEL
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.NetworkBaseTypes.SIGNONSTATE"
  pred SIGNONSTATE_CHALLENGE = SIGNONSTATE_NONE
  pred SIGNONSTATE_CONNECTED = SIGNONSTATE_CHALLENGE
  pred SIGNONSTATE_NEW = SIGNONSTATE_CONNECTED
  pred SIGNONSTATE_PRESPAWN = SIGNONSTATE_NEW
  pred SIGNONSTATE_SPAWN = SIGNONSTATE_PRESPAWN
  pred SIGNONSTATE_FULL = SIGNONSTATE_SPAWN
  pred SIGNONSTATE_CHANGELEVEL = SIGNONSTATE_FULL
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.NetworkBaseTypes.SIGNONSTATE"

instance P'.Wire SIGNONSTATE where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB SIGNONSTATE

instance P'.MessageAPI msg' (msg' -> SIGNONSTATE) SIGNONSTATE where
  getVal m' f' = f' m'

instance P'.ReflectEnum SIGNONSTATE where
  reflectEnum
   = [(0, "SIGNONSTATE_NONE", SIGNONSTATE_NONE), (1, "SIGNONSTATE_CHALLENGE", SIGNONSTATE_CHALLENGE),
      (2, "SIGNONSTATE_CONNECTED", SIGNONSTATE_CONNECTED), (3, "SIGNONSTATE_NEW", SIGNONSTATE_NEW),
      (4, "SIGNONSTATE_PRESPAWN", SIGNONSTATE_PRESPAWN), (5, "SIGNONSTATE_SPAWN", SIGNONSTATE_SPAWN),
      (6, "SIGNONSTATE_FULL", SIGNONSTATE_FULL), (7, "SIGNONSTATE_CHANGELEVEL", SIGNONSTATE_CHANGELEVEL)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Networkbasetypes.SIGNONSTATE") ["Shrine", "Wire", "Protobuf", "Common"] ["NetworkBaseTypes"]
        "SIGNONSTATE")
      ["Shrine", "Wire", "Protobuf", "Common", "NetworkBaseTypes", "SIGNONSTATE.hs"]
      [(0, "SIGNONSTATE_NONE"), (1, "SIGNONSTATE_CHALLENGE"), (2, "SIGNONSTATE_CONNECTED"), (3, "SIGNONSTATE_NEW"),
       (4, "SIGNONSTATE_PRESPAWN"), (5, "SIGNONSTATE_SPAWN"), (6, "SIGNONSTATE_FULL"), (7, "SIGNONSTATE_CHANGELEVEL")]

instance P'.TextType SIGNONSTATE where
  tellT = P'.tellShow
  getT = P'.getRead