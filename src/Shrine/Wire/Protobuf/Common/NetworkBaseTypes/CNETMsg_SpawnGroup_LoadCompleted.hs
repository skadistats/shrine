{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SpawnGroup_LoadCompleted (CNETMsg_SpawnGroup_LoadCompleted(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CNETMsg_SpawnGroup_LoadCompleted = CNETMsg_SpawnGroup_LoadCompleted{spawngrouphandle :: !(P'.Maybe P'.Word32)}
                                      deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                Prelude'.Generic)

instance P'.Mergeable CNETMsg_SpawnGroup_LoadCompleted where
  mergeAppend (CNETMsg_SpawnGroup_LoadCompleted x'1) (CNETMsg_SpawnGroup_LoadCompleted y'1)
   = CNETMsg_SpawnGroup_LoadCompleted (P'.mergeAppend x'1 y'1)

instance P'.Default CNETMsg_SpawnGroup_LoadCompleted where
  defaultValue = CNETMsg_SpawnGroup_LoadCompleted P'.defaultValue

instance P'.Wire CNETMsg_SpawnGroup_LoadCompleted where
  wireSize ft' self'@(CNETMsg_SpawnGroup_LoadCompleted x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1)
  wirePut ft' self'@(CNETMsg_SpawnGroup_LoadCompleted x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{spawngrouphandle = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CNETMsg_SpawnGroup_LoadCompleted) CNETMsg_SpawnGroup_LoadCompleted where
  getVal m' f' = f' m'

instance P'.GPB CNETMsg_SpawnGroup_LoadCompleted

instance P'.ReflectDescriptor CNETMsg_SpawnGroup_LoadCompleted where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_LoadCompleted\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CNETMsg_SpawnGroup_LoadCompleted\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CNETMsg_SpawnGroup_LoadCompleted.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_LoadCompleted.spawngrouphandle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_LoadCompleted\"], baseName' = FName \"spawngrouphandle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CNETMsg_SpawnGroup_LoadCompleted where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CNETMsg_SpawnGroup_LoadCompleted where
  textPut msg
   = do
       P'.tellT "spawngrouphandle" (spawngrouphandle msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'spawngrouphandle]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'spawngrouphandle
         = P'.try
            (do
               v <- P'.getT "spawngrouphandle"
               Prelude'.return (\ o -> o{spawngrouphandle = v}))