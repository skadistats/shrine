{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.NET_Messages (NET_Messages(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data NET_Messages = Net_NOP
                  | Net_Disconnect
                  | Net_File
                  | Net_SplitScreenUser
                  | Net_Tick
                  | Net_StringCmd
                  | Net_SetConVar
                  | Net_SignonState
                  | Net_SpawnGroup_Load
                  | Net_SpawnGroup_ManifestUpdate
                  | Net_SpawnGroup_SetCreationTick
                  | Net_SpawnGroup_Unload
                  | Net_SpawnGroup_LoadCompleted
                  | Net_ReliableMessageEndMarker
                  deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                            Prelude'.Generic)

instance P'.Mergeable NET_Messages

instance Prelude'.Bounded NET_Messages where
  minBound = Net_NOP
  maxBound = Net_ReliableMessageEndMarker

instance P'.Default NET_Messages where
  defaultValue = Net_NOP

toMaybe'Enum :: Prelude'.Int -> P'.Maybe NET_Messages
toMaybe'Enum 0 = Prelude'.Just Net_NOP
toMaybe'Enum 1 = Prelude'.Just Net_Disconnect
toMaybe'Enum 2 = Prelude'.Just Net_File
toMaybe'Enum 3 = Prelude'.Just Net_SplitScreenUser
toMaybe'Enum 4 = Prelude'.Just Net_Tick
toMaybe'Enum 5 = Prelude'.Just Net_StringCmd
toMaybe'Enum 6 = Prelude'.Just Net_SetConVar
toMaybe'Enum 7 = Prelude'.Just Net_SignonState
toMaybe'Enum 8 = Prelude'.Just Net_SpawnGroup_Load
toMaybe'Enum 9 = Prelude'.Just Net_SpawnGroup_ManifestUpdate
toMaybe'Enum 11 = Prelude'.Just Net_SpawnGroup_SetCreationTick
toMaybe'Enum 12 = Prelude'.Just Net_SpawnGroup_Unload
toMaybe'Enum 13 = Prelude'.Just Net_SpawnGroup_LoadCompleted
toMaybe'Enum 14 = Prelude'.Just Net_ReliableMessageEndMarker
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum NET_Messages where
  fromEnum Net_NOP = 0
  fromEnum Net_Disconnect = 1
  fromEnum Net_File = 2
  fromEnum Net_SplitScreenUser = 3
  fromEnum Net_Tick = 4
  fromEnum Net_StringCmd = 5
  fromEnum Net_SetConVar = 6
  fromEnum Net_SignonState = 7
  fromEnum Net_SpawnGroup_Load = 8
  fromEnum Net_SpawnGroup_ManifestUpdate = 9
  fromEnum Net_SpawnGroup_SetCreationTick = 11
  fromEnum Net_SpawnGroup_Unload = 12
  fromEnum Net_SpawnGroup_LoadCompleted = 13
  fromEnum Net_ReliableMessageEndMarker = 14
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.NetworkBaseTypes.NET_Messages")
      . toMaybe'Enum
  succ Net_NOP = Net_Disconnect
  succ Net_Disconnect = Net_File
  succ Net_File = Net_SplitScreenUser
  succ Net_SplitScreenUser = Net_Tick
  succ Net_Tick = Net_StringCmd
  succ Net_StringCmd = Net_SetConVar
  succ Net_SetConVar = Net_SignonState
  succ Net_SignonState = Net_SpawnGroup_Load
  succ Net_SpawnGroup_Load = Net_SpawnGroup_ManifestUpdate
  succ Net_SpawnGroup_ManifestUpdate = Net_SpawnGroup_SetCreationTick
  succ Net_SpawnGroup_SetCreationTick = Net_SpawnGroup_Unload
  succ Net_SpawnGroup_Unload = Net_SpawnGroup_LoadCompleted
  succ Net_SpawnGroup_LoadCompleted = Net_ReliableMessageEndMarker
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.NetworkBaseTypes.NET_Messages"
  pred Net_Disconnect = Net_NOP
  pred Net_File = Net_Disconnect
  pred Net_SplitScreenUser = Net_File
  pred Net_Tick = Net_SplitScreenUser
  pred Net_StringCmd = Net_Tick
  pred Net_SetConVar = Net_StringCmd
  pred Net_SignonState = Net_SetConVar
  pred Net_SpawnGroup_Load = Net_SignonState
  pred Net_SpawnGroup_ManifestUpdate = Net_SpawnGroup_Load
  pred Net_SpawnGroup_SetCreationTick = Net_SpawnGroup_ManifestUpdate
  pred Net_SpawnGroup_Unload = Net_SpawnGroup_SetCreationTick
  pred Net_SpawnGroup_LoadCompleted = Net_SpawnGroup_Unload
  pred Net_ReliableMessageEndMarker = Net_SpawnGroup_LoadCompleted
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.NetworkBaseTypes.NET_Messages"

instance P'.Wire NET_Messages where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB NET_Messages

instance P'.MessageAPI msg' (msg' -> NET_Messages) NET_Messages where
  getVal m' f' = f' m'

instance P'.ReflectEnum NET_Messages where
  reflectEnum
   = [(0, "Net_NOP", Net_NOP), (1, "Net_Disconnect", Net_Disconnect), (2, "Net_File", Net_File),
      (3, "Net_SplitScreenUser", Net_SplitScreenUser), (4, "Net_Tick", Net_Tick), (5, "Net_StringCmd", Net_StringCmd),
      (6, "Net_SetConVar", Net_SetConVar), (7, "Net_SignonState", Net_SignonState), (8, "Net_SpawnGroup_Load", Net_SpawnGroup_Load),
      (9, "Net_SpawnGroup_ManifestUpdate", Net_SpawnGroup_ManifestUpdate),
      (11, "Net_SpawnGroup_SetCreationTick", Net_SpawnGroup_SetCreationTick), (12, "Net_SpawnGroup_Unload", Net_SpawnGroup_Unload),
      (13, "Net_SpawnGroup_LoadCompleted", Net_SpawnGroup_LoadCompleted),
      (14, "Net_ReliableMessageEndMarker", Net_ReliableMessageEndMarker)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Networkbasetypes.NET_Messages") ["Shrine", "Wire", "Protobuf", "Common"] ["NetworkBaseTypes"]
        "NET_Messages")
      ["Shrine", "Wire", "Protobuf", "Common", "NetworkBaseTypes", "NET_Messages.hs"]
      [(0, "Net_NOP"), (1, "Net_Disconnect"), (2, "Net_File"), (3, "Net_SplitScreenUser"), (4, "Net_Tick"), (5, "Net_StringCmd"),
       (6, "Net_SetConVar"), (7, "Net_SignonState"), (8, "Net_SpawnGroup_Load"), (9, "Net_SpawnGroup_ManifestUpdate"),
       (11, "Net_SpawnGroup_SetCreationTick"), (12, "Net_SpawnGroup_Unload"), (13, "Net_SpawnGroup_LoadCompleted"),
       (14, "Net_ReliableMessageEndMarker")]

instance P'.TextType NET_Messages where
  tellT = P'.tellShow
  getT = P'.getRead