{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SpawnGroup_SetCreationTick (CNETMsg_SpawnGroup_SetCreationTick(..))
       where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CNETMsg_SpawnGroup_SetCreationTick = CNETMsg_SpawnGroup_SetCreationTick{spawngrouphandle :: !(P'.Maybe P'.Word32),
                                                                             tickcount :: !(P'.Maybe P'.Int32),
                                                                             creationsequence :: !(P'.Maybe P'.Word32)}
                                        deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                  Prelude'.Generic)

instance P'.Mergeable CNETMsg_SpawnGroup_SetCreationTick where
  mergeAppend (CNETMsg_SpawnGroup_SetCreationTick x'1 x'2 x'3) (CNETMsg_SpawnGroup_SetCreationTick y'1 y'2 y'3)
   = CNETMsg_SpawnGroup_SetCreationTick (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default CNETMsg_SpawnGroup_SetCreationTick where
  defaultValue = CNETMsg_SpawnGroup_SetCreationTick P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CNETMsg_SpawnGroup_SetCreationTick where
  wireSize ft' self'@(CNETMsg_SpawnGroup_SetCreationTick x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 13 x'3)
  wirePut ft' self'@(CNETMsg_SpawnGroup_SetCreationTick x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 13 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{spawngrouphandle = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{tickcount = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{creationsequence = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CNETMsg_SpawnGroup_SetCreationTick) CNETMsg_SpawnGroup_SetCreationTick where
  getVal m' f' = f' m'

instance P'.GPB CNETMsg_SpawnGroup_SetCreationTick

instance P'.ReflectDescriptor CNETMsg_SpawnGroup_SetCreationTick where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_SetCreationTick\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CNETMsg_SpawnGroup_SetCreationTick\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CNETMsg_SpawnGroup_SetCreationTick.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_SetCreationTick.spawngrouphandle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_SetCreationTick\"], baseName' = FName \"spawngrouphandle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_SetCreationTick.tickcount\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_SetCreationTick\"], baseName' = FName \"tickcount\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_SetCreationTick.creationsequence\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_SetCreationTick\"], baseName' = FName \"creationsequence\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CNETMsg_SpawnGroup_SetCreationTick where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CNETMsg_SpawnGroup_SetCreationTick where
  textPut msg
   = do
       P'.tellT "spawngrouphandle" (spawngrouphandle msg)
       P'.tellT "tickcount" (tickcount msg)
       P'.tellT "creationsequence" (creationsequence msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'spawngrouphandle, parse'tickcount, parse'creationsequence]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'spawngrouphandle
         = P'.try
            (do
               v <- P'.getT "spawngrouphandle"
               Prelude'.return (\ o -> o{spawngrouphandle = v}))
        parse'tickcount
         = P'.try
            (do
               v <- P'.getT "tickcount"
               Prelude'.return (\ o -> o{tickcount = v}))
        parse'creationsequence
         = P'.try
            (do
               v <- P'.getT "creationsequence"
               Prelude'.return (\ o -> o{creationsequence = v}))