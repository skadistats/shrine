{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SpawnGroup_Load (CNETMsg_SpawnGroup_Load(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgQAngle as NetworkBaseTypes (CMsgQAngle)
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CNETMsg_SpawnGroup_Load = CNETMsg_SpawnGroup_Load{worldname :: !(P'.Maybe P'.Utf8), entitylumpname :: !(P'.Maybe P'.Utf8),
                                                       entityfiltername :: !(P'.Maybe P'.Utf8),
                                                       spawngrouphandle :: !(P'.Maybe P'.Word32),
                                                       spawngroupownerhandle :: !(P'.Maybe P'.Word32),
                                                       world_offset_pos :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                                       world_offset_angle :: !(P'.Maybe NetworkBaseTypes.CMsgQAngle),
                                                       spawngroupmanifest :: !(P'.Maybe P'.ByteString),
                                                       flags :: !(P'.Maybe P'.Word32), tickcount :: !(P'.Maybe P'.Int32),
                                                       manifestincomplete :: !(P'.Maybe P'.Bool),
                                                       localnamefixup :: !(P'.Maybe P'.Utf8),
                                                       parentnamefixup :: !(P'.Maybe P'.Utf8),
                                                       manifestloadpriority :: !(P'.Maybe P'.Int32),
                                                       worldgroupid :: !(P'.Maybe P'.Word32),
                                                       creationsequence :: !(P'.Maybe P'.Word32),
                                                       savegamefilename :: !(P'.Maybe P'.Utf8),
                                                       spawngroupparenthandle :: !(P'.Maybe P'.Word32)}
                             deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CNETMsg_SpawnGroup_Load where
  mergeAppend (CNETMsg_SpawnGroup_Load x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18)
   (CNETMsg_SpawnGroup_Load y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18)
   = CNETMsg_SpawnGroup_Load (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)

instance P'.Default CNETMsg_SpawnGroup_Load where
  defaultValue
   = CNETMsg_SpawnGroup_Load P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CNETMsg_SpawnGroup_Load where
  wireSize ft' self'@(CNETMsg_SpawnGroup_Load x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 9 x'1 + P'.wireSizeOpt 1 9 x'2 + P'.wireSizeOpt 1 9 x'3 + P'.wireSizeOpt 1 13 x'4 +
             P'.wireSizeOpt 1 13 x'5
             + P'.wireSizeOpt 1 11 x'6
             + P'.wireSizeOpt 1 11 x'7
             + P'.wireSizeOpt 1 12 x'8
             + P'.wireSizeOpt 1 13 x'9
             + P'.wireSizeOpt 1 5 x'10
             + P'.wireSizeOpt 1 8 x'11
             + P'.wireSizeOpt 1 9 x'12
             + P'.wireSizeOpt 1 9 x'13
             + P'.wireSizeOpt 1 5 x'14
             + P'.wireSizeOpt 1 13 x'15
             + P'.wireSizeOpt 2 13 x'16
             + P'.wireSizeOpt 2 9 x'17
             + P'.wireSizeOpt 2 13 x'18)
  wirePut ft' self'@(CNETMsg_SpawnGroup_Load x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
             P'.wirePutOpt 18 9 x'2
             P'.wirePutOpt 26 9 x'3
             P'.wirePutOpt 32 13 x'4
             P'.wirePutOpt 40 13 x'5
             P'.wirePutOpt 50 11 x'6
             P'.wirePutOpt 58 11 x'7
             P'.wirePutOpt 66 12 x'8
             P'.wirePutOpt 72 13 x'9
             P'.wirePutOpt 80 5 x'10
             P'.wirePutOpt 88 8 x'11
             P'.wirePutOpt 98 9 x'12
             P'.wirePutOpt 106 9 x'13
             P'.wirePutOpt 112 5 x'14
             P'.wirePutOpt 120 13 x'15
             P'.wirePutOpt 128 13 x'16
             P'.wirePutOpt 138 9 x'17
             P'.wirePutOpt 144 13 x'18
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{worldname = Prelude'.Just new'Field}) (P'.wireGet 9)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{entitylumpname = Prelude'.Just new'Field}) (P'.wireGet 9)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{entityfiltername = Prelude'.Just new'Field}) (P'.wireGet 9)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{spawngrouphandle = Prelude'.Just new'Field}) (P'.wireGet 13)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{spawngroupownerhandle = Prelude'.Just new'Field}) (P'.wireGet 13)
             50 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{world_offset_pos = P'.mergeAppend (world_offset_pos old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             58 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{world_offset_angle = P'.mergeAppend (world_offset_angle old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             66 -> Prelude'.fmap (\ !new'Field -> old'Self{spawngroupmanifest = Prelude'.Just new'Field}) (P'.wireGet 12)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{flags = Prelude'.Just new'Field}) (P'.wireGet 13)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{tickcount = Prelude'.Just new'Field}) (P'.wireGet 5)
             88 -> Prelude'.fmap (\ !new'Field -> old'Self{manifestincomplete = Prelude'.Just new'Field}) (P'.wireGet 8)
             98 -> Prelude'.fmap (\ !new'Field -> old'Self{localnamefixup = Prelude'.Just new'Field}) (P'.wireGet 9)
             106 -> Prelude'.fmap (\ !new'Field -> old'Self{parentnamefixup = Prelude'.Just new'Field}) (P'.wireGet 9)
             112 -> Prelude'.fmap (\ !new'Field -> old'Self{manifestloadpriority = Prelude'.Just new'Field}) (P'.wireGet 5)
             120 -> Prelude'.fmap (\ !new'Field -> old'Self{worldgroupid = Prelude'.Just new'Field}) (P'.wireGet 13)
             128 -> Prelude'.fmap (\ !new'Field -> old'Self{creationsequence = Prelude'.Just new'Field}) (P'.wireGet 13)
             138 -> Prelude'.fmap (\ !new'Field -> old'Self{savegamefilename = Prelude'.Just new'Field}) (P'.wireGet 9)
             144 -> Prelude'.fmap (\ !new'Field -> old'Self{spawngroupparenthandle = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CNETMsg_SpawnGroup_Load) CNETMsg_SpawnGroup_Load where
  getVal m' f' = f' m'

instance P'.GPB CNETMsg_SpawnGroup_Load

instance P'.ReflectDescriptor CNETMsg_SpawnGroup_Load where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList [10, 18, 26, 32, 40, 50, 58, 66, 72, 80, 88, 98, 106, 112, 120, 128, 138, 144])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CNETMsg_SpawnGroup_Load\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CNETMsg_SpawnGroup_Load.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.worldname\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"worldname\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.entitylumpname\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"entitylumpname\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.entityfiltername\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"entityfiltername\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.spawngrouphandle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"spawngrouphandle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.spawngroupownerhandle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"spawngroupownerhandle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.world_offset_pos\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"world_offset_pos\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 50}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.world_offset_angle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"world_offset_angle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 58}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgQAngle\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgQAngle\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.spawngroupmanifest\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"spawngroupmanifest\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 66}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.flags\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"flags\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.tickcount\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"tickcount\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.manifestincomplete\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"manifestincomplete\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 88}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.localnamefixup\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"localnamefixup\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 98}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.parentnamefixup\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"parentnamefixup\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 106}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.manifestloadpriority\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"manifestloadpriority\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 112}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.worldgroupid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"worldgroupid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 120}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.creationsequence\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"creationsequence\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 128}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.savegamefilename\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"savegamefilename\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 138}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SpawnGroup_Load.spawngroupparenthandle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SpawnGroup_Load\"], baseName' = FName \"spawngroupparenthandle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 144}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CNETMsg_SpawnGroup_Load where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CNETMsg_SpawnGroup_Load where
  textPut msg
   = do
       P'.tellT "worldname" (worldname msg)
       P'.tellT "entitylumpname" (entitylumpname msg)
       P'.tellT "entityfiltername" (entityfiltername msg)
       P'.tellT "spawngrouphandle" (spawngrouphandle msg)
       P'.tellT "spawngroupownerhandle" (spawngroupownerhandle msg)
       P'.tellT "world_offset_pos" (world_offset_pos msg)
       P'.tellT "world_offset_angle" (world_offset_angle msg)
       P'.tellT "spawngroupmanifest" (spawngroupmanifest msg)
       P'.tellT "flags" (flags msg)
       P'.tellT "tickcount" (tickcount msg)
       P'.tellT "manifestincomplete" (manifestincomplete msg)
       P'.tellT "localnamefixup" (localnamefixup msg)
       P'.tellT "parentnamefixup" (parentnamefixup msg)
       P'.tellT "manifestloadpriority" (manifestloadpriority msg)
       P'.tellT "worldgroupid" (worldgroupid msg)
       P'.tellT "creationsequence" (creationsequence msg)
       P'.tellT "savegamefilename" (savegamefilename msg)
       P'.tellT "spawngroupparenthandle" (spawngroupparenthandle msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'worldname, parse'entitylumpname, parse'entityfiltername, parse'spawngrouphandle,
                   parse'spawngroupownerhandle, parse'world_offset_pos, parse'world_offset_angle, parse'spawngroupmanifest,
                   parse'flags, parse'tickcount, parse'manifestincomplete, parse'localnamefixup, parse'parentnamefixup,
                   parse'manifestloadpriority, parse'worldgroupid, parse'creationsequence, parse'savegamefilename,
                   parse'spawngroupparenthandle])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'worldname
         = P'.try
            (do
               v <- P'.getT "worldname"
               Prelude'.return (\ o -> o{worldname = v}))
        parse'entitylumpname
         = P'.try
            (do
               v <- P'.getT "entitylumpname"
               Prelude'.return (\ o -> o{entitylumpname = v}))
        parse'entityfiltername
         = P'.try
            (do
               v <- P'.getT "entityfiltername"
               Prelude'.return (\ o -> o{entityfiltername = v}))
        parse'spawngrouphandle
         = P'.try
            (do
               v <- P'.getT "spawngrouphandle"
               Prelude'.return (\ o -> o{spawngrouphandle = v}))
        parse'spawngroupownerhandle
         = P'.try
            (do
               v <- P'.getT "spawngroupownerhandle"
               Prelude'.return (\ o -> o{spawngroupownerhandle = v}))
        parse'world_offset_pos
         = P'.try
            (do
               v <- P'.getT "world_offset_pos"
               Prelude'.return (\ o -> o{world_offset_pos = v}))
        parse'world_offset_angle
         = P'.try
            (do
               v <- P'.getT "world_offset_angle"
               Prelude'.return (\ o -> o{world_offset_angle = v}))
        parse'spawngroupmanifest
         = P'.try
            (do
               v <- P'.getT "spawngroupmanifest"
               Prelude'.return (\ o -> o{spawngroupmanifest = v}))
        parse'flags
         = P'.try
            (do
               v <- P'.getT "flags"
               Prelude'.return (\ o -> o{flags = v}))
        parse'tickcount
         = P'.try
            (do
               v <- P'.getT "tickcount"
               Prelude'.return (\ o -> o{tickcount = v}))
        parse'manifestincomplete
         = P'.try
            (do
               v <- P'.getT "manifestincomplete"
               Prelude'.return (\ o -> o{manifestincomplete = v}))
        parse'localnamefixup
         = P'.try
            (do
               v <- P'.getT "localnamefixup"
               Prelude'.return (\ o -> o{localnamefixup = v}))
        parse'parentnamefixup
         = P'.try
            (do
               v <- P'.getT "parentnamefixup"
               Prelude'.return (\ o -> o{parentnamefixup = v}))
        parse'manifestloadpriority
         = P'.try
            (do
               v <- P'.getT "manifestloadpriority"
               Prelude'.return (\ o -> o{manifestloadpriority = v}))
        parse'worldgroupid
         = P'.try
            (do
               v <- P'.getT "worldgroupid"
               Prelude'.return (\ o -> o{worldgroupid = v}))
        parse'creationsequence
         = P'.try
            (do
               v <- P'.getT "creationsequence"
               Prelude'.return (\ o -> o{creationsequence = v}))
        parse'savegamefilename
         = P'.try
            (do
               v <- P'.getT "savegamefilename"
               Prelude'.return (\ o -> o{savegamefilename = v}))
        parse'spawngroupparenthandle
         = P'.try
            (do
               v <- P'.getT "spawngroupparenthandle"
               Prelude'.return (\ o -> o{spawngroupparenthandle = v}))