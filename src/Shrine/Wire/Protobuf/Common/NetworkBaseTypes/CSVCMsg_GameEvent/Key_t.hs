{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CSVCMsg_GameEvent.Key_t (Key_t(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data Key_t = Key_t{type' :: !(P'.Maybe P'.Int32), val_string :: !(P'.Maybe P'.Utf8), val_float :: !(P'.Maybe P'.Float),
                   val_long :: !(P'.Maybe P'.Int32), val_short :: !(P'.Maybe P'.Int32), val_byte :: !(P'.Maybe P'.Int32),
                   val_bool :: !(P'.Maybe P'.Bool), val_uint64 :: !(P'.Maybe P'.Word64)}
           deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Key_t where
  mergeAppend (Key_t x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8) (Key_t y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8)
   = Key_t (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)

instance P'.Default Key_t where
  defaultValue
   = Key_t P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue

instance P'.Wire Key_t where
  wireSize ft' self'@(Key_t x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 9 x'2 + P'.wireSizeOpt 1 2 x'3 + P'.wireSizeOpt 1 5 x'4 +
             P'.wireSizeOpt 1 5 x'5
             + P'.wireSizeOpt 1 5 x'6
             + P'.wireSizeOpt 1 8 x'7
             + P'.wireSizeOpt 1 4 x'8)
  wirePut ft' self'@(Key_t x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 18 9 x'2
             P'.wirePutOpt 29 2 x'3
             P'.wirePutOpt 32 5 x'4
             P'.wirePutOpt 40 5 x'5
             P'.wirePutOpt 48 5 x'6
             P'.wirePutOpt 56 8 x'7
             P'.wirePutOpt 64 4 x'8
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{type' = Prelude'.Just new'Field}) (P'.wireGet 5)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{val_string = Prelude'.Just new'Field}) (P'.wireGet 9)
             29 -> Prelude'.fmap (\ !new'Field -> old'Self{val_float = Prelude'.Just new'Field}) (P'.wireGet 2)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{val_long = Prelude'.Just new'Field}) (P'.wireGet 5)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{val_short = Prelude'.Just new'Field}) (P'.wireGet 5)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{val_byte = Prelude'.Just new'Field}) (P'.wireGet 5)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{val_bool = Prelude'.Just new'Field}) (P'.wireGet 8)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{val_uint64 = Prelude'.Just new'Field}) (P'.wireGet 4)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Key_t) Key_t where
  getVal m' f' = f' m'

instance P'.GPB Key_t

instance P'.ReflectDescriptor Key_t where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18, 29, 32, 40, 48, 56, 64])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CSVCMsg_GameEvent.key_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameEvent\"], baseName = MName \"Key_t\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CSVCMsg_GameEvent\",\"Key_t.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameEvent.key_t.type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameEvent\",MName \"Key_t\"], baseName' = FName \"type'\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameEvent.key_t.val_string\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameEvent\",MName \"Key_t\"], baseName' = FName \"val_string\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameEvent.key_t.val_float\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameEvent\",MName \"Key_t\"], baseName' = FName \"val_float\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 29}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameEvent.key_t.val_long\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameEvent\",MName \"Key_t\"], baseName' = FName \"val_long\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameEvent.key_t.val_short\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameEvent\",MName \"Key_t\"], baseName' = FName \"val_short\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameEvent.key_t.val_byte\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameEvent\",MName \"Key_t\"], baseName' = FName \"val_byte\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameEvent.key_t.val_bool\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameEvent\",MName \"Key_t\"], baseName' = FName \"val_bool\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsg_GameEvent.key_t.val_uint64\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsg_GameEvent\",MName \"Key_t\"], baseName' = FName \"val_uint64\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Key_t where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Key_t where
  textPut msg
   = do
       P'.tellT "type" (type' msg)
       P'.tellT "val_string" (val_string msg)
       P'.tellT "val_float" (val_float msg)
       P'.tellT "val_long" (val_long msg)
       P'.tellT "val_short" (val_short msg)
       P'.tellT "val_byte" (val_byte msg)
       P'.tellT "val_bool" (val_bool msg)
       P'.tellT "val_uint64" (val_uint64 msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'type', parse'val_string, parse'val_float, parse'val_long, parse'val_short, parse'val_byte, parse'val_bool,
                   parse'val_uint64])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'type'
         = P'.try
            (do
               v <- P'.getT "type"
               Prelude'.return (\ o -> o{type' = v}))
        parse'val_string
         = P'.try
            (do
               v <- P'.getT "val_string"
               Prelude'.return (\ o -> o{val_string = v}))
        parse'val_float
         = P'.try
            (do
               v <- P'.getT "val_float"
               Prelude'.return (\ o -> o{val_float = v}))
        parse'val_long
         = P'.try
            (do
               v <- P'.getT "val_long"
               Prelude'.return (\ o -> o{val_long = v}))
        parse'val_short
         = P'.try
            (do
               v <- P'.getT "val_short"
               Prelude'.return (\ o -> o{val_short = v}))
        parse'val_byte
         = P'.try
            (do
               v <- P'.getT "val_byte"
               Prelude'.return (\ o -> o{val_byte = v}))
        parse'val_bool
         = P'.try
            (do
               v <- P'.getT "val_bool"
               Prelude'.return (\ o -> o{val_bool = v}))
        parse'val_uint64
         = P'.try
            (do
               v <- P'.getT "val_uint64"
               Prelude'.return (\ o -> o{val_uint64 = v}))