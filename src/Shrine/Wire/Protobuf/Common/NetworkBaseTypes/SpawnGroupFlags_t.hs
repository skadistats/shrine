{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.SpawnGroupFlags_t (SpawnGroupFlags_t(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data SpawnGroupFlags_t = SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE
                       | SPAWN_GROUP_DONT_SPAWN_ENTITIES
                       | SPAWN_GROUP_SYNCHRONOUS_SPAWN
                       | SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP
                       | SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES
                       | SPAWN_GROUP_SAVE_ENTITIES
                       | SPAWN_GROUP_BLOCK_UNTIL_LOADED
                       | SPAWN_GROUP_LOAD_STREAMING_DATA
                       | SPAWN_GROUP_CREATE_NEW_SCENE_WORLD
                       deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                 Prelude'.Generic)

instance P'.Mergeable SpawnGroupFlags_t

instance Prelude'.Bounded SpawnGroupFlags_t where
  minBound = SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE
  maxBound = SPAWN_GROUP_CREATE_NEW_SCENE_WORLD

instance P'.Default SpawnGroupFlags_t where
  defaultValue = SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE

toMaybe'Enum :: Prelude'.Int -> P'.Maybe SpawnGroupFlags_t
toMaybe'Enum 1 = Prelude'.Just SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE
toMaybe'Enum 2 = Prelude'.Just SPAWN_GROUP_DONT_SPAWN_ENTITIES
toMaybe'Enum 4 = Prelude'.Just SPAWN_GROUP_SYNCHRONOUS_SPAWN
toMaybe'Enum 8 = Prelude'.Just SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP
toMaybe'Enum 16 = Prelude'.Just SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES
toMaybe'Enum 32 = Prelude'.Just SPAWN_GROUP_SAVE_ENTITIES
toMaybe'Enum 64 = Prelude'.Just SPAWN_GROUP_BLOCK_UNTIL_LOADED
toMaybe'Enum 128 = Prelude'.Just SPAWN_GROUP_LOAD_STREAMING_DATA
toMaybe'Enum 256 = Prelude'.Just SPAWN_GROUP_CREATE_NEW_SCENE_WORLD
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum SpawnGroupFlags_t where
  fromEnum SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE = 1
  fromEnum SPAWN_GROUP_DONT_SPAWN_ENTITIES = 2
  fromEnum SPAWN_GROUP_SYNCHRONOUS_SPAWN = 4
  fromEnum SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP = 8
  fromEnum SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES = 16
  fromEnum SPAWN_GROUP_SAVE_ENTITIES = 32
  fromEnum SPAWN_GROUP_BLOCK_UNTIL_LOADED = 64
  fromEnum SPAWN_GROUP_LOAD_STREAMING_DATA = 128
  fromEnum SPAWN_GROUP_CREATE_NEW_SCENE_WORLD = 256
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.NetworkBaseTypes.SpawnGroupFlags_t")
      . toMaybe'Enum
  succ SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE = SPAWN_GROUP_DONT_SPAWN_ENTITIES
  succ SPAWN_GROUP_DONT_SPAWN_ENTITIES = SPAWN_GROUP_SYNCHRONOUS_SPAWN
  succ SPAWN_GROUP_SYNCHRONOUS_SPAWN = SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP
  succ SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP = SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES
  succ SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES = SPAWN_GROUP_SAVE_ENTITIES
  succ SPAWN_GROUP_SAVE_ENTITIES = SPAWN_GROUP_BLOCK_UNTIL_LOADED
  succ SPAWN_GROUP_BLOCK_UNTIL_LOADED = SPAWN_GROUP_LOAD_STREAMING_DATA
  succ SPAWN_GROUP_LOAD_STREAMING_DATA = SPAWN_GROUP_CREATE_NEW_SCENE_WORLD
  succ _
   = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.NetworkBaseTypes.SpawnGroupFlags_t"
  pred SPAWN_GROUP_DONT_SPAWN_ENTITIES = SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE
  pred SPAWN_GROUP_SYNCHRONOUS_SPAWN = SPAWN_GROUP_DONT_SPAWN_ENTITIES
  pred SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP = SPAWN_GROUP_SYNCHRONOUS_SPAWN
  pred SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES = SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP
  pred SPAWN_GROUP_SAVE_ENTITIES = SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES
  pred SPAWN_GROUP_BLOCK_UNTIL_LOADED = SPAWN_GROUP_SAVE_ENTITIES
  pred SPAWN_GROUP_LOAD_STREAMING_DATA = SPAWN_GROUP_BLOCK_UNTIL_LOADED
  pred SPAWN_GROUP_CREATE_NEW_SCENE_WORLD = SPAWN_GROUP_LOAD_STREAMING_DATA
  pred _
   = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.NetworkBaseTypes.SpawnGroupFlags_t"

instance P'.Wire SpawnGroupFlags_t where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB SpawnGroupFlags_t

instance P'.MessageAPI msg' (msg' -> SpawnGroupFlags_t) SpawnGroupFlags_t where
  getVal m' f' = f' m'

instance P'.ReflectEnum SpawnGroupFlags_t where
  reflectEnum
   = [(1, "SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE", SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE),
      (2, "SPAWN_GROUP_DONT_SPAWN_ENTITIES", SPAWN_GROUP_DONT_SPAWN_ENTITIES),
      (4, "SPAWN_GROUP_SYNCHRONOUS_SPAWN", SPAWN_GROUP_SYNCHRONOUS_SPAWN),
      (8, "SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP", SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP),
      (16, "SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES", SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES),
      (32, "SPAWN_GROUP_SAVE_ENTITIES", SPAWN_GROUP_SAVE_ENTITIES),
      (64, "SPAWN_GROUP_BLOCK_UNTIL_LOADED", SPAWN_GROUP_BLOCK_UNTIL_LOADED),
      (128, "SPAWN_GROUP_LOAD_STREAMING_DATA", SPAWN_GROUP_LOAD_STREAMING_DATA),
      (256, "SPAWN_GROUP_CREATE_NEW_SCENE_WORLD", SPAWN_GROUP_CREATE_NEW_SCENE_WORLD)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Networkbasetypes.SpawnGroupFlags_t") ["Shrine", "Wire", "Protobuf", "Common"] ["NetworkBaseTypes"]
        "SpawnGroupFlags_t")
      ["Shrine", "Wire", "Protobuf", "Common", "NetworkBaseTypes", "SpawnGroupFlags_t.hs"]
      [(1, "SPAWN_GROUP_LOAD_ENTITIES_FROM_SAVE"), (2, "SPAWN_GROUP_DONT_SPAWN_ENTITIES"), (4, "SPAWN_GROUP_SYNCHRONOUS_SPAWN"),
       (8, "SPAWN_GROUP_IS_INITIAL_SPAWN_GROUP"), (16, "SPAWN_GROUP_CREATE_CLIENT_ONLY_ENTITIES"),
       (32, "SPAWN_GROUP_SAVE_ENTITIES"), (64, "SPAWN_GROUP_BLOCK_UNTIL_LOADED"), (128, "SPAWN_GROUP_LOAD_STREAMING_DATA"),
       (256, "SPAWN_GROUP_CREATE_NEW_SCENE_WORLD")]

instance P'.TextType SpawnGroupFlags_t where
  tellT = P'.tellShow
  getT = P'.getRead