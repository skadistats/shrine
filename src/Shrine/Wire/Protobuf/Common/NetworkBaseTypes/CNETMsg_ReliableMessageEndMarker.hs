{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_ReliableMessageEndMarker (CNETMsg_ReliableMessageEndMarker(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CNETMsg_ReliableMessageEndMarker = CNETMsg_ReliableMessageEndMarker{}
                                      deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                Prelude'.Generic)

instance P'.Mergeable CNETMsg_ReliableMessageEndMarker where
  mergeAppend CNETMsg_ReliableMessageEndMarker CNETMsg_ReliableMessageEndMarker = CNETMsg_ReliableMessageEndMarker

instance P'.Default CNETMsg_ReliableMessageEndMarker where
  defaultValue = CNETMsg_ReliableMessageEndMarker

instance P'.Wire CNETMsg_ReliableMessageEndMarker where
  wireSize ft' self'@(CNETMsg_ReliableMessageEndMarker)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = 0
  wirePut ft' self'@(CNETMsg_ReliableMessageEndMarker)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             Prelude'.return ()
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CNETMsg_ReliableMessageEndMarker) CNETMsg_ReliableMessageEndMarker where
  getVal m' f' = f' m'

instance P'.GPB CNETMsg_ReliableMessageEndMarker

instance P'.ReflectDescriptor CNETMsg_ReliableMessageEndMarker where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CNETMsg_ReliableMessageEndMarker\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CNETMsg_ReliableMessageEndMarker\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CNETMsg_ReliableMessageEndMarker.hs\"], isGroup = False, fields = fromList [], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CNETMsg_ReliableMessageEndMarker where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CNETMsg_ReliableMessageEndMarker where
  textPut msg = Prelude'.return ()
  textGet = Prelude'.return P'.defaultValue
    where