{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_Tick (CNETMsg_Tick(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CNETMsg_Tick = CNETMsg_Tick{tick :: !(P'.Maybe P'.Word32), host_frametime :: !(P'.Maybe P'.Word32),
                                 host_frametime_std_deviation :: !(P'.Maybe P'.Word32),
                                 host_computationtime :: !(P'.Maybe P'.Word32),
                                 host_computationtime_std_deviation :: !(P'.Maybe P'.Word32),
                                 host_framestarttime_std_deviation :: !(P'.Maybe P'.Word32)}
                  deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CNETMsg_Tick where
  mergeAppend (CNETMsg_Tick x'1 x'2 x'3 x'4 x'5 x'6) (CNETMsg_Tick y'1 y'2 y'3 y'4 y'5 y'6)
   = CNETMsg_Tick (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CNETMsg_Tick where
  defaultValue = CNETMsg_Tick P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CNETMsg_Tick where
  wireSize ft' self'@(CNETMsg_Tick x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 13 x'3 + P'.wireSizeOpt 1 13 x'4 +
             P'.wireSizeOpt 1 13 x'5
             + P'.wireSizeOpt 1 13 x'6)
  wirePut ft' self'@(CNETMsg_Tick x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 13 x'3
             P'.wirePutOpt 32 13 x'4
             P'.wirePutOpt 40 13 x'5
             P'.wirePutOpt 48 13 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{tick = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{host_frametime = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{host_frametime_std_deviation = Prelude'.Just new'Field}) (P'.wireGet 13)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{host_computationtime = Prelude'.Just new'Field}) (P'.wireGet 13)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{host_computationtime_std_deviation = Prelude'.Just new'Field})
                    (P'.wireGet 13)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{host_framestarttime_std_deviation = Prelude'.Just new'Field})
                    (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CNETMsg_Tick) CNETMsg_Tick where
  getVal m' f' = f' m'

instance P'.GPB CNETMsg_Tick

instance P'.ReflectDescriptor CNETMsg_Tick where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 32, 40, 48])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CNETMsg_Tick\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CNETMsg_Tick\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CNETMsg_Tick.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_Tick.tick\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_Tick\"], baseName' = FName \"tick\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_Tick.host_frametime\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_Tick\"], baseName' = FName \"host_frametime\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_Tick.host_frametime_std_deviation\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_Tick\"], baseName' = FName \"host_frametime_std_deviation\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_Tick.host_computationtime\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_Tick\"], baseName' = FName \"host_computationtime\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_Tick.host_computationtime_std_deviation\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_Tick\"], baseName' = FName \"host_computationtime_std_deviation\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_Tick.host_framestarttime_std_deviation\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_Tick\"], baseName' = FName \"host_framestarttime_std_deviation\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CNETMsg_Tick where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CNETMsg_Tick where
  textPut msg
   = do
       P'.tellT "tick" (tick msg)
       P'.tellT "host_frametime" (host_frametime msg)
       P'.tellT "host_frametime_std_deviation" (host_frametime_std_deviation msg)
       P'.tellT "host_computationtime" (host_computationtime msg)
       P'.tellT "host_computationtime_std_deviation" (host_computationtime_std_deviation msg)
       P'.tellT "host_framestarttime_std_deviation" (host_framestarttime_std_deviation msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'tick, parse'host_frametime, parse'host_frametime_std_deviation, parse'host_computationtime,
                   parse'host_computationtime_std_deviation, parse'host_framestarttime_std_deviation])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'tick
         = P'.try
            (do
               v <- P'.getT "tick"
               Prelude'.return (\ o -> o{tick = v}))
        parse'host_frametime
         = P'.try
            (do
               v <- P'.getT "host_frametime"
               Prelude'.return (\ o -> o{host_frametime = v}))
        parse'host_frametime_std_deviation
         = P'.try
            (do
               v <- P'.getT "host_frametime_std_deviation"
               Prelude'.return (\ o -> o{host_frametime_std_deviation = v}))
        parse'host_computationtime
         = P'.try
            (do
               v <- P'.getT "host_computationtime"
               Prelude'.return (\ o -> o{host_computationtime = v}))
        parse'host_computationtime_std_deviation
         = P'.try
            (do
               v <- P'.getT "host_computationtime_std_deviation"
               Prelude'.return (\ o -> o{host_computationtime_std_deviation = v}))
        parse'host_framestarttime_std_deviation
         = P'.try
            (do
               v <- P'.getT "host_framestarttime_std_deviation"
               Prelude'.return (\ o -> o{host_framestarttime_std_deviation = v}))