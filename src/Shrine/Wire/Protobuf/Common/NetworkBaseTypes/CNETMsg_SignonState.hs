{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CNETMsg_SignonState (CNETMsg_SignonState(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CNETMsg_SignonState = CNETMsg_SignonState{signon_state :: !(P'.Maybe P'.Word32), spawn_count :: !(P'.Maybe P'.Word32),
                                               num_server_players :: !(P'.Maybe P'.Word32), players_networkids :: !(P'.Seq P'.Utf8),
                                               map_name :: !(P'.Maybe P'.Utf8), addons :: !(P'.Maybe P'.Utf8)}
                         deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CNETMsg_SignonState where
  mergeAppend (CNETMsg_SignonState x'1 x'2 x'3 x'4 x'5 x'6) (CNETMsg_SignonState y'1 y'2 y'3 y'4 y'5 y'6)
   = CNETMsg_SignonState (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CNETMsg_SignonState where
  defaultValue = CNETMsg_SignonState P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CNETMsg_SignonState where
  wireSize ft' self'@(CNETMsg_SignonState x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 13 x'3 + P'.wireSizeRep 1 9 x'4 +
             P'.wireSizeOpt 1 9 x'5
             + P'.wireSizeOpt 1 9 x'6)
  wirePut ft' self'@(CNETMsg_SignonState x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 13 x'3
             P'.wirePutRep 34 9 x'4
             P'.wirePutOpt 42 9 x'5
             P'.wirePutOpt 50 9 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{signon_state = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{spawn_count = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{num_server_players = Prelude'.Just new'Field}) (P'.wireGet 13)
             34 -> Prelude'.fmap (\ !new'Field -> old'Self{players_networkids = P'.append (players_networkids old'Self) new'Field})
                    (P'.wireGet 9)
             42 -> Prelude'.fmap (\ !new'Field -> old'Self{map_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             50 -> Prelude'.fmap (\ !new'Field -> old'Self{addons = Prelude'.Just new'Field}) (P'.wireGet 9)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CNETMsg_SignonState) CNETMsg_SignonState where
  getVal m' f' = f' m'

instance P'.GPB CNETMsg_SignonState

instance P'.ReflectDescriptor CNETMsg_SignonState where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 34, 42, 50])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CNETMsg_SignonState\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CNETMsg_SignonState\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CNETMsg_SignonState.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SignonState.signon_state\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SignonState\"], baseName' = FName \"signon_state\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SignonState.spawn_count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SignonState\"], baseName' = FName \"spawn_count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SignonState.num_server_players\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SignonState\"], baseName' = FName \"num_server_players\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SignonState.players_networkids\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SignonState\"], baseName' = FName \"players_networkids\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 34}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SignonState.map_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SignonState\"], baseName' = FName \"map_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CNETMsg_SignonState.addons\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CNETMsg_SignonState\"], baseName' = FName \"addons\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 50}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CNETMsg_SignonState where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CNETMsg_SignonState where
  textPut msg
   = do
       P'.tellT "signon_state" (signon_state msg)
       P'.tellT "spawn_count" (spawn_count msg)
       P'.tellT "num_server_players" (num_server_players msg)
       P'.tellT "players_networkids" (players_networkids msg)
       P'.tellT "map_name" (map_name msg)
       P'.tellT "addons" (addons msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'signon_state, parse'spawn_count, parse'num_server_players, parse'players_networkids, parse'map_name,
                   parse'addons])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'signon_state
         = P'.try
            (do
               v <- P'.getT "signon_state"
               Prelude'.return (\ o -> o{signon_state = v}))
        parse'spawn_count
         = P'.try
            (do
               v <- P'.getT "spawn_count"
               Prelude'.return (\ o -> o{spawn_count = v}))
        parse'num_server_players
         = P'.try
            (do
               v <- P'.getT "num_server_players"
               Prelude'.return (\ o -> o{num_server_players = v}))
        parse'players_networkids
         = P'.try
            (do
               v <- P'.getT "players_networkids"
               Prelude'.return (\ o -> o{players_networkids = P'.append (players_networkids o) v}))
        parse'map_name
         = P'.try
            (do
               v <- P'.getT "map_name"
               Prelude'.return (\ o -> o{map_name = v}))
        parse'addons
         = P'.try
            (do
               v <- P'.getT "addons"
               Prelude'.return (\ o -> o{addons = v}))