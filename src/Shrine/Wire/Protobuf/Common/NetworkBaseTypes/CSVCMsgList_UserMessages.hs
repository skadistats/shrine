{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CSVCMsgList_UserMessages (CSVCMsgList_UserMessages(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CSVCMsgList_UserMessages.Usermsg_t
       as NetworkBaseTypes.CSVCMsgList_UserMessages (Usermsg_t)

data CSVCMsgList_UserMessages = CSVCMsgList_UserMessages{usermsgs :: !(P'.Seq NetworkBaseTypes.CSVCMsgList_UserMessages.Usermsg_t)}
                              deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                        Prelude'.Generic)

instance P'.Mergeable CSVCMsgList_UserMessages where
  mergeAppend (CSVCMsgList_UserMessages x'1) (CSVCMsgList_UserMessages y'1) = CSVCMsgList_UserMessages (P'.mergeAppend x'1 y'1)

instance P'.Default CSVCMsgList_UserMessages where
  defaultValue = CSVCMsgList_UserMessages P'.defaultValue

instance P'.Wire CSVCMsgList_UserMessages where
  wireSize ft' self'@(CSVCMsgList_UserMessages x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 11 x'1)
  wirePut ft' self'@(CSVCMsgList_UserMessages x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 10 11 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{usermsgs = P'.append (usermsgs old'Self) new'Field}) (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsgList_UserMessages) CSVCMsgList_UserMessages where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsgList_UserMessages

instance P'.ReflectDescriptor CSVCMsgList_UserMessages where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Networkbasetypes.CSVCMsgList_UserMessages\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CSVCMsgList_UserMessages\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetworkBaseTypes\",\"CSVCMsgList_UserMessages.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Networkbasetypes.CSVCMsgList_UserMessages.usermsgs\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetworkBaseTypes\",MName \"CSVCMsgList_UserMessages\"], baseName' = FName \"usermsgs\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CSVCMsgList_UserMessages.usermsg_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\",MName \"CSVCMsgList_UserMessages\"], baseName = MName \"Usermsg_t\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsgList_UserMessages where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsgList_UserMessages where
  textPut msg
   = do
       P'.tellT "usermsgs" (usermsgs msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'usermsgs]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'usermsgs
         = P'.try
            (do
               v <- P'.getT "usermsgs"
               Prelude'.return (\ o -> o{usermsgs = P'.append (usermsgs o) v}))