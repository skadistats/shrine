{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_SetView (CSVCMsg_SetView(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CSVCMsg_SetView = CSVCMsg_SetView{entity_index :: !(P'.Maybe P'.Int32), slot :: !(P'.Maybe P'.Int32)}
                     deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CSVCMsg_SetView where
  mergeAppend (CSVCMsg_SetView x'1 x'2) (CSVCMsg_SetView y'1 y'2)
   = CSVCMsg_SetView (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CSVCMsg_SetView where
  defaultValue = CSVCMsg_SetView P'.defaultValue P'.defaultValue

instance P'.Wire CSVCMsg_SetView where
  wireSize ft' self'@(CSVCMsg_SetView x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2)
  wirePut ft' self'@(CSVCMsg_SetView x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{entity_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{slot = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_SetView) CSVCMsg_SetView where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_SetView

instance P'.ReflectDescriptor CSVCMsg_SetView where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CSVCMsg_SetView\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CSVCMsg_SetView\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CSVCMsg_SetView.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_SetView.entity_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_SetView\"], baseName' = FName \"entity_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_SetView.slot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_SetView\"], baseName' = FName \"slot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_SetView where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_SetView where
  textPut msg
   = do
       P'.tellT "entity_index" (entity_index msg)
       P'.tellT "slot" (slot msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'entity_index, parse'slot]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'entity_index
         = P'.try
            (do
               v <- P'.getT "entity_index"
               Prelude'.return (\ o -> o{entity_index = v}))
        parse'slot
         = P'.try
            (do
               v <- P'.getT "slot"
               Prelude'.return (\ o -> o{slot = v}))