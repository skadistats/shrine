{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CCLCMsg_SplitPlayerConnect (CCLCMsg_SplitPlayerConnect(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CCLCMsg_SplitPlayerConnect = CCLCMsg_SplitPlayerConnect{playername :: !(P'.Maybe P'.Utf8)}
                                deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                          Prelude'.Generic)

instance P'.Mergeable CCLCMsg_SplitPlayerConnect where
  mergeAppend (CCLCMsg_SplitPlayerConnect x'1) (CCLCMsg_SplitPlayerConnect y'1)
   = CCLCMsg_SplitPlayerConnect (P'.mergeAppend x'1 y'1)

instance P'.Default CCLCMsg_SplitPlayerConnect where
  defaultValue = CCLCMsg_SplitPlayerConnect P'.defaultValue

instance P'.Wire CCLCMsg_SplitPlayerConnect where
  wireSize ft' self'@(CCLCMsg_SplitPlayerConnect x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 9 x'1)
  wirePut ft' self'@(CCLCMsg_SplitPlayerConnect x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{playername = Prelude'.Just new'Field}) (P'.wireGet 9)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CCLCMsg_SplitPlayerConnect) CCLCMsg_SplitPlayerConnect where
  getVal m' f' = f' m'

instance P'.GPB CCLCMsg_SplitPlayerConnect

instance P'.ReflectDescriptor CCLCMsg_SplitPlayerConnect where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CCLCMsg_SplitPlayerConnect\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CCLCMsg_SplitPlayerConnect\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CCLCMsg_SplitPlayerConnect.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CCLCMsg_SplitPlayerConnect.playername\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CCLCMsg_SplitPlayerConnect\"], baseName' = FName \"playername\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CCLCMsg_SplitPlayerConnect where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CCLCMsg_SplitPlayerConnect where
  textPut msg
   = do
       P'.tellT "playername" (playername msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'playername]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'playername
         = P'.try
            (do
               v <- P'.getT "playername"
               Prelude'.return (\ o -> o{playername = v}))