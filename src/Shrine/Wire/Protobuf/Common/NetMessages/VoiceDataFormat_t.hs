{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.VoiceDataFormat_t (VoiceDataFormat_t(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data VoiceDataFormat_t = VOICEDATA_FORMAT_STEAM
                       | VOICEDATA_FORMAT_ENGINE
                       deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                 Prelude'.Generic)

instance P'.Mergeable VoiceDataFormat_t

instance Prelude'.Bounded VoiceDataFormat_t where
  minBound = VOICEDATA_FORMAT_STEAM
  maxBound = VOICEDATA_FORMAT_ENGINE

instance P'.Default VoiceDataFormat_t where
  defaultValue = VOICEDATA_FORMAT_STEAM

toMaybe'Enum :: Prelude'.Int -> P'.Maybe VoiceDataFormat_t
toMaybe'Enum 0 = Prelude'.Just VOICEDATA_FORMAT_STEAM
toMaybe'Enum 1 = Prelude'.Just VOICEDATA_FORMAT_ENGINE
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum VoiceDataFormat_t where
  fromEnum VOICEDATA_FORMAT_STEAM = 0
  fromEnum VOICEDATA_FORMAT_ENGINE = 1
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.NetMessages.VoiceDataFormat_t")
      . toMaybe'Enum
  succ VOICEDATA_FORMAT_STEAM = VOICEDATA_FORMAT_ENGINE
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.NetMessages.VoiceDataFormat_t"
  pred VOICEDATA_FORMAT_ENGINE = VOICEDATA_FORMAT_STEAM
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.NetMessages.VoiceDataFormat_t"

instance P'.Wire VoiceDataFormat_t where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB VoiceDataFormat_t

instance P'.MessageAPI msg' (msg' -> VoiceDataFormat_t) VoiceDataFormat_t where
  getVal m' f' = f' m'

instance P'.ReflectEnum VoiceDataFormat_t where
  reflectEnum = [(0, "VOICEDATA_FORMAT_STEAM", VOICEDATA_FORMAT_STEAM), (1, "VOICEDATA_FORMAT_ENGINE", VOICEDATA_FORMAT_ENGINE)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Netmessages.VoiceDataFormat_t") ["Shrine", "Wire", "Protobuf", "Common"] ["NetMessages"]
        "VoiceDataFormat_t")
      ["Shrine", "Wire", "Protobuf", "Common", "NetMessages", "VoiceDataFormat_t.hs"]
      [(0, "VOICEDATA_FORMAT_STEAM"), (1, "VOICEDATA_FORMAT_ENGINE")]

instance P'.TextType VoiceDataFormat_t where
  tellT = P'.tellShow
  getT = P'.getRead