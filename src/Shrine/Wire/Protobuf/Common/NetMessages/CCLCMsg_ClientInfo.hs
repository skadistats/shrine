{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CCLCMsg_ClientInfo (CCLCMsg_ClientInfo(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CCLCMsg_ClientInfo = CCLCMsg_ClientInfo{send_table_crc :: !(P'.Maybe P'.Word32), server_count :: !(P'.Maybe P'.Word32),
                                             is_hltv :: !(P'.Maybe P'.Bool), is_replay :: !(P'.Maybe P'.Bool),
                                             friends_id :: !(P'.Maybe P'.Word32), friends_name :: !(P'.Maybe P'.Utf8)}
                        deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CCLCMsg_ClientInfo where
  mergeAppend (CCLCMsg_ClientInfo x'1 x'2 x'3 x'4 x'5 x'6) (CCLCMsg_ClientInfo y'1 y'2 y'3 y'4 y'5 y'6)
   = CCLCMsg_ClientInfo (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CCLCMsg_ClientInfo where
  defaultValue = CCLCMsg_ClientInfo P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CCLCMsg_ClientInfo where
  wireSize ft' self'@(CCLCMsg_ClientInfo x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 7 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 8 x'3 + P'.wireSizeOpt 1 8 x'4 +
             P'.wireSizeOpt 1 13 x'5
             + P'.wireSizeOpt 1 9 x'6)
  wirePut ft' self'@(CCLCMsg_ClientInfo x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 13 7 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 8 x'3
             P'.wirePutOpt 32 8 x'4
             P'.wirePutOpt 40 13 x'5
             P'.wirePutOpt 50 9 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             13 -> Prelude'.fmap (\ !new'Field -> old'Self{send_table_crc = Prelude'.Just new'Field}) (P'.wireGet 7)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{server_count = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{is_hltv = Prelude'.Just new'Field}) (P'.wireGet 8)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{is_replay = Prelude'.Just new'Field}) (P'.wireGet 8)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{friends_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             50 -> Prelude'.fmap (\ !new'Field -> old'Self{friends_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CCLCMsg_ClientInfo) CCLCMsg_ClientInfo where
  getVal m' f' = f' m'

instance P'.GPB CCLCMsg_ClientInfo

instance P'.ReflectDescriptor CCLCMsg_ClientInfo where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [13, 16, 24, 32, 40, 50])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CCLCMsg_ClientInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CCLCMsg_ClientInfo\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CCLCMsg_ClientInfo.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CCLCMsg_ClientInfo.send_table_crc\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CCLCMsg_ClientInfo\"], baseName' = FName \"send_table_crc\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 13}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CCLCMsg_ClientInfo.server_count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CCLCMsg_ClientInfo\"], baseName' = FName \"server_count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CCLCMsg_ClientInfo.is_hltv\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CCLCMsg_ClientInfo\"], baseName' = FName \"is_hltv\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CCLCMsg_ClientInfo.is_replay\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CCLCMsg_ClientInfo\"], baseName' = FName \"is_replay\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CCLCMsg_ClientInfo.friends_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CCLCMsg_ClientInfo\"], baseName' = FName \"friends_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CCLCMsg_ClientInfo.friends_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CCLCMsg_ClientInfo\"], baseName' = FName \"friends_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 50}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CCLCMsg_ClientInfo where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CCLCMsg_ClientInfo where
  textPut msg
   = do
       P'.tellT "send_table_crc" (send_table_crc msg)
       P'.tellT "server_count" (server_count msg)
       P'.tellT "is_hltv" (is_hltv msg)
       P'.tellT "is_replay" (is_replay msg)
       P'.tellT "friends_id" (friends_id msg)
       P'.tellT "friends_name" (friends_name msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'send_table_crc, parse'server_count, parse'is_hltv, parse'is_replay, parse'friends_id, parse'friends_name])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'send_table_crc
         = P'.try
            (do
               v <- P'.getT "send_table_crc"
               Prelude'.return (\ o -> o{send_table_crc = v}))
        parse'server_count
         = P'.try
            (do
               v <- P'.getT "server_count"
               Prelude'.return (\ o -> o{server_count = v}))
        parse'is_hltv
         = P'.try
            (do
               v <- P'.getT "is_hltv"
               Prelude'.return (\ o -> o{is_hltv = v}))
        parse'is_replay
         = P'.try
            (do
               v <- P'.getT "is_replay"
               Prelude'.return (\ o -> o{is_replay = v}))
        parse'friends_id
         = P'.try
            (do
               v <- P'.getT "friends_id"
               Prelude'.return (\ o -> o{friends_id = v}))
        parse'friends_name
         = P'.try
            (do
               v <- P'.getT "friends_name"
               Prelude'.return (\ o -> o{friends_name = v}))