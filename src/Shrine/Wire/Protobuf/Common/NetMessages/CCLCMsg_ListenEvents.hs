{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CCLCMsg_ListenEvents (CCLCMsg_ListenEvents(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CCLCMsg_ListenEvents = CCLCMsg_ListenEvents{event_mask :: !(P'.Seq P'.Word32)}
                          deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CCLCMsg_ListenEvents where
  mergeAppend (CCLCMsg_ListenEvents x'1) (CCLCMsg_ListenEvents y'1) = CCLCMsg_ListenEvents (P'.mergeAppend x'1 y'1)

instance P'.Default CCLCMsg_ListenEvents where
  defaultValue = CCLCMsg_ListenEvents P'.defaultValue

instance P'.Wire CCLCMsg_ListenEvents where
  wireSize ft' self'@(CCLCMsg_ListenEvents x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 7 x'1)
  wirePut ft' self'@(CCLCMsg_ListenEvents x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 13 7 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             13 -> Prelude'.fmap (\ !new'Field -> old'Self{event_mask = P'.append (event_mask old'Self) new'Field}) (P'.wireGet 7)
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{event_mask = P'.mergeAppend (event_mask old'Self) new'Field})
                    (P'.wireGetPacked 7)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CCLCMsg_ListenEvents) CCLCMsg_ListenEvents where
  getVal m' f' = f' m'

instance P'.GPB CCLCMsg_ListenEvents

instance P'.ReflectDescriptor CCLCMsg_ListenEvents where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 13])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CCLCMsg_ListenEvents\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CCLCMsg_ListenEvents\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CCLCMsg_ListenEvents.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CCLCMsg_ListenEvents.event_mask\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CCLCMsg_ListenEvents\"], baseName' = FName \"event_mask\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 13}, packedTag = Just (WireTag {getWireTag = 13},WireTag {getWireTag = 10}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CCLCMsg_ListenEvents where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CCLCMsg_ListenEvents where
  textPut msg
   = do
       P'.tellT "event_mask" (event_mask msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'event_mask]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'event_mask
         = P'.try
            (do
               v <- P'.getT "event_mask"
               Prelude'.return (\ o -> o{event_mask = P'.append (event_mask o) v}))