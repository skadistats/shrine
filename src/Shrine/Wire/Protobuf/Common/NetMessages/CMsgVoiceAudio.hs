{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CMsgVoiceAudio (CMsgVoiceAudio(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetMessages.VoiceDataFormat_t as NetMessages (VoiceDataFormat_t)

data CMsgVoiceAudio = CMsgVoiceAudio{format :: !(P'.Maybe NetMessages.VoiceDataFormat_t), voice_data :: !(P'.Maybe P'.ByteString),
                                     sequence_bytes :: !(P'.Maybe P'.Int32), section_number :: !(P'.Maybe P'.Word32),
                                     sample_rate :: !(P'.Maybe P'.Word32), uncompressed_sample_offset :: !(P'.Maybe P'.Word32)}
                    deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgVoiceAudio where
  mergeAppend (CMsgVoiceAudio x'1 x'2 x'3 x'4 x'5 x'6) (CMsgVoiceAudio y'1 y'2 y'3 y'4 y'5 y'6)
   = CMsgVoiceAudio (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CMsgVoiceAudio where
  defaultValue
   = CMsgVoiceAudio (Prelude'.Just (Prelude'.read "VOICEDATA_FORMAT_STEAM")) P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CMsgVoiceAudio where
  wireSize ft' self'@(CMsgVoiceAudio x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 14 x'1 + P'.wireSizeOpt 1 12 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeOpt 1 13 x'4 +
             P'.wireSizeOpt 1 13 x'5
             + P'.wireSizeOpt 1 13 x'6)
  wirePut ft' self'@(CMsgVoiceAudio x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 14 x'1
             P'.wirePutOpt 18 12 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutOpt 32 13 x'4
             P'.wirePutOpt 40 13 x'5
             P'.wirePutOpt 48 13 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{format = Prelude'.Just new'Field}) (P'.wireGet 14)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{voice_data = Prelude'.Just new'Field}) (P'.wireGet 12)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{sequence_bytes = Prelude'.Just new'Field}) (P'.wireGet 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{section_number = Prelude'.Just new'Field}) (P'.wireGet 13)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{sample_rate = Prelude'.Just new'Field}) (P'.wireGet 13)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{uncompressed_sample_offset = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgVoiceAudio) CMsgVoiceAudio where
  getVal m' f' = f' m'

instance P'.GPB CMsgVoiceAudio

instance P'.ReflectDescriptor CMsgVoiceAudio where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18, 24, 32, 40, 48])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CMsgVoiceAudio\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CMsgVoiceAudio\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CMsgVoiceAudio.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CMsgVoiceAudio.format\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CMsgVoiceAudio\"], baseName' = FName \"format\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Netmessages.VoiceDataFormat_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"VoiceDataFormat_t\"}), hsRawDefault = Just \"VOICEDATA_FORMAT_STEAM\", hsDefault = Just (HsDef'Enum \"VOICEDATA_FORMAT_STEAM\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CMsgVoiceAudio.voice_data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CMsgVoiceAudio\"], baseName' = FName \"voice_data\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CMsgVoiceAudio.sequence_bytes\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CMsgVoiceAudio\"], baseName' = FName \"sequence_bytes\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CMsgVoiceAudio.section_number\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CMsgVoiceAudio\"], baseName' = FName \"section_number\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CMsgVoiceAudio.sample_rate\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CMsgVoiceAudio\"], baseName' = FName \"sample_rate\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CMsgVoiceAudio.uncompressed_sample_offset\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CMsgVoiceAudio\"], baseName' = FName \"uncompressed_sample_offset\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgVoiceAudio where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgVoiceAudio where
  textPut msg
   = do
       P'.tellT "format" (format msg)
       P'.tellT "voice_data" (voice_data msg)
       P'.tellT "sequence_bytes" (sequence_bytes msg)
       P'.tellT "section_number" (section_number msg)
       P'.tellT "sample_rate" (sample_rate msg)
       P'.tellT "uncompressed_sample_offset" (uncompressed_sample_offset msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'format, parse'voice_data, parse'sequence_bytes, parse'section_number, parse'sample_rate,
                   parse'uncompressed_sample_offset])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'format
         = P'.try
            (do
               v <- P'.getT "format"
               Prelude'.return (\ o -> o{format = v}))
        parse'voice_data
         = P'.try
            (do
               v <- P'.getT "voice_data"
               Prelude'.return (\ o -> o{voice_data = v}))
        parse'sequence_bytes
         = P'.try
            (do
               v <- P'.getT "sequence_bytes"
               Prelude'.return (\ o -> o{sequence_bytes = v}))
        parse'section_number
         = P'.try
            (do
               v <- P'.getT "section_number"
               Prelude'.return (\ o -> o{section_number = v}))
        parse'sample_rate
         = P'.try
            (do
               v <- P'.getT "sample_rate"
               Prelude'.return (\ o -> o{sample_rate = v}))
        parse'uncompressed_sample_offset
         = P'.try
            (do
               v <- P'.getT "uncompressed_sample_offset"
               Prelude'.return (\ o -> o{uncompressed_sample_offset = v}))