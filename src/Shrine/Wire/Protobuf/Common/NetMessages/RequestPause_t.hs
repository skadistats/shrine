{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.RequestPause_t (RequestPause_t(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data RequestPause_t = RP_PAUSE
                    | RP_UNPAUSE
                    | RP_TOGGLEPAUSE
                    deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                              Prelude'.Generic)

instance P'.Mergeable RequestPause_t

instance Prelude'.Bounded RequestPause_t where
  minBound = RP_PAUSE
  maxBound = RP_TOGGLEPAUSE

instance P'.Default RequestPause_t where
  defaultValue = RP_PAUSE

toMaybe'Enum :: Prelude'.Int -> P'.Maybe RequestPause_t
toMaybe'Enum 0 = Prelude'.Just RP_PAUSE
toMaybe'Enum 1 = Prelude'.Just RP_UNPAUSE
toMaybe'Enum 2 = Prelude'.Just RP_TOGGLEPAUSE
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum RequestPause_t where
  fromEnum RP_PAUSE = 0
  fromEnum RP_UNPAUSE = 1
  fromEnum RP_TOGGLEPAUSE = 2
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.NetMessages.RequestPause_t")
      . toMaybe'Enum
  succ RP_PAUSE = RP_UNPAUSE
  succ RP_UNPAUSE = RP_TOGGLEPAUSE
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.NetMessages.RequestPause_t"
  pred RP_UNPAUSE = RP_PAUSE
  pred RP_TOGGLEPAUSE = RP_UNPAUSE
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.NetMessages.RequestPause_t"

instance P'.Wire RequestPause_t where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB RequestPause_t

instance P'.MessageAPI msg' (msg' -> RequestPause_t) RequestPause_t where
  getVal m' f' = f' m'

instance P'.ReflectEnum RequestPause_t where
  reflectEnum = [(0, "RP_PAUSE", RP_PAUSE), (1, "RP_UNPAUSE", RP_UNPAUSE), (2, "RP_TOGGLEPAUSE", RP_TOGGLEPAUSE)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Netmessages.RequestPause_t") ["Shrine", "Wire", "Protobuf", "Common"] ["NetMessages"] "RequestPause_t")
      ["Shrine", "Wire", "Protobuf", "Common", "NetMessages", "RequestPause_t.hs"]
      [(0, "RP_PAUSE"), (1, "RP_UNPAUSE"), (2, "RP_TOGGLEPAUSE")]

instance P'.TextType RequestPause_t where
  tellT = P'.tellShow
  getT = P'.getRead