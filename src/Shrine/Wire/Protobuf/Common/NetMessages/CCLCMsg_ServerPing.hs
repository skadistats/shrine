{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CCLCMsg_ServerPing (CCLCMsg_ServerPing(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CCLCMsg_ServerPing = CCLCMsg_ServerPing{}
                        deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CCLCMsg_ServerPing where
  mergeAppend CCLCMsg_ServerPing CCLCMsg_ServerPing = CCLCMsg_ServerPing

instance P'.Default CCLCMsg_ServerPing where
  defaultValue = CCLCMsg_ServerPing

instance P'.Wire CCLCMsg_ServerPing where
  wireSize ft' self'@(CCLCMsg_ServerPing)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = 0
  wirePut ft' self'@(CCLCMsg_ServerPing)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             Prelude'.return ()
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CCLCMsg_ServerPing) CCLCMsg_ServerPing where
  getVal m' f' = f' m'

instance P'.GPB CCLCMsg_ServerPing

instance P'.ReflectDescriptor CCLCMsg_ServerPing where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CCLCMsg_ServerPing\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CCLCMsg_ServerPing\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CCLCMsg_ServerPing.hs\"], isGroup = False, fields = fromList [], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CCLCMsg_ServerPing where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CCLCMsg_ServerPing where
  textPut msg = Prelude'.return ()
  textGet = Prelude'.return P'.defaultValue
    where