{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_Sounds.Sounddata_t (Sounddata_t(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data Sounddata_t = Sounddata_t{origin_x :: !(P'.Maybe P'.Int32), origin_y :: !(P'.Maybe P'.Int32), origin_z :: !(P'.Maybe P'.Int32),
                               volume :: !(P'.Maybe P'.Word32), delay_value :: !(P'.Maybe P'.Float),
                               sequence_number :: !(P'.Maybe P'.Int32), entity_index :: !(P'.Maybe P'.Int32),
                               channel :: !(P'.Maybe P'.Int32), pitch :: !(P'.Maybe P'.Int32), flags :: !(P'.Maybe P'.Int32),
                               sound_num :: !(P'.Maybe P'.Word32), sound_num_handle :: !(P'.Maybe P'.Word32),
                               speaker_entity :: !(P'.Maybe P'.Int32), random_seed :: !(P'.Maybe P'.Int32),
                               sound_level :: !(P'.Maybe P'.Int32), is_sentence :: !(P'.Maybe P'.Bool),
                               is_ambient :: !(P'.Maybe P'.Bool), guid :: !(P'.Maybe P'.Word32),
                               sound_resource_id :: !(P'.Maybe P'.Word64)}
                 deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Sounddata_t where
  mergeAppend (Sounddata_t x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19)
   (Sounddata_t y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18 y'19)
   = Sounddata_t (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)
      (P'.mergeAppend x'19 y'19)

instance P'.Default Sounddata_t where
  defaultValue
   = Sounddata_t P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire Sounddata_t where
  wireSize ft' self'@(Sounddata_t x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 17 x'1 + P'.wireSizeOpt 1 17 x'2 + P'.wireSizeOpt 1 17 x'3 + P'.wireSizeOpt 1 13 x'4 +
             P'.wireSizeOpt 1 2 x'5
             + P'.wireSizeOpt 1 5 x'6
             + P'.wireSizeOpt 1 5 x'7
             + P'.wireSizeOpt 1 5 x'8
             + P'.wireSizeOpt 1 5 x'9
             + P'.wireSizeOpt 1 5 x'10
             + P'.wireSizeOpt 1 13 x'11
             + P'.wireSizeOpt 1 7 x'12
             + P'.wireSizeOpt 1 5 x'13
             + P'.wireSizeOpt 1 5 x'14
             + P'.wireSizeOpt 1 5 x'15
             + P'.wireSizeOpt 2 8 x'16
             + P'.wireSizeOpt 2 8 x'17
             + P'.wireSizeOpt 2 13 x'18
             + P'.wireSizeOpt 2 6 x'19)
  wirePut ft' self'@(Sounddata_t x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 17 x'1
             P'.wirePutOpt 16 17 x'2
             P'.wirePutOpt 24 17 x'3
             P'.wirePutOpt 32 13 x'4
             P'.wirePutOpt 45 2 x'5
             P'.wirePutOpt 48 5 x'6
             P'.wirePutOpt 56 5 x'7
             P'.wirePutOpt 64 5 x'8
             P'.wirePutOpt 72 5 x'9
             P'.wirePutOpt 80 5 x'10
             P'.wirePutOpt 88 13 x'11
             P'.wirePutOpt 101 7 x'12
             P'.wirePutOpt 104 5 x'13
             P'.wirePutOpt 112 5 x'14
             P'.wirePutOpt 120 5 x'15
             P'.wirePutOpt 128 8 x'16
             P'.wirePutOpt 136 8 x'17
             P'.wirePutOpt 144 13 x'18
             P'.wirePutOpt 153 6 x'19
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{origin_x = Prelude'.Just new'Field}) (P'.wireGet 17)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{origin_y = Prelude'.Just new'Field}) (P'.wireGet 17)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{origin_z = Prelude'.Just new'Field}) (P'.wireGet 17)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{volume = Prelude'.Just new'Field}) (P'.wireGet 13)
             45 -> Prelude'.fmap (\ !new'Field -> old'Self{delay_value = Prelude'.Just new'Field}) (P'.wireGet 2)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{sequence_number = Prelude'.Just new'Field}) (P'.wireGet 5)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{entity_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{channel = Prelude'.Just new'Field}) (P'.wireGet 5)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{pitch = Prelude'.Just new'Field}) (P'.wireGet 5)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{flags = Prelude'.Just new'Field}) (P'.wireGet 5)
             88 -> Prelude'.fmap (\ !new'Field -> old'Self{sound_num = Prelude'.Just new'Field}) (P'.wireGet 13)
             101 -> Prelude'.fmap (\ !new'Field -> old'Self{sound_num_handle = Prelude'.Just new'Field}) (P'.wireGet 7)
             104 -> Prelude'.fmap (\ !new'Field -> old'Self{speaker_entity = Prelude'.Just new'Field}) (P'.wireGet 5)
             112 -> Prelude'.fmap (\ !new'Field -> old'Self{random_seed = Prelude'.Just new'Field}) (P'.wireGet 5)
             120 -> Prelude'.fmap (\ !new'Field -> old'Self{sound_level = Prelude'.Just new'Field}) (P'.wireGet 5)
             128 -> Prelude'.fmap (\ !new'Field -> old'Self{is_sentence = Prelude'.Just new'Field}) (P'.wireGet 8)
             136 -> Prelude'.fmap (\ !new'Field -> old'Self{is_ambient = Prelude'.Just new'Field}) (P'.wireGet 8)
             144 -> Prelude'.fmap (\ !new'Field -> old'Self{guid = Prelude'.Just new'Field}) (P'.wireGet 13)
             153 -> Prelude'.fmap (\ !new'Field -> old'Self{sound_resource_id = Prelude'.Just new'Field}) (P'.wireGet 6)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Sounddata_t) Sounddata_t where
  getVal m' f' = f' m'

instance P'.GPB Sounddata_t

instance P'.ReflectDescriptor Sounddata_t where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList [8, 16, 24, 32, 45, 48, 56, 64, 72, 80, 88, 101, 104, 112, 120, 128, 136, 144, 153])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\"], baseName = MName \"Sounddata_t\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CSVCMsg_Sounds\",\"Sounddata_t.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.origin_x\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"origin_x\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.origin_y\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"origin_y\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.origin_z\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"origin_z\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.volume\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"volume\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.delay_value\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"delay_value\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 45}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.sequence_number\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"sequence_number\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.entity_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"entity_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.channel\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"channel\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.pitch\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"pitch\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.flags\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"flags\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.sound_num\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"sound_num\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 88}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.sound_num_handle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"sound_num_handle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 101}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.speaker_entity\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"speaker_entity\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 104}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.random_seed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"random_seed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 112}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.sound_level\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"sound_level\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 120}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.is_sentence\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"is_sentence\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 128}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.is_ambient\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"is_ambient\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 136}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.guid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"guid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 144}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_Sounds.sounddata_t.sound_resource_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_Sounds\",MName \"Sounddata_t\"], baseName' = FName \"sound_resource_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 19}, wireTag = WireTag {getWireTag = 153}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Sounddata_t where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Sounddata_t where
  textPut msg
   = do
       P'.tellT "origin_x" (origin_x msg)
       P'.tellT "origin_y" (origin_y msg)
       P'.tellT "origin_z" (origin_z msg)
       P'.tellT "volume" (volume msg)
       P'.tellT "delay_value" (delay_value msg)
       P'.tellT "sequence_number" (sequence_number msg)
       P'.tellT "entity_index" (entity_index msg)
       P'.tellT "channel" (channel msg)
       P'.tellT "pitch" (pitch msg)
       P'.tellT "flags" (flags msg)
       P'.tellT "sound_num" (sound_num msg)
       P'.tellT "sound_num_handle" (sound_num_handle msg)
       P'.tellT "speaker_entity" (speaker_entity msg)
       P'.tellT "random_seed" (random_seed msg)
       P'.tellT "sound_level" (sound_level msg)
       P'.tellT "is_sentence" (is_sentence msg)
       P'.tellT "is_ambient" (is_ambient msg)
       P'.tellT "guid" (guid msg)
       P'.tellT "sound_resource_id" (sound_resource_id msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'origin_x, parse'origin_y, parse'origin_z, parse'volume, parse'delay_value, parse'sequence_number,
                   parse'entity_index, parse'channel, parse'pitch, parse'flags, parse'sound_num, parse'sound_num_handle,
                   parse'speaker_entity, parse'random_seed, parse'sound_level, parse'is_sentence, parse'is_ambient, parse'guid,
                   parse'sound_resource_id])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'origin_x
         = P'.try
            (do
               v <- P'.getT "origin_x"
               Prelude'.return (\ o -> o{origin_x = v}))
        parse'origin_y
         = P'.try
            (do
               v <- P'.getT "origin_y"
               Prelude'.return (\ o -> o{origin_y = v}))
        parse'origin_z
         = P'.try
            (do
               v <- P'.getT "origin_z"
               Prelude'.return (\ o -> o{origin_z = v}))
        parse'volume
         = P'.try
            (do
               v <- P'.getT "volume"
               Prelude'.return (\ o -> o{volume = v}))
        parse'delay_value
         = P'.try
            (do
               v <- P'.getT "delay_value"
               Prelude'.return (\ o -> o{delay_value = v}))
        parse'sequence_number
         = P'.try
            (do
               v <- P'.getT "sequence_number"
               Prelude'.return (\ o -> o{sequence_number = v}))
        parse'entity_index
         = P'.try
            (do
               v <- P'.getT "entity_index"
               Prelude'.return (\ o -> o{entity_index = v}))
        parse'channel
         = P'.try
            (do
               v <- P'.getT "channel"
               Prelude'.return (\ o -> o{channel = v}))
        parse'pitch
         = P'.try
            (do
               v <- P'.getT "pitch"
               Prelude'.return (\ o -> o{pitch = v}))
        parse'flags
         = P'.try
            (do
               v <- P'.getT "flags"
               Prelude'.return (\ o -> o{flags = v}))
        parse'sound_num
         = P'.try
            (do
               v <- P'.getT "sound_num"
               Prelude'.return (\ o -> o{sound_num = v}))
        parse'sound_num_handle
         = P'.try
            (do
               v <- P'.getT "sound_num_handle"
               Prelude'.return (\ o -> o{sound_num_handle = v}))
        parse'speaker_entity
         = P'.try
            (do
               v <- P'.getT "speaker_entity"
               Prelude'.return (\ o -> o{speaker_entity = v}))
        parse'random_seed
         = P'.try
            (do
               v <- P'.getT "random_seed"
               Prelude'.return (\ o -> o{random_seed = v}))
        parse'sound_level
         = P'.try
            (do
               v <- P'.getT "sound_level"
               Prelude'.return (\ o -> o{sound_level = v}))
        parse'is_sentence
         = P'.try
            (do
               v <- P'.getT "is_sentence"
               Prelude'.return (\ o -> o{is_sentence = v}))
        parse'is_ambient
         = P'.try
            (do
               v <- P'.getT "is_ambient"
               Prelude'.return (\ o -> o{is_ambient = v}))
        parse'guid
         = P'.try
            (do
               v <- P'.getT "guid"
               Prelude'.return (\ o -> o{guid = v}))
        parse'sound_resource_id
         = P'.try
            (do
               v <- P'.getT "sound_resource_id"
               Prelude'.return (\ o -> o{sound_resource_id = v}))