{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_ServerInfo (CSVCMsg_ServerInfo(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CSVCMsg_GameSessionConfiguration as NetworkBaseTypes
       (CSVCMsg_GameSessionConfiguration)

data CSVCMsg_ServerInfo = CSVCMsg_ServerInfo{protocol :: !(P'.Maybe P'.Int32), server_count :: !(P'.Maybe P'.Int32),
                                             is_dedicated :: !(P'.Maybe P'.Bool), is_hltv :: !(P'.Maybe P'.Bool),
                                             is_replay :: !(P'.Maybe P'.Bool), c_os :: !(P'.Maybe P'.Int32),
                                             map_crc :: !(P'.Maybe P'.Word32), client_crc :: !(P'.Maybe P'.Word32),
                                             string_table_crc :: !(P'.Maybe P'.Word32), max_clients :: !(P'.Maybe P'.Int32),
                                             max_classes :: !(P'.Maybe P'.Int32), player_slot :: !(P'.Maybe P'.Int32),
                                             tick_interval :: !(P'.Maybe P'.Float), game_dir :: !(P'.Maybe P'.Utf8),
                                             map_name :: !(P'.Maybe P'.Utf8), sky_name :: !(P'.Maybe P'.Utf8),
                                             host_name :: !(P'.Maybe P'.Utf8), addon_name :: !(P'.Maybe P'.Utf8),
                                             game_session_config :: !(P'.Maybe NetworkBaseTypes.CSVCMsg_GameSessionConfiguration),
                                             game_session_manifest :: !(P'.Maybe P'.ByteString)}
                        deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CSVCMsg_ServerInfo where
  mergeAppend (CSVCMsg_ServerInfo x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20)
   (CSVCMsg_ServerInfo y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18 y'19 y'20)
   = CSVCMsg_ServerInfo (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)
      (P'.mergeAppend x'19 y'19)
      (P'.mergeAppend x'20 y'20)

instance P'.Default CSVCMsg_ServerInfo where
  defaultValue
   = CSVCMsg_ServerInfo P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CSVCMsg_ServerInfo where
  wireSize ft' self'@(CSVCMsg_ServerInfo x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 8 x'3 + P'.wireSizeOpt 1 8 x'4 +
             P'.wireSizeOpt 1 8 x'5
             + P'.wireSizeOpt 1 5 x'6
             + P'.wireSizeOpt 1 7 x'7
             + P'.wireSizeOpt 1 7 x'8
             + P'.wireSizeOpt 1 7 x'9
             + P'.wireSizeOpt 1 5 x'10
             + P'.wireSizeOpt 1 5 x'11
             + P'.wireSizeOpt 1 5 x'12
             + P'.wireSizeOpt 1 2 x'13
             + P'.wireSizeOpt 1 9 x'14
             + P'.wireSizeOpt 1 9 x'15
             + P'.wireSizeOpt 2 9 x'16
             + P'.wireSizeOpt 2 9 x'17
             + P'.wireSizeOpt 2 9 x'18
             + P'.wireSizeOpt 2 11 x'19
             + P'.wireSizeOpt 2 12 x'20)
  wirePut ft' self'@(CSVCMsg_ServerInfo x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 8 x'3
             P'.wirePutOpt 32 8 x'4
             P'.wirePutOpt 40 8 x'5
             P'.wirePutOpt 48 5 x'6
             P'.wirePutOpt 61 7 x'7
             P'.wirePutOpt 69 7 x'8
             P'.wirePutOpt 77 7 x'9
             P'.wirePutOpt 80 5 x'10
             P'.wirePutOpt 88 5 x'11
             P'.wirePutOpt 96 5 x'12
             P'.wirePutOpt 109 2 x'13
             P'.wirePutOpt 114 9 x'14
             P'.wirePutOpt 122 9 x'15
             P'.wirePutOpt 130 9 x'16
             P'.wirePutOpt 138 9 x'17
             P'.wirePutOpt 146 9 x'18
             P'.wirePutOpt 154 11 x'19
             P'.wirePutOpt 162 12 x'20
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{protocol = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{server_count = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{is_dedicated = Prelude'.Just new'Field}) (P'.wireGet 8)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{is_hltv = Prelude'.Just new'Field}) (P'.wireGet 8)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{is_replay = Prelude'.Just new'Field}) (P'.wireGet 8)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{c_os = Prelude'.Just new'Field}) (P'.wireGet 5)
             61 -> Prelude'.fmap (\ !new'Field -> old'Self{map_crc = Prelude'.Just new'Field}) (P'.wireGet 7)
             69 -> Prelude'.fmap (\ !new'Field -> old'Self{client_crc = Prelude'.Just new'Field}) (P'.wireGet 7)
             77 -> Prelude'.fmap (\ !new'Field -> old'Self{string_table_crc = Prelude'.Just new'Field}) (P'.wireGet 7)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{max_clients = Prelude'.Just new'Field}) (P'.wireGet 5)
             88 -> Prelude'.fmap (\ !new'Field -> old'Self{max_classes = Prelude'.Just new'Field}) (P'.wireGet 5)
             96 -> Prelude'.fmap (\ !new'Field -> old'Self{player_slot = Prelude'.Just new'Field}) (P'.wireGet 5)
             109 -> Prelude'.fmap (\ !new'Field -> old'Self{tick_interval = Prelude'.Just new'Field}) (P'.wireGet 2)
             114 -> Prelude'.fmap (\ !new'Field -> old'Self{game_dir = Prelude'.Just new'Field}) (P'.wireGet 9)
             122 -> Prelude'.fmap (\ !new'Field -> old'Self{map_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             130 -> Prelude'.fmap (\ !new'Field -> old'Self{sky_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             138 -> Prelude'.fmap (\ !new'Field -> old'Self{host_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             146 -> Prelude'.fmap (\ !new'Field -> old'Self{addon_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             154 -> Prelude'.fmap
                     (\ !new'Field ->
                       old'Self{game_session_config = P'.mergeAppend (game_session_config old'Self) (Prelude'.Just new'Field)})
                     (P'.wireGet 11)
             162 -> Prelude'.fmap (\ !new'Field -> old'Self{game_session_manifest = Prelude'.Just new'Field}) (P'.wireGet 12)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_ServerInfo) CSVCMsg_ServerInfo where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_ServerInfo

instance P'.ReflectDescriptor CSVCMsg_ServerInfo where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList [8, 16, 24, 32, 40, 48, 61, 69, 77, 80, 88, 96, 109, 114, 122, 130, 138, 146, 154, 162])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CSVCMsg_ServerInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CSVCMsg_ServerInfo\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CSVCMsg_ServerInfo.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.protocol\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"protocol\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.server_count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"server_count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.is_dedicated\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"is_dedicated\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.is_hltv\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"is_hltv\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.is_replay\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"is_replay\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.c_os\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"c_os\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.map_crc\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"map_crc\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 61}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.client_crc\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"client_crc\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 69}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.string_table_crc\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"string_table_crc\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 77}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.max_clients\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"max_clients\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.max_classes\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"max_classes\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 88}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.player_slot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"player_slot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 96}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.tick_interval\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"tick_interval\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 109}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.game_dir\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"game_dir\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 114}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.map_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"map_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 122}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.sky_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"sky_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 130}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.host_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"host_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 138}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.addon_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"addon_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 146}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.game_session_config\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"game_session_config\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 19}, wireTag = WireTag {getWireTag = 154}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CSVCMsg_GameSessionConfiguration\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CSVCMsg_GameSessionConfiguration\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ServerInfo.game_session_manifest\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ServerInfo\"], baseName' = FName \"game_session_manifest\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 20}, wireTag = WireTag {getWireTag = 162}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_ServerInfo where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_ServerInfo where
  textPut msg
   = do
       P'.tellT "protocol" (protocol msg)
       P'.tellT "server_count" (server_count msg)
       P'.tellT "is_dedicated" (is_dedicated msg)
       P'.tellT "is_hltv" (is_hltv msg)
       P'.tellT "is_replay" (is_replay msg)
       P'.tellT "c_os" (c_os msg)
       P'.tellT "map_crc" (map_crc msg)
       P'.tellT "client_crc" (client_crc msg)
       P'.tellT "string_table_crc" (string_table_crc msg)
       P'.tellT "max_clients" (max_clients msg)
       P'.tellT "max_classes" (max_classes msg)
       P'.tellT "player_slot" (player_slot msg)
       P'.tellT "tick_interval" (tick_interval msg)
       P'.tellT "game_dir" (game_dir msg)
       P'.tellT "map_name" (map_name msg)
       P'.tellT "sky_name" (sky_name msg)
       P'.tellT "host_name" (host_name msg)
       P'.tellT "addon_name" (addon_name msg)
       P'.tellT "game_session_config" (game_session_config msg)
       P'.tellT "game_session_manifest" (game_session_manifest msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'protocol, parse'server_count, parse'is_dedicated, parse'is_hltv, parse'is_replay, parse'c_os,
                   parse'map_crc, parse'client_crc, parse'string_table_crc, parse'max_clients, parse'max_classes, parse'player_slot,
                   parse'tick_interval, parse'game_dir, parse'map_name, parse'sky_name, parse'host_name, parse'addon_name,
                   parse'game_session_config, parse'game_session_manifest])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'protocol
         = P'.try
            (do
               v <- P'.getT "protocol"
               Prelude'.return (\ o -> o{protocol = v}))
        parse'server_count
         = P'.try
            (do
               v <- P'.getT "server_count"
               Prelude'.return (\ o -> o{server_count = v}))
        parse'is_dedicated
         = P'.try
            (do
               v <- P'.getT "is_dedicated"
               Prelude'.return (\ o -> o{is_dedicated = v}))
        parse'is_hltv
         = P'.try
            (do
               v <- P'.getT "is_hltv"
               Prelude'.return (\ o -> o{is_hltv = v}))
        parse'is_replay
         = P'.try
            (do
               v <- P'.getT "is_replay"
               Prelude'.return (\ o -> o{is_replay = v}))
        parse'c_os
         = P'.try
            (do
               v <- P'.getT "c_os"
               Prelude'.return (\ o -> o{c_os = v}))
        parse'map_crc
         = P'.try
            (do
               v <- P'.getT "map_crc"
               Prelude'.return (\ o -> o{map_crc = v}))
        parse'client_crc
         = P'.try
            (do
               v <- P'.getT "client_crc"
               Prelude'.return (\ o -> o{client_crc = v}))
        parse'string_table_crc
         = P'.try
            (do
               v <- P'.getT "string_table_crc"
               Prelude'.return (\ o -> o{string_table_crc = v}))
        parse'max_clients
         = P'.try
            (do
               v <- P'.getT "max_clients"
               Prelude'.return (\ o -> o{max_clients = v}))
        parse'max_classes
         = P'.try
            (do
               v <- P'.getT "max_classes"
               Prelude'.return (\ o -> o{max_classes = v}))
        parse'player_slot
         = P'.try
            (do
               v <- P'.getT "player_slot"
               Prelude'.return (\ o -> o{player_slot = v}))
        parse'tick_interval
         = P'.try
            (do
               v <- P'.getT "tick_interval"
               Prelude'.return (\ o -> o{tick_interval = v}))
        parse'game_dir
         = P'.try
            (do
               v <- P'.getT "game_dir"
               Prelude'.return (\ o -> o{game_dir = v}))
        parse'map_name
         = P'.try
            (do
               v <- P'.getT "map_name"
               Prelude'.return (\ o -> o{map_name = v}))
        parse'sky_name
         = P'.try
            (do
               v <- P'.getT "sky_name"
               Prelude'.return (\ o -> o{sky_name = v}))
        parse'host_name
         = P'.try
            (do
               v <- P'.getT "host_name"
               Prelude'.return (\ o -> o{host_name = v}))
        parse'addon_name
         = P'.try
            (do
               v <- P'.getT "addon_name"
               Prelude'.return (\ o -> o{addon_name = v}))
        parse'game_session_config
         = P'.try
            (do
               v <- P'.getT "game_session_config"
               Prelude'.return (\ o -> o{game_session_config = v}))
        parse'game_session_manifest
         = P'.try
            (do
               v <- P'.getT "game_session_manifest"
               Prelude'.return (\ o -> o{game_session_manifest = v}))