{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CCLCMsg_SplitPlayerDisconnect (CCLCMsg_SplitPlayerDisconnect(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CCLCMsg_SplitPlayerDisconnect = CCLCMsg_SplitPlayerDisconnect{slot :: !(P'.Maybe P'.Int32)}
                                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                             Prelude'.Generic)

instance P'.Mergeable CCLCMsg_SplitPlayerDisconnect where
  mergeAppend (CCLCMsg_SplitPlayerDisconnect x'1) (CCLCMsg_SplitPlayerDisconnect y'1)
   = CCLCMsg_SplitPlayerDisconnect (P'.mergeAppend x'1 y'1)

instance P'.Default CCLCMsg_SplitPlayerDisconnect where
  defaultValue = CCLCMsg_SplitPlayerDisconnect P'.defaultValue

instance P'.Wire CCLCMsg_SplitPlayerDisconnect where
  wireSize ft' self'@(CCLCMsg_SplitPlayerDisconnect x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1)
  wirePut ft' self'@(CCLCMsg_SplitPlayerDisconnect x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{slot = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CCLCMsg_SplitPlayerDisconnect) CCLCMsg_SplitPlayerDisconnect where
  getVal m' f' = f' m'

instance P'.GPB CCLCMsg_SplitPlayerDisconnect

instance P'.ReflectDescriptor CCLCMsg_SplitPlayerDisconnect where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CCLCMsg_SplitPlayerDisconnect\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CCLCMsg_SplitPlayerDisconnect\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CCLCMsg_SplitPlayerDisconnect.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CCLCMsg_SplitPlayerDisconnect.slot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CCLCMsg_SplitPlayerDisconnect\"], baseName' = FName \"slot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CCLCMsg_SplitPlayerDisconnect where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CCLCMsg_SplitPlayerDisconnect where
  textPut msg
   = do
       P'.tellT "slot" (slot msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'slot]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'slot
         = P'.try
            (do
               v <- P'.getT "slot"
               Prelude'.return (\ o -> o{slot = v}))