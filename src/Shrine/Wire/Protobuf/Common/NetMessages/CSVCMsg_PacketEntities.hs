{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_PacketEntities (CSVCMsg_PacketEntities(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CSVCMsg_PacketEntities = CSVCMsg_PacketEntities{max_entries :: !(P'.Maybe P'.Int32), updated_entries :: !(P'.Maybe P'.Int32),
                                                     is_delta :: !(P'.Maybe P'.Bool), update_baseline :: !(P'.Maybe P'.Bool),
                                                     baseline :: !(P'.Maybe P'.Int32), delta_from :: !(P'.Maybe P'.Int32),
                                                     entity_data :: !(P'.Maybe P'.ByteString),
                                                     pending_full_frame :: !(P'.Maybe P'.Bool),
                                                     active_spawngroup_handle :: !(P'.Maybe P'.Word32),
                                                     max_spawngroup_creationsequence :: !(P'.Maybe P'.Word32)}
                            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CSVCMsg_PacketEntities where
  mergeAppend (CSVCMsg_PacketEntities x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   (CSVCMsg_PacketEntities y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10)
   = CSVCMsg_PacketEntities (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)

instance P'.Default CSVCMsg_PacketEntities where
  defaultValue
   = CSVCMsg_PacketEntities P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CSVCMsg_PacketEntities where
  wireSize ft' self'@(CSVCMsg_PacketEntities x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 8 x'3 + P'.wireSizeOpt 1 8 x'4 +
             P'.wireSizeOpt 1 5 x'5
             + P'.wireSizeOpt 1 5 x'6
             + P'.wireSizeOpt 1 12 x'7
             + P'.wireSizeOpt 1 8 x'8
             + P'.wireSizeOpt 1 13 x'9
             + P'.wireSizeOpt 1 13 x'10)
  wirePut ft' self'@(CSVCMsg_PacketEntities x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 8 x'3
             P'.wirePutOpt 32 8 x'4
             P'.wirePutOpt 40 5 x'5
             P'.wirePutOpt 48 5 x'6
             P'.wirePutOpt 58 12 x'7
             P'.wirePutOpt 64 8 x'8
             P'.wirePutOpt 72 13 x'9
             P'.wirePutOpt 80 13 x'10
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{max_entries = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{updated_entries = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{is_delta = Prelude'.Just new'Field}) (P'.wireGet 8)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{update_baseline = Prelude'.Just new'Field}) (P'.wireGet 8)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{baseline = Prelude'.Just new'Field}) (P'.wireGet 5)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{delta_from = Prelude'.Just new'Field}) (P'.wireGet 5)
             58 -> Prelude'.fmap (\ !new'Field -> old'Self{entity_data = Prelude'.Just new'Field}) (P'.wireGet 12)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{pending_full_frame = Prelude'.Just new'Field}) (P'.wireGet 8)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{active_spawngroup_handle = Prelude'.Just new'Field}) (P'.wireGet 13)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{max_spawngroup_creationsequence = Prelude'.Just new'Field})
                    (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_PacketEntities) CSVCMsg_PacketEntities where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_PacketEntities

instance P'.ReflectDescriptor CSVCMsg_PacketEntities where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 32, 40, 48, 58, 64, 72, 80])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CSVCMsg_PacketEntities\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CSVCMsg_PacketEntities\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CSVCMsg_PacketEntities.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.max_entries\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"max_entries\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.updated_entries\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"updated_entries\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.is_delta\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"is_delta\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.update_baseline\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"update_baseline\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.baseline\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"baseline\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.delta_from\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"delta_from\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.entity_data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"entity_data\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 58}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.pending_full_frame\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"pending_full_frame\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.active_spawngroup_handle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"active_spawngroup_handle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_PacketEntities.max_spawngroup_creationsequence\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_PacketEntities\"], baseName' = FName \"max_spawngroup_creationsequence\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_PacketEntities where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_PacketEntities where
  textPut msg
   = do
       P'.tellT "max_entries" (max_entries msg)
       P'.tellT "updated_entries" (updated_entries msg)
       P'.tellT "is_delta" (is_delta msg)
       P'.tellT "update_baseline" (update_baseline msg)
       P'.tellT "baseline" (baseline msg)
       P'.tellT "delta_from" (delta_from msg)
       P'.tellT "entity_data" (entity_data msg)
       P'.tellT "pending_full_frame" (pending_full_frame msg)
       P'.tellT "active_spawngroup_handle" (active_spawngroup_handle msg)
       P'.tellT "max_spawngroup_creationsequence" (max_spawngroup_creationsequence msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'max_entries, parse'updated_entries, parse'is_delta, parse'update_baseline, parse'baseline,
                   parse'delta_from, parse'entity_data, parse'pending_full_frame, parse'active_spawngroup_handle,
                   parse'max_spawngroup_creationsequence])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'max_entries
         = P'.try
            (do
               v <- P'.getT "max_entries"
               Prelude'.return (\ o -> o{max_entries = v}))
        parse'updated_entries
         = P'.try
            (do
               v <- P'.getT "updated_entries"
               Prelude'.return (\ o -> o{updated_entries = v}))
        parse'is_delta
         = P'.try
            (do
               v <- P'.getT "is_delta"
               Prelude'.return (\ o -> o{is_delta = v}))
        parse'update_baseline
         = P'.try
            (do
               v <- P'.getT "update_baseline"
               Prelude'.return (\ o -> o{update_baseline = v}))
        parse'baseline
         = P'.try
            (do
               v <- P'.getT "baseline"
               Prelude'.return (\ o -> o{baseline = v}))
        parse'delta_from
         = P'.try
            (do
               v <- P'.getT "delta_from"
               Prelude'.return (\ o -> o{delta_from = v}))
        parse'entity_data
         = P'.try
            (do
               v <- P'.getT "entity_data"
               Prelude'.return (\ o -> o{entity_data = v}))
        parse'pending_full_frame
         = P'.try
            (do
               v <- P'.getT "pending_full_frame"
               Prelude'.return (\ o -> o{pending_full_frame = v}))
        parse'active_spawngroup_handle
         = P'.try
            (do
               v <- P'.getT "active_spawngroup_handle"
               Prelude'.return (\ o -> o{active_spawngroup_handle = v}))
        parse'max_spawngroup_creationsequence
         = P'.try
            (do
               v <- P'.getT "max_spawngroup_creationsequence"
               Prelude'.return (\ o -> o{max_spawngroup_creationsequence = v}))