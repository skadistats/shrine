{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_UpdateStringTable (CSVCMsg_UpdateStringTable(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CSVCMsg_UpdateStringTable = CSVCMsg_UpdateStringTable{table_id :: !(P'.Maybe P'.Int32),
                                                           num_changed_entries :: !(P'.Maybe P'.Int32),
                                                           string_data :: !(P'.Maybe P'.ByteString)}
                               deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                         Prelude'.Generic)

instance P'.Mergeable CSVCMsg_UpdateStringTable where
  mergeAppend (CSVCMsg_UpdateStringTable x'1 x'2 x'3) (CSVCMsg_UpdateStringTable y'1 y'2 y'3)
   = CSVCMsg_UpdateStringTable (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default CSVCMsg_UpdateStringTable where
  defaultValue = CSVCMsg_UpdateStringTable P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CSVCMsg_UpdateStringTable where
  wireSize ft' self'@(CSVCMsg_UpdateStringTable x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 12 x'3)
  wirePut ft' self'@(CSVCMsg_UpdateStringTable x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 26 12 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{table_id = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{num_changed_entries = Prelude'.Just new'Field}) (P'.wireGet 5)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{string_data = Prelude'.Just new'Field}) (P'.wireGet 12)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_UpdateStringTable) CSVCMsg_UpdateStringTable where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_UpdateStringTable

instance P'.ReflectDescriptor CSVCMsg_UpdateStringTable where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 26])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CSVCMsg_UpdateStringTable\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CSVCMsg_UpdateStringTable\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CSVCMsg_UpdateStringTable.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_UpdateStringTable.table_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_UpdateStringTable\"], baseName' = FName \"table_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_UpdateStringTable.num_changed_entries\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_UpdateStringTable\"], baseName' = FName \"num_changed_entries\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_UpdateStringTable.string_data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_UpdateStringTable\"], baseName' = FName \"string_data\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_UpdateStringTable where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_UpdateStringTable where
  textPut msg
   = do
       P'.tellT "table_id" (table_id msg)
       P'.tellT "num_changed_entries" (num_changed_entries msg)
       P'.tellT "string_data" (string_data msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'table_id, parse'num_changed_entries, parse'string_data]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'table_id
         = P'.try
            (do
               v <- P'.getT "table_id"
               Prelude'.return (\ o -> o{table_id = v}))
        parse'num_changed_entries
         = P'.try
            (do
               v <- P'.getT "num_changed_entries"
               Prelude'.return (\ o -> o{num_changed_entries = v}))
        parse'string_data
         = P'.try
            (do
               v <- P'.getT "string_data"
               Prelude'.return (\ o -> o{string_data = v}))