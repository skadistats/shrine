{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_ClassInfo (CSVCMsg_ClassInfo(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CSVCMsg_ClassInfo.Class_t as NetMessages.CSVCMsg_ClassInfo (Class_t)

data CSVCMsg_ClassInfo = CSVCMsg_ClassInfo{create_on_client :: !(P'.Maybe P'.Bool),
                                           classes :: !(P'.Seq NetMessages.CSVCMsg_ClassInfo.Class_t)}
                       deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CSVCMsg_ClassInfo where
  mergeAppend (CSVCMsg_ClassInfo x'1 x'2) (CSVCMsg_ClassInfo y'1 y'2)
   = CSVCMsg_ClassInfo (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CSVCMsg_ClassInfo where
  defaultValue = CSVCMsg_ClassInfo P'.defaultValue P'.defaultValue

instance P'.Wire CSVCMsg_ClassInfo where
  wireSize ft' self'@(CSVCMsg_ClassInfo x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 8 x'1 + P'.wireSizeRep 1 11 x'2)
  wirePut ft' self'@(CSVCMsg_ClassInfo x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 8 x'1
             P'.wirePutRep 18 11 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{create_on_client = Prelude'.Just new'Field}) (P'.wireGet 8)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{classes = P'.append (classes old'Self) new'Field}) (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_ClassInfo) CSVCMsg_ClassInfo where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_ClassInfo

instance P'.ReflectDescriptor CSVCMsg_ClassInfo where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Netmessages.CSVCMsg_ClassInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CSVCMsg_ClassInfo\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"NetMessages\",\"CSVCMsg_ClassInfo.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ClassInfo.create_on_client\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ClassInfo\"], baseName' = FName \"create_on_client\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Netmessages.CSVCMsg_ClassInfo.classes\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"NetMessages\",MName \"CSVCMsg_ClassInfo\"], baseName' = FName \"classes\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Netmessages.CSVCMsg_ClassInfo.class_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetMessages\",MName \"CSVCMsg_ClassInfo\"], baseName = MName \"Class_t\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_ClassInfo where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_ClassInfo where
  textPut msg
   = do
       P'.tellT "create_on_client" (create_on_client msg)
       P'.tellT "classes" (classes msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'create_on_client, parse'classes]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'create_on_client
         = P'.try
            (do
               v <- P'.getT "create_on_client"
               Prelude'.return (\ o -> o{create_on_client = v}))
        parse'classes
         = P'.try
            (do
               v <- P'.getT "classes"
               Prelude'.return (\ o -> o{classes = P'.append (classes o) v}))