{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.Demo.CDemoFullPacket (CDemoFullPacket(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoPacket as Demo (CDemoPacket)
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoStringTables as Demo (CDemoStringTables)

data CDemoFullPacket = CDemoFullPacket{string_table :: !(P'.Maybe Demo.CDemoStringTables), packet :: !(P'.Maybe Demo.CDemoPacket)}
                     deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CDemoFullPacket where
  mergeAppend (CDemoFullPacket x'1 x'2) (CDemoFullPacket y'1 y'2)
   = CDemoFullPacket (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CDemoFullPacket where
  defaultValue = CDemoFullPacket P'.defaultValue P'.defaultValue

instance P'.Wire CDemoFullPacket where
  wireSize ft' self'@(CDemoFullPacket x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 11 x'1 + P'.wireSizeOpt 1 11 x'2)
  wirePut ft' self'@(CDemoFullPacket x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 11 x'1
             P'.wirePutOpt 18 11 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{string_table = P'.mergeAppend (string_table old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{packet = P'.mergeAppend (packet old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDemoFullPacket) CDemoFullPacket where
  getVal m' f' = f' m'

instance P'.GPB CDemoFullPacket

instance P'.ReflectDescriptor CDemoFullPacket where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Demo.CDemoFullPacket\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\"], baseName = MName \"CDemoFullPacket\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"Demo\",\"CDemoFullPacket.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoFullPacket.string_table\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoFullPacket\"], baseName' = FName \"string_table\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Demo.CDemoStringTables\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\"], baseName = MName \"CDemoStringTables\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoFullPacket.packet\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoFullPacket\"], baseName' = FName \"packet\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Demo.CDemoPacket\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\"], baseName = MName \"CDemoPacket\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDemoFullPacket where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDemoFullPacket where
  textPut msg
   = do
       P'.tellT "string_table" (string_table msg)
       P'.tellT "packet" (packet msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'string_table, parse'packet]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'string_table
         = P'.try
            (do
               v <- P'.getT "string_table"
               Prelude'.return (\ o -> o{string_table = v}))
        parse'packet
         = P'.try
            (do
               v <- P'.getT "packet"
               Prelude'.return (\ o -> o{packet = v}))