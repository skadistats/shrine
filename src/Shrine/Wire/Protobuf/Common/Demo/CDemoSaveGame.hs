{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.Demo.CDemoSaveGame (CDemoSaveGame(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDemoSaveGame = CDemoSaveGame{data' :: !(P'.Maybe P'.ByteString), steam_id :: !(P'.Maybe P'.Word64),
                                   signature :: !(P'.Maybe P'.Word64), version :: !(P'.Maybe P'.Int32)}
                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CDemoSaveGame where
  mergeAppend (CDemoSaveGame x'1 x'2 x'3 x'4) (CDemoSaveGame y'1 y'2 y'3 y'4)
   = CDemoSaveGame (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)

instance P'.Default CDemoSaveGame where
  defaultValue = CDemoSaveGame P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CDemoSaveGame where
  wireSize ft' self'@(CDemoSaveGame x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 12 x'1 + P'.wireSizeOpt 1 6 x'2 + P'.wireSizeOpt 1 6 x'3 + P'.wireSizeOpt 1 5 x'4)
  wirePut ft' self'@(CDemoSaveGame x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 12 x'1
             P'.wirePutOpt 17 6 x'2
             P'.wirePutOpt 25 6 x'3
             P'.wirePutOpt 32 5 x'4
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{data' = Prelude'.Just new'Field}) (P'.wireGet 12)
             17 -> Prelude'.fmap (\ !new'Field -> old'Self{steam_id = Prelude'.Just new'Field}) (P'.wireGet 6)
             25 -> Prelude'.fmap (\ !new'Field -> old'Self{signature = Prelude'.Just new'Field}) (P'.wireGet 6)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{version = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDemoSaveGame) CDemoSaveGame where
  getVal m' f' = f' m'

instance P'.GPB CDemoSaveGame

instance P'.ReflectDescriptor CDemoSaveGame where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 17, 25, 32])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Demo.CDemoSaveGame\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\"], baseName = MName \"CDemoSaveGame\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"Demo\",\"CDemoSaveGame.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoSaveGame.data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoSaveGame\"], baseName' = FName \"data'\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoSaveGame.steam_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoSaveGame\"], baseName' = FName \"steam_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 17}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoSaveGame.signature\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoSaveGame\"], baseName' = FName \"signature\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 25}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoSaveGame.version\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoSaveGame\"], baseName' = FName \"version\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDemoSaveGame where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDemoSaveGame where
  textPut msg
   = do
       P'.tellT "data" (data' msg)
       P'.tellT "steam_id" (steam_id msg)
       P'.tellT "signature" (signature msg)
       P'.tellT "version" (version msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'data', parse'steam_id, parse'signature, parse'version]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'data'
         = P'.try
            (do
               v <- P'.getT "data"
               Prelude'.return (\ o -> o{data' = v}))
        parse'steam_id
         = P'.try
            (do
               v <- P'.getT "steam_id"
               Prelude'.return (\ o -> o{steam_id = v}))
        parse'signature
         = P'.try
            (do
               v <- P'.getT "signature"
               Prelude'.return (\ o -> o{signature = v}))
        parse'version
         = P'.try
            (do
               v <- P'.getT "version"
               Prelude'.return (\ o -> o{version = v}))