{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.Demo.CDemoStringTables.Table_t (Table_t(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.Demo.CDemoStringTables.Items_t as Demo.CDemoStringTables (Items_t)

data Table_t = Table_t{table_name :: !(P'.Maybe P'.Utf8), items :: !(P'.Seq Demo.CDemoStringTables.Items_t),
                       items_clientside :: !(P'.Seq Demo.CDemoStringTables.Items_t), table_flags :: !(P'.Maybe P'.Int32)}
             deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Table_t where
  mergeAppend (Table_t x'1 x'2 x'3 x'4) (Table_t y'1 y'2 y'3 y'4)
   = Table_t (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)

instance P'.Default Table_t where
  defaultValue = Table_t P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire Table_t where
  wireSize ft' self'@(Table_t x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 9 x'1 + P'.wireSizeRep 1 11 x'2 + P'.wireSizeRep 1 11 x'3 + P'.wireSizeOpt 1 5 x'4)
  wirePut ft' self'@(Table_t x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
             P'.wirePutRep 18 11 x'2
             P'.wirePutRep 26 11 x'3
             P'.wirePutOpt 32 5 x'4
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{table_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{items = P'.append (items old'Self) new'Field}) (P'.wireGet 11)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{items_clientside = P'.append (items_clientside old'Self) new'Field})
                    (P'.wireGet 11)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{table_flags = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Table_t) Table_t where
  getVal m' f' = f' m'

instance P'.GPB Table_t

instance P'.ReflectDescriptor Table_t where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18, 26, 32])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Demo.CDemoStringTables.table_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\",MName \"CDemoStringTables\"], baseName = MName \"Table_t\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"Demo\",\"CDemoStringTables\",\"Table_t.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoStringTables.table_t.table_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoStringTables\",MName \"Table_t\"], baseName' = FName \"table_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoStringTables.table_t.items\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoStringTables\",MName \"Table_t\"], baseName' = FName \"items\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Demo.CDemoStringTables.items_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\",MName \"CDemoStringTables\"], baseName = MName \"Items_t\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoStringTables.table_t.items_clientside\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoStringTables\",MName \"Table_t\"], baseName' = FName \"items_clientside\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Demo.CDemoStringTables.items_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\",MName \"CDemoStringTables\"], baseName = MName \"Items_t\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoStringTables.table_t.table_flags\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoStringTables\",MName \"Table_t\"], baseName' = FName \"table_flags\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Table_t where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Table_t where
  textPut msg
   = do
       P'.tellT "table_name" (table_name msg)
       P'.tellT "items" (items msg)
       P'.tellT "items_clientside" (items_clientside msg)
       P'.tellT "table_flags" (table_flags msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'table_name, parse'items, parse'items_clientside, parse'table_flags]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'table_name
         = P'.try
            (do
               v <- P'.getT "table_name"
               Prelude'.return (\ o -> o{table_name = v}))
        parse'items
         = P'.try
            (do
               v <- P'.getT "items"
               Prelude'.return (\ o -> o{items = P'.append (items o) v}))
        parse'items_clientside
         = P'.try
            (do
               v <- P'.getT "items_clientside"
               Prelude'.return (\ o -> o{items_clientside = P'.append (items_clientside o) v}))
        parse'table_flags
         = P'.try
            (do
               v <- P'.getT "table_flags"
               Prelude'.return (\ o -> o{table_flags = v}))