{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.Demo.EDemoCommands (EDemoCommands(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EDemoCommands = DEM_Error
                   | DEM_Stop
                   | DEM_FileHeader
                   | DEM_FileInfo
                   | DEM_SyncTick
                   | DEM_SendTables
                   | DEM_ClassInfo
                   | DEM_StringTables
                   | DEM_Packet
                   | DEM_SignonPacket
                   | DEM_ConsoleCmd
                   | DEM_CustomData
                   | DEM_CustomDataCallbacks
                   | DEM_UserCmd
                   | DEM_FullPacket
                   | DEM_SaveGame
                   | DEM_SpawnGroups
                   | DEM_Max
                   | DEM_IsCompressed_S1
                   | DEM_IsCompressed_S2
                   deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                             Prelude'.Generic)

instance P'.Mergeable EDemoCommands

instance Prelude'.Bounded EDemoCommands where
  minBound = DEM_Error
  maxBound = DEM_IsCompressed_S2

instance P'.Default EDemoCommands where
  defaultValue = DEM_Error

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EDemoCommands
toMaybe'Enum (-1) = Prelude'.Just DEM_Error
toMaybe'Enum 0 = Prelude'.Just DEM_Stop
toMaybe'Enum 1 = Prelude'.Just DEM_FileHeader
toMaybe'Enum 2 = Prelude'.Just DEM_FileInfo
toMaybe'Enum 3 = Prelude'.Just DEM_SyncTick
toMaybe'Enum 4 = Prelude'.Just DEM_SendTables
toMaybe'Enum 5 = Prelude'.Just DEM_ClassInfo
toMaybe'Enum 6 = Prelude'.Just DEM_StringTables
toMaybe'Enum 7 = Prelude'.Just DEM_Packet
toMaybe'Enum 8 = Prelude'.Just DEM_SignonPacket
toMaybe'Enum 9 = Prelude'.Just DEM_ConsoleCmd
toMaybe'Enum 10 = Prelude'.Just DEM_CustomData
toMaybe'Enum 11 = Prelude'.Just DEM_CustomDataCallbacks
toMaybe'Enum 12 = Prelude'.Just DEM_UserCmd
toMaybe'Enum 13 = Prelude'.Just DEM_FullPacket
toMaybe'Enum 14 = Prelude'.Just DEM_SaveGame
toMaybe'Enum 15 = Prelude'.Just DEM_SpawnGroups
toMaybe'Enum 16 = Prelude'.Just DEM_Max
toMaybe'Enum 112 = Prelude'.Just DEM_IsCompressed_S1
toMaybe'Enum 64 = Prelude'.Just DEM_IsCompressed_S2
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EDemoCommands where
  fromEnum DEM_Error = (-1)
  fromEnum DEM_Stop = 0
  fromEnum DEM_FileHeader = 1
  fromEnum DEM_FileInfo = 2
  fromEnum DEM_SyncTick = 3
  fromEnum DEM_SendTables = 4
  fromEnum DEM_ClassInfo = 5
  fromEnum DEM_StringTables = 6
  fromEnum DEM_Packet = 7
  fromEnum DEM_SignonPacket = 8
  fromEnum DEM_ConsoleCmd = 9
  fromEnum DEM_CustomData = 10
  fromEnum DEM_CustomDataCallbacks = 11
  fromEnum DEM_UserCmd = 12
  fromEnum DEM_FullPacket = 13
  fromEnum DEM_SaveGame = 14
  fromEnum DEM_SpawnGroups = 15
  fromEnum DEM_Max = 16
  fromEnum DEM_IsCompressed_S1 = 112
  fromEnum DEM_IsCompressed_S2 = 64
  toEnum
   = P'.fromMaybe (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.Demo.EDemoCommands")
      . toMaybe'Enum
  succ DEM_Error = DEM_Stop
  succ DEM_Stop = DEM_FileHeader
  succ DEM_FileHeader = DEM_FileInfo
  succ DEM_FileInfo = DEM_SyncTick
  succ DEM_SyncTick = DEM_SendTables
  succ DEM_SendTables = DEM_ClassInfo
  succ DEM_ClassInfo = DEM_StringTables
  succ DEM_StringTables = DEM_Packet
  succ DEM_Packet = DEM_SignonPacket
  succ DEM_SignonPacket = DEM_ConsoleCmd
  succ DEM_ConsoleCmd = DEM_CustomData
  succ DEM_CustomData = DEM_CustomDataCallbacks
  succ DEM_CustomDataCallbacks = DEM_UserCmd
  succ DEM_UserCmd = DEM_FullPacket
  succ DEM_FullPacket = DEM_SaveGame
  succ DEM_SaveGame = DEM_SpawnGroups
  succ DEM_SpawnGroups = DEM_Max
  succ DEM_Max = DEM_IsCompressed_S1
  succ DEM_IsCompressed_S1 = DEM_IsCompressed_S2
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.Demo.EDemoCommands"
  pred DEM_Stop = DEM_Error
  pred DEM_FileHeader = DEM_Stop
  pred DEM_FileInfo = DEM_FileHeader
  pred DEM_SyncTick = DEM_FileInfo
  pred DEM_SendTables = DEM_SyncTick
  pred DEM_ClassInfo = DEM_SendTables
  pred DEM_StringTables = DEM_ClassInfo
  pred DEM_Packet = DEM_StringTables
  pred DEM_SignonPacket = DEM_Packet
  pred DEM_ConsoleCmd = DEM_SignonPacket
  pred DEM_CustomData = DEM_ConsoleCmd
  pred DEM_CustomDataCallbacks = DEM_CustomData
  pred DEM_UserCmd = DEM_CustomDataCallbacks
  pred DEM_FullPacket = DEM_UserCmd
  pred DEM_SaveGame = DEM_FullPacket
  pred DEM_SpawnGroups = DEM_SaveGame
  pred DEM_Max = DEM_SpawnGroups
  pred DEM_IsCompressed_S1 = DEM_Max
  pred DEM_IsCompressed_S2 = DEM_IsCompressed_S1
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.Demo.EDemoCommands"

instance P'.Wire EDemoCommands where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EDemoCommands

instance P'.MessageAPI msg' (msg' -> EDemoCommands) EDemoCommands where
  getVal m' f' = f' m'

instance P'.ReflectEnum EDemoCommands where
  reflectEnum
   = [((-1), "DEM_Error", DEM_Error), (0, "DEM_Stop", DEM_Stop), (1, "DEM_FileHeader", DEM_FileHeader),
      (2, "DEM_FileInfo", DEM_FileInfo), (3, "DEM_SyncTick", DEM_SyncTick), (4, "DEM_SendTables", DEM_SendTables),
      (5, "DEM_ClassInfo", DEM_ClassInfo), (6, "DEM_StringTables", DEM_StringTables), (7, "DEM_Packet", DEM_Packet),
      (8, "DEM_SignonPacket", DEM_SignonPacket), (9, "DEM_ConsoleCmd", DEM_ConsoleCmd), (10, "DEM_CustomData", DEM_CustomData),
      (11, "DEM_CustomDataCallbacks", DEM_CustomDataCallbacks), (12, "DEM_UserCmd", DEM_UserCmd),
      (13, "DEM_FullPacket", DEM_FullPacket), (14, "DEM_SaveGame", DEM_SaveGame), (15, "DEM_SpawnGroups", DEM_SpawnGroups),
      (16, "DEM_Max", DEM_Max), (112, "DEM_IsCompressed_S1", DEM_IsCompressed_S1), (64, "DEM_IsCompressed_S2", DEM_IsCompressed_S2)]
  reflectEnumInfo _
   = P'.EnumInfo (P'.makePNF (P'.pack ".Demo.EDemoCommands") ["Shrine", "Wire", "Protobuf", "Common"] ["Demo"] "EDemoCommands")
      ["Shrine", "Wire", "Protobuf", "Common", "Demo", "EDemoCommands.hs"]
      [((-1), "DEM_Error"), (0, "DEM_Stop"), (1, "DEM_FileHeader"), (2, "DEM_FileInfo"), (3, "DEM_SyncTick"), (4, "DEM_SendTables"),
       (5, "DEM_ClassInfo"), (6, "DEM_StringTables"), (7, "DEM_Packet"), (8, "DEM_SignonPacket"), (9, "DEM_ConsoleCmd"),
       (10, "DEM_CustomData"), (11, "DEM_CustomDataCallbacks"), (12, "DEM_UserCmd"), (13, "DEM_FullPacket"), (14, "DEM_SaveGame"),
       (15, "DEM_SpawnGroups"), (16, "DEM_Max"), (112, "DEM_IsCompressed_S1"), (64, "DEM_IsCompressed_S2")]

instance P'.TextType EDemoCommands where
  tellT = P'.tellShow
  getT = P'.getRead