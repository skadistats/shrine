{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.Demo.CDemoCustomDataCallbacks (CDemoCustomDataCallbacks(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDemoCustomDataCallbacks = CDemoCustomDataCallbacks{save_id :: !(P'.Seq P'.Utf8)}
                              deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                        Prelude'.Generic)

instance P'.Mergeable CDemoCustomDataCallbacks where
  mergeAppend (CDemoCustomDataCallbacks x'1) (CDemoCustomDataCallbacks y'1) = CDemoCustomDataCallbacks (P'.mergeAppend x'1 y'1)

instance P'.Default CDemoCustomDataCallbacks where
  defaultValue = CDemoCustomDataCallbacks P'.defaultValue

instance P'.Wire CDemoCustomDataCallbacks where
  wireSize ft' self'@(CDemoCustomDataCallbacks x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 9 x'1)
  wirePut ft' self'@(CDemoCustomDataCallbacks x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 10 9 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{save_id = P'.append (save_id old'Self) new'Field}) (P'.wireGet 9)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDemoCustomDataCallbacks) CDemoCustomDataCallbacks where
  getVal m' f' = f' m'

instance P'.GPB CDemoCustomDataCallbacks

instance P'.ReflectDescriptor CDemoCustomDataCallbacks where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Demo.CDemoCustomDataCallbacks\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\"], baseName = MName \"CDemoCustomDataCallbacks\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"Demo\",\"CDemoCustomDataCallbacks.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CDemoCustomDataCallbacks.save_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CDemoCustomDataCallbacks\"], baseName' = FName \"save_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDemoCustomDataCallbacks where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDemoCustomDataCallbacks where
  textPut msg
   = do
       P'.tellT "save_id" (save_id msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'save_id]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'save_id
         = P'.try
            (do
               v <- P'.getT "save_id"
               Prelude'.return (\ o -> o{save_id = P'.append (save_id o) v}))