{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.Demo.CGameInfo.CDotaGameInfo (CDotaGameInfo(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.Demo.CGameInfo.CDotaGameInfo.CHeroSelectEvent as Demo.CGameInfo.CDotaGameInfo
       (CHeroSelectEvent)
import qualified Shrine.Wire.Protobuf.Common.Demo.CGameInfo.CDotaGameInfo.CPlayerInfo as Demo.CGameInfo.CDotaGameInfo (CPlayerInfo)

data CDotaGameInfo = CDotaGameInfo{match_id :: !(P'.Maybe P'.Word64), game_mode :: !(P'.Maybe P'.Int32),
                                   game_winner :: !(P'.Maybe P'.Int32),
                                   player_info :: !(P'.Seq Demo.CGameInfo.CDotaGameInfo.CPlayerInfo),
                                   leagueid :: !(P'.Maybe P'.Word32),
                                   picks_bans :: !(P'.Seq Demo.CGameInfo.CDotaGameInfo.CHeroSelectEvent),
                                   radiant_team_id :: !(P'.Maybe P'.Word32), dire_team_id :: !(P'.Maybe P'.Word32),
                                   radiant_team_tag :: !(P'.Maybe P'.Utf8), dire_team_tag :: !(P'.Maybe P'.Utf8),
                                   end_time :: !(P'.Maybe P'.Word32)}
                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CDotaGameInfo where
  mergeAppend (CDotaGameInfo x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11)
   (CDotaGameInfo y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11)
   = CDotaGameInfo (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)

instance P'.Default CDotaGameInfo where
  defaultValue
   = CDotaGameInfo P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CDotaGameInfo where
  wireSize ft' self'@(CDotaGameInfo x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 4 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeRep 1 11 x'4 +
             P'.wireSizeOpt 1 13 x'5
             + P'.wireSizeRep 1 11 x'6
             + P'.wireSizeOpt 1 13 x'7
             + P'.wireSizeOpt 1 13 x'8
             + P'.wireSizeOpt 1 9 x'9
             + P'.wireSizeOpt 1 9 x'10
             + P'.wireSizeOpt 1 13 x'11)
  wirePut ft' self'@(CDotaGameInfo x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 4 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutRep 34 11 x'4
             P'.wirePutOpt 40 13 x'5
             P'.wirePutRep 50 11 x'6
             P'.wirePutOpt 56 13 x'7
             P'.wirePutOpt 64 13 x'8
             P'.wirePutOpt 74 9 x'9
             P'.wirePutOpt 82 9 x'10
             P'.wirePutOpt 88 13 x'11
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{match_id = Prelude'.Just new'Field}) (P'.wireGet 4)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{game_mode = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{game_winner = Prelude'.Just new'Field}) (P'.wireGet 5)
             34 -> Prelude'.fmap (\ !new'Field -> old'Self{player_info = P'.append (player_info old'Self) new'Field})
                    (P'.wireGet 11)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{leagueid = Prelude'.Just new'Field}) (P'.wireGet 13)
             50 -> Prelude'.fmap (\ !new'Field -> old'Self{picks_bans = P'.append (picks_bans old'Self) new'Field}) (P'.wireGet 11)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{radiant_team_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{dire_team_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             74 -> Prelude'.fmap (\ !new'Field -> old'Self{radiant_team_tag = Prelude'.Just new'Field}) (P'.wireGet 9)
             82 -> Prelude'.fmap (\ !new'Field -> old'Self{dire_team_tag = Prelude'.Just new'Field}) (P'.wireGet 9)
             88 -> Prelude'.fmap (\ !new'Field -> old'Self{end_time = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDotaGameInfo) CDotaGameInfo where
  getVal m' f' = f' m'

instance P'.GPB CDotaGameInfo

instance P'.ReflectDescriptor CDotaGameInfo where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 34, 40, 50, 56, 64, 74, 82, 88])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Demo.CGameInfo.CDotaGameInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\",MName \"CGameInfo\"], baseName = MName \"CDotaGameInfo\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"Demo\",\"CGameInfo\",\"CDotaGameInfo.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.match_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"match_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.game_mode\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"game_mode\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.game_winner\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"game_winner\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.player_info\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"player_info\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 34}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Demo.CGameInfo.CDotaGameInfo.CPlayerInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName = MName \"CPlayerInfo\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.leagueid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"leagueid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.picks_bans\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"picks_bans\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 50}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Demo.CGameInfo.CDotaGameInfo.CHeroSelectEvent\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName = MName \"CHeroSelectEvent\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.radiant_team_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"radiant_team_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.dire_team_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"dire_team_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.radiant_team_tag\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"radiant_team_tag\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 74}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.dire_team_tag\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"dire_team_tag\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 82}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.end_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName' = FName \"end_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 88}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDotaGameInfo where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDotaGameInfo where
  textPut msg
   = do
       P'.tellT "match_id" (match_id msg)
       P'.tellT "game_mode" (game_mode msg)
       P'.tellT "game_winner" (game_winner msg)
       P'.tellT "player_info" (player_info msg)
       P'.tellT "leagueid" (leagueid msg)
       P'.tellT "picks_bans" (picks_bans msg)
       P'.tellT "radiant_team_id" (radiant_team_id msg)
       P'.tellT "dire_team_id" (dire_team_id msg)
       P'.tellT "radiant_team_tag" (radiant_team_tag msg)
       P'.tellT "dire_team_tag" (dire_team_tag msg)
       P'.tellT "end_time" (end_time msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'match_id, parse'game_mode, parse'game_winner, parse'player_info, parse'leagueid, parse'picks_bans,
                   parse'radiant_team_id, parse'dire_team_id, parse'radiant_team_tag, parse'dire_team_tag, parse'end_time])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'match_id
         = P'.try
            (do
               v <- P'.getT "match_id"
               Prelude'.return (\ o -> o{match_id = v}))
        parse'game_mode
         = P'.try
            (do
               v <- P'.getT "game_mode"
               Prelude'.return (\ o -> o{game_mode = v}))
        parse'game_winner
         = P'.try
            (do
               v <- P'.getT "game_winner"
               Prelude'.return (\ o -> o{game_winner = v}))
        parse'player_info
         = P'.try
            (do
               v <- P'.getT "player_info"
               Prelude'.return (\ o -> o{player_info = P'.append (player_info o) v}))
        parse'leagueid
         = P'.try
            (do
               v <- P'.getT "leagueid"
               Prelude'.return (\ o -> o{leagueid = v}))
        parse'picks_bans
         = P'.try
            (do
               v <- P'.getT "picks_bans"
               Prelude'.return (\ o -> o{picks_bans = P'.append (picks_bans o) v}))
        parse'radiant_team_id
         = P'.try
            (do
               v <- P'.getT "radiant_team_id"
               Prelude'.return (\ o -> o{radiant_team_id = v}))
        parse'dire_team_id
         = P'.try
            (do
               v <- P'.getT "dire_team_id"
               Prelude'.return (\ o -> o{dire_team_id = v}))
        parse'radiant_team_tag
         = P'.try
            (do
               v <- P'.getT "radiant_team_tag"
               Prelude'.return (\ o -> o{radiant_team_tag = v}))
        parse'dire_team_tag
         = P'.try
            (do
               v <- P'.getT "dire_team_tag"
               Prelude'.return (\ o -> o{dire_team_tag = v}))
        parse'end_time
         = P'.try
            (do
               v <- P'.getT "end_time"
               Prelude'.return (\ o -> o{end_time = v}))