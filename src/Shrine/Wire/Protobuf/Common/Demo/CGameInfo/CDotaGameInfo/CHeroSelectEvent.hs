{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.Demo.CGameInfo.CDotaGameInfo.CHeroSelectEvent (CHeroSelectEvent(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CHeroSelectEvent = CHeroSelectEvent{is_pick :: !(P'.Maybe P'.Bool), team :: !(P'.Maybe P'.Word32),
                                         hero_id :: !(P'.Maybe P'.Word32)}
                      deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CHeroSelectEvent where
  mergeAppend (CHeroSelectEvent x'1 x'2 x'3) (CHeroSelectEvent y'1 y'2 y'3)
   = CHeroSelectEvent (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default CHeroSelectEvent where
  defaultValue = CHeroSelectEvent P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CHeroSelectEvent where
  wireSize ft' self'@(CHeroSelectEvent x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 8 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 13 x'3)
  wirePut ft' self'@(CHeroSelectEvent x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 8 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 13 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{is_pick = Prelude'.Just new'Field}) (P'.wireGet 8)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{team = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{hero_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CHeroSelectEvent) CHeroSelectEvent where
  getVal m' f' = f' m'

instance P'.GPB CHeroSelectEvent

instance P'.ReflectDescriptor CHeroSelectEvent where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Demo.CGameInfo.CDotaGameInfo.CHeroSelectEvent\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName = MName \"CHeroSelectEvent\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"Demo\",\"CGameInfo\",\"CDotaGameInfo\",\"CHeroSelectEvent.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.CHeroSelectEvent.is_pick\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\",MName \"CHeroSelectEvent\"], baseName' = FName \"is_pick\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.CHeroSelectEvent.team\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\",MName \"CHeroSelectEvent\"], baseName' = FName \"team\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.CHeroSelectEvent.hero_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\",MName \"CHeroSelectEvent\"], baseName' = FName \"hero_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CHeroSelectEvent where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CHeroSelectEvent where
  textPut msg
   = do
       P'.tellT "is_pick" (is_pick msg)
       P'.tellT "team" (team msg)
       P'.tellT "hero_id" (hero_id msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'is_pick, parse'team, parse'hero_id]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'is_pick
         = P'.try
            (do
               v <- P'.getT "is_pick"
               Prelude'.return (\ o -> o{is_pick = v}))
        parse'team
         = P'.try
            (do
               v <- P'.getT "team"
               Prelude'.return (\ o -> o{team = v}))
        parse'hero_id
         = P'.try
            (do
               v <- P'.getT "hero_id"
               Prelude'.return (\ o -> o{hero_id = v}))