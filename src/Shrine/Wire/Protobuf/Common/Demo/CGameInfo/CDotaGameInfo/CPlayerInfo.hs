{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.Demo.CGameInfo.CDotaGameInfo.CPlayerInfo (CPlayerInfo(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CPlayerInfo = CPlayerInfo{hero_name :: !(P'.Maybe P'.Utf8), player_name :: !(P'.Maybe P'.Utf8),
                               is_fake_client :: !(P'.Maybe P'.Bool), steamid :: !(P'.Maybe P'.Word64),
                               game_team :: !(P'.Maybe P'.Int32)}
                 deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CPlayerInfo where
  mergeAppend (CPlayerInfo x'1 x'2 x'3 x'4 x'5) (CPlayerInfo y'1 y'2 y'3 y'4 y'5)
   = CPlayerInfo (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)

instance P'.Default CPlayerInfo where
  defaultValue = CPlayerInfo P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CPlayerInfo where
  wireSize ft' self'@(CPlayerInfo x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 9 x'1 + P'.wireSizeOpt 1 9 x'2 + P'.wireSizeOpt 1 8 x'3 + P'.wireSizeOpt 1 4 x'4 +
             P'.wireSizeOpt 1 5 x'5)
  wirePut ft' self'@(CPlayerInfo x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
             P'.wirePutOpt 18 9 x'2
             P'.wirePutOpt 24 8 x'3
             P'.wirePutOpt 32 4 x'4
             P'.wirePutOpt 40 5 x'5
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{hero_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{player_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{is_fake_client = Prelude'.Just new'Field}) (P'.wireGet 8)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{steamid = Prelude'.Just new'Field}) (P'.wireGet 4)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{game_team = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CPlayerInfo) CPlayerInfo where
  getVal m' f' = f' m'

instance P'.GPB CPlayerInfo

instance P'.ReflectDescriptor CPlayerInfo where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18, 24, 32, 40])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Demo.CGameInfo.CDotaGameInfo.CPlayerInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\"], baseName = MName \"CPlayerInfo\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"Demo\",\"CGameInfo\",\"CDotaGameInfo\",\"CPlayerInfo.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.CPlayerInfo.hero_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\",MName \"CPlayerInfo\"], baseName' = FName \"hero_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.CPlayerInfo.player_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\",MName \"CPlayerInfo\"], baseName' = FName \"player_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.CPlayerInfo.is_fake_client\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\",MName \"CPlayerInfo\"], baseName' = FName \"is_fake_client\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.CPlayerInfo.steamid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\",MName \"CPlayerInfo\"], baseName' = FName \"steamid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Demo.CGameInfo.CDotaGameInfo.CPlayerInfo.game_team\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"Demo\",MName \"CGameInfo\",MName \"CDotaGameInfo\",MName \"CPlayerInfo\"], baseName' = FName \"game_team\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CPlayerInfo where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CPlayerInfo where
  textPut msg
   = do
       P'.tellT "hero_name" (hero_name msg)
       P'.tellT "player_name" (player_name msg)
       P'.tellT "is_fake_client" (is_fake_client msg)
       P'.tellT "steamid" (steamid msg)
       P'.tellT "game_team" (game_team msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'hero_name, parse'player_name, parse'is_fake_client, parse'steamid, parse'game_team])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'hero_name
         = P'.try
            (do
               v <- P'.getT "hero_name"
               Prelude'.return (\ o -> o{hero_name = v}))
        parse'player_name
         = P'.try
            (do
               v <- P'.getT "player_name"
               Prelude'.return (\ o -> o{player_name = v}))
        parse'is_fake_client
         = P'.try
            (do
               v <- P'.getT "is_fake_client"
               Prelude'.return (\ o -> o{is_fake_client = v}))
        parse'steamid
         = P'.try
            (do
               v <- P'.getT "steamid"
               Prelude'.return (\ o -> o{steamid = v}))
        parse'game_team
         = P'.try
            (do
               v <- P'.getT "game_team"
               Prelude'.return (\ o -> o{game_team = v}))