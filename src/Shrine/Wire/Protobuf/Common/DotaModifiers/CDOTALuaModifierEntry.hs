{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaModifiers.CDOTALuaModifierEntry (CDOTALuaModifierEntry(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTALuaModifierEntry = CDOTALuaModifierEntry{modifier_type :: !(P'.Int32), modifier_filename :: !(P'.Utf8)}
                           deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CDOTALuaModifierEntry where
  mergeAppend (CDOTALuaModifierEntry x'1 x'2) (CDOTALuaModifierEntry y'1 y'2)
   = CDOTALuaModifierEntry (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CDOTALuaModifierEntry where
  defaultValue = CDOTALuaModifierEntry P'.defaultValue P'.defaultValue

instance P'.Wire CDOTALuaModifierEntry where
  wireSize ft' self'@(CDOTALuaModifierEntry x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeReq 1 5 x'1 + P'.wireSizeReq 1 9 x'2)
  wirePut ft' self'@(CDOTALuaModifierEntry x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutReq 8 5 x'1
             P'.wirePutReq 18 9 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{modifier_type = new'Field}) (P'.wireGet 5)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{modifier_filename = new'Field}) (P'.wireGet 9)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTALuaModifierEntry) CDOTALuaModifierEntry where
  getVal m' f' = f' m'

instance P'.GPB CDOTALuaModifierEntry

instance P'.ReflectDescriptor CDOTALuaModifierEntry where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList [8, 18]) (P'.fromDistinctAscList [8, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Modifiers.CDOTALuaModifierEntry\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaModifiers\"], baseName = MName \"CDOTALuaModifierEntry\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaModifiers\",\"CDOTALuaModifierEntry.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTALuaModifierEntry.modifier_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTALuaModifierEntry\"], baseName' = FName \"modifier_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTALuaModifierEntry.modifier_filename\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTALuaModifierEntry\"], baseName' = FName \"modifier_filename\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTALuaModifierEntry where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTALuaModifierEntry where
  textPut msg
   = do
       P'.tellT "modifier_type" (modifier_type msg)
       P'.tellT "modifier_filename" (modifier_filename msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'modifier_type, parse'modifier_filename]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'modifier_type
         = P'.try
            (do
               v <- P'.getT "modifier_type"
               Prelude'.return (\ o -> o{modifier_type = v}))
        parse'modifier_filename
         = P'.try
            (do
               v <- P'.getT "modifier_filename"
               Prelude'.return (\ o -> o{modifier_filename = v}))