{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaModifiers.DOTA_MODIFIER_ENTRY_TYPE (DOTA_MODIFIER_ENTRY_TYPE(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data DOTA_MODIFIER_ENTRY_TYPE = DOTA_MODIFIER_ENTRY_TYPE_ACTIVE
                              | DOTA_MODIFIER_ENTRY_TYPE_REMOVED
                              deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                        Prelude'.Generic)

instance P'.Mergeable DOTA_MODIFIER_ENTRY_TYPE

instance Prelude'.Bounded DOTA_MODIFIER_ENTRY_TYPE where
  minBound = DOTA_MODIFIER_ENTRY_TYPE_ACTIVE
  maxBound = DOTA_MODIFIER_ENTRY_TYPE_REMOVED

instance P'.Default DOTA_MODIFIER_ENTRY_TYPE where
  defaultValue = DOTA_MODIFIER_ENTRY_TYPE_ACTIVE

toMaybe'Enum :: Prelude'.Int -> P'.Maybe DOTA_MODIFIER_ENTRY_TYPE
toMaybe'Enum 1 = Prelude'.Just DOTA_MODIFIER_ENTRY_TYPE_ACTIVE
toMaybe'Enum 2 = Prelude'.Just DOTA_MODIFIER_ENTRY_TYPE_REMOVED
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum DOTA_MODIFIER_ENTRY_TYPE where
  fromEnum DOTA_MODIFIER_ENTRY_TYPE_ACTIVE = 1
  fromEnum DOTA_MODIFIER_ENTRY_TYPE_REMOVED = 2
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaModifiers.DOTA_MODIFIER_ENTRY_TYPE")
      . toMaybe'Enum
  succ DOTA_MODIFIER_ENTRY_TYPE_ACTIVE = DOTA_MODIFIER_ENTRY_TYPE_REMOVED
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaModifiers.DOTA_MODIFIER_ENTRY_TYPE"
  pred DOTA_MODIFIER_ENTRY_TYPE_REMOVED = DOTA_MODIFIER_ENTRY_TYPE_ACTIVE
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaModifiers.DOTA_MODIFIER_ENTRY_TYPE"

instance P'.Wire DOTA_MODIFIER_ENTRY_TYPE where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB DOTA_MODIFIER_ENTRY_TYPE

instance P'.MessageAPI msg' (msg' -> DOTA_MODIFIER_ENTRY_TYPE) DOTA_MODIFIER_ENTRY_TYPE where
  getVal m' f' = f' m'

instance P'.ReflectEnum DOTA_MODIFIER_ENTRY_TYPE where
  reflectEnum
   = [(1, "DOTA_MODIFIER_ENTRY_TYPE_ACTIVE", DOTA_MODIFIER_ENTRY_TYPE_ACTIVE),
      (2, "DOTA_MODIFIER_ENTRY_TYPE_REMOVED", DOTA_MODIFIER_ENTRY_TYPE_REMOVED)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Modifiers.DOTA_MODIFIER_ENTRY_TYPE") ["Shrine", "Wire", "Protobuf", "Common"] ["DotaModifiers"]
        "DOTA_MODIFIER_ENTRY_TYPE")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaModifiers", "DOTA_MODIFIER_ENTRY_TYPE.hs"]
      [(1, "DOTA_MODIFIER_ENTRY_TYPE_ACTIVE"), (2, "DOTA_MODIFIER_ENTRY_TYPE_REMOVED")]

instance P'.TextType DOTA_MODIFIER_ENTRY_TYPE where
  tellT = P'.tellShow
  getT = P'.getRead