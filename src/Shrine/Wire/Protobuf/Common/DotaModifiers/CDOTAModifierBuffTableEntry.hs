{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaModifiers.CDOTAModifierBuffTableEntry (CDOTAModifierBuffTableEntry(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaModifiers.DOTA_MODIFIER_ENTRY_TYPE as DotaModifiers (DOTA_MODIFIER_ENTRY_TYPE)
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CDOTAModifierBuffTableEntry = CDOTAModifierBuffTableEntry{entry_type :: !(DotaModifiers.DOTA_MODIFIER_ENTRY_TYPE),
                                                               parent :: !(P'.Int32), index :: !(P'.Int32),
                                                               serial_num :: !(P'.Int32), modifier_class :: !(P'.Maybe P'.Int32),
                                                               ability_level :: !(P'.Maybe P'.Int32),
                                                               stack_count :: !(P'.Maybe P'.Int32),
                                                               creation_time :: !(P'.Maybe P'.Float),
                                                               duration :: !(P'.Maybe P'.Float), caster :: !(P'.Maybe P'.Int32),
                                                               ability :: !(P'.Maybe P'.Int32), armor :: !(P'.Maybe P'.Int32),
                                                               fade_time :: !(P'.Maybe P'.Float), subtle :: !(P'.Maybe P'.Bool),
                                                               channel_time :: !(P'.Maybe P'.Float),
                                                               v_start :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                                               v_end :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                                               portal_loop_appear :: !(P'.Maybe P'.Utf8),
                                                               portal_loop_disappear :: !(P'.Maybe P'.Utf8),
                                                               hero_loop_appear :: !(P'.Maybe P'.Utf8),
                                                               hero_loop_disappear :: !(P'.Maybe P'.Utf8),
                                                               movement_speed :: !(P'.Maybe P'.Int32), aura :: !(P'.Maybe P'.Bool),
                                                               activity :: !(P'.Maybe P'.Int32), damage :: !(P'.Maybe P'.Int32),
                                                               range :: !(P'.Maybe P'.Int32),
                                                               dd_modifier_index :: !(P'.Maybe P'.Int32),
                                                               dd_ability_index :: !(P'.Maybe P'.Int32),
                                                               illusion_label :: !(P'.Maybe P'.Utf8), active :: !(P'.Maybe P'.Bool),
                                                               lua_name :: !(P'.Maybe P'.Utf8)}
                                 deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                           Prelude'.Generic)

instance P'.Mergeable CDOTAModifierBuffTableEntry where
  mergeAppend
   (CDOTAModifierBuffTableEntry x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22
     x'23 x'24 x'25 x'26 x'27 x'28 x'29 x'30 x'31)
   (CDOTAModifierBuffTableEntry y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18 y'19 y'20 y'21 y'22
     y'23 y'24 y'25 y'26 y'27 y'28 y'29 y'30 y'31)
   = CDOTAModifierBuffTableEntry (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)
      (P'.mergeAppend x'19 y'19)
      (P'.mergeAppend x'20 y'20)
      (P'.mergeAppend x'21 y'21)
      (P'.mergeAppend x'22 y'22)
      (P'.mergeAppend x'23 y'23)
      (P'.mergeAppend x'24 y'24)
      (P'.mergeAppend x'25 y'25)
      (P'.mergeAppend x'26 y'26)
      (P'.mergeAppend x'27 y'27)
      (P'.mergeAppend x'28 y'28)
      (P'.mergeAppend x'29 y'29)
      (P'.mergeAppend x'30 y'30)
      (P'.mergeAppend x'31 y'31)

instance P'.Default CDOTAModifierBuffTableEntry where
  defaultValue
   = CDOTAModifierBuffTableEntry (Prelude'.read "DOTA_MODIFIER_ENTRY_TYPE_ACTIVE") P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      (Prelude'.Just (-1.0))
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAModifierBuffTableEntry where
  wireSize ft'
   self'@(CDOTAModifierBuffTableEntry x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20
           x'21 x'22 x'23 x'24 x'25 x'26 x'27 x'28 x'29 x'30 x'31)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeReq 1 14 x'1 + P'.wireSizeReq 1 5 x'2 + P'.wireSizeReq 1 5 x'3 + P'.wireSizeReq 1 5 x'4 +
             P'.wireSizeOpt 1 5 x'5
             + P'.wireSizeOpt 1 5 x'6
             + P'.wireSizeOpt 1 5 x'7
             + P'.wireSizeOpt 1 2 x'8
             + P'.wireSizeOpt 1 2 x'9
             + P'.wireSizeOpt 1 5 x'10
             + P'.wireSizeOpt 1 5 x'11
             + P'.wireSizeOpt 1 5 x'12
             + P'.wireSizeOpt 1 2 x'13
             + P'.wireSizeOpt 1 8 x'14
             + P'.wireSizeOpt 1 2 x'15
             + P'.wireSizeOpt 2 11 x'16
             + P'.wireSizeOpt 2 11 x'17
             + P'.wireSizeOpt 2 9 x'18
             + P'.wireSizeOpt 2 9 x'19
             + P'.wireSizeOpt 2 9 x'20
             + P'.wireSizeOpt 2 9 x'21
             + P'.wireSizeOpt 2 5 x'22
             + P'.wireSizeOpt 2 8 x'23
             + P'.wireSizeOpt 2 5 x'24
             + P'.wireSizeOpt 2 5 x'25
             + P'.wireSizeOpt 2 5 x'26
             + P'.wireSizeOpt 2 5 x'27
             + P'.wireSizeOpt 2 5 x'28
             + P'.wireSizeOpt 2 9 x'29
             + P'.wireSizeOpt 2 8 x'30
             + P'.wireSizeOpt 2 9 x'31)
  wirePut ft'
   self'@(CDOTAModifierBuffTableEntry x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20
           x'21 x'22 x'23 x'24 x'25 x'26 x'27 x'28 x'29 x'30 x'31)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutReq 8 14 x'1
             P'.wirePutReq 16 5 x'2
             P'.wirePutReq 24 5 x'3
             P'.wirePutReq 32 5 x'4
             P'.wirePutOpt 40 5 x'5
             P'.wirePutOpt 48 5 x'6
             P'.wirePutOpt 56 5 x'7
             P'.wirePutOpt 69 2 x'8
             P'.wirePutOpt 77 2 x'9
             P'.wirePutOpt 80 5 x'10
             P'.wirePutOpt 88 5 x'11
             P'.wirePutOpt 96 5 x'12
             P'.wirePutOpt 109 2 x'13
             P'.wirePutOpt 112 8 x'14
             P'.wirePutOpt 125 2 x'15
             P'.wirePutOpt 130 11 x'16
             P'.wirePutOpt 138 11 x'17
             P'.wirePutOpt 146 9 x'18
             P'.wirePutOpt 154 9 x'19
             P'.wirePutOpt 162 9 x'20
             P'.wirePutOpt 170 9 x'21
             P'.wirePutOpt 176 5 x'22
             P'.wirePutOpt 184 8 x'23
             P'.wirePutOpt 192 5 x'24
             P'.wirePutOpt 200 5 x'25
             P'.wirePutOpt 208 5 x'26
             P'.wirePutOpt 216 5 x'27
             P'.wirePutOpt 224 5 x'28
             P'.wirePutOpt 234 9 x'29
             P'.wirePutOpt 240 8 x'30
             P'.wirePutOpt 250 9 x'31
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{entry_type = new'Field}) (P'.wireGet 14)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{parent = new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{index = new'Field}) (P'.wireGet 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{serial_num = new'Field}) (P'.wireGet 5)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{modifier_class = Prelude'.Just new'Field}) (P'.wireGet 5)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{ability_level = Prelude'.Just new'Field}) (P'.wireGet 5)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{stack_count = Prelude'.Just new'Field}) (P'.wireGet 5)
             69 -> Prelude'.fmap (\ !new'Field -> old'Self{creation_time = Prelude'.Just new'Field}) (P'.wireGet 2)
             77 -> Prelude'.fmap (\ !new'Field -> old'Self{duration = Prelude'.Just new'Field}) (P'.wireGet 2)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{caster = Prelude'.Just new'Field}) (P'.wireGet 5)
             88 -> Prelude'.fmap (\ !new'Field -> old'Self{ability = Prelude'.Just new'Field}) (P'.wireGet 5)
             96 -> Prelude'.fmap (\ !new'Field -> old'Self{armor = Prelude'.Just new'Field}) (P'.wireGet 5)
             109 -> Prelude'.fmap (\ !new'Field -> old'Self{fade_time = Prelude'.Just new'Field}) (P'.wireGet 2)
             112 -> Prelude'.fmap (\ !new'Field -> old'Self{subtle = Prelude'.Just new'Field}) (P'.wireGet 8)
             125 -> Prelude'.fmap (\ !new'Field -> old'Self{channel_time = Prelude'.Just new'Field}) (P'.wireGet 2)
             130 -> Prelude'.fmap (\ !new'Field -> old'Self{v_start = P'.mergeAppend (v_start old'Self) (Prelude'.Just new'Field)})
                     (P'.wireGet 11)
             138 -> Prelude'.fmap (\ !new'Field -> old'Self{v_end = P'.mergeAppend (v_end old'Self) (Prelude'.Just new'Field)})
                     (P'.wireGet 11)
             146 -> Prelude'.fmap (\ !new'Field -> old'Self{portal_loop_appear = Prelude'.Just new'Field}) (P'.wireGet 9)
             154 -> Prelude'.fmap (\ !new'Field -> old'Self{portal_loop_disappear = Prelude'.Just new'Field}) (P'.wireGet 9)
             162 -> Prelude'.fmap (\ !new'Field -> old'Self{hero_loop_appear = Prelude'.Just new'Field}) (P'.wireGet 9)
             170 -> Prelude'.fmap (\ !new'Field -> old'Self{hero_loop_disappear = Prelude'.Just new'Field}) (P'.wireGet 9)
             176 -> Prelude'.fmap (\ !new'Field -> old'Self{movement_speed = Prelude'.Just new'Field}) (P'.wireGet 5)
             184 -> Prelude'.fmap (\ !new'Field -> old'Self{aura = Prelude'.Just new'Field}) (P'.wireGet 8)
             192 -> Prelude'.fmap (\ !new'Field -> old'Self{activity = Prelude'.Just new'Field}) (P'.wireGet 5)
             200 -> Prelude'.fmap (\ !new'Field -> old'Self{damage = Prelude'.Just new'Field}) (P'.wireGet 5)
             208 -> Prelude'.fmap (\ !new'Field -> old'Self{range = Prelude'.Just new'Field}) (P'.wireGet 5)
             216 -> Prelude'.fmap (\ !new'Field -> old'Self{dd_modifier_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             224 -> Prelude'.fmap (\ !new'Field -> old'Self{dd_ability_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             234 -> Prelude'.fmap (\ !new'Field -> old'Self{illusion_label = Prelude'.Just new'Field}) (P'.wireGet 9)
             240 -> Prelude'.fmap (\ !new'Field -> old'Self{active = Prelude'.Just new'Field}) (P'.wireGet 8)
             250 -> Prelude'.fmap (\ !new'Field -> old'Self{lua_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAModifierBuffTableEntry) CDOTAModifierBuffTableEntry where
  getVal m' f' = f' m'

instance P'.GPB CDOTAModifierBuffTableEntry

instance P'.ReflectDescriptor CDOTAModifierBuffTableEntry where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [8, 16, 24, 32])
      (P'.fromDistinctAscList
        [8, 16, 24, 32, 40, 48, 56, 69, 77, 80, 88, 96, 109, 112, 125, 130, 138, 146, 154, 162, 170, 176, 184, 192, 200, 208, 216,
         224, 234, 240, 250])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaModifiers\"], baseName = MName \"CDOTAModifierBuffTableEntry\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaModifiers\",\"CDOTAModifierBuffTableEntry.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.entry_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"entry_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Modifiers.DOTA_MODIFIER_ENTRY_TYPE\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaModifiers\"], baseName = MName \"DOTA_MODIFIER_ENTRY_TYPE\"}), hsRawDefault = Just \"DOTA_MODIFIER_ENTRY_TYPE_ACTIVE\", hsDefault = Just (HsDef'Enum \"DOTA_MODIFIER_ENTRY_TYPE_ACTIVE\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.parent\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"parent\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.serial_num\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"serial_num\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.modifier_class\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"modifier_class\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.ability_level\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"ability_level\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.stack_count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"stack_count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.creation_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"creation_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 69}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.duration\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"duration\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 77}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Just \"-1.0\", hsDefault = Just (HsDef'RealFloat (SRF'Rational ((-1) % 1)))},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.caster\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"caster\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.ability\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"ability\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 88}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.armor\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"armor\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 96}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.fade_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"fade_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 109}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.subtle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"subtle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 112}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.channel_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"channel_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 125}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.v_start\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"v_start\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 130}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.v_end\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"v_end\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 138}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.portal_loop_appear\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"portal_loop_appear\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 146}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.portal_loop_disappear\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"portal_loop_disappear\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 19}, wireTag = WireTag {getWireTag = 154}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.hero_loop_appear\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"hero_loop_appear\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 20}, wireTag = WireTag {getWireTag = 162}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.hero_loop_disappear\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"hero_loop_disappear\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 21}, wireTag = WireTag {getWireTag = 170}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.movement_speed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"movement_speed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 22}, wireTag = WireTag {getWireTag = 176}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.aura\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"aura\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 23}, wireTag = WireTag {getWireTag = 184}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.activity\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"activity\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 24}, wireTag = WireTag {getWireTag = 192}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.damage\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"damage\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 25}, wireTag = WireTag {getWireTag = 200}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.range\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"range\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 26}, wireTag = WireTag {getWireTag = 208}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.dd_modifier_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"dd_modifier_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 27}, wireTag = WireTag {getWireTag = 216}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.dd_ability_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"dd_ability_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 28}, wireTag = WireTag {getWireTag = 224}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.illusion_label\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"illusion_label\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 29}, wireTag = WireTag {getWireTag = 234}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.active\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"active\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 30}, wireTag = WireTag {getWireTag = 240}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Modifiers.CDOTAModifierBuffTableEntry.lua_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaModifiers\",MName \"CDOTAModifierBuffTableEntry\"], baseName' = FName \"lua_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 31}, wireTag = WireTag {getWireTag = 250}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAModifierBuffTableEntry where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAModifierBuffTableEntry where
  textPut msg
   = do
       P'.tellT "entry_type" (entry_type msg)
       P'.tellT "parent" (parent msg)
       P'.tellT "index" (index msg)
       P'.tellT "serial_num" (serial_num msg)
       P'.tellT "modifier_class" (modifier_class msg)
       P'.tellT "ability_level" (ability_level msg)
       P'.tellT "stack_count" (stack_count msg)
       P'.tellT "creation_time" (creation_time msg)
       P'.tellT "duration" (duration msg)
       P'.tellT "caster" (caster msg)
       P'.tellT "ability" (ability msg)
       P'.tellT "armor" (armor msg)
       P'.tellT "fade_time" (fade_time msg)
       P'.tellT "subtle" (subtle msg)
       P'.tellT "channel_time" (channel_time msg)
       P'.tellT "v_start" (v_start msg)
       P'.tellT "v_end" (v_end msg)
       P'.tellT "portal_loop_appear" (portal_loop_appear msg)
       P'.tellT "portal_loop_disappear" (portal_loop_disappear msg)
       P'.tellT "hero_loop_appear" (hero_loop_appear msg)
       P'.tellT "hero_loop_disappear" (hero_loop_disappear msg)
       P'.tellT "movement_speed" (movement_speed msg)
       P'.tellT "aura" (aura msg)
       P'.tellT "activity" (activity msg)
       P'.tellT "damage" (damage msg)
       P'.tellT "range" (range msg)
       P'.tellT "dd_modifier_index" (dd_modifier_index msg)
       P'.tellT "dd_ability_index" (dd_ability_index msg)
       P'.tellT "illusion_label" (illusion_label msg)
       P'.tellT "active" (active msg)
       P'.tellT "lua_name" (lua_name msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'entry_type, parse'parent, parse'index, parse'serial_num, parse'modifier_class, parse'ability_level,
                   parse'stack_count, parse'creation_time, parse'duration, parse'caster, parse'ability, parse'armor,
                   parse'fade_time, parse'subtle, parse'channel_time, parse'v_start, parse'v_end, parse'portal_loop_appear,
                   parse'portal_loop_disappear, parse'hero_loop_appear, parse'hero_loop_disappear, parse'movement_speed, parse'aura,
                   parse'activity, parse'damage, parse'range, parse'dd_modifier_index, parse'dd_ability_index, parse'illusion_label,
                   parse'active, parse'lua_name])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'entry_type
         = P'.try
            (do
               v <- P'.getT "entry_type"
               Prelude'.return (\ o -> o{entry_type = v}))
        parse'parent
         = P'.try
            (do
               v <- P'.getT "parent"
               Prelude'.return (\ o -> o{parent = v}))
        parse'index
         = P'.try
            (do
               v <- P'.getT "index"
               Prelude'.return (\ o -> o{index = v}))
        parse'serial_num
         = P'.try
            (do
               v <- P'.getT "serial_num"
               Prelude'.return (\ o -> o{serial_num = v}))
        parse'modifier_class
         = P'.try
            (do
               v <- P'.getT "modifier_class"
               Prelude'.return (\ o -> o{modifier_class = v}))
        parse'ability_level
         = P'.try
            (do
               v <- P'.getT "ability_level"
               Prelude'.return (\ o -> o{ability_level = v}))
        parse'stack_count
         = P'.try
            (do
               v <- P'.getT "stack_count"
               Prelude'.return (\ o -> o{stack_count = v}))
        parse'creation_time
         = P'.try
            (do
               v <- P'.getT "creation_time"
               Prelude'.return (\ o -> o{creation_time = v}))
        parse'duration
         = P'.try
            (do
               v <- P'.getT "duration"
               Prelude'.return (\ o -> o{duration = v}))
        parse'caster
         = P'.try
            (do
               v <- P'.getT "caster"
               Prelude'.return (\ o -> o{caster = v}))
        parse'ability
         = P'.try
            (do
               v <- P'.getT "ability"
               Prelude'.return (\ o -> o{ability = v}))
        parse'armor
         = P'.try
            (do
               v <- P'.getT "armor"
               Prelude'.return (\ o -> o{armor = v}))
        parse'fade_time
         = P'.try
            (do
               v <- P'.getT "fade_time"
               Prelude'.return (\ o -> o{fade_time = v}))
        parse'subtle
         = P'.try
            (do
               v <- P'.getT "subtle"
               Prelude'.return (\ o -> o{subtle = v}))
        parse'channel_time
         = P'.try
            (do
               v <- P'.getT "channel_time"
               Prelude'.return (\ o -> o{channel_time = v}))
        parse'v_start
         = P'.try
            (do
               v <- P'.getT "v_start"
               Prelude'.return (\ o -> o{v_start = v}))
        parse'v_end
         = P'.try
            (do
               v <- P'.getT "v_end"
               Prelude'.return (\ o -> o{v_end = v}))
        parse'portal_loop_appear
         = P'.try
            (do
               v <- P'.getT "portal_loop_appear"
               Prelude'.return (\ o -> o{portal_loop_appear = v}))
        parse'portal_loop_disappear
         = P'.try
            (do
               v <- P'.getT "portal_loop_disappear"
               Prelude'.return (\ o -> o{portal_loop_disappear = v}))
        parse'hero_loop_appear
         = P'.try
            (do
               v <- P'.getT "hero_loop_appear"
               Prelude'.return (\ o -> o{hero_loop_appear = v}))
        parse'hero_loop_disappear
         = P'.try
            (do
               v <- P'.getT "hero_loop_disappear"
               Prelude'.return (\ o -> o{hero_loop_disappear = v}))
        parse'movement_speed
         = P'.try
            (do
               v <- P'.getT "movement_speed"
               Prelude'.return (\ o -> o{movement_speed = v}))
        parse'aura
         = P'.try
            (do
               v <- P'.getT "aura"
               Prelude'.return (\ o -> o{aura = v}))
        parse'activity
         = P'.try
            (do
               v <- P'.getT "activity"
               Prelude'.return (\ o -> o{activity = v}))
        parse'damage
         = P'.try
            (do
               v <- P'.getT "damage"
               Prelude'.return (\ o -> o{damage = v}))
        parse'range
         = P'.try
            (do
               v <- P'.getT "range"
               Prelude'.return (\ o -> o{range = v}))
        parse'dd_modifier_index
         = P'.try
            (do
               v <- P'.getT "dd_modifier_index"
               Prelude'.return (\ o -> o{dd_modifier_index = v}))
        parse'dd_ability_index
         = P'.try
            (do
               v <- P'.getT "dd_ability_index"
               Prelude'.return (\ o -> o{dd_ability_index = v}))
        parse'illusion_label
         = P'.try
            (do
               v <- P'.getT "illusion_label"
               Prelude'.return (\ o -> o{illusion_label = v}))
        parse'active
         = P'.try
            (do
               v <- P'.getT "active"
               Prelude'.return (\ o -> o{active = v}))
        parse'lua_name
         = P'.try
            (do
               v <- P'.getT "lua_name"
               Prelude'.return (\ o -> o{lua_name = v}))