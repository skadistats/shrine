{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAResponseQuerySerialized (CDOTAResponseQuerySerialized(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAResponseQuerySerialized.Fact
       as DotaUserMessages.CDOTAResponseQuerySerialized (Fact)

data CDOTAResponseQuerySerialized = CDOTAResponseQuerySerialized{facts ::
                                                                 !(P'.Seq DotaUserMessages.CDOTAResponseQuerySerialized.Fact)}
                                  deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                            Prelude'.Generic)

instance P'.Mergeable CDOTAResponseQuerySerialized where
  mergeAppend (CDOTAResponseQuerySerialized x'1) (CDOTAResponseQuerySerialized y'1)
   = CDOTAResponseQuerySerialized (P'.mergeAppend x'1 y'1)

instance P'.Default CDOTAResponseQuerySerialized where
  defaultValue = CDOTAResponseQuerySerialized P'.defaultValue

instance P'.Wire CDOTAResponseQuerySerialized where
  wireSize ft' self'@(CDOTAResponseQuerySerialized x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 11 x'1)
  wirePut ft' self'@(CDOTAResponseQuerySerialized x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 10 11 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{facts = P'.append (facts old'Self) new'Field}) (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAResponseQuerySerialized) CDOTAResponseQuerySerialized where
  getVal m' f' = f' m'

instance P'.GPB CDOTAResponseQuerySerialized

instance P'.ReflectDescriptor CDOTAResponseQuerySerialized where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAResponseQuerySerialized\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAResponseQuerySerialized.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized.facts\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAResponseQuerySerialized\"], baseName' = FName \"facts\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized.Fact\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAResponseQuerySerialized\"], baseName = MName \"Fact\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAResponseQuerySerialized where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAResponseQuerySerialized where
  textPut msg
   = do
       P'.tellT "facts" (facts msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'facts]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'facts
         = P'.try
            (do
               v <- P'.getT "facts"
               Prelude'.return (\ o -> o{facts = P'.append (facts o) v}))