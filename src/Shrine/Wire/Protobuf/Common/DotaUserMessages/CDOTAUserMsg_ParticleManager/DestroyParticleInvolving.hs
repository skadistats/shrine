{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ParticleManager.DestroyParticleInvolving
       (DestroyParticleInvolving(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data DestroyParticleInvolving = DestroyParticleInvolving{destroy_immediately :: !(P'.Maybe P'.Bool),
                                                         entity_handle :: !(P'.Maybe P'.Int32)}
                              deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                        Prelude'.Generic)

instance P'.Mergeable DestroyParticleInvolving where
  mergeAppend (DestroyParticleInvolving x'1 x'2) (DestroyParticleInvolving y'1 y'2)
   = DestroyParticleInvolving (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default DestroyParticleInvolving where
  defaultValue = DestroyParticleInvolving P'.defaultValue P'.defaultValue

instance P'.Wire DestroyParticleInvolving where
  wireSize ft' self'@(DestroyParticleInvolving x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 8 x'1 + P'.wireSizeOpt 1 5 x'2)
  wirePut ft' self'@(DestroyParticleInvolving x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 8 x'1
             P'.wirePutOpt 24 5 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{destroy_immediately = Prelude'.Just new'Field}) (P'.wireGet 8)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{entity_handle = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> DestroyParticleInvolving) DestroyParticleInvolving where
  getVal m' f' = f' m'

instance P'.GPB DestroyParticleInvolving

instance P'.ReflectDescriptor DestroyParticleInvolving where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 24])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_ParticleManager.DestroyParticleInvolving\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ParticleManager\"], baseName = MName \"DestroyParticleInvolving\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_ParticleManager\",\"DestroyParticleInvolving.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ParticleManager.DestroyParticleInvolving.destroy_immediately\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ParticleManager\",MName \"DestroyParticleInvolving\"], baseName' = FName \"destroy_immediately\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ParticleManager.DestroyParticleInvolving.entity_handle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ParticleManager\",MName \"DestroyParticleInvolving\"], baseName' = FName \"entity_handle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType DestroyParticleInvolving where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg DestroyParticleInvolving where
  textPut msg
   = do
       P'.tellT "destroy_immediately" (destroy_immediately msg)
       P'.tellT "entity_handle" (entity_handle msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'destroy_immediately, parse'entity_handle]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'destroy_immediately
         = P'.try
            (do
               v <- P'.getT "destroy_immediately"
               Prelude'.return (\ o -> o{destroy_immediately = v}))
        parse'entity_handle
         = P'.try
            (do
               v <- P'.getT "entity_handle"
               Prelude'.return (\ o -> o{entity_handle = v}))