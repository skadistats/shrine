{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ParticleManager.SetParticleFoWProperties
       (SetParticleFoWProperties(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data SetParticleFoWProperties = SetParticleFoWProperties{fow_control_point :: !(P'.Maybe P'.Int32),
                                                         fow_control_point2 :: !(P'.Maybe P'.Int32),
                                                         fow_radius :: !(P'.Maybe P'.Float)}
                              deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                        Prelude'.Generic)

instance P'.Mergeable SetParticleFoWProperties where
  mergeAppend (SetParticleFoWProperties x'1 x'2 x'3) (SetParticleFoWProperties y'1 y'2 y'3)
   = SetParticleFoWProperties (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default SetParticleFoWProperties where
  defaultValue = SetParticleFoWProperties P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire SetParticleFoWProperties where
  wireSize ft' self'@(SetParticleFoWProperties x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 2 x'3)
  wirePut ft' self'@(SetParticleFoWProperties x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 29 2 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{fow_control_point = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{fow_control_point2 = Prelude'.Just new'Field}) (P'.wireGet 5)
             29 -> Prelude'.fmap (\ !new'Field -> old'Self{fow_radius = Prelude'.Just new'Field}) (P'.wireGet 2)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> SetParticleFoWProperties) SetParticleFoWProperties where
  getVal m' f' = f' m'

instance P'.GPB SetParticleFoWProperties

instance P'.ReflectDescriptor SetParticleFoWProperties where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 29])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_ParticleManager.SetParticleFoWProperties\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ParticleManager\"], baseName = MName \"SetParticleFoWProperties\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_ParticleManager\",\"SetParticleFoWProperties.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ParticleManager.SetParticleFoWProperties.fow_control_point\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ParticleManager\",MName \"SetParticleFoWProperties\"], baseName' = FName \"fow_control_point\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ParticleManager.SetParticleFoWProperties.fow_control_point2\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ParticleManager\",MName \"SetParticleFoWProperties\"], baseName' = FName \"fow_control_point2\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ParticleManager.SetParticleFoWProperties.fow_radius\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ParticleManager\",MName \"SetParticleFoWProperties\"], baseName' = FName \"fow_radius\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 29}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType SetParticleFoWProperties where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg SetParticleFoWProperties where
  textPut msg
   = do
       P'.tellT "fow_control_point" (fow_control_point msg)
       P'.tellT "fow_control_point2" (fow_control_point2 msg)
       P'.tellT "fow_radius" (fow_radius msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'fow_control_point, parse'fow_control_point2, parse'fow_radius]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'fow_control_point
         = P'.try
            (do
               v <- P'.getT "fow_control_point"
               Prelude'.return (\ o -> o{fow_control_point = v}))
        parse'fow_control_point2
         = P'.try
            (do
               v <- P'.getT "fow_control_point2"
               Prelude'.return (\ o -> o{fow_control_point2 = v}))
        parse'fow_radius
         = P'.try
            (do
               v <- P'.getT "fow_radius"
               Prelude'.return (\ o -> o{fow_radius = v}))