{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ParticleManager.UpdateParticleShouldDraw
       (UpdateParticleShouldDraw(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data UpdateParticleShouldDraw = UpdateParticleShouldDraw{should_draw :: !(P'.Maybe P'.Bool)}
                              deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                        Prelude'.Generic)

instance P'.Mergeable UpdateParticleShouldDraw where
  mergeAppend (UpdateParticleShouldDraw x'1) (UpdateParticleShouldDraw y'1) = UpdateParticleShouldDraw (P'.mergeAppend x'1 y'1)

instance P'.Default UpdateParticleShouldDraw where
  defaultValue = UpdateParticleShouldDraw P'.defaultValue

instance P'.Wire UpdateParticleShouldDraw where
  wireSize ft' self'@(UpdateParticleShouldDraw x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 8 x'1)
  wirePut ft' self'@(UpdateParticleShouldDraw x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 8 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{should_draw = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> UpdateParticleShouldDraw) UpdateParticleShouldDraw where
  getVal m' f' = f' m'

instance P'.GPB UpdateParticleShouldDraw

instance P'.ReflectDescriptor UpdateParticleShouldDraw where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_ParticleManager.UpdateParticleShouldDraw\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleShouldDraw\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_ParticleManager\",\"UpdateParticleShouldDraw.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ParticleManager.UpdateParticleShouldDraw.should_draw\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ParticleManager\",MName \"UpdateParticleShouldDraw\"], baseName' = FName \"should_draw\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType UpdateParticleShouldDraw where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg UpdateParticleShouldDraw where
  textPut msg
   = do
       P'.tellT "should_draw" (should_draw msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'should_draw]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'should_draw
         = P'.try
            (do
               v <- P'.getT "should_draw"
               Prelude'.return (\ o -> o{should_draw = v}))