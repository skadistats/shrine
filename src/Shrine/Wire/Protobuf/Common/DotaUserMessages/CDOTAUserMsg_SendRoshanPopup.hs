{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SendRoshanPopup (CDOTAUserMsg_SendRoshanPopup(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTAUserMsg_SendRoshanPopup = CDOTAUserMsg_SendRoshanPopup{reclaimed :: !(P'.Maybe P'.Bool), gametime :: !(P'.Maybe P'.Int32)}
                                  deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                            Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_SendRoshanPopup where
  mergeAppend (CDOTAUserMsg_SendRoshanPopup x'1 x'2) (CDOTAUserMsg_SendRoshanPopup y'1 y'2)
   = CDOTAUserMsg_SendRoshanPopup (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CDOTAUserMsg_SendRoshanPopup where
  defaultValue = CDOTAUserMsg_SendRoshanPopup P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAUserMsg_SendRoshanPopup where
  wireSize ft' self'@(CDOTAUserMsg_SendRoshanPopup x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 8 x'1 + P'.wireSizeOpt 1 5 x'2)
  wirePut ft' self'@(CDOTAUserMsg_SendRoshanPopup x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 8 x'1
             P'.wirePutOpt 16 5 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{reclaimed = Prelude'.Just new'Field}) (P'.wireGet 8)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{gametime = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_SendRoshanPopup) CDOTAUserMsg_SendRoshanPopup where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_SendRoshanPopup

instance P'.ReflectDescriptor CDOTAUserMsg_SendRoshanPopup where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_SendRoshanPopup\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_SendRoshanPopup\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_SendRoshanPopup.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SendRoshanPopup.reclaimed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SendRoshanPopup\"], baseName' = FName \"reclaimed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SendRoshanPopup.gametime\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SendRoshanPopup\"], baseName' = FName \"gametime\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_SendRoshanPopup where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_SendRoshanPopup where
  textPut msg
   = do
       P'.tellT "reclaimed" (reclaimed msg)
       P'.tellT "gametime" (gametime msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'reclaimed, parse'gametime]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'reclaimed
         = P'.try
            (do
               v <- P'.getT "reclaimed"
               Prelude'.return (\ o -> o{reclaimed = v}))
        parse'gametime
         = P'.try
            (do
               v <- P'.getT "gametime"
               Prelude'.return (\ o -> o{gametime = v}))