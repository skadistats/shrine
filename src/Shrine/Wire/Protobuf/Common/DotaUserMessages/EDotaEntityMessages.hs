{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.EDotaEntityMessages (EDotaEntityMessages(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EDotaEntityMessages = DOTA_UNIT_SPEECH
                         | DOTA_UNIT_SPEECH_MUTE
                         | DOTA_UNIT_ADD_GESTURE
                         | DOTA_UNIT_REMOVE_GESTURE
                         | DOTA_UNIT_REMOVE_ALL_GESTURES
                         | DOTA_UNIT_FADE_GESTURE
                         | DOTA_UNIT_SPEECH_CLIENTSIDE_RULES
                         deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                   Prelude'.Generic)

instance P'.Mergeable EDotaEntityMessages

instance Prelude'.Bounded EDotaEntityMessages where
  minBound = DOTA_UNIT_SPEECH
  maxBound = DOTA_UNIT_SPEECH_CLIENTSIDE_RULES

instance P'.Default EDotaEntityMessages where
  defaultValue = DOTA_UNIT_SPEECH

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EDotaEntityMessages
toMaybe'Enum 0 = Prelude'.Just DOTA_UNIT_SPEECH
toMaybe'Enum 1 = Prelude'.Just DOTA_UNIT_SPEECH_MUTE
toMaybe'Enum 2 = Prelude'.Just DOTA_UNIT_ADD_GESTURE
toMaybe'Enum 3 = Prelude'.Just DOTA_UNIT_REMOVE_GESTURE
toMaybe'Enum 4 = Prelude'.Just DOTA_UNIT_REMOVE_ALL_GESTURES
toMaybe'Enum 6 = Prelude'.Just DOTA_UNIT_FADE_GESTURE
toMaybe'Enum 7 = Prelude'.Just DOTA_UNIT_SPEECH_CLIENTSIDE_RULES
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EDotaEntityMessages where
  fromEnum DOTA_UNIT_SPEECH = 0
  fromEnum DOTA_UNIT_SPEECH_MUTE = 1
  fromEnum DOTA_UNIT_ADD_GESTURE = 2
  fromEnum DOTA_UNIT_REMOVE_GESTURE = 3
  fromEnum DOTA_UNIT_REMOVE_ALL_GESTURES = 4
  fromEnum DOTA_UNIT_FADE_GESTURE = 6
  fromEnum DOTA_UNIT_SPEECH_CLIENTSIDE_RULES = 7
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EDotaEntityMessages")
      . toMaybe'Enum
  succ DOTA_UNIT_SPEECH = DOTA_UNIT_SPEECH_MUTE
  succ DOTA_UNIT_SPEECH_MUTE = DOTA_UNIT_ADD_GESTURE
  succ DOTA_UNIT_ADD_GESTURE = DOTA_UNIT_REMOVE_GESTURE
  succ DOTA_UNIT_REMOVE_GESTURE = DOTA_UNIT_REMOVE_ALL_GESTURES
  succ DOTA_UNIT_REMOVE_ALL_GESTURES = DOTA_UNIT_FADE_GESTURE
  succ DOTA_UNIT_FADE_GESTURE = DOTA_UNIT_SPEECH_CLIENTSIDE_RULES
  succ _
   = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EDotaEntityMessages"
  pred DOTA_UNIT_SPEECH_MUTE = DOTA_UNIT_SPEECH
  pred DOTA_UNIT_ADD_GESTURE = DOTA_UNIT_SPEECH_MUTE
  pred DOTA_UNIT_REMOVE_GESTURE = DOTA_UNIT_ADD_GESTURE
  pred DOTA_UNIT_REMOVE_ALL_GESTURES = DOTA_UNIT_REMOVE_GESTURE
  pred DOTA_UNIT_FADE_GESTURE = DOTA_UNIT_REMOVE_ALL_GESTURES
  pred DOTA_UNIT_SPEECH_CLIENTSIDE_RULES = DOTA_UNIT_FADE_GESTURE
  pred _
   = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EDotaEntityMessages"

instance P'.Wire EDotaEntityMessages where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EDotaEntityMessages

instance P'.MessageAPI msg' (msg' -> EDotaEntityMessages) EDotaEntityMessages where
  getVal m' f' = f' m'

instance P'.ReflectEnum EDotaEntityMessages where
  reflectEnum
   = [(0, "DOTA_UNIT_SPEECH", DOTA_UNIT_SPEECH), (1, "DOTA_UNIT_SPEECH_MUTE", DOTA_UNIT_SPEECH_MUTE),
      (2, "DOTA_UNIT_ADD_GESTURE", DOTA_UNIT_ADD_GESTURE), (3, "DOTA_UNIT_REMOVE_GESTURE", DOTA_UNIT_REMOVE_GESTURE),
      (4, "DOTA_UNIT_REMOVE_ALL_GESTURES", DOTA_UNIT_REMOVE_ALL_GESTURES), (6, "DOTA_UNIT_FADE_GESTURE", DOTA_UNIT_FADE_GESTURE),
      (7, "DOTA_UNIT_SPEECH_CLIENTSIDE_RULES", DOTA_UNIT_SPEECH_CLIENTSIDE_RULES)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.EDotaEntityMessages") ["Shrine", "Wire", "Protobuf", "Common"] ["DotaUserMessages"]
        "EDotaEntityMessages")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "EDotaEntityMessages.hs"]
      [(0, "DOTA_UNIT_SPEECH"), (1, "DOTA_UNIT_SPEECH_MUTE"), (2, "DOTA_UNIT_ADD_GESTURE"), (3, "DOTA_UNIT_REMOVE_GESTURE"),
       (4, "DOTA_UNIT_REMOVE_ALL_GESTURES"), (6, "DOTA_UNIT_FADE_GESTURE"), (7, "DOTA_UNIT_SPEECH_CLIENTSIDE_RULES")]

instance P'.TextType EDotaEntityMessages where
  tellT = P'.tellShow
  getT = P'.getRead