{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_StatsHeroPositionInfo.PositionPair (PositionPair(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_POSITION_CATEGORY as DotaUserMessages (DOTA_POSITION_CATEGORY)

data PositionPair = PositionPair{position_category :: !(P'.Maybe DotaUserMessages.DOTA_POSITION_CATEGORY),
                                 position_count :: !(P'.Maybe P'.Word32)}
                  deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable PositionPair where
  mergeAppend (PositionPair x'1 x'2) (PositionPair y'1 y'2) = PositionPair (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default PositionPair where
  defaultValue = PositionPair (Prelude'.Just (Prelude'.read "DOTA_POSITION_NONE")) P'.defaultValue

instance P'.Wire PositionPair where
  wireSize ft' self'@(PositionPair x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 14 x'1 + P'.wireSizeOpt 1 13 x'2)
  wirePut ft' self'@(PositionPair x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 14 x'1
             P'.wirePutOpt 16 13 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{position_category = Prelude'.Just new'Field}) (P'.wireGet 14)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{position_count = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> PositionPair) PositionPair where
  getVal m' f' = f' m'

instance P'.GPB PositionPair

instance P'.ReflectDescriptor PositionPair where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsHeroPositionInfo.PositionPair\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsHeroPositionInfo\"], baseName = MName \"PositionPair\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_StatsHeroPositionInfo\",\"PositionPair.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsHeroPositionInfo.PositionPair.position_category\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsHeroPositionInfo\",MName \"PositionPair\"], baseName' = FName \"position_category\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.DOTA_POSITION_CATEGORY\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"DOTA_POSITION_CATEGORY\"}), hsRawDefault = Just \"DOTA_POSITION_NONE\", hsDefault = Just (HsDef'Enum \"DOTA_POSITION_NONE\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsHeroPositionInfo.PositionPair.position_count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsHeroPositionInfo\",MName \"PositionPair\"], baseName' = FName \"position_count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType PositionPair where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg PositionPair where
  textPut msg
   = do
       P'.tellT "position_category" (position_category msg)
       P'.tellT "position_count" (position_count msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'position_category, parse'position_count]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'position_category
         = P'.try
            (do
               v <- P'.getT "position_category"
               Prelude'.return (\ o -> o{position_category = v}))
        parse'position_count
         = P'.try
            (do
               v <- P'.getT "position_count"
               Prelude'.return (\ o -> o{position_count = v}))