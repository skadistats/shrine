{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_PredictionResult.Prediction (Prediction(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_PredictionResult.Prediction.EResult
       as DotaUserMessages.CDOTAUserMsg_PredictionResult.Prediction (EResult)

data Prediction = Prediction{item_def :: !(P'.Maybe P'.Word32), num_correct :: !(P'.Maybe P'.Word32),
                             num_fails :: !(P'.Maybe P'.Word32),
                             result :: !(P'.Maybe DotaUserMessages.CDOTAUserMsg_PredictionResult.Prediction.EResult),
                             granted_item_defs :: !(P'.Seq P'.Word32)}
                deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Prediction where
  mergeAppend (Prediction x'1 x'2 x'3 x'4 x'5) (Prediction y'1 y'2 y'3 y'4 y'5)
   = Prediction (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)

instance P'.Default Prediction where
  defaultValue
   = Prediction P'.defaultValue P'.defaultValue P'.defaultValue (Prelude'.Just (Prelude'.read "K_eResult_ItemGranted"))
      P'.defaultValue

instance P'.Wire Prediction where
  wireSize ft' self'@(Prediction x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 13 x'3 + P'.wireSizeOpt 1 14 x'4 +
             P'.wireSizeRep 1 13 x'5)
  wirePut ft' self'@(Prediction x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 13 x'3
             P'.wirePutOpt 32 14 x'4
             P'.wirePutRep 48 13 x'5
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{item_def = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{num_correct = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{num_fails = Prelude'.Just new'Field}) (P'.wireGet 13)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{result = Prelude'.Just new'Field}) (P'.wireGet 14)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{granted_item_defs = P'.append (granted_item_defs old'Self) new'Field})
                    (P'.wireGet 13)
             50 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{granted_item_defs = P'.mergeAppend (granted_item_defs old'Self) new'Field})
                    (P'.wireGetPacked 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Prediction) Prediction where
  getVal m' f' = f' m'

instance P'.GPB Prediction

instance P'.ReflectDescriptor Prediction where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 32, 48, 50])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_PredictionResult.Prediction\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_PredictionResult\"], baseName = MName \"Prediction\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_PredictionResult\",\"Prediction.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_PredictionResult.Prediction.item_def\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_PredictionResult\",MName \"Prediction\"], baseName' = FName \"item_def\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_PredictionResult.Prediction.num_correct\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_PredictionResult\",MName \"Prediction\"], baseName' = FName \"num_correct\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_PredictionResult.Prediction.num_fails\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_PredictionResult\",MName \"Prediction\"], baseName' = FName \"num_fails\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_PredictionResult.Prediction.result\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_PredictionResult\",MName \"Prediction\"], baseName' = FName \"result\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_PredictionResult.Prediction.EResult\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_PredictionResult\",MName \"Prediction\"], baseName = MName \"EResult\"}), hsRawDefault = Just \"k_eResult_ItemGranted\", hsDefault = Just (HsDef'Enum \"K_eResult_ItemGranted\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_PredictionResult.Prediction.granted_item_defs\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_PredictionResult\",MName \"Prediction\"], baseName' = FName \"granted_item_defs\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Just (WireTag {getWireTag = 48},WireTag {getWireTag = 50}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Prediction where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Prediction where
  textPut msg
   = do
       P'.tellT "item_def" (item_def msg)
       P'.tellT "num_correct" (num_correct msg)
       P'.tellT "num_fails" (num_fails msg)
       P'.tellT "result" (result msg)
       P'.tellT "granted_item_defs" (granted_item_defs msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'item_def, parse'num_correct, parse'num_fails, parse'result, parse'granted_item_defs])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'item_def
         = P'.try
            (do
               v <- P'.getT "item_def"
               Prelude'.return (\ o -> o{item_def = v}))
        parse'num_correct
         = P'.try
            (do
               v <- P'.getT "num_correct"
               Prelude'.return (\ o -> o{num_correct = v}))
        parse'num_fails
         = P'.try
            (do
               v <- P'.getT "num_fails"
               Prelude'.return (\ o -> o{num_fails = v}))
        parse'result
         = P'.try
            (do
               v <- P'.getT "result"
               Prelude'.return (\ o -> o{result = v}))
        parse'granted_item_defs
         = P'.try
            (do
               v <- P'.getT "granted_item_defs"
               Prelude'.return (\ o -> o{granted_item_defs = P'.append (granted_item_defs o) v}))