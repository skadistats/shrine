{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_PredictionResult.Prediction.EResult (EResult(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EResult = K_eResult_ItemGranted
             | K_eResult_Destroyed
             deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable EResult

instance Prelude'.Bounded EResult where
  minBound = K_eResult_ItemGranted
  maxBound = K_eResult_Destroyed

instance P'.Default EResult where
  defaultValue = K_eResult_ItemGranted

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EResult
toMaybe'Enum 1 = Prelude'.Just K_eResult_ItemGranted
toMaybe'Enum 2 = Prelude'.Just K_eResult_Destroyed
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EResult where
  fromEnum K_eResult_ItemGranted = 1
  fromEnum K_eResult_Destroyed = 2
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_PredictionResult.Prediction.EResult")
      . toMaybe'Enum
  succ K_eResult_ItemGranted = K_eResult_Destroyed
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_PredictionResult.Prediction.EResult"
  pred K_eResult_Destroyed = K_eResult_ItemGranted
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_PredictionResult.Prediction.EResult"

instance P'.Wire EResult where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EResult

instance P'.MessageAPI msg' (msg' -> EResult) EResult where
  getVal m' f' = f' m'

instance P'.ReflectEnum EResult where
  reflectEnum = [(1, "K_eResult_ItemGranted", K_eResult_ItemGranted), (2, "K_eResult_Destroyed", K_eResult_Destroyed)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.CDOTAUserMsg_PredictionResult.Prediction.EResult")
        ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaUserMessages", "CDOTAUserMsg_PredictionResult", "Prediction"]
        "EResult")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "CDOTAUserMsg_PredictionResult", "Prediction", "EResult.hs"]
      [(1, "K_eResult_ItemGranted"), (2, "K_eResult_Destroyed")]

instance P'.TextType EResult where
  tellT = P'.tellShow
  getT = P'.getRead