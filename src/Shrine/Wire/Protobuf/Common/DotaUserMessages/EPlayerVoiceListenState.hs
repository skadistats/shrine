{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.EPlayerVoiceListenState (EPlayerVoiceListenState(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EPlayerVoiceListenState = KPVLS_None
                             | KPVLS_DeniedChatBanned
                             | KPVLS_DeniedPartner
                             | KPVLS_DeniedHLTVTalkerNotSpectator
                             | KPVLS_DeniedHLTVNoTalkerPlayerID
                             | KPVLS_DeniedHLTVTalkerNotBroadcaster
                             | KPVLS_DeniedTeamSpectator
                             | KPVLS_DeniedStudent
                             | KPVLS_Denied
                             | KPVLS_AllowHLTVTalkerIsBroadcaster
                             | KPVLS_AllowCoBroadcaster
                             | KPVLS_AllowAllChat
                             | KPVLS_AllowStudentToCoach
                             | KPVLS_AllowFellowStudent
                             | KPVLS_AllowTalkerIsCoach
                             | KPVLS_AllowCoachHearTeam
                             | KPVLS_AllowSameTeam
                             | KPVLS_AllowShowcase
                             deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                       Prelude'.Generic)

instance P'.Mergeable EPlayerVoiceListenState

instance Prelude'.Bounded EPlayerVoiceListenState where
  minBound = KPVLS_None
  maxBound = KPVLS_AllowShowcase

instance P'.Default EPlayerVoiceListenState where
  defaultValue = KPVLS_None

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EPlayerVoiceListenState
toMaybe'Enum 0 = Prelude'.Just KPVLS_None
toMaybe'Enum 1 = Prelude'.Just KPVLS_DeniedChatBanned
toMaybe'Enum 2 = Prelude'.Just KPVLS_DeniedPartner
toMaybe'Enum 3 = Prelude'.Just KPVLS_DeniedHLTVTalkerNotSpectator
toMaybe'Enum 4 = Prelude'.Just KPVLS_DeniedHLTVNoTalkerPlayerID
toMaybe'Enum 5 = Prelude'.Just KPVLS_DeniedHLTVTalkerNotBroadcaster
toMaybe'Enum 6 = Prelude'.Just KPVLS_DeniedTeamSpectator
toMaybe'Enum 8 = Prelude'.Just KPVLS_DeniedStudent
toMaybe'Enum 64 = Prelude'.Just KPVLS_Denied
toMaybe'Enum 65 = Prelude'.Just KPVLS_AllowHLTVTalkerIsBroadcaster
toMaybe'Enum 66 = Prelude'.Just KPVLS_AllowCoBroadcaster
toMaybe'Enum 67 = Prelude'.Just KPVLS_AllowAllChat
toMaybe'Enum 68 = Prelude'.Just KPVLS_AllowStudentToCoach
toMaybe'Enum 69 = Prelude'.Just KPVLS_AllowFellowStudent
toMaybe'Enum 70 = Prelude'.Just KPVLS_AllowTalkerIsCoach
toMaybe'Enum 71 = Prelude'.Just KPVLS_AllowCoachHearTeam
toMaybe'Enum 72 = Prelude'.Just KPVLS_AllowSameTeam
toMaybe'Enum 73 = Prelude'.Just KPVLS_AllowShowcase
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EPlayerVoiceListenState where
  fromEnum KPVLS_None = 0
  fromEnum KPVLS_DeniedChatBanned = 1
  fromEnum KPVLS_DeniedPartner = 2
  fromEnum KPVLS_DeniedHLTVTalkerNotSpectator = 3
  fromEnum KPVLS_DeniedHLTVNoTalkerPlayerID = 4
  fromEnum KPVLS_DeniedHLTVTalkerNotBroadcaster = 5
  fromEnum KPVLS_DeniedTeamSpectator = 6
  fromEnum KPVLS_DeniedStudent = 8
  fromEnum KPVLS_Denied = 64
  fromEnum KPVLS_AllowHLTVTalkerIsBroadcaster = 65
  fromEnum KPVLS_AllowCoBroadcaster = 66
  fromEnum KPVLS_AllowAllChat = 67
  fromEnum KPVLS_AllowStudentToCoach = 68
  fromEnum KPVLS_AllowFellowStudent = 69
  fromEnum KPVLS_AllowTalkerIsCoach = 70
  fromEnum KPVLS_AllowCoachHearTeam = 71
  fromEnum KPVLS_AllowSameTeam = 72
  fromEnum KPVLS_AllowShowcase = 73
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EPlayerVoiceListenState")
      . toMaybe'Enum
  succ KPVLS_None = KPVLS_DeniedChatBanned
  succ KPVLS_DeniedChatBanned = KPVLS_DeniedPartner
  succ KPVLS_DeniedPartner = KPVLS_DeniedHLTVTalkerNotSpectator
  succ KPVLS_DeniedHLTVTalkerNotSpectator = KPVLS_DeniedHLTVNoTalkerPlayerID
  succ KPVLS_DeniedHLTVNoTalkerPlayerID = KPVLS_DeniedHLTVTalkerNotBroadcaster
  succ KPVLS_DeniedHLTVTalkerNotBroadcaster = KPVLS_DeniedTeamSpectator
  succ KPVLS_DeniedTeamSpectator = KPVLS_DeniedStudent
  succ KPVLS_DeniedStudent = KPVLS_Denied
  succ KPVLS_Denied = KPVLS_AllowHLTVTalkerIsBroadcaster
  succ KPVLS_AllowHLTVTalkerIsBroadcaster = KPVLS_AllowCoBroadcaster
  succ KPVLS_AllowCoBroadcaster = KPVLS_AllowAllChat
  succ KPVLS_AllowAllChat = KPVLS_AllowStudentToCoach
  succ KPVLS_AllowStudentToCoach = KPVLS_AllowFellowStudent
  succ KPVLS_AllowFellowStudent = KPVLS_AllowTalkerIsCoach
  succ KPVLS_AllowTalkerIsCoach = KPVLS_AllowCoachHearTeam
  succ KPVLS_AllowCoachHearTeam = KPVLS_AllowSameTeam
  succ KPVLS_AllowSameTeam = KPVLS_AllowShowcase
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EPlayerVoiceListenState"
  pred KPVLS_DeniedChatBanned = KPVLS_None
  pred KPVLS_DeniedPartner = KPVLS_DeniedChatBanned
  pred KPVLS_DeniedHLTVTalkerNotSpectator = KPVLS_DeniedPartner
  pred KPVLS_DeniedHLTVNoTalkerPlayerID = KPVLS_DeniedHLTVTalkerNotSpectator
  pred KPVLS_DeniedHLTVTalkerNotBroadcaster = KPVLS_DeniedHLTVNoTalkerPlayerID
  pred KPVLS_DeniedTeamSpectator = KPVLS_DeniedHLTVTalkerNotBroadcaster
  pred KPVLS_DeniedStudent = KPVLS_DeniedTeamSpectator
  pred KPVLS_Denied = KPVLS_DeniedStudent
  pred KPVLS_AllowHLTVTalkerIsBroadcaster = KPVLS_Denied
  pred KPVLS_AllowCoBroadcaster = KPVLS_AllowHLTVTalkerIsBroadcaster
  pred KPVLS_AllowAllChat = KPVLS_AllowCoBroadcaster
  pred KPVLS_AllowStudentToCoach = KPVLS_AllowAllChat
  pred KPVLS_AllowFellowStudent = KPVLS_AllowStudentToCoach
  pred KPVLS_AllowTalkerIsCoach = KPVLS_AllowFellowStudent
  pred KPVLS_AllowCoachHearTeam = KPVLS_AllowTalkerIsCoach
  pred KPVLS_AllowSameTeam = KPVLS_AllowCoachHearTeam
  pred KPVLS_AllowShowcase = KPVLS_AllowSameTeam
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EPlayerVoiceListenState"

instance P'.Wire EPlayerVoiceListenState where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EPlayerVoiceListenState

instance P'.MessageAPI msg' (msg' -> EPlayerVoiceListenState) EPlayerVoiceListenState where
  getVal m' f' = f' m'

instance P'.ReflectEnum EPlayerVoiceListenState where
  reflectEnum
   = [(0, "KPVLS_None", KPVLS_None), (1, "KPVLS_DeniedChatBanned", KPVLS_DeniedChatBanned),
      (2, "KPVLS_DeniedPartner", KPVLS_DeniedPartner),
      (3, "KPVLS_DeniedHLTVTalkerNotSpectator", KPVLS_DeniedHLTVTalkerNotSpectator),
      (4, "KPVLS_DeniedHLTVNoTalkerPlayerID", KPVLS_DeniedHLTVNoTalkerPlayerID),
      (5, "KPVLS_DeniedHLTVTalkerNotBroadcaster", KPVLS_DeniedHLTVTalkerNotBroadcaster),
      (6, "KPVLS_DeniedTeamSpectator", KPVLS_DeniedTeamSpectator), (8, "KPVLS_DeniedStudent", KPVLS_DeniedStudent),
      (64, "KPVLS_Denied", KPVLS_Denied), (65, "KPVLS_AllowHLTVTalkerIsBroadcaster", KPVLS_AllowHLTVTalkerIsBroadcaster),
      (66, "KPVLS_AllowCoBroadcaster", KPVLS_AllowCoBroadcaster), (67, "KPVLS_AllowAllChat", KPVLS_AllowAllChat),
      (68, "KPVLS_AllowStudentToCoach", KPVLS_AllowStudentToCoach), (69, "KPVLS_AllowFellowStudent", KPVLS_AllowFellowStudent),
      (70, "KPVLS_AllowTalkerIsCoach", KPVLS_AllowTalkerIsCoach), (71, "KPVLS_AllowCoachHearTeam", KPVLS_AllowCoachHearTeam),
      (72, "KPVLS_AllowSameTeam", KPVLS_AllowSameTeam), (73, "KPVLS_AllowShowcase", KPVLS_AllowShowcase)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.EPlayerVoiceListenState") ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaUserMessages"]
        "EPlayerVoiceListenState")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "EPlayerVoiceListenState.hs"]
      [(0, "KPVLS_None"), (1, "KPVLS_DeniedChatBanned"), (2, "KPVLS_DeniedPartner"), (3, "KPVLS_DeniedHLTVTalkerNotSpectator"),
       (4, "KPVLS_DeniedHLTVNoTalkerPlayerID"), (5, "KPVLS_DeniedHLTVTalkerNotBroadcaster"), (6, "KPVLS_DeniedTeamSpectator"),
       (8, "KPVLS_DeniedStudent"), (64, "KPVLS_Denied"), (65, "KPVLS_AllowHLTVTalkerIsBroadcaster"),
       (66, "KPVLS_AllowCoBroadcaster"), (67, "KPVLS_AllowAllChat"), (68, "KPVLS_AllowStudentToCoach"),
       (69, "KPVLS_AllowFellowStudent"), (70, "KPVLS_AllowTalkerIsCoach"), (71, "KPVLS_AllowCoachHearTeam"),
       (72, "KPVLS_AllowSameTeam"), (73, "KPVLS_AllowShowcase")]

instance P'.TextType EPlayerVoiceListenState where
  tellT = P'.tellShow
  getT = P'.getRead