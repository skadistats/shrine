{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.EHeroStatType (EHeroStatType(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EHeroStatType = K_EHeroStatType_None
                   | K_EHeroStatType_AxeTotalDamage
                   | K_EHeroStatType_BattleHungerDamage
                   | K_EHeroStatType_CounterHelixDamage
                   | K_EHeroStatType_CullingBladeDamage
                   | K_EHeroStatType_BerserkersCallCastCount
                   | K_EHeroStatType_BerserkersCallHeroesHitAverage
                   | K_EHeroStatType_BerserkersCallOtherUnitsHit
                   | K_EHeroStatType_BerserkersCallHeroAttacksTaken
                   | K_EHeroStatType_BerserkersCallOtherAttacksTaken
                   | K_EHeroStatType_BattleHungerCastCount
                   | K_EHeroStatType_BattleHungerPotentialDuration
                   | K_EHeroStatType_BattleHungerAverageDuration
                   | K_EHeroStatType_CounterHelixProcCount
                   | K_EHeroStatType_CounterHelixHeroProcCount
                   | K_EHeroStatType_CounterHelixHeroesHitAverage
                   | K_EHeroStatType_CounterHelixOtherUnitsHitCount
                   | K_EHeroStatType_CullingBladeCastCount
                   | K_EHeroStatType_CullingBladeKillCount
                   | K_EHeroStatType_CullingBladeAverageHealthCulled
                   | K_EHeroStatType_CullingBladeAverageDamageAvailable
                   | K_EHeroStatType_CullingBladeHeroBuffAverage
                   deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                             Prelude'.Generic)

instance P'.Mergeable EHeroStatType

instance Prelude'.Bounded EHeroStatType where
  minBound = K_EHeroStatType_None
  maxBound = K_EHeroStatType_CullingBladeHeroBuffAverage

instance P'.Default EHeroStatType where
  defaultValue = K_EHeroStatType_None

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EHeroStatType
toMaybe'Enum 0 = Prelude'.Just K_EHeroStatType_None
toMaybe'Enum 2000 = Prelude'.Just K_EHeroStatType_AxeTotalDamage
toMaybe'Enum 2001 = Prelude'.Just K_EHeroStatType_BattleHungerDamage
toMaybe'Enum 2002 = Prelude'.Just K_EHeroStatType_CounterHelixDamage
toMaybe'Enum 2003 = Prelude'.Just K_EHeroStatType_CullingBladeDamage
toMaybe'Enum 2004 = Prelude'.Just K_EHeroStatType_BerserkersCallCastCount
toMaybe'Enum 2005 = Prelude'.Just K_EHeroStatType_BerserkersCallHeroesHitAverage
toMaybe'Enum 2006 = Prelude'.Just K_EHeroStatType_BerserkersCallOtherUnitsHit
toMaybe'Enum 2007 = Prelude'.Just K_EHeroStatType_BerserkersCallHeroAttacksTaken
toMaybe'Enum 2008 = Prelude'.Just K_EHeroStatType_BerserkersCallOtherAttacksTaken
toMaybe'Enum 2009 = Prelude'.Just K_EHeroStatType_BattleHungerCastCount
toMaybe'Enum 2010 = Prelude'.Just K_EHeroStatType_BattleHungerPotentialDuration
toMaybe'Enum 2011 = Prelude'.Just K_EHeroStatType_BattleHungerAverageDuration
toMaybe'Enum 2012 = Prelude'.Just K_EHeroStatType_CounterHelixProcCount
toMaybe'Enum 2013 = Prelude'.Just K_EHeroStatType_CounterHelixHeroProcCount
toMaybe'Enum 2014 = Prelude'.Just K_EHeroStatType_CounterHelixHeroesHitAverage
toMaybe'Enum 2015 = Prelude'.Just K_EHeroStatType_CounterHelixOtherUnitsHitCount
toMaybe'Enum 2016 = Prelude'.Just K_EHeroStatType_CullingBladeCastCount
toMaybe'Enum 2017 = Prelude'.Just K_EHeroStatType_CullingBladeKillCount
toMaybe'Enum 2018 = Prelude'.Just K_EHeroStatType_CullingBladeAverageHealthCulled
toMaybe'Enum 2019 = Prelude'.Just K_EHeroStatType_CullingBladeAverageDamageAvailable
toMaybe'Enum 2020 = Prelude'.Just K_EHeroStatType_CullingBladeHeroBuffAverage
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EHeroStatType where
  fromEnum K_EHeroStatType_None = 0
  fromEnum K_EHeroStatType_AxeTotalDamage = 2000
  fromEnum K_EHeroStatType_BattleHungerDamage = 2001
  fromEnum K_EHeroStatType_CounterHelixDamage = 2002
  fromEnum K_EHeroStatType_CullingBladeDamage = 2003
  fromEnum K_EHeroStatType_BerserkersCallCastCount = 2004
  fromEnum K_EHeroStatType_BerserkersCallHeroesHitAverage = 2005
  fromEnum K_EHeroStatType_BerserkersCallOtherUnitsHit = 2006
  fromEnum K_EHeroStatType_BerserkersCallHeroAttacksTaken = 2007
  fromEnum K_EHeroStatType_BerserkersCallOtherAttacksTaken = 2008
  fromEnum K_EHeroStatType_BattleHungerCastCount = 2009
  fromEnum K_EHeroStatType_BattleHungerPotentialDuration = 2010
  fromEnum K_EHeroStatType_BattleHungerAverageDuration = 2011
  fromEnum K_EHeroStatType_CounterHelixProcCount = 2012
  fromEnum K_EHeroStatType_CounterHelixHeroProcCount = 2013
  fromEnum K_EHeroStatType_CounterHelixHeroesHitAverage = 2014
  fromEnum K_EHeroStatType_CounterHelixOtherUnitsHitCount = 2015
  fromEnum K_EHeroStatType_CullingBladeCastCount = 2016
  fromEnum K_EHeroStatType_CullingBladeKillCount = 2017
  fromEnum K_EHeroStatType_CullingBladeAverageHealthCulled = 2018
  fromEnum K_EHeroStatType_CullingBladeAverageDamageAvailable = 2019
  fromEnum K_EHeroStatType_CullingBladeHeroBuffAverage = 2020
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EHeroStatType")
      . toMaybe'Enum
  succ K_EHeroStatType_None = K_EHeroStatType_AxeTotalDamage
  succ K_EHeroStatType_AxeTotalDamage = K_EHeroStatType_BattleHungerDamage
  succ K_EHeroStatType_BattleHungerDamage = K_EHeroStatType_CounterHelixDamage
  succ K_EHeroStatType_CounterHelixDamage = K_EHeroStatType_CullingBladeDamage
  succ K_EHeroStatType_CullingBladeDamage = K_EHeroStatType_BerserkersCallCastCount
  succ K_EHeroStatType_BerserkersCallCastCount = K_EHeroStatType_BerserkersCallHeroesHitAverage
  succ K_EHeroStatType_BerserkersCallHeroesHitAverage = K_EHeroStatType_BerserkersCallOtherUnitsHit
  succ K_EHeroStatType_BerserkersCallOtherUnitsHit = K_EHeroStatType_BerserkersCallHeroAttacksTaken
  succ K_EHeroStatType_BerserkersCallHeroAttacksTaken = K_EHeroStatType_BerserkersCallOtherAttacksTaken
  succ K_EHeroStatType_BerserkersCallOtherAttacksTaken = K_EHeroStatType_BattleHungerCastCount
  succ K_EHeroStatType_BattleHungerCastCount = K_EHeroStatType_BattleHungerPotentialDuration
  succ K_EHeroStatType_BattleHungerPotentialDuration = K_EHeroStatType_BattleHungerAverageDuration
  succ K_EHeroStatType_BattleHungerAverageDuration = K_EHeroStatType_CounterHelixProcCount
  succ K_EHeroStatType_CounterHelixProcCount = K_EHeroStatType_CounterHelixHeroProcCount
  succ K_EHeroStatType_CounterHelixHeroProcCount = K_EHeroStatType_CounterHelixHeroesHitAverage
  succ K_EHeroStatType_CounterHelixHeroesHitAverage = K_EHeroStatType_CounterHelixOtherUnitsHitCount
  succ K_EHeroStatType_CounterHelixOtherUnitsHitCount = K_EHeroStatType_CullingBladeCastCount
  succ K_EHeroStatType_CullingBladeCastCount = K_EHeroStatType_CullingBladeKillCount
  succ K_EHeroStatType_CullingBladeKillCount = K_EHeroStatType_CullingBladeAverageHealthCulled
  succ K_EHeroStatType_CullingBladeAverageHealthCulled = K_EHeroStatType_CullingBladeAverageDamageAvailable
  succ K_EHeroStatType_CullingBladeAverageDamageAvailable = K_EHeroStatType_CullingBladeHeroBuffAverage
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EHeroStatType"
  pred K_EHeroStatType_AxeTotalDamage = K_EHeroStatType_None
  pred K_EHeroStatType_BattleHungerDamage = K_EHeroStatType_AxeTotalDamage
  pred K_EHeroStatType_CounterHelixDamage = K_EHeroStatType_BattleHungerDamage
  pred K_EHeroStatType_CullingBladeDamage = K_EHeroStatType_CounterHelixDamage
  pred K_EHeroStatType_BerserkersCallCastCount = K_EHeroStatType_CullingBladeDamage
  pred K_EHeroStatType_BerserkersCallHeroesHitAverage = K_EHeroStatType_BerserkersCallCastCount
  pred K_EHeroStatType_BerserkersCallOtherUnitsHit = K_EHeroStatType_BerserkersCallHeroesHitAverage
  pred K_EHeroStatType_BerserkersCallHeroAttacksTaken = K_EHeroStatType_BerserkersCallOtherUnitsHit
  pred K_EHeroStatType_BerserkersCallOtherAttacksTaken = K_EHeroStatType_BerserkersCallHeroAttacksTaken
  pred K_EHeroStatType_BattleHungerCastCount = K_EHeroStatType_BerserkersCallOtherAttacksTaken
  pred K_EHeroStatType_BattleHungerPotentialDuration = K_EHeroStatType_BattleHungerCastCount
  pred K_EHeroStatType_BattleHungerAverageDuration = K_EHeroStatType_BattleHungerPotentialDuration
  pred K_EHeroStatType_CounterHelixProcCount = K_EHeroStatType_BattleHungerAverageDuration
  pred K_EHeroStatType_CounterHelixHeroProcCount = K_EHeroStatType_CounterHelixProcCount
  pred K_EHeroStatType_CounterHelixHeroesHitAverage = K_EHeroStatType_CounterHelixHeroProcCount
  pred K_EHeroStatType_CounterHelixOtherUnitsHitCount = K_EHeroStatType_CounterHelixHeroesHitAverage
  pred K_EHeroStatType_CullingBladeCastCount = K_EHeroStatType_CounterHelixOtherUnitsHitCount
  pred K_EHeroStatType_CullingBladeKillCount = K_EHeroStatType_CullingBladeCastCount
  pred K_EHeroStatType_CullingBladeAverageHealthCulled = K_EHeroStatType_CullingBladeKillCount
  pred K_EHeroStatType_CullingBladeAverageDamageAvailable = K_EHeroStatType_CullingBladeAverageHealthCulled
  pred K_EHeroStatType_CullingBladeHeroBuffAverage = K_EHeroStatType_CullingBladeAverageDamageAvailable
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EHeroStatType"

instance P'.Wire EHeroStatType where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EHeroStatType

instance P'.MessageAPI msg' (msg' -> EHeroStatType) EHeroStatType where
  getVal m' f' = f' m'

instance P'.ReflectEnum EHeroStatType where
  reflectEnum
   = [(0, "K_EHeroStatType_None", K_EHeroStatType_None), (2000, "K_EHeroStatType_AxeTotalDamage", K_EHeroStatType_AxeTotalDamage),
      (2001, "K_EHeroStatType_BattleHungerDamage", K_EHeroStatType_BattleHungerDamage),
      (2002, "K_EHeroStatType_CounterHelixDamage", K_EHeroStatType_CounterHelixDamage),
      (2003, "K_EHeroStatType_CullingBladeDamage", K_EHeroStatType_CullingBladeDamage),
      (2004, "K_EHeroStatType_BerserkersCallCastCount", K_EHeroStatType_BerserkersCallCastCount),
      (2005, "K_EHeroStatType_BerserkersCallHeroesHitAverage", K_EHeroStatType_BerserkersCallHeroesHitAverage),
      (2006, "K_EHeroStatType_BerserkersCallOtherUnitsHit", K_EHeroStatType_BerserkersCallOtherUnitsHit),
      (2007, "K_EHeroStatType_BerserkersCallHeroAttacksTaken", K_EHeroStatType_BerserkersCallHeroAttacksTaken),
      (2008, "K_EHeroStatType_BerserkersCallOtherAttacksTaken", K_EHeroStatType_BerserkersCallOtherAttacksTaken),
      (2009, "K_EHeroStatType_BattleHungerCastCount", K_EHeroStatType_BattleHungerCastCount),
      (2010, "K_EHeroStatType_BattleHungerPotentialDuration", K_EHeroStatType_BattleHungerPotentialDuration),
      (2011, "K_EHeroStatType_BattleHungerAverageDuration", K_EHeroStatType_BattleHungerAverageDuration),
      (2012, "K_EHeroStatType_CounterHelixProcCount", K_EHeroStatType_CounterHelixProcCount),
      (2013, "K_EHeroStatType_CounterHelixHeroProcCount", K_EHeroStatType_CounterHelixHeroProcCount),
      (2014, "K_EHeroStatType_CounterHelixHeroesHitAverage", K_EHeroStatType_CounterHelixHeroesHitAverage),
      (2015, "K_EHeroStatType_CounterHelixOtherUnitsHitCount", K_EHeroStatType_CounterHelixOtherUnitsHitCount),
      (2016, "K_EHeroStatType_CullingBladeCastCount", K_EHeroStatType_CullingBladeCastCount),
      (2017, "K_EHeroStatType_CullingBladeKillCount", K_EHeroStatType_CullingBladeKillCount),
      (2018, "K_EHeroStatType_CullingBladeAverageHealthCulled", K_EHeroStatType_CullingBladeAverageHealthCulled),
      (2019, "K_EHeroStatType_CullingBladeAverageDamageAvailable", K_EHeroStatType_CullingBladeAverageDamageAvailable),
      (2020, "K_EHeroStatType_CullingBladeHeroBuffAverage", K_EHeroStatType_CullingBladeHeroBuffAverage)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.EHeroStatType") ["Shrine", "Wire", "Protobuf", "Common"] ["DotaUserMessages"]
        "EHeroStatType")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "EHeroStatType.hs"]
      [(0, "K_EHeroStatType_None"), (2000, "K_EHeroStatType_AxeTotalDamage"), (2001, "K_EHeroStatType_BattleHungerDamage"),
       (2002, "K_EHeroStatType_CounterHelixDamage"), (2003, "K_EHeroStatType_CullingBladeDamage"),
       (2004, "K_EHeroStatType_BerserkersCallCastCount"), (2005, "K_EHeroStatType_BerserkersCallHeroesHitAverage"),
       (2006, "K_EHeroStatType_BerserkersCallOtherUnitsHit"), (2007, "K_EHeroStatType_BerserkersCallHeroAttacksTaken"),
       (2008, "K_EHeroStatType_BerserkersCallOtherAttacksTaken"), (2009, "K_EHeroStatType_BattleHungerCastCount"),
       (2010, "K_EHeroStatType_BattleHungerPotentialDuration"), (2011, "K_EHeroStatType_BattleHungerAverageDuration"),
       (2012, "K_EHeroStatType_CounterHelixProcCount"), (2013, "K_EHeroStatType_CounterHelixHeroProcCount"),
       (2014, "K_EHeroStatType_CounterHelixHeroesHitAverage"), (2015, "K_EHeroStatType_CounterHelixOtherUnitsHitCount"),
       (2016, "K_EHeroStatType_CullingBladeCastCount"), (2017, "K_EHeroStatType_CullingBladeKillCount"),
       (2018, "K_EHeroStatType_CullingBladeAverageHealthCulled"), (2019, "K_EHeroStatType_CullingBladeAverageDamageAvailable"),
       (2020, "K_EHeroStatType_CullingBladeHeroBuffAverage")]

instance P'.TextType EHeroStatType where
  tellT = P'.tellShow
  getT = P'.getRead