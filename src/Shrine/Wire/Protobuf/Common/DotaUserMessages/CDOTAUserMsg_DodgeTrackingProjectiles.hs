{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_DodgeTrackingProjectiles
       (CDOTAUserMsg_DodgeTrackingProjectiles(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTAUserMsg_DodgeTrackingProjectiles = CDOTAUserMsg_DodgeTrackingProjectiles{entindex :: !(P'.Int32),
                                                                                   attacks_only :: !(P'.Maybe P'.Bool)}
                                           deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                     Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_DodgeTrackingProjectiles where
  mergeAppend (CDOTAUserMsg_DodgeTrackingProjectiles x'1 x'2) (CDOTAUserMsg_DodgeTrackingProjectiles y'1 y'2)
   = CDOTAUserMsg_DodgeTrackingProjectiles (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CDOTAUserMsg_DodgeTrackingProjectiles where
  defaultValue = CDOTAUserMsg_DodgeTrackingProjectiles P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAUserMsg_DodgeTrackingProjectiles where
  wireSize ft' self'@(CDOTAUserMsg_DodgeTrackingProjectiles x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeReq 1 5 x'1 + P'.wireSizeOpt 1 8 x'2)
  wirePut ft' self'@(CDOTAUserMsg_DodgeTrackingProjectiles x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutReq 8 5 x'1
             P'.wirePutOpt 16 8 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{entindex = new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{attacks_only = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_DodgeTrackingProjectiles) CDOTAUserMsg_DodgeTrackingProjectiles where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_DodgeTrackingProjectiles

instance P'.ReflectDescriptor CDOTAUserMsg_DodgeTrackingProjectiles where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList [8]) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_DodgeTrackingProjectiles\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_DodgeTrackingProjectiles\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_DodgeTrackingProjectiles.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_DodgeTrackingProjectiles.entindex\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_DodgeTrackingProjectiles\"], baseName' = FName \"entindex\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_DodgeTrackingProjectiles.attacks_only\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_DodgeTrackingProjectiles\"], baseName' = FName \"attacks_only\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_DodgeTrackingProjectiles where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_DodgeTrackingProjectiles where
  textPut msg
   = do
       P'.tellT "entindex" (entindex msg)
       P'.tellT "attacks_only" (attacks_only msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'entindex, parse'attacks_only]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'entindex
         = P'.try
            (do
               v <- P'.getT "entindex"
               Prelude'.return (\ o -> o{entindex = v}))
        parse'attacks_only
         = P'.try
            (do
               v <- P'.getT "attacks_only"
               Prelude'.return (\ o -> o{attacks_only = v}))