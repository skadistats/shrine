{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_StatsKillDetails (CDOTAUserMsg_StatsKillDetails(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_StatsPlayerKillShare as DotaUserMessages
       (CDOTAUserMsg_StatsPlayerKillShare)

data CDOTAUserMsg_StatsKillDetails = CDOTAUserMsg_StatsKillDetails{victim_id :: !(P'.Maybe P'.Word32),
                                                                   kill_shares ::
                                                                   !(P'.Seq DotaUserMessages.CDOTAUserMsg_StatsPlayerKillShare),
                                                                   damage_to_kill :: !(P'.Maybe P'.Word32),
                                                                   effective_health :: !(P'.Maybe P'.Word32),
                                                                   death_time :: !(P'.Maybe P'.Float),
                                                                   killer_id :: !(P'.Maybe P'.Word32)}
                                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                             Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_StatsKillDetails where
  mergeAppend (CDOTAUserMsg_StatsKillDetails x'1 x'2 x'3 x'4 x'5 x'6) (CDOTAUserMsg_StatsKillDetails y'1 y'2 y'3 y'4 y'5 y'6)
   = CDOTAUserMsg_StatsKillDetails (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)
      (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CDOTAUserMsg_StatsKillDetails where
  defaultValue
   = CDOTAUserMsg_StatsKillDetails P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAUserMsg_StatsKillDetails where
  wireSize ft' self'@(CDOTAUserMsg_StatsKillDetails x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeRep 1 11 x'2 + P'.wireSizeOpt 1 13 x'3 + P'.wireSizeOpt 1 13 x'4 +
             P'.wireSizeOpt 1 2 x'5
             + P'.wireSizeOpt 1 13 x'6)
  wirePut ft' self'@(CDOTAUserMsg_StatsKillDetails x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutRep 18 11 x'2
             P'.wirePutOpt 24 13 x'3
             P'.wirePutOpt 32 13 x'4
             P'.wirePutOpt 45 2 x'5
             P'.wirePutOpt 48 13 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{victim_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{kill_shares = P'.append (kill_shares old'Self) new'Field})
                    (P'.wireGet 11)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{damage_to_kill = Prelude'.Just new'Field}) (P'.wireGet 13)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{effective_health = Prelude'.Just new'Field}) (P'.wireGet 13)
             45 -> Prelude'.fmap (\ !new'Field -> old'Self{death_time = Prelude'.Just new'Field}) (P'.wireGet 2)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{killer_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_StatsKillDetails) CDOTAUserMsg_StatsKillDetails where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_StatsKillDetails

instance P'.ReflectDescriptor CDOTAUserMsg_StatsKillDetails where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18, 24, 32, 45, 48])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsKillDetails\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_StatsKillDetails\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_StatsKillDetails.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsKillDetails.victim_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsKillDetails\"], baseName' = FName \"victim_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsKillDetails.kill_shares\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsKillDetails\"], baseName' = FName \"kill_shares\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsPlayerKillShare\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_StatsPlayerKillShare\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsKillDetails.damage_to_kill\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsKillDetails\"], baseName' = FName \"damage_to_kill\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsKillDetails.effective_health\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsKillDetails\"], baseName' = FName \"effective_health\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsKillDetails.death_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsKillDetails\"], baseName' = FName \"death_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 45}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsKillDetails.killer_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsKillDetails\"], baseName' = FName \"killer_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_StatsKillDetails where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_StatsKillDetails where
  textPut msg
   = do
       P'.tellT "victim_id" (victim_id msg)
       P'.tellT "kill_shares" (kill_shares msg)
       P'.tellT "damage_to_kill" (damage_to_kill msg)
       P'.tellT "effective_health" (effective_health msg)
       P'.tellT "death_time" (death_time msg)
       P'.tellT "killer_id" (killer_id msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'victim_id, parse'kill_shares, parse'damage_to_kill, parse'effective_health, parse'death_time,
                   parse'killer_id])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'victim_id
         = P'.try
            (do
               v <- P'.getT "victim_id"
               Prelude'.return (\ o -> o{victim_id = v}))
        parse'kill_shares
         = P'.try
            (do
               v <- P'.getT "kill_shares"
               Prelude'.return (\ o -> o{kill_shares = P'.append (kill_shares o) v}))
        parse'damage_to_kill
         = P'.try
            (do
               v <- P'.getT "damage_to_kill"
               Prelude'.return (\ o -> o{damage_to_kill = v}))
        parse'effective_health
         = P'.try
            (do
               v <- P'.getT "effective_health"
               Prelude'.return (\ o -> o{effective_health = v}))
        parse'death_time
         = P'.try
            (do
               v <- P'.getT "death_time"
               Prelude'.return (\ o -> o{death_time = v}))
        parse'killer_id
         = P'.try
            (do
               v <- P'.getT "killer_id"
               Prelude'.return (\ o -> o{killer_id = v}))