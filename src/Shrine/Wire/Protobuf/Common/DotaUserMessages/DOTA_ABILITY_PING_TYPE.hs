{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ABILITY_PING_TYPE (DOTA_ABILITY_PING_TYPE(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data DOTA_ABILITY_PING_TYPE = ABILITY_PING_READY
                            | ABILITY_PING_MANA
                            | ABILITY_PING_COOLDOWN
                            | ABILITY_PING_ENEMY
                            | ABILITY_PING_UNLEARNED
                            | ABILITY_PING_INBACKPACK
                            deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                      Prelude'.Generic)

instance P'.Mergeable DOTA_ABILITY_PING_TYPE

instance Prelude'.Bounded DOTA_ABILITY_PING_TYPE where
  minBound = ABILITY_PING_READY
  maxBound = ABILITY_PING_INBACKPACK

instance P'.Default DOTA_ABILITY_PING_TYPE where
  defaultValue = ABILITY_PING_READY

toMaybe'Enum :: Prelude'.Int -> P'.Maybe DOTA_ABILITY_PING_TYPE
toMaybe'Enum 1 = Prelude'.Just ABILITY_PING_READY
toMaybe'Enum 2 = Prelude'.Just ABILITY_PING_MANA
toMaybe'Enum 3 = Prelude'.Just ABILITY_PING_COOLDOWN
toMaybe'Enum 4 = Prelude'.Just ABILITY_PING_ENEMY
toMaybe'Enum 5 = Prelude'.Just ABILITY_PING_UNLEARNED
toMaybe'Enum 6 = Prelude'.Just ABILITY_PING_INBACKPACK
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum DOTA_ABILITY_PING_TYPE where
  fromEnum ABILITY_PING_READY = 1
  fromEnum ABILITY_PING_MANA = 2
  fromEnum ABILITY_PING_COOLDOWN = 3
  fromEnum ABILITY_PING_ENEMY = 4
  fromEnum ABILITY_PING_UNLEARNED = 5
  fromEnum ABILITY_PING_INBACKPACK = 6
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ABILITY_PING_TYPE")
      . toMaybe'Enum
  succ ABILITY_PING_READY = ABILITY_PING_MANA
  succ ABILITY_PING_MANA = ABILITY_PING_COOLDOWN
  succ ABILITY_PING_COOLDOWN = ABILITY_PING_ENEMY
  succ ABILITY_PING_ENEMY = ABILITY_PING_UNLEARNED
  succ ABILITY_PING_UNLEARNED = ABILITY_PING_INBACKPACK
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ABILITY_PING_TYPE"
  pred ABILITY_PING_MANA = ABILITY_PING_READY
  pred ABILITY_PING_COOLDOWN = ABILITY_PING_MANA
  pred ABILITY_PING_ENEMY = ABILITY_PING_COOLDOWN
  pred ABILITY_PING_UNLEARNED = ABILITY_PING_ENEMY
  pred ABILITY_PING_INBACKPACK = ABILITY_PING_UNLEARNED
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ABILITY_PING_TYPE"

instance P'.Wire DOTA_ABILITY_PING_TYPE where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB DOTA_ABILITY_PING_TYPE

instance P'.MessageAPI msg' (msg' -> DOTA_ABILITY_PING_TYPE) DOTA_ABILITY_PING_TYPE where
  getVal m' f' = f' m'

instance P'.ReflectEnum DOTA_ABILITY_PING_TYPE where
  reflectEnum
   = [(1, "ABILITY_PING_READY", ABILITY_PING_READY), (2, "ABILITY_PING_MANA", ABILITY_PING_MANA),
      (3, "ABILITY_PING_COOLDOWN", ABILITY_PING_COOLDOWN), (4, "ABILITY_PING_ENEMY", ABILITY_PING_ENEMY),
      (5, "ABILITY_PING_UNLEARNED", ABILITY_PING_UNLEARNED), (6, "ABILITY_PING_INBACKPACK", ABILITY_PING_INBACKPACK)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.DOTA_ABILITY_PING_TYPE") ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaUserMessages"]
        "DOTA_ABILITY_PING_TYPE")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "DOTA_ABILITY_PING_TYPE.hs"]
      [(1, "ABILITY_PING_READY"), (2, "ABILITY_PING_MANA"), (3, "ABILITY_PING_COOLDOWN"), (4, "ABILITY_PING_ENEMY"),
       (5, "ABILITY_PING_UNLEARNED"), (6, "ABILITY_PING_INBACKPACK")]

instance P'.TextType DOTA_ABILITY_PING_TYPE where
  tellT = P'.tellShow
  getT = P'.getRead