{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_REPLAY_STATE_EVENT (DOTA_REPLAY_STATE_EVENT(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data DOTA_REPLAY_STATE_EVENT = DOTA_REPLAY_STATE_EVENT_GAME_START
                             | DOTA_REPLAY_STATE_EVENT_STARTING_HORN
                             | DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD
                             | DOTA_REPLAY_STATE_EVENT_SHOWCASE
                             deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                       Prelude'.Generic)

instance P'.Mergeable DOTA_REPLAY_STATE_EVENT

instance Prelude'.Bounded DOTA_REPLAY_STATE_EVENT where
  minBound = DOTA_REPLAY_STATE_EVENT_GAME_START
  maxBound = DOTA_REPLAY_STATE_EVENT_SHOWCASE

instance P'.Default DOTA_REPLAY_STATE_EVENT where
  defaultValue = DOTA_REPLAY_STATE_EVENT_GAME_START

toMaybe'Enum :: Prelude'.Int -> P'.Maybe DOTA_REPLAY_STATE_EVENT
toMaybe'Enum 1 = Prelude'.Just DOTA_REPLAY_STATE_EVENT_GAME_START
toMaybe'Enum 2 = Prelude'.Just DOTA_REPLAY_STATE_EVENT_STARTING_HORN
toMaybe'Enum 3 = Prelude'.Just DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD
toMaybe'Enum 4 = Prelude'.Just DOTA_REPLAY_STATE_EVENT_SHOWCASE
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum DOTA_REPLAY_STATE_EVENT where
  fromEnum DOTA_REPLAY_STATE_EVENT_GAME_START = 1
  fromEnum DOTA_REPLAY_STATE_EVENT_STARTING_HORN = 2
  fromEnum DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD = 3
  fromEnum DOTA_REPLAY_STATE_EVENT_SHOWCASE = 4
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_REPLAY_STATE_EVENT")
      . toMaybe'Enum
  succ DOTA_REPLAY_STATE_EVENT_GAME_START = DOTA_REPLAY_STATE_EVENT_STARTING_HORN
  succ DOTA_REPLAY_STATE_EVENT_STARTING_HORN = DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD
  succ DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD = DOTA_REPLAY_STATE_EVENT_SHOWCASE
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_REPLAY_STATE_EVENT"
  pred DOTA_REPLAY_STATE_EVENT_STARTING_HORN = DOTA_REPLAY_STATE_EVENT_GAME_START
  pred DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD = DOTA_REPLAY_STATE_EVENT_STARTING_HORN
  pred DOTA_REPLAY_STATE_EVENT_SHOWCASE = DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_REPLAY_STATE_EVENT"

instance P'.Wire DOTA_REPLAY_STATE_EVENT where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB DOTA_REPLAY_STATE_EVENT

instance P'.MessageAPI msg' (msg' -> DOTA_REPLAY_STATE_EVENT) DOTA_REPLAY_STATE_EVENT where
  getVal m' f' = f' m'

instance P'.ReflectEnum DOTA_REPLAY_STATE_EVENT where
  reflectEnum
   = [(1, "DOTA_REPLAY_STATE_EVENT_GAME_START", DOTA_REPLAY_STATE_EVENT_GAME_START),
      (2, "DOTA_REPLAY_STATE_EVENT_STARTING_HORN", DOTA_REPLAY_STATE_EVENT_STARTING_HORN),
      (3, "DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD", DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD),
      (4, "DOTA_REPLAY_STATE_EVENT_SHOWCASE", DOTA_REPLAY_STATE_EVENT_SHOWCASE)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.DOTA_REPLAY_STATE_EVENT") ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaUserMessages"]
        "DOTA_REPLAY_STATE_EVENT")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "DOTA_REPLAY_STATE_EVENT.hs"]
      [(1, "DOTA_REPLAY_STATE_EVENT_GAME_START"), (2, "DOTA_REPLAY_STATE_EVENT_STARTING_HORN"),
       (3, "DOTA_REPLAY_STATE_EVENT_FIRST_BLOOD"), (4, "DOTA_REPLAY_STATE_EVENT_SHOWCASE")]

instance P'.TextType DOTA_REPLAY_STATE_EVENT where
  tellT = P'.tellShow
  getT = P'.getRead