{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ABILITY_TARGET_TYPE (DOTA_ABILITY_TARGET_TYPE(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data DOTA_ABILITY_TARGET_TYPE = DOTA_ABILITY_TARGET_NONE
                              | DOTA_ABILITY_TARGET_SELF
                              | DOTA_ABILITY_TARGET_ALLY_HERO
                              | DOTA_ABILITY_TARGET_ALLY_CREEP
                              | DOTA_ABILITY_TARGET_ENEMY_HERO
                              | DOTA_ABILITY_TARGET_ENEMY_CREEP
                              deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                        Prelude'.Generic)

instance P'.Mergeable DOTA_ABILITY_TARGET_TYPE

instance Prelude'.Bounded DOTA_ABILITY_TARGET_TYPE where
  minBound = DOTA_ABILITY_TARGET_NONE
  maxBound = DOTA_ABILITY_TARGET_ENEMY_CREEP

instance P'.Default DOTA_ABILITY_TARGET_TYPE where
  defaultValue = DOTA_ABILITY_TARGET_NONE

toMaybe'Enum :: Prelude'.Int -> P'.Maybe DOTA_ABILITY_TARGET_TYPE
toMaybe'Enum 0 = Prelude'.Just DOTA_ABILITY_TARGET_NONE
toMaybe'Enum 1 = Prelude'.Just DOTA_ABILITY_TARGET_SELF
toMaybe'Enum 2 = Prelude'.Just DOTA_ABILITY_TARGET_ALLY_HERO
toMaybe'Enum 3 = Prelude'.Just DOTA_ABILITY_TARGET_ALLY_CREEP
toMaybe'Enum 4 = Prelude'.Just DOTA_ABILITY_TARGET_ENEMY_HERO
toMaybe'Enum 5 = Prelude'.Just DOTA_ABILITY_TARGET_ENEMY_CREEP
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum DOTA_ABILITY_TARGET_TYPE where
  fromEnum DOTA_ABILITY_TARGET_NONE = 0
  fromEnum DOTA_ABILITY_TARGET_SELF = 1
  fromEnum DOTA_ABILITY_TARGET_ALLY_HERO = 2
  fromEnum DOTA_ABILITY_TARGET_ALLY_CREEP = 3
  fromEnum DOTA_ABILITY_TARGET_ENEMY_HERO = 4
  fromEnum DOTA_ABILITY_TARGET_ENEMY_CREEP = 5
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ABILITY_TARGET_TYPE")
      . toMaybe'Enum
  succ DOTA_ABILITY_TARGET_NONE = DOTA_ABILITY_TARGET_SELF
  succ DOTA_ABILITY_TARGET_SELF = DOTA_ABILITY_TARGET_ALLY_HERO
  succ DOTA_ABILITY_TARGET_ALLY_HERO = DOTA_ABILITY_TARGET_ALLY_CREEP
  succ DOTA_ABILITY_TARGET_ALLY_CREEP = DOTA_ABILITY_TARGET_ENEMY_HERO
  succ DOTA_ABILITY_TARGET_ENEMY_HERO = DOTA_ABILITY_TARGET_ENEMY_CREEP
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ABILITY_TARGET_TYPE"
  pred DOTA_ABILITY_TARGET_SELF = DOTA_ABILITY_TARGET_NONE
  pred DOTA_ABILITY_TARGET_ALLY_HERO = DOTA_ABILITY_TARGET_SELF
  pred DOTA_ABILITY_TARGET_ALLY_CREEP = DOTA_ABILITY_TARGET_ALLY_HERO
  pred DOTA_ABILITY_TARGET_ENEMY_HERO = DOTA_ABILITY_TARGET_ALLY_CREEP
  pred DOTA_ABILITY_TARGET_ENEMY_CREEP = DOTA_ABILITY_TARGET_ENEMY_HERO
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ABILITY_TARGET_TYPE"

instance P'.Wire DOTA_ABILITY_TARGET_TYPE where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB DOTA_ABILITY_TARGET_TYPE

instance P'.MessageAPI msg' (msg' -> DOTA_ABILITY_TARGET_TYPE) DOTA_ABILITY_TARGET_TYPE where
  getVal m' f' = f' m'

instance P'.ReflectEnum DOTA_ABILITY_TARGET_TYPE where
  reflectEnum
   = [(0, "DOTA_ABILITY_TARGET_NONE", DOTA_ABILITY_TARGET_NONE), (1, "DOTA_ABILITY_TARGET_SELF", DOTA_ABILITY_TARGET_SELF),
      (2, "DOTA_ABILITY_TARGET_ALLY_HERO", DOTA_ABILITY_TARGET_ALLY_HERO),
      (3, "DOTA_ABILITY_TARGET_ALLY_CREEP", DOTA_ABILITY_TARGET_ALLY_CREEP),
      (4, "DOTA_ABILITY_TARGET_ENEMY_HERO", DOTA_ABILITY_TARGET_ENEMY_HERO),
      (5, "DOTA_ABILITY_TARGET_ENEMY_CREEP", DOTA_ABILITY_TARGET_ENEMY_CREEP)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.DOTA_ABILITY_TARGET_TYPE") ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaUserMessages"]
        "DOTA_ABILITY_TARGET_TYPE")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "DOTA_ABILITY_TARGET_TYPE.hs"]
      [(0, "DOTA_ABILITY_TARGET_NONE"), (1, "DOTA_ABILITY_TARGET_SELF"), (2, "DOTA_ABILITY_TARGET_ALLY_HERO"),
       (3, "DOTA_ABILITY_TARGET_ALLY_CREEP"), (4, "DOTA_ABILITY_TARGET_ENEMY_HERO"), (5, "DOTA_ABILITY_TARGET_ENEMY_CREEP")]

instance P'.TextType DOTA_ABILITY_TARGET_TYPE where
  tellT = P'.tellShow
  getT = P'.getRead