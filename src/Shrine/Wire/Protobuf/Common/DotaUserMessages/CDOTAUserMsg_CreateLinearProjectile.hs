{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_CreateLinearProjectile (CDOTAUserMsg_CreateLinearProjectile(..))
       where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgVector2D as NetworkBaseTypes (CMsgVector2D)

data CDOTAUserMsg_CreateLinearProjectile = CDOTAUserMsg_CreateLinearProjectile{origin :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                                                               velocity ::
                                                                               !(P'.Maybe NetworkBaseTypes.CMsgVector2D),
                                                                               latency :: !(P'.Maybe P'.Int32),
                                                                               entindex :: !(P'.Maybe P'.Int32),
                                                                               particle_index :: !(P'.Maybe P'.Word64),
                                                                               handle :: !(P'.Maybe P'.Int32),
                                                                               acceleration ::
                                                                               !(P'.Maybe NetworkBaseTypes.CMsgVector2D),
                                                                               max_speed :: !(P'.Maybe P'.Float),
                                                                               fow_radius :: !(P'.Maybe P'.Float),
                                                                               sticky_fow_reveal :: !(P'.Maybe P'.Bool)}
                                         deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                   Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_CreateLinearProjectile where
  mergeAppend (CDOTAUserMsg_CreateLinearProjectile x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   (CDOTAUserMsg_CreateLinearProjectile y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10)
   = CDOTAUserMsg_CreateLinearProjectile (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)
      (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)

instance P'.Default CDOTAUserMsg_CreateLinearProjectile where
  defaultValue
   = CDOTAUserMsg_CreateLinearProjectile P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAUserMsg_CreateLinearProjectile where
  wireSize ft' self'@(CDOTAUserMsg_CreateLinearProjectile x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 11 x'1 + P'.wireSizeOpt 1 11 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeOpt 1 5 x'4 +
             P'.wireSizeOpt 1 4 x'5
             + P'.wireSizeOpt 1 5 x'6
             + P'.wireSizeOpt 1 11 x'7
             + P'.wireSizeOpt 1 2 x'8
             + P'.wireSizeOpt 1 2 x'9
             + P'.wireSizeOpt 1 8 x'10)
  wirePut ft' self'@(CDOTAUserMsg_CreateLinearProjectile x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 11 x'1
             P'.wirePutOpt 18 11 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutOpt 32 5 x'4
             P'.wirePutOpt 40 4 x'5
             P'.wirePutOpt 48 5 x'6
             P'.wirePutOpt 58 11 x'7
             P'.wirePutOpt 69 2 x'8
             P'.wirePutOpt 77 2 x'9
             P'.wirePutOpt 80 8 x'10
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{origin = P'.mergeAppend (origin old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{velocity = P'.mergeAppend (velocity old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{latency = Prelude'.Just new'Field}) (P'.wireGet 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{entindex = Prelude'.Just new'Field}) (P'.wireGet 5)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{particle_index = Prelude'.Just new'Field}) (P'.wireGet 4)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{handle = Prelude'.Just new'Field}) (P'.wireGet 5)
             58 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{acceleration = P'.mergeAppend (acceleration old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             69 -> Prelude'.fmap (\ !new'Field -> old'Self{max_speed = Prelude'.Just new'Field}) (P'.wireGet 2)
             77 -> Prelude'.fmap (\ !new'Field -> old'Self{fow_radius = Prelude'.Just new'Field}) (P'.wireGet 2)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{sticky_fow_reveal = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_CreateLinearProjectile) CDOTAUserMsg_CreateLinearProjectile where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_CreateLinearProjectile

instance P'.ReflectDescriptor CDOTAUserMsg_CreateLinearProjectile where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18, 24, 32, 40, 48, 58, 69, 77, 80])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_CreateLinearProjectile\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_CreateLinearProjectile.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.origin\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"origin\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.velocity\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"velocity\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector2D\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector2D\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.latency\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"latency\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.entindex\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"entindex\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.particle_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"particle_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.handle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"handle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.acceleration\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"acceleration\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 58}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector2D\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector2D\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.max_speed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"max_speed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 69}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.fow_radius\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"fow_radius\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 77}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_CreateLinearProjectile.sticky_fow_reveal\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_CreateLinearProjectile\"], baseName' = FName \"sticky_fow_reveal\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_CreateLinearProjectile where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_CreateLinearProjectile where
  textPut msg
   = do
       P'.tellT "origin" (origin msg)
       P'.tellT "velocity" (velocity msg)
       P'.tellT "latency" (latency msg)
       P'.tellT "entindex" (entindex msg)
       P'.tellT "particle_index" (particle_index msg)
       P'.tellT "handle" (handle msg)
       P'.tellT "acceleration" (acceleration msg)
       P'.tellT "max_speed" (max_speed msg)
       P'.tellT "fow_radius" (fow_radius msg)
       P'.tellT "sticky_fow_reveal" (sticky_fow_reveal msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'origin, parse'velocity, parse'latency, parse'entindex, parse'particle_index, parse'handle,
                   parse'acceleration, parse'max_speed, parse'fow_radius, parse'sticky_fow_reveal])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'origin
         = P'.try
            (do
               v <- P'.getT "origin"
               Prelude'.return (\ o -> o{origin = v}))
        parse'velocity
         = P'.try
            (do
               v <- P'.getT "velocity"
               Prelude'.return (\ o -> o{velocity = v}))
        parse'latency
         = P'.try
            (do
               v <- P'.getT "latency"
               Prelude'.return (\ o -> o{latency = v}))
        parse'entindex
         = P'.try
            (do
               v <- P'.getT "entindex"
               Prelude'.return (\ o -> o{entindex = v}))
        parse'particle_index
         = P'.try
            (do
               v <- P'.getT "particle_index"
               Prelude'.return (\ o -> o{particle_index = v}))
        parse'handle
         = P'.try
            (do
               v <- P'.getT "handle"
               Prelude'.return (\ o -> o{handle = v}))
        parse'acceleration
         = P'.try
            (do
               v <- P'.getT "acceleration"
               Prelude'.return (\ o -> o{acceleration = v}))
        parse'max_speed
         = P'.try
            (do
               v <- P'.getT "max_speed"
               Prelude'.return (\ o -> o{max_speed = v}))
        parse'fow_radius
         = P'.try
            (do
               v <- P'.getT "fow_radius"
               Prelude'.return (\ o -> o{fow_radius = v}))
        parse'sticky_fow_reveal
         = P'.try
            (do
               v <- P'.getT "sticky_fow_reveal"
               Prelude'.return (\ o -> o{sticky_fow_reveal = v}))