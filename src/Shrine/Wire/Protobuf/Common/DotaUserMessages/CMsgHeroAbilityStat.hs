{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CMsgHeroAbilityStat (CMsgHeroAbilityStat(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.EHeroStatType as DotaUserMessages (EHeroStatType)

data CMsgHeroAbilityStat = CMsgHeroAbilityStat{stat_type :: !(P'.Maybe DotaUserMessages.EHeroStatType),
                                               int_value :: !(P'.Maybe P'.Int32), float_value :: !(P'.Maybe P'.Float)}
                         deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgHeroAbilityStat where
  mergeAppend (CMsgHeroAbilityStat x'1 x'2 x'3) (CMsgHeroAbilityStat y'1 y'2 y'3)
   = CMsgHeroAbilityStat (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default CMsgHeroAbilityStat where
  defaultValue = CMsgHeroAbilityStat (Prelude'.Just (Prelude'.read "K_EHeroStatType_None")) P'.defaultValue P'.defaultValue

instance P'.Wire CMsgHeroAbilityStat where
  wireSize ft' self'@(CMsgHeroAbilityStat x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 14 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 2 x'3)
  wirePut ft' self'@(CMsgHeroAbilityStat x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 14 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 29 2 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{stat_type = Prelude'.Just new'Field}) (P'.wireGet 14)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{int_value = Prelude'.Just new'Field}) (P'.wireGet 5)
             29 -> Prelude'.fmap (\ !new'Field -> old'Self{float_value = Prelude'.Just new'Field}) (P'.wireGet 2)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgHeroAbilityStat) CMsgHeroAbilityStat where
  getVal m' f' = f' m'

instance P'.GPB CMsgHeroAbilityStat

instance P'.ReflectDescriptor CMsgHeroAbilityStat where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 29])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CMsgHeroAbilityStat\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CMsgHeroAbilityStat\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CMsgHeroAbilityStat.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgHeroAbilityStat.stat_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgHeroAbilityStat\"], baseName' = FName \"stat_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.EHeroStatType\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"EHeroStatType\"}), hsRawDefault = Just \"k_EHeroStatType_None\", hsDefault = Just (HsDef'Enum \"K_EHeroStatType_None\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgHeroAbilityStat.int_value\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgHeroAbilityStat\"], baseName' = FName \"int_value\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgHeroAbilityStat.float_value\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgHeroAbilityStat\"], baseName' = FName \"float_value\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 29}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgHeroAbilityStat where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgHeroAbilityStat where
  textPut msg
   = do
       P'.tellT "stat_type" (stat_type msg)
       P'.tellT "int_value" (int_value msg)
       P'.tellT "float_value" (float_value msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'stat_type, parse'int_value, parse'float_value]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'stat_type
         = P'.try
            (do
               v <- P'.getT "stat_type"
               Prelude'.return (\ o -> o{stat_type = v}))
        parse'int_value
         = P'.try
            (do
               v <- P'.getT "int_value"
               Prelude'.return (\ o -> o{int_value = v}))
        parse'float_value
         = P'.try
            (do
               v <- P'.getT "float_value"
               Prelude'.return (\ o -> o{float_value = v}))