{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ChatEvent (CDOTAUserMsg_ChatEvent(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_CHAT_MESSAGE as DotaUserMessages (DOTA_CHAT_MESSAGE)

data CDOTAUserMsg_ChatEvent = CDOTAUserMsg_ChatEvent{type' :: !(P'.Maybe DotaUserMessages.DOTA_CHAT_MESSAGE),
                                                     value :: !(P'.Maybe P'.Word32), playerid_1 :: !(P'.Maybe P'.Int32),
                                                     playerid_2 :: !(P'.Maybe P'.Int32), playerid_3 :: !(P'.Maybe P'.Int32),
                                                     playerid_4 :: !(P'.Maybe P'.Int32), playerid_5 :: !(P'.Maybe P'.Int32),
                                                     playerid_6 :: !(P'.Maybe P'.Int32), value2 :: !(P'.Maybe P'.Word32),
                                                     value3 :: !(P'.Maybe P'.Word32)}
                            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_ChatEvent where
  mergeAppend (CDOTAUserMsg_ChatEvent x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   (CDOTAUserMsg_ChatEvent y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10)
   = CDOTAUserMsg_ChatEvent (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)

instance P'.Default CDOTAUserMsg_ChatEvent where
  defaultValue
   = CDOTAUserMsg_ChatEvent (Prelude'.Just (Prelude'.read "CHAT_MESSAGE_INVALID")) P'.defaultValue (Prelude'.Just (-1))
      (Prelude'.Just (-1))
      (Prelude'.Just (-1))
      (Prelude'.Just (-1))
      (Prelude'.Just (-1))
      (Prelude'.Just (-1))
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAUserMsg_ChatEvent where
  wireSize ft' self'@(CDOTAUserMsg_ChatEvent x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 14 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 17 x'3 + P'.wireSizeOpt 1 17 x'4 +
             P'.wireSizeOpt 1 17 x'5
             + P'.wireSizeOpt 1 17 x'6
             + P'.wireSizeOpt 1 17 x'7
             + P'.wireSizeOpt 1 17 x'8
             + P'.wireSizeOpt 1 13 x'9
             + P'.wireSizeOpt 1 13 x'10)
  wirePut ft' self'@(CDOTAUserMsg_ChatEvent x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 14 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 17 x'3
             P'.wirePutOpt 32 17 x'4
             P'.wirePutOpt 40 17 x'5
             P'.wirePutOpt 48 17 x'6
             P'.wirePutOpt 56 17 x'7
             P'.wirePutOpt 64 17 x'8
             P'.wirePutOpt 72 13 x'9
             P'.wirePutOpt 80 13 x'10
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{type' = Prelude'.Just new'Field}) (P'.wireGet 14)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{value = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{playerid_1 = Prelude'.Just new'Field}) (P'.wireGet 17)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{playerid_2 = Prelude'.Just new'Field}) (P'.wireGet 17)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{playerid_3 = Prelude'.Just new'Field}) (P'.wireGet 17)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{playerid_4 = Prelude'.Just new'Field}) (P'.wireGet 17)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{playerid_5 = Prelude'.Just new'Field}) (P'.wireGet 17)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{playerid_6 = Prelude'.Just new'Field}) (P'.wireGet 17)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{value2 = Prelude'.Just new'Field}) (P'.wireGet 13)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{value3 = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_ChatEvent) CDOTAUserMsg_ChatEvent where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_ChatEvent

instance P'.ReflectDescriptor CDOTAUserMsg_ChatEvent where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 32, 40, 48, 56, 64, 72, 80])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_ChatEvent\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_ChatEvent.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"type'\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.DOTA_CHAT_MESSAGE\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"DOTA_CHAT_MESSAGE\"}), hsRawDefault = Just \"CHAT_MESSAGE_INVALID\", hsDefault = Just (HsDef'Enum \"CHAT_MESSAGE_INVALID\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.value\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"value\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.playerid_1\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"playerid_1\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Just \"-1\", hsDefault = Just (HsDef'Integer (-1))},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.playerid_2\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"playerid_2\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Just \"-1\", hsDefault = Just (HsDef'Integer (-1))},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.playerid_3\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"playerid_3\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Just \"-1\", hsDefault = Just (HsDef'Integer (-1))},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.playerid_4\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"playerid_4\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Just \"-1\", hsDefault = Just (HsDef'Integer (-1))},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.playerid_5\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"playerid_5\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Just \"-1\", hsDefault = Just (HsDef'Integer (-1))},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.playerid_6\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"playerid_6\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Just \"-1\", hsDefault = Just (HsDef'Integer (-1))},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.value2\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"value2\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ChatEvent.value3\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ChatEvent\"], baseName' = FName \"value3\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_ChatEvent where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_ChatEvent where
  textPut msg
   = do
       P'.tellT "type" (type' msg)
       P'.tellT "value" (value msg)
       P'.tellT "playerid_1" (playerid_1 msg)
       P'.tellT "playerid_2" (playerid_2 msg)
       P'.tellT "playerid_3" (playerid_3 msg)
       P'.tellT "playerid_4" (playerid_4 msg)
       P'.tellT "playerid_5" (playerid_5 msg)
       P'.tellT "playerid_6" (playerid_6 msg)
       P'.tellT "value2" (value2 msg)
       P'.tellT "value3" (value3 msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'type', parse'value, parse'playerid_1, parse'playerid_2, parse'playerid_3, parse'playerid_4,
                   parse'playerid_5, parse'playerid_6, parse'value2, parse'value3])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'type'
         = P'.try
            (do
               v <- P'.getT "type"
               Prelude'.return (\ o -> o{type' = v}))
        parse'value
         = P'.try
            (do
               v <- P'.getT "value"
               Prelude'.return (\ o -> o{value = v}))
        parse'playerid_1
         = P'.try
            (do
               v <- P'.getT "playerid_1"
               Prelude'.return (\ o -> o{playerid_1 = v}))
        parse'playerid_2
         = P'.try
            (do
               v <- P'.getT "playerid_2"
               Prelude'.return (\ o -> o{playerid_2 = v}))
        parse'playerid_3
         = P'.try
            (do
               v <- P'.getT "playerid_3"
               Prelude'.return (\ o -> o{playerid_3 = v}))
        parse'playerid_4
         = P'.try
            (do
               v <- P'.getT "playerid_4"
               Prelude'.return (\ o -> o{playerid_4 = v}))
        parse'playerid_5
         = P'.try
            (do
               v <- P'.getT "playerid_5"
               Prelude'.return (\ o -> o{playerid_5 = v}))
        parse'playerid_6
         = P'.try
            (do
               v <- P'.getT "playerid_6"
               Prelude'.return (\ o -> o{playerid_6 = v}))
        parse'value2
         = P'.try
            (do
               v <- P'.getT "value2"
               Prelude'.return (\ o -> o{value2 = v}))
        parse'value3
         = P'.try
            (do
               v <- P'.getT "value3"
               Prelude'.return (\ o -> o{value3 = v}))