{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ProjectionAbility (CDOTAUserMsg_ProjectionAbility(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CDOTAUserMsg_ProjectionAbility = CDOTAUserMsg_ProjectionAbility{ability_id :: !(P'.Maybe P'.Word32),
                                                                     caster_ent_index :: !(P'.Maybe P'.Int32),
                                                                     caster_team :: !(P'.Maybe P'.Int32),
                                                                     channel_end :: !(P'.Maybe P'.Bool),
                                                                     origin :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                                                     track_caster_only :: !(P'.Maybe P'.Bool),
                                                                     end_time :: !(P'.Maybe P'.Float),
                                                                     victim_ent_index :: !(P'.Maybe P'.Int32)}
                                    deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                              Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_ProjectionAbility where
  mergeAppend (CDOTAUserMsg_ProjectionAbility x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   (CDOTAUserMsg_ProjectionAbility y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8)
   = CDOTAUserMsg_ProjectionAbility (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)
      (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)

instance P'.Default CDOTAUserMsg_ProjectionAbility where
  defaultValue
   = CDOTAUserMsg_ProjectionAbility P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAUserMsg_ProjectionAbility where
  wireSize ft' self'@(CDOTAUserMsg_ProjectionAbility x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeOpt 1 8 x'4 +
             P'.wireSizeOpt 1 11 x'5
             + P'.wireSizeOpt 1 8 x'6
             + P'.wireSizeOpt 1 2 x'7
             + P'.wireSizeOpt 1 5 x'8)
  wirePut ft' self'@(CDOTAUserMsg_ProjectionAbility x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutOpt 32 8 x'4
             P'.wirePutOpt 42 11 x'5
             P'.wirePutOpt 48 8 x'6
             P'.wirePutOpt 61 2 x'7
             P'.wirePutOpt 64 5 x'8
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{ability_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{caster_ent_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{caster_team = Prelude'.Just new'Field}) (P'.wireGet 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{channel_end = Prelude'.Just new'Field}) (P'.wireGet 8)
             42 -> Prelude'.fmap (\ !new'Field -> old'Self{origin = P'.mergeAppend (origin old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{track_caster_only = Prelude'.Just new'Field}) (P'.wireGet 8)
             61 -> Prelude'.fmap (\ !new'Field -> old'Self{end_time = Prelude'.Just new'Field}) (P'.wireGet 2)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{victim_ent_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_ProjectionAbility) CDOTAUserMsg_ProjectionAbility where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_ProjectionAbility

instance P'.ReflectDescriptor CDOTAUserMsg_ProjectionAbility where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 32, 42, 48, 61, 64])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_ProjectionAbility\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_ProjectionAbility\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_ProjectionAbility.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ProjectionAbility.ability_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ProjectionAbility\"], baseName' = FName \"ability_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ProjectionAbility.caster_ent_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ProjectionAbility\"], baseName' = FName \"caster_ent_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ProjectionAbility.caster_team\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ProjectionAbility\"], baseName' = FName \"caster_team\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ProjectionAbility.channel_end\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ProjectionAbility\"], baseName' = FName \"channel_end\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ProjectionAbility.origin\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ProjectionAbility\"], baseName' = FName \"origin\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ProjectionAbility.track_caster_only\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ProjectionAbility\"], baseName' = FName \"track_caster_only\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ProjectionAbility.end_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ProjectionAbility\"], baseName' = FName \"end_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 61}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ProjectionAbility.victim_ent_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ProjectionAbility\"], baseName' = FName \"victim_ent_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_ProjectionAbility where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_ProjectionAbility where
  textPut msg
   = do
       P'.tellT "ability_id" (ability_id msg)
       P'.tellT "caster_ent_index" (caster_ent_index msg)
       P'.tellT "caster_team" (caster_team msg)
       P'.tellT "channel_end" (channel_end msg)
       P'.tellT "origin" (origin msg)
       P'.tellT "track_caster_only" (track_caster_only msg)
       P'.tellT "end_time" (end_time msg)
       P'.tellT "victim_ent_index" (victim_ent_index msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'ability_id, parse'caster_ent_index, parse'caster_team, parse'channel_end, parse'origin,
                   parse'track_caster_only, parse'end_time, parse'victim_ent_index])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'ability_id
         = P'.try
            (do
               v <- P'.getT "ability_id"
               Prelude'.return (\ o -> o{ability_id = v}))
        parse'caster_ent_index
         = P'.try
            (do
               v <- P'.getT "caster_ent_index"
               Prelude'.return (\ o -> o{caster_ent_index = v}))
        parse'caster_team
         = P'.try
            (do
               v <- P'.getT "caster_team"
               Prelude'.return (\ o -> o{caster_team = v}))
        parse'channel_end
         = P'.try
            (do
               v <- P'.getT "channel_end"
               Prelude'.return (\ o -> o{channel_end = v}))
        parse'origin
         = P'.try
            (do
               v <- P'.getT "origin"
               Prelude'.return (\ o -> o{origin = v}))
        parse'track_caster_only
         = P'.try
            (do
               v <- P'.getT "track_caster_only"
               Prelude'.return (\ o -> o{track_caster_only = v}))
        parse'end_time
         = P'.try
            (do
               v <- P'.getT "end_time"
               Prelude'.return (\ o -> o{end_time = v}))
        parse'victim_ent_index
         = P'.try
            (do
               v <- P'.getT "victim_ent_index"
               Prelude'.return (\ o -> o{victim_ent_index = v}))