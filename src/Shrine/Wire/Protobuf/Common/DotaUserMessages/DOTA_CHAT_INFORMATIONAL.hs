{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_CHAT_INFORMATIONAL (DOTA_CHAT_INFORMATIONAL(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data DOTA_CHAT_INFORMATIONAL = INFO_COOP_BATTLE_POINTS_RULES
                             | INFO_FROSTIVUS_ABANDON_REMINDER
                             | INFO_RANKED_REMINDER
                             | INFO_COOP_LOW_PRIORITY_PASSIVE_REMINDER
                             deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                       Prelude'.Generic)

instance P'.Mergeable DOTA_CHAT_INFORMATIONAL

instance Prelude'.Bounded DOTA_CHAT_INFORMATIONAL where
  minBound = INFO_COOP_BATTLE_POINTS_RULES
  maxBound = INFO_COOP_LOW_PRIORITY_PASSIVE_REMINDER

instance P'.Default DOTA_CHAT_INFORMATIONAL where
  defaultValue = INFO_COOP_BATTLE_POINTS_RULES

toMaybe'Enum :: Prelude'.Int -> P'.Maybe DOTA_CHAT_INFORMATIONAL
toMaybe'Enum 1 = Prelude'.Just INFO_COOP_BATTLE_POINTS_RULES
toMaybe'Enum 2 = Prelude'.Just INFO_FROSTIVUS_ABANDON_REMINDER
toMaybe'Enum 3 = Prelude'.Just INFO_RANKED_REMINDER
toMaybe'Enum 4 = Prelude'.Just INFO_COOP_LOW_PRIORITY_PASSIVE_REMINDER
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum DOTA_CHAT_INFORMATIONAL where
  fromEnum INFO_COOP_BATTLE_POINTS_RULES = 1
  fromEnum INFO_FROSTIVUS_ABANDON_REMINDER = 2
  fromEnum INFO_RANKED_REMINDER = 3
  fromEnum INFO_COOP_LOW_PRIORITY_PASSIVE_REMINDER = 4
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_CHAT_INFORMATIONAL")
      . toMaybe'Enum
  succ INFO_COOP_BATTLE_POINTS_RULES = INFO_FROSTIVUS_ABANDON_REMINDER
  succ INFO_FROSTIVUS_ABANDON_REMINDER = INFO_RANKED_REMINDER
  succ INFO_RANKED_REMINDER = INFO_COOP_LOW_PRIORITY_PASSIVE_REMINDER
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_CHAT_INFORMATIONAL"
  pred INFO_FROSTIVUS_ABANDON_REMINDER = INFO_COOP_BATTLE_POINTS_RULES
  pred INFO_RANKED_REMINDER = INFO_FROSTIVUS_ABANDON_REMINDER
  pred INFO_COOP_LOW_PRIORITY_PASSIVE_REMINDER = INFO_RANKED_REMINDER
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_CHAT_INFORMATIONAL"

instance P'.Wire DOTA_CHAT_INFORMATIONAL where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB DOTA_CHAT_INFORMATIONAL

instance P'.MessageAPI msg' (msg' -> DOTA_CHAT_INFORMATIONAL) DOTA_CHAT_INFORMATIONAL where
  getVal m' f' = f' m'

instance P'.ReflectEnum DOTA_CHAT_INFORMATIONAL where
  reflectEnum
   = [(1, "INFO_COOP_BATTLE_POINTS_RULES", INFO_COOP_BATTLE_POINTS_RULES),
      (2, "INFO_FROSTIVUS_ABANDON_REMINDER", INFO_FROSTIVUS_ABANDON_REMINDER), (3, "INFO_RANKED_REMINDER", INFO_RANKED_REMINDER),
      (4, "INFO_COOP_LOW_PRIORITY_PASSIVE_REMINDER", INFO_COOP_LOW_PRIORITY_PASSIVE_REMINDER)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.DOTA_CHAT_INFORMATIONAL") ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaUserMessages"]
        "DOTA_CHAT_INFORMATIONAL")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "DOTA_CHAT_INFORMATIONAL.hs"]
      [(1, "INFO_COOP_BATTLE_POINTS_RULES"), (2, "INFO_FROSTIVUS_ABANDON_REMINDER"), (3, "INFO_RANKED_REMINDER"),
       (4, "INFO_COOP_LOW_PRIORITY_PASSIVE_REMINDER")]

instance P'.TextType DOTA_CHAT_INFORMATIONAL where
  tellT = P'.tellShow
  getT = P'.getRead