{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CMsgCombatAnalyzerPlayerStat (CMsgCombatAnalyzerPlayerStat(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CMsgHeroAbilityStat as DotaUserMessages (CMsgHeroAbilityStat)

data CMsgCombatAnalyzerPlayerStat = CMsgCombatAnalyzerPlayerStat{account_id :: !(P'.Maybe P'.Word32),
                                                                 hero_ability_stats ::
                                                                 !(P'.Seq DotaUserMessages.CMsgHeroAbilityStat)}
                                  deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                            Prelude'.Generic)

instance P'.Mergeable CMsgCombatAnalyzerPlayerStat where
  mergeAppend (CMsgCombatAnalyzerPlayerStat x'1 x'2) (CMsgCombatAnalyzerPlayerStat y'1 y'2)
   = CMsgCombatAnalyzerPlayerStat (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMsgCombatAnalyzerPlayerStat where
  defaultValue = CMsgCombatAnalyzerPlayerStat P'.defaultValue P'.defaultValue

instance P'.Wire CMsgCombatAnalyzerPlayerStat where
  wireSize ft' self'@(CMsgCombatAnalyzerPlayerStat x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeRep 1 11 x'2)
  wirePut ft' self'@(CMsgCombatAnalyzerPlayerStat x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutRep 18 11 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{account_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{hero_ability_stats = P'.append (hero_ability_stats old'Self) new'Field})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgCombatAnalyzerPlayerStat) CMsgCombatAnalyzerPlayerStat where
  getVal m' f' = f' m'

instance P'.GPB CMsgCombatAnalyzerPlayerStat

instance P'.ReflectDescriptor CMsgCombatAnalyzerPlayerStat where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CMsgCombatAnalyzerPlayerStat\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CMsgCombatAnalyzerPlayerStat\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CMsgCombatAnalyzerPlayerStat.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgCombatAnalyzerPlayerStat.account_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgCombatAnalyzerPlayerStat\"], baseName' = FName \"account_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgCombatAnalyzerPlayerStat.hero_ability_stats\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgCombatAnalyzerPlayerStat\"], baseName' = FName \"hero_ability_stats\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CMsgHeroAbilityStat\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CMsgHeroAbilityStat\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgCombatAnalyzerPlayerStat where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgCombatAnalyzerPlayerStat where
  textPut msg
   = do
       P'.tellT "account_id" (account_id msg)
       P'.tellT "hero_ability_stats" (hero_ability_stats msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'account_id, parse'hero_ability_stats]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'account_id
         = P'.try
            (do
               v <- P'.getT "account_id"
               Prelude'.return (\ o -> o{account_id = v}))
        parse'hero_ability_stats
         = P'.try
            (do
               v <- P'.getT "hero_ability_stats"
               Prelude'.return (\ o -> o{hero_ability_stats = P'.append (hero_ability_stats o) v}))