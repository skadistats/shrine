{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTASpeechMatchOnClient (CDOTASpeechMatchOnClient(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAResponseQuerySerialized as DotaUserMessages
       (CDOTAResponseQuerySerialized)

data CDOTASpeechMatchOnClient = CDOTASpeechMatchOnClient{concept :: !(P'.Maybe P'.Int32), recipient_type :: !(P'.Maybe P'.Int32),
                                                         responsequery :: !(P'.Maybe DotaUserMessages.CDOTAResponseQuerySerialized),
                                                         randomseed :: !(P'.Maybe P'.Int32)}
                              deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                        Prelude'.Generic)

instance P'.Mergeable CDOTASpeechMatchOnClient where
  mergeAppend (CDOTASpeechMatchOnClient x'1 x'2 x'3 x'4) (CDOTASpeechMatchOnClient y'1 y'2 y'3 y'4)
   = CDOTASpeechMatchOnClient (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)

instance P'.Default CDOTASpeechMatchOnClient where
  defaultValue = CDOTASpeechMatchOnClient P'.defaultValue P'.defaultValue P'.defaultValue (Prelude'.Just 0)

instance P'.Wire CDOTASpeechMatchOnClient where
  wireSize ft' self'@(CDOTASpeechMatchOnClient x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 11 x'3 + P'.wireSizeOpt 1 15 x'4)
  wirePut ft' self'@(CDOTASpeechMatchOnClient x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 26 11 x'3
             P'.wirePutOpt 37 15 x'4
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{concept = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{recipient_type = Prelude'.Just new'Field}) (P'.wireGet 5)
             26 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{responsequery = P'.mergeAppend (responsequery old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             37 -> Prelude'.fmap (\ !new'Field -> old'Self{randomseed = Prelude'.Just new'Field}) (P'.wireGet 15)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTASpeechMatchOnClient) CDOTASpeechMatchOnClient where
  getVal m' f' = f' m'

instance P'.GPB CDOTASpeechMatchOnClient

instance P'.ReflectDescriptor CDOTASpeechMatchOnClient where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 26, 37])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTASpeechMatchOnClient\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTASpeechMatchOnClient\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTASpeechMatchOnClient.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTASpeechMatchOnClient.concept\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTASpeechMatchOnClient\"], baseName' = FName \"concept\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTASpeechMatchOnClient.recipient_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTASpeechMatchOnClient\"], baseName' = FName \"recipient_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTASpeechMatchOnClient.responsequery\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTASpeechMatchOnClient\"], baseName' = FName \"responsequery\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAResponseQuerySerialized\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTASpeechMatchOnClient.randomseed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTASpeechMatchOnClient\"], baseName' = FName \"randomseed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 37}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 15}, typeName = Nothing, hsRawDefault = Just \"0\", hsDefault = Just (HsDef'Integer 0)}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTASpeechMatchOnClient where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTASpeechMatchOnClient where
  textPut msg
   = do
       P'.tellT "concept" (concept msg)
       P'.tellT "recipient_type" (recipient_type msg)
       P'.tellT "responsequery" (responsequery msg)
       P'.tellT "randomseed" (randomseed msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'concept, parse'recipient_type, parse'responsequery, parse'randomseed]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'concept
         = P'.try
            (do
               v <- P'.getT "concept"
               Prelude'.return (\ o -> o{concept = v}))
        parse'recipient_type
         = P'.try
            (do
               v <- P'.getT "recipient_type"
               Prelude'.return (\ o -> o{recipient_type = v}))
        parse'responsequery
         = P'.try
            (do
               v <- P'.getT "responsequery"
               Prelude'.return (\ o -> o{responsequery = v}))
        parse'randomseed
         = P'.try
            (do
               v <- P'.getT "randomseed"
               Prelude'.return (\ o -> o{randomseed = v}))