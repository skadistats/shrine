{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SpectatorPlayerUnitOrders
       (CDOTAUserMsg_SpectatorPlayerUnitOrders(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CDOTAUserMsg_SpectatorPlayerUnitOrders = CDOTAUserMsg_SpectatorPlayerUnitOrders{entindex :: !(P'.Maybe P'.Int32),
                                                                                     order_type :: !(P'.Maybe P'.Int32),
                                                                                     units :: !(P'.Seq P'.Int32),
                                                                                     target_index :: !(P'.Maybe P'.Int32),
                                                                                     ability_index :: !(P'.Maybe P'.Int32),
                                                                                     position ::
                                                                                     !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                                                                     queue :: !(P'.Maybe P'.Bool),
                                                                                     sequence_number :: !(P'.Maybe P'.Int32)}
                                            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                      Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_SpectatorPlayerUnitOrders where
  mergeAppend (CDOTAUserMsg_SpectatorPlayerUnitOrders x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   (CDOTAUserMsg_SpectatorPlayerUnitOrders y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8)
   = CDOTAUserMsg_SpectatorPlayerUnitOrders (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)
      (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)

instance P'.Default CDOTAUserMsg_SpectatorPlayerUnitOrders where
  defaultValue
   = CDOTAUserMsg_SpectatorPlayerUnitOrders P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAUserMsg_SpectatorPlayerUnitOrders where
  wireSize ft' self'@(CDOTAUserMsg_SpectatorPlayerUnitOrders x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeRep 1 5 x'3 + P'.wireSizeOpt 1 5 x'4 +
             P'.wireSizeOpt 1 5 x'5
             + P'.wireSizeOpt 1 11 x'6
             + P'.wireSizeOpt 1 8 x'7
             + P'.wireSizeOpt 1 5 x'8)
  wirePut ft' self'@(CDOTAUserMsg_SpectatorPlayerUnitOrders x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutRep 24 5 x'3
             P'.wirePutOpt 32 5 x'4
             P'.wirePutOpt 40 5 x'5
             P'.wirePutOpt 50 11 x'6
             P'.wirePutOpt 56 8 x'7
             P'.wirePutOpt 64 5 x'8
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{entindex = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{order_type = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{units = P'.append (units old'Self) new'Field}) (P'.wireGet 5)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{units = P'.mergeAppend (units old'Self) new'Field}) (P'.wireGetPacked 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{target_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{ability_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             50 -> Prelude'.fmap (\ !new'Field -> old'Self{position = P'.mergeAppend (position old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{queue = Prelude'.Just new'Field}) (P'.wireGet 8)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{sequence_number = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_SpectatorPlayerUnitOrders) CDOTAUserMsg_SpectatorPlayerUnitOrders where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_SpectatorPlayerUnitOrders

instance P'.ReflectDescriptor CDOTAUserMsg_SpectatorPlayerUnitOrders where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 26, 32, 40, 50, 56, 64])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_SpectatorPlayerUnitOrders\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_SpectatorPlayerUnitOrders\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_SpectatorPlayerUnitOrders.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SpectatorPlayerUnitOrders.entindex\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SpectatorPlayerUnitOrders\"], baseName' = FName \"entindex\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SpectatorPlayerUnitOrders.order_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SpectatorPlayerUnitOrders\"], baseName' = FName \"order_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SpectatorPlayerUnitOrders.units\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SpectatorPlayerUnitOrders\"], baseName' = FName \"units\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Just (WireTag {getWireTag = 24},WireTag {getWireTag = 26}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SpectatorPlayerUnitOrders.target_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SpectatorPlayerUnitOrders\"], baseName' = FName \"target_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SpectatorPlayerUnitOrders.ability_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SpectatorPlayerUnitOrders\"], baseName' = FName \"ability_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SpectatorPlayerUnitOrders.position\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SpectatorPlayerUnitOrders\"], baseName' = FName \"position\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 50}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SpectatorPlayerUnitOrders.queue\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SpectatorPlayerUnitOrders\"], baseName' = FName \"queue\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SpectatorPlayerUnitOrders.sequence_number\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SpectatorPlayerUnitOrders\"], baseName' = FName \"sequence_number\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_SpectatorPlayerUnitOrders where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_SpectatorPlayerUnitOrders where
  textPut msg
   = do
       P'.tellT "entindex" (entindex msg)
       P'.tellT "order_type" (order_type msg)
       P'.tellT "units" (units msg)
       P'.tellT "target_index" (target_index msg)
       P'.tellT "ability_index" (ability_index msg)
       P'.tellT "position" (position msg)
       P'.tellT "queue" (queue msg)
       P'.tellT "sequence_number" (sequence_number msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'entindex, parse'order_type, parse'units, parse'target_index, parse'ability_index, parse'position,
                   parse'queue, parse'sequence_number])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'entindex
         = P'.try
            (do
               v <- P'.getT "entindex"
               Prelude'.return (\ o -> o{entindex = v}))
        parse'order_type
         = P'.try
            (do
               v <- P'.getT "order_type"
               Prelude'.return (\ o -> o{order_type = v}))
        parse'units
         = P'.try
            (do
               v <- P'.getT "units"
               Prelude'.return (\ o -> o{units = P'.append (units o) v}))
        parse'target_index
         = P'.try
            (do
               v <- P'.getT "target_index"
               Prelude'.return (\ o -> o{target_index = v}))
        parse'ability_index
         = P'.try
            (do
               v <- P'.getT "ability_index"
               Prelude'.return (\ o -> o{ability_index = v}))
        parse'position
         = P'.try
            (do
               v <- P'.getT "position"
               Prelude'.return (\ o -> o{position = v}))
        parse'queue
         = P'.try
            (do
               v <- P'.getT "queue"
               Prelude'.return (\ o -> o{queue = v}))
        parse'sequence_number
         = P'.try
            (do
               v <- P'.getT "sequence_number"
               Prelude'.return (\ o -> o{sequence_number = v}))