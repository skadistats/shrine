{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAResponseQuerySerialized.Fact (Fact(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAResponseQuerySerialized.Fact.ValueType
       as DotaUserMessages.CDOTAResponseQuerySerialized.Fact (ValueType)

data Fact = Fact{key :: !(P'.Int32), valtype :: !(DotaUserMessages.CDOTAResponseQuerySerialized.Fact.ValueType),
                 val_numeric :: !(P'.Maybe P'.Float), val_string :: !(P'.Maybe P'.Utf8)}
          deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Fact where
  mergeAppend (Fact x'1 x'2 x'3 x'4) (Fact y'1 y'2 y'3 y'4)
   = Fact (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)

instance P'.Default Fact where
  defaultValue = Fact P'.defaultValue (Prelude'.read "NUMERIC") P'.defaultValue P'.defaultValue

instance P'.Wire Fact where
  wireSize ft' self'@(Fact x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeReq 1 5 x'1 + P'.wireSizeReq 1 14 x'2 + P'.wireSizeOpt 1 2 x'3 + P'.wireSizeOpt 1 9 x'4)
  wirePut ft' self'@(Fact x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutReq 8 5 x'1
             P'.wirePutReq 16 14 x'2
             P'.wirePutOpt 29 2 x'3
             P'.wirePutOpt 34 9 x'4
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{key = new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{valtype = new'Field}) (P'.wireGet 14)
             29 -> Prelude'.fmap (\ !new'Field -> old'Self{val_numeric = Prelude'.Just new'Field}) (P'.wireGet 2)
             34 -> Prelude'.fmap (\ !new'Field -> old'Self{val_string = Prelude'.Just new'Field}) (P'.wireGet 9)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Fact) Fact where
  getVal m' f' = f' m'

instance P'.GPB Fact

instance P'.ReflectDescriptor Fact where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList [8, 16]) (P'.fromDistinctAscList [8, 16, 29, 34])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized.Fact\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAResponseQuerySerialized\"], baseName = MName \"Fact\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAResponseQuerySerialized\",\"Fact.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized.Fact.key\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAResponseQuerySerialized\",MName \"Fact\"], baseName' = FName \"key\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized.Fact.valtype\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAResponseQuerySerialized\",MName \"Fact\"], baseName' = FName \"valtype\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized.Fact.ValueType\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAResponseQuerySerialized\",MName \"Fact\"], baseName = MName \"ValueType\"}), hsRawDefault = Just \"NUMERIC\", hsDefault = Just (HsDef'Enum \"NUMERIC\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized.Fact.val_numeric\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAResponseQuerySerialized\",MName \"Fact\"], baseName' = FName \"val_numeric\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 29}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAResponseQuerySerialized.Fact.val_string\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAResponseQuerySerialized\",MName \"Fact\"], baseName' = FName \"val_string\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 34}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Fact where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Fact where
  textPut msg
   = do
       P'.tellT "key" (key msg)
       P'.tellT "valtype" (valtype msg)
       P'.tellT "val_numeric" (val_numeric msg)
       P'.tellT "val_string" (val_string msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'key, parse'valtype, parse'val_numeric, parse'val_string]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'key
         = P'.try
            (do
               v <- P'.getT "key"
               Prelude'.return (\ o -> o{key = v}))
        parse'valtype
         = P'.try
            (do
               v <- P'.getT "valtype"
               Prelude'.return (\ o -> o{valtype = v}))
        parse'val_numeric
         = P'.try
            (do
               v <- P'.getT "val_numeric"
               Prelude'.return (\ o -> o{val_numeric = v}))
        parse'val_string
         = P'.try
            (do
               v <- P'.getT "val_string"
               Prelude'.return (\ o -> o{val_string = v}))