{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAResponseQuerySerialized.Fact.ValueType (ValueType(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data ValueType = NUMERIC
               | STRING
               deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                         Prelude'.Generic)

instance P'.Mergeable ValueType

instance Prelude'.Bounded ValueType where
  minBound = NUMERIC
  maxBound = STRING

instance P'.Default ValueType where
  defaultValue = NUMERIC

toMaybe'Enum :: Prelude'.Int -> P'.Maybe ValueType
toMaybe'Enum 1 = Prelude'.Just NUMERIC
toMaybe'Enum 2 = Prelude'.Just STRING
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum ValueType where
  fromEnum NUMERIC = 1
  fromEnum STRING = 2
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAResponseQuerySerialized.Fact.ValueType")
      . toMaybe'Enum
  succ NUMERIC = STRING
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAResponseQuerySerialized.Fact.ValueType"
  pred STRING = NUMERIC
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAResponseQuerySerialized.Fact.ValueType"

instance P'.Wire ValueType where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB ValueType

instance P'.MessageAPI msg' (msg' -> ValueType) ValueType where
  getVal m' f' = f' m'

instance P'.ReflectEnum ValueType where
  reflectEnum = [(1, "NUMERIC", NUMERIC), (2, "STRING", STRING)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.CDOTAResponseQuerySerialized.Fact.ValueType")
        ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaUserMessages", "CDOTAResponseQuerySerialized", "Fact"]
        "ValueType")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "CDOTAResponseQuerySerialized", "Fact", "ValueType.hs"]
      [(1, "NUMERIC"), (2, "STRING")]

instance P'.TextType ValueType where
  tellT = P'.tellShow
  getT = P'.getRead