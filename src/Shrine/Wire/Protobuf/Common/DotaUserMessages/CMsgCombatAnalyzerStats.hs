{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CMsgCombatAnalyzerStats (CMsgCombatAnalyzerStats(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CMsgCombatAnalyzerPlayerStat as DotaUserMessages
       (CMsgCombatAnalyzerPlayerStat)

data CMsgCombatAnalyzerStats = CMsgCombatAnalyzerStats{match_id :: !(P'.Maybe P'.Word64),
                                                       player_stats :: !(P'.Seq DotaUserMessages.CMsgCombatAnalyzerPlayerStat)}
                             deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgCombatAnalyzerStats where
  mergeAppend (CMsgCombatAnalyzerStats x'1 x'2) (CMsgCombatAnalyzerStats y'1 y'2)
   = CMsgCombatAnalyzerStats (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMsgCombatAnalyzerStats where
  defaultValue = CMsgCombatAnalyzerStats P'.defaultValue P'.defaultValue

instance P'.Wire CMsgCombatAnalyzerStats where
  wireSize ft' self'@(CMsgCombatAnalyzerStats x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 4 x'1 + P'.wireSizeRep 1 11 x'2)
  wirePut ft' self'@(CMsgCombatAnalyzerStats x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 4 x'1
             P'.wirePutRep 18 11 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{match_id = Prelude'.Just new'Field}) (P'.wireGet 4)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{player_stats = P'.append (player_stats old'Self) new'Field})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgCombatAnalyzerStats) CMsgCombatAnalyzerStats where
  getVal m' f' = f' m'

instance P'.GPB CMsgCombatAnalyzerStats

instance P'.ReflectDescriptor CMsgCombatAnalyzerStats where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CMsgCombatAnalyzerStats\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CMsgCombatAnalyzerStats\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CMsgCombatAnalyzerStats.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgCombatAnalyzerStats.match_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgCombatAnalyzerStats\"], baseName' = FName \"match_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgCombatAnalyzerStats.player_stats\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgCombatAnalyzerStats\"], baseName' = FName \"player_stats\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CMsgCombatAnalyzerPlayerStat\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CMsgCombatAnalyzerPlayerStat\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgCombatAnalyzerStats where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgCombatAnalyzerStats where
  textPut msg
   = do
       P'.tellT "match_id" (match_id msg)
       P'.tellT "player_stats" (player_stats msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'match_id, parse'player_stats]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'match_id
         = P'.try
            (do
               v <- P'.getT "match_id"
               Prelude'.return (\ o -> o{match_id = v}))
        parse'player_stats
         = P'.try
            (do
               v <- P'.getT "player_stats"
               Prelude'.return (\ o -> o{player_stats = P'.append (player_stats o) v}))