{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CMsgDOTACombatLogEntry (CMsgDOTACombatLogEntry(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_COMBATLOG_TYPES as DotaUserMessages (DOTA_COMBATLOG_TYPES)

data CMsgDOTACombatLogEntry = CMsgDOTACombatLogEntry{type' :: !(P'.Maybe DotaUserMessages.DOTA_COMBATLOG_TYPES),
                                                     target_name :: !(P'.Maybe P'.Word32),
                                                     target_source_name :: !(P'.Maybe P'.Word32),
                                                     attacker_name :: !(P'.Maybe P'.Word32),
                                                     damage_source_name :: !(P'.Maybe P'.Word32),
                                                     inflictor_name :: !(P'.Maybe P'.Word32),
                                                     is_attacker_illusion :: !(P'.Maybe P'.Bool),
                                                     is_attacker_hero :: !(P'.Maybe P'.Bool),
                                                     is_target_illusion :: !(P'.Maybe P'.Bool),
                                                     is_target_hero :: !(P'.Maybe P'.Bool),
                                                     is_visible_radiant :: !(P'.Maybe P'.Bool),
                                                     is_visible_dire :: !(P'.Maybe P'.Bool), value :: !(P'.Maybe P'.Word32),
                                                     health :: !(P'.Maybe P'.Int32), timestamp :: !(P'.Maybe P'.Float),
                                                     stun_duration :: !(P'.Maybe P'.Float), slow_duration :: !(P'.Maybe P'.Float),
                                                     is_ability_toggle_on :: !(P'.Maybe P'.Bool),
                                                     is_ability_toggle_off :: !(P'.Maybe P'.Bool),
                                                     ability_level :: !(P'.Maybe P'.Word32), location_x :: !(P'.Maybe P'.Float),
                                                     location_y :: !(P'.Maybe P'.Float), gold_reason :: !(P'.Maybe P'.Word32),
                                                     timestamp_raw :: !(P'.Maybe P'.Float),
                                                     modifier_duration :: !(P'.Maybe P'.Float), xp_reason :: !(P'.Maybe P'.Word32),
                                                     last_hits :: !(P'.Maybe P'.Word32), attacker_team :: !(P'.Maybe P'.Word32),
                                                     target_team :: !(P'.Maybe P'.Word32),
                                                     obs_wards_placed :: !(P'.Maybe P'.Word32),
                                                     assist_player0 :: !(P'.Maybe P'.Word32),
                                                     assist_player1 :: !(P'.Maybe P'.Word32),
                                                     assist_player2 :: !(P'.Maybe P'.Word32),
                                                     assist_player3 :: !(P'.Maybe P'.Word32), stack_count :: !(P'.Maybe P'.Word32),
                                                     hidden_modifier :: !(P'.Maybe P'.Bool),
                                                     is_target_building :: !(P'.Maybe P'.Bool),
                                                     neutral_camp_type :: !(P'.Maybe P'.Word32), rune_type :: !(P'.Maybe P'.Word32),
                                                     assist_players :: !(P'.Seq P'.Word32), is_heal_save :: !(P'.Maybe P'.Bool),
                                                     is_ultimate_ability :: !(P'.Maybe P'.Bool),
                                                     attacker_hero_level :: !(P'.Maybe P'.Word32),
                                                     target_hero_level :: !(P'.Maybe P'.Word32), xpm :: !(P'.Maybe P'.Word32),
                                                     gpm :: !(P'.Maybe P'.Word32), event_location :: !(P'.Maybe P'.Word32),
                                                     target_is_self :: !(P'.Maybe P'.Bool), damage_type :: !(P'.Maybe P'.Word32),
                                                     invisibility_modifier :: !(P'.Maybe P'.Bool),
                                                     damage_category :: !(P'.Maybe P'.Word32), networth :: !(P'.Maybe P'.Word32),
                                                     building_type :: !(P'.Maybe P'.Word32),
                                                     modifier_elapsed_duration :: !(P'.Maybe P'.Float),
                                                     silence_modifier :: !(P'.Maybe P'.Bool),
                                                     heal_from_lifesteal :: !(P'.Maybe P'.Bool),
                                                     modifier_purged :: !(P'.Maybe P'.Bool), spell_evaded :: !(P'.Maybe P'.Bool),
                                                     motion_controller_modifier :: !(P'.Maybe P'.Bool),
                                                     long_range_kill :: !(P'.Maybe P'.Bool),
                                                     modifier_purge_ability :: !(P'.Maybe P'.Word32),
                                                     modifier_purge_npc :: !(P'.Maybe P'.Word32),
                                                     root_modifier :: !(P'.Maybe P'.Bool),
                                                     total_unit_death_count :: !(P'.Maybe P'.Word32),
                                                     aura_modifier :: !(P'.Maybe P'.Bool),
                                                     armor_debuff_modifier :: !(P'.Maybe P'.Bool),
                                                     no_physical_damage_modifier :: !(P'.Maybe P'.Bool)}
                            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgDOTACombatLogEntry where
  mergeAppend
   (CMsgDOTACombatLogEntry x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23
     x'24 x'25 x'26 x'27 x'28 x'29 x'30 x'31 x'32 x'33 x'34 x'35 x'36 x'37 x'38 x'39 x'40 x'41 x'42 x'43 x'44 x'45 x'46 x'47 x'48
     x'49 x'50 x'51 x'52 x'53 x'54 x'55 x'56 x'57 x'58 x'59 x'60 x'61 x'62 x'63 x'64 x'65 x'66 x'67)
   (CMsgDOTACombatLogEntry y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18 y'19 y'20 y'21 y'22 y'23
     y'24 y'25 y'26 y'27 y'28 y'29 y'30 y'31 y'32 y'33 y'34 y'35 y'36 y'37 y'38 y'39 y'40 y'41 y'42 y'43 y'44 y'45 y'46 y'47 y'48
     y'49 y'50 y'51 y'52 y'53 y'54 y'55 y'56 y'57 y'58 y'59 y'60 y'61 y'62 y'63 y'64 y'65 y'66 y'67)
   = CMsgDOTACombatLogEntry (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)
      (P'.mergeAppend x'19 y'19)
      (P'.mergeAppend x'20 y'20)
      (P'.mergeAppend x'21 y'21)
      (P'.mergeAppend x'22 y'22)
      (P'.mergeAppend x'23 y'23)
      (P'.mergeAppend x'24 y'24)
      (P'.mergeAppend x'25 y'25)
      (P'.mergeAppend x'26 y'26)
      (P'.mergeAppend x'27 y'27)
      (P'.mergeAppend x'28 y'28)
      (P'.mergeAppend x'29 y'29)
      (P'.mergeAppend x'30 y'30)
      (P'.mergeAppend x'31 y'31)
      (P'.mergeAppend x'32 y'32)
      (P'.mergeAppend x'33 y'33)
      (P'.mergeAppend x'34 y'34)
      (P'.mergeAppend x'35 y'35)
      (P'.mergeAppend x'36 y'36)
      (P'.mergeAppend x'37 y'37)
      (P'.mergeAppend x'38 y'38)
      (P'.mergeAppend x'39 y'39)
      (P'.mergeAppend x'40 y'40)
      (P'.mergeAppend x'41 y'41)
      (P'.mergeAppend x'42 y'42)
      (P'.mergeAppend x'43 y'43)
      (P'.mergeAppend x'44 y'44)
      (P'.mergeAppend x'45 y'45)
      (P'.mergeAppend x'46 y'46)
      (P'.mergeAppend x'47 y'47)
      (P'.mergeAppend x'48 y'48)
      (P'.mergeAppend x'49 y'49)
      (P'.mergeAppend x'50 y'50)
      (P'.mergeAppend x'51 y'51)
      (P'.mergeAppend x'52 y'52)
      (P'.mergeAppend x'53 y'53)
      (P'.mergeAppend x'54 y'54)
      (P'.mergeAppend x'55 y'55)
      (P'.mergeAppend x'56 y'56)
      (P'.mergeAppend x'57 y'57)
      (P'.mergeAppend x'58 y'58)
      (P'.mergeAppend x'59 y'59)
      (P'.mergeAppend x'60 y'60)
      (P'.mergeAppend x'61 y'61)
      (P'.mergeAppend x'62 y'62)
      (P'.mergeAppend x'63 y'63)
      (P'.mergeAppend x'64 y'64)
      (P'.mergeAppend x'65 y'65)
      (P'.mergeAppend x'66 y'66)
      (P'.mergeAppend x'67 y'67)

instance P'.Default CMsgDOTACombatLogEntry where
  defaultValue
   = CMsgDOTACombatLogEntry (Prelude'.Just (Prelude'.read "DOTA_COMBATLOG_DAMAGE")) P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CMsgDOTACombatLogEntry where
  wireSize ft'
   self'@(CMsgDOTACombatLogEntry x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21
           x'22 x'23 x'24 x'25 x'26 x'27 x'28 x'29 x'30 x'31 x'32 x'33 x'34 x'35 x'36 x'37 x'38 x'39 x'40 x'41 x'42 x'43 x'44 x'45
           x'46 x'47 x'48 x'49 x'50 x'51 x'52 x'53 x'54 x'55 x'56 x'57 x'58 x'59 x'60 x'61 x'62 x'63 x'64 x'65 x'66 x'67)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 14 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 13 x'3 + P'.wireSizeOpt 1 13 x'4 +
             P'.wireSizeOpt 1 13 x'5
             + P'.wireSizeOpt 1 13 x'6
             + P'.wireSizeOpt 1 8 x'7
             + P'.wireSizeOpt 1 8 x'8
             + P'.wireSizeOpt 1 8 x'9
             + P'.wireSizeOpt 1 8 x'10
             + P'.wireSizeOpt 1 8 x'11
             + P'.wireSizeOpt 1 8 x'12
             + P'.wireSizeOpt 1 13 x'13
             + P'.wireSizeOpt 1 5 x'14
             + P'.wireSizeOpt 1 2 x'15
             + P'.wireSizeOpt 2 2 x'16
             + P'.wireSizeOpt 2 2 x'17
             + P'.wireSizeOpt 2 8 x'18
             + P'.wireSizeOpt 2 8 x'19
             + P'.wireSizeOpt 2 13 x'20
             + P'.wireSizeOpt 2 2 x'21
             + P'.wireSizeOpt 2 2 x'22
             + P'.wireSizeOpt 2 13 x'23
             + P'.wireSizeOpt 2 2 x'24
             + P'.wireSizeOpt 2 2 x'25
             + P'.wireSizeOpt 2 13 x'26
             + P'.wireSizeOpt 2 13 x'27
             + P'.wireSizeOpt 2 13 x'28
             + P'.wireSizeOpt 2 13 x'29
             + P'.wireSizeOpt 2 13 x'30
             + P'.wireSizeOpt 2 13 x'31
             + P'.wireSizeOpt 2 13 x'32
             + P'.wireSizeOpt 2 13 x'33
             + P'.wireSizeOpt 2 13 x'34
             + P'.wireSizeOpt 2 13 x'35
             + P'.wireSizeOpt 2 8 x'36
             + P'.wireSizeOpt 2 8 x'37
             + P'.wireSizeOpt 2 13 x'38
             + P'.wireSizeOpt 2 13 x'39
             + P'.wireSizeRep 2 13 x'40
             + P'.wireSizeOpt 2 8 x'41
             + P'.wireSizeOpt 2 8 x'42
             + P'.wireSizeOpt 2 13 x'43
             + P'.wireSizeOpt 2 13 x'44
             + P'.wireSizeOpt 2 13 x'45
             + P'.wireSizeOpt 2 13 x'46
             + P'.wireSizeOpt 2 13 x'47
             + P'.wireSizeOpt 2 8 x'48
             + P'.wireSizeOpt 2 13 x'49
             + P'.wireSizeOpt 2 8 x'50
             + P'.wireSizeOpt 2 13 x'51
             + P'.wireSizeOpt 2 13 x'52
             + P'.wireSizeOpt 2 13 x'53
             + P'.wireSizeOpt 2 2 x'54
             + P'.wireSizeOpt 2 8 x'55
             + P'.wireSizeOpt 2 8 x'56
             + P'.wireSizeOpt 2 8 x'57
             + P'.wireSizeOpt 2 8 x'58
             + P'.wireSizeOpt 2 8 x'59
             + P'.wireSizeOpt 2 8 x'60
             + P'.wireSizeOpt 2 13 x'61
             + P'.wireSizeOpt 2 13 x'62
             + P'.wireSizeOpt 2 8 x'63
             + P'.wireSizeOpt 2 13 x'64
             + P'.wireSizeOpt 2 8 x'65
             + P'.wireSizeOpt 2 8 x'66
             + P'.wireSizeOpt 2 8 x'67)
  wirePut ft'
   self'@(CMsgDOTACombatLogEntry x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21
           x'22 x'23 x'24 x'25 x'26 x'27 x'28 x'29 x'30 x'31 x'32 x'33 x'34 x'35 x'36 x'37 x'38 x'39 x'40 x'41 x'42 x'43 x'44 x'45
           x'46 x'47 x'48 x'49 x'50 x'51 x'52 x'53 x'54 x'55 x'56 x'57 x'58 x'59 x'60 x'61 x'62 x'63 x'64 x'65 x'66 x'67)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 14 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 13 x'3
             P'.wirePutOpt 32 13 x'4
             P'.wirePutOpt 40 13 x'5
             P'.wirePutOpt 48 13 x'6
             P'.wirePutOpt 56 8 x'7
             P'.wirePutOpt 64 8 x'8
             P'.wirePutOpt 72 8 x'9
             P'.wirePutOpt 80 8 x'10
             P'.wirePutOpt 88 8 x'11
             P'.wirePutOpt 96 8 x'12
             P'.wirePutOpt 104 13 x'13
             P'.wirePutOpt 112 5 x'14
             P'.wirePutOpt 125 2 x'15
             P'.wirePutOpt 133 2 x'16
             P'.wirePutOpt 141 2 x'17
             P'.wirePutOpt 144 8 x'18
             P'.wirePutOpt 152 8 x'19
             P'.wirePutOpt 160 13 x'20
             P'.wirePutOpt 173 2 x'21
             P'.wirePutOpt 181 2 x'22
             P'.wirePutOpt 184 13 x'23
             P'.wirePutOpt 197 2 x'24
             P'.wirePutOpt 205 2 x'25
             P'.wirePutOpt 208 13 x'26
             P'.wirePutOpt 216 13 x'27
             P'.wirePutOpt 224 13 x'28
             P'.wirePutOpt 232 13 x'29
             P'.wirePutOpt 240 13 x'30
             P'.wirePutOpt 248 13 x'31
             P'.wirePutOpt 256 13 x'32
             P'.wirePutOpt 264 13 x'33
             P'.wirePutOpt 272 13 x'34
             P'.wirePutOpt 280 13 x'35
             P'.wirePutOpt 288 8 x'36
             P'.wirePutOpt 296 8 x'37
             P'.wirePutOpt 304 13 x'38
             P'.wirePutOpt 312 13 x'39
             P'.wirePutRep 320 13 x'40
             P'.wirePutOpt 328 8 x'41
             P'.wirePutOpt 336 8 x'42
             P'.wirePutOpt 344 13 x'43
             P'.wirePutOpt 352 13 x'44
             P'.wirePutOpt 360 13 x'45
             P'.wirePutOpt 368 13 x'46
             P'.wirePutOpt 376 13 x'47
             P'.wirePutOpt 384 8 x'48
             P'.wirePutOpt 392 13 x'49
             P'.wirePutOpt 400 8 x'50
             P'.wirePutOpt 408 13 x'51
             P'.wirePutOpt 416 13 x'52
             P'.wirePutOpt 424 13 x'53
             P'.wirePutOpt 437 2 x'54
             P'.wirePutOpt 440 8 x'55
             P'.wirePutOpt 448 8 x'56
             P'.wirePutOpt 456 8 x'57
             P'.wirePutOpt 464 8 x'58
             P'.wirePutOpt 472 8 x'59
             P'.wirePutOpt 480 8 x'60
             P'.wirePutOpt 488 13 x'61
             P'.wirePutOpt 496 13 x'62
             P'.wirePutOpt 504 8 x'63
             P'.wirePutOpt 512 13 x'64
             P'.wirePutOpt 520 8 x'65
             P'.wirePutOpt 528 8 x'66
             P'.wirePutOpt 536 8 x'67
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{type' = Prelude'.Just new'Field}) (P'.wireGet 14)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{target_name = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{target_source_name = Prelude'.Just new'Field}) (P'.wireGet 13)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{attacker_name = Prelude'.Just new'Field}) (P'.wireGet 13)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{damage_source_name = Prelude'.Just new'Field}) (P'.wireGet 13)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{inflictor_name = Prelude'.Just new'Field}) (P'.wireGet 13)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{is_attacker_illusion = Prelude'.Just new'Field}) (P'.wireGet 8)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{is_attacker_hero = Prelude'.Just new'Field}) (P'.wireGet 8)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{is_target_illusion = Prelude'.Just new'Field}) (P'.wireGet 8)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{is_target_hero = Prelude'.Just new'Field}) (P'.wireGet 8)
             88 -> Prelude'.fmap (\ !new'Field -> old'Self{is_visible_radiant = Prelude'.Just new'Field}) (P'.wireGet 8)
             96 -> Prelude'.fmap (\ !new'Field -> old'Self{is_visible_dire = Prelude'.Just new'Field}) (P'.wireGet 8)
             104 -> Prelude'.fmap (\ !new'Field -> old'Self{value = Prelude'.Just new'Field}) (P'.wireGet 13)
             112 -> Prelude'.fmap (\ !new'Field -> old'Self{health = Prelude'.Just new'Field}) (P'.wireGet 5)
             125 -> Prelude'.fmap (\ !new'Field -> old'Self{timestamp = Prelude'.Just new'Field}) (P'.wireGet 2)
             133 -> Prelude'.fmap (\ !new'Field -> old'Self{stun_duration = Prelude'.Just new'Field}) (P'.wireGet 2)
             141 -> Prelude'.fmap (\ !new'Field -> old'Self{slow_duration = Prelude'.Just new'Field}) (P'.wireGet 2)
             144 -> Prelude'.fmap (\ !new'Field -> old'Self{is_ability_toggle_on = Prelude'.Just new'Field}) (P'.wireGet 8)
             152 -> Prelude'.fmap (\ !new'Field -> old'Self{is_ability_toggle_off = Prelude'.Just new'Field}) (P'.wireGet 8)
             160 -> Prelude'.fmap (\ !new'Field -> old'Self{ability_level = Prelude'.Just new'Field}) (P'.wireGet 13)
             173 -> Prelude'.fmap (\ !new'Field -> old'Self{location_x = Prelude'.Just new'Field}) (P'.wireGet 2)
             181 -> Prelude'.fmap (\ !new'Field -> old'Self{location_y = Prelude'.Just new'Field}) (P'.wireGet 2)
             184 -> Prelude'.fmap (\ !new'Field -> old'Self{gold_reason = Prelude'.Just new'Field}) (P'.wireGet 13)
             197 -> Prelude'.fmap (\ !new'Field -> old'Self{timestamp_raw = Prelude'.Just new'Field}) (P'.wireGet 2)
             205 -> Prelude'.fmap (\ !new'Field -> old'Self{modifier_duration = Prelude'.Just new'Field}) (P'.wireGet 2)
             208 -> Prelude'.fmap (\ !new'Field -> old'Self{xp_reason = Prelude'.Just new'Field}) (P'.wireGet 13)
             216 -> Prelude'.fmap (\ !new'Field -> old'Self{last_hits = Prelude'.Just new'Field}) (P'.wireGet 13)
             224 -> Prelude'.fmap (\ !new'Field -> old'Self{attacker_team = Prelude'.Just new'Field}) (P'.wireGet 13)
             232 -> Prelude'.fmap (\ !new'Field -> old'Self{target_team = Prelude'.Just new'Field}) (P'.wireGet 13)
             240 -> Prelude'.fmap (\ !new'Field -> old'Self{obs_wards_placed = Prelude'.Just new'Field}) (P'.wireGet 13)
             248 -> Prelude'.fmap (\ !new'Field -> old'Self{assist_player0 = Prelude'.Just new'Field}) (P'.wireGet 13)
             256 -> Prelude'.fmap (\ !new'Field -> old'Self{assist_player1 = Prelude'.Just new'Field}) (P'.wireGet 13)
             264 -> Prelude'.fmap (\ !new'Field -> old'Self{assist_player2 = Prelude'.Just new'Field}) (P'.wireGet 13)
             272 -> Prelude'.fmap (\ !new'Field -> old'Self{assist_player3 = Prelude'.Just new'Field}) (P'.wireGet 13)
             280 -> Prelude'.fmap (\ !new'Field -> old'Self{stack_count = Prelude'.Just new'Field}) (P'.wireGet 13)
             288 -> Prelude'.fmap (\ !new'Field -> old'Self{hidden_modifier = Prelude'.Just new'Field}) (P'.wireGet 8)
             296 -> Prelude'.fmap (\ !new'Field -> old'Self{is_target_building = Prelude'.Just new'Field}) (P'.wireGet 8)
             304 -> Prelude'.fmap (\ !new'Field -> old'Self{neutral_camp_type = Prelude'.Just new'Field}) (P'.wireGet 13)
             312 -> Prelude'.fmap (\ !new'Field -> old'Self{rune_type = Prelude'.Just new'Field}) (P'.wireGet 13)
             320 -> Prelude'.fmap (\ !new'Field -> old'Self{assist_players = P'.append (assist_players old'Self) new'Field})
                     (P'.wireGet 13)
             322 -> Prelude'.fmap (\ !new'Field -> old'Self{assist_players = P'.mergeAppend (assist_players old'Self) new'Field})
                     (P'.wireGetPacked 13)
             328 -> Prelude'.fmap (\ !new'Field -> old'Self{is_heal_save = Prelude'.Just new'Field}) (P'.wireGet 8)
             336 -> Prelude'.fmap (\ !new'Field -> old'Self{is_ultimate_ability = Prelude'.Just new'Field}) (P'.wireGet 8)
             344 -> Prelude'.fmap (\ !new'Field -> old'Self{attacker_hero_level = Prelude'.Just new'Field}) (P'.wireGet 13)
             352 -> Prelude'.fmap (\ !new'Field -> old'Self{target_hero_level = Prelude'.Just new'Field}) (P'.wireGet 13)
             360 -> Prelude'.fmap (\ !new'Field -> old'Self{xpm = Prelude'.Just new'Field}) (P'.wireGet 13)
             368 -> Prelude'.fmap (\ !new'Field -> old'Self{gpm = Prelude'.Just new'Field}) (P'.wireGet 13)
             376 -> Prelude'.fmap (\ !new'Field -> old'Self{event_location = Prelude'.Just new'Field}) (P'.wireGet 13)
             384 -> Prelude'.fmap (\ !new'Field -> old'Self{target_is_self = Prelude'.Just new'Field}) (P'.wireGet 8)
             392 -> Prelude'.fmap (\ !new'Field -> old'Self{damage_type = Prelude'.Just new'Field}) (P'.wireGet 13)
             400 -> Prelude'.fmap (\ !new'Field -> old'Self{invisibility_modifier = Prelude'.Just new'Field}) (P'.wireGet 8)
             408 -> Prelude'.fmap (\ !new'Field -> old'Self{damage_category = Prelude'.Just new'Field}) (P'.wireGet 13)
             416 -> Prelude'.fmap (\ !new'Field -> old'Self{networth = Prelude'.Just new'Field}) (P'.wireGet 13)
             424 -> Prelude'.fmap (\ !new'Field -> old'Self{building_type = Prelude'.Just new'Field}) (P'.wireGet 13)
             437 -> Prelude'.fmap (\ !new'Field -> old'Self{modifier_elapsed_duration = Prelude'.Just new'Field}) (P'.wireGet 2)
             440 -> Prelude'.fmap (\ !new'Field -> old'Self{silence_modifier = Prelude'.Just new'Field}) (P'.wireGet 8)
             448 -> Prelude'.fmap (\ !new'Field -> old'Self{heal_from_lifesteal = Prelude'.Just new'Field}) (P'.wireGet 8)
             456 -> Prelude'.fmap (\ !new'Field -> old'Self{modifier_purged = Prelude'.Just new'Field}) (P'.wireGet 8)
             464 -> Prelude'.fmap (\ !new'Field -> old'Self{spell_evaded = Prelude'.Just new'Field}) (P'.wireGet 8)
             472 -> Prelude'.fmap (\ !new'Field -> old'Self{motion_controller_modifier = Prelude'.Just new'Field}) (P'.wireGet 8)
             480 -> Prelude'.fmap (\ !new'Field -> old'Self{long_range_kill = Prelude'.Just new'Field}) (P'.wireGet 8)
             488 -> Prelude'.fmap (\ !new'Field -> old'Self{modifier_purge_ability = Prelude'.Just new'Field}) (P'.wireGet 13)
             496 -> Prelude'.fmap (\ !new'Field -> old'Self{modifier_purge_npc = Prelude'.Just new'Field}) (P'.wireGet 13)
             504 -> Prelude'.fmap (\ !new'Field -> old'Self{root_modifier = Prelude'.Just new'Field}) (P'.wireGet 8)
             512 -> Prelude'.fmap (\ !new'Field -> old'Self{total_unit_death_count = Prelude'.Just new'Field}) (P'.wireGet 13)
             520 -> Prelude'.fmap (\ !new'Field -> old'Self{aura_modifier = Prelude'.Just new'Field}) (P'.wireGet 8)
             528 -> Prelude'.fmap (\ !new'Field -> old'Self{armor_debuff_modifier = Prelude'.Just new'Field}) (P'.wireGet 8)
             536 -> Prelude'.fmap (\ !new'Field -> old'Self{no_physical_damage_modifier = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgDOTACombatLogEntry) CMsgDOTACombatLogEntry where
  getVal m' f' = f' m'

instance P'.GPB CMsgDOTACombatLogEntry

instance P'.ReflectDescriptor CMsgDOTACombatLogEntry where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList
        [8, 16, 24, 32, 40, 48, 56, 64, 72, 80, 88, 96, 104, 112, 125, 133, 141, 144, 152, 160, 173, 181, 184, 197, 205, 208, 216,
         224, 232, 240, 248, 256, 264, 272, 280, 288, 296, 304, 312, 320, 322, 328, 336, 344, 352, 360, 368, 376, 384, 392, 400,
         408, 416, 424, 437, 440, 448, 456, 464, 472, 480, 488, 496, 504, 512, 520, 528, 536])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CMsgDOTACombatLogEntry\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CMsgDOTACombatLogEntry.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"type'\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.DOTA_COMBATLOG_TYPES\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"DOTA_COMBATLOG_TYPES\"}), hsRawDefault = Just \"DOTA_COMBATLOG_DAMAGE\", hsDefault = Just (HsDef'Enum \"DOTA_COMBATLOG_DAMAGE\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.target_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"target_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.target_source_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"target_source_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.attacker_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"attacker_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.damage_source_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"damage_source_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.inflictor_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"inflictor_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_attacker_illusion\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_attacker_illusion\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_attacker_hero\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_attacker_hero\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_target_illusion\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_target_illusion\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_target_hero\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_target_hero\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_visible_radiant\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_visible_radiant\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 88}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_visible_dire\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_visible_dire\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 96}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.value\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"value\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 104}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.health\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"health\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 112}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.timestamp\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"timestamp\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 125}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.stun_duration\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"stun_duration\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 133}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.slow_duration\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"slow_duration\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 141}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_ability_toggle_on\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_ability_toggle_on\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 144}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_ability_toggle_off\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_ability_toggle_off\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 19}, wireTag = WireTag {getWireTag = 152}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.ability_level\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"ability_level\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 20}, wireTag = WireTag {getWireTag = 160}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.location_x\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"location_x\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 21}, wireTag = WireTag {getWireTag = 173}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.location_y\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"location_y\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 22}, wireTag = WireTag {getWireTag = 181}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.gold_reason\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"gold_reason\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 23}, wireTag = WireTag {getWireTag = 184}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.timestamp_raw\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"timestamp_raw\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 24}, wireTag = WireTag {getWireTag = 197}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.modifier_duration\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"modifier_duration\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 25}, wireTag = WireTag {getWireTag = 205}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.xp_reason\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"xp_reason\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 26}, wireTag = WireTag {getWireTag = 208}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.last_hits\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"last_hits\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 27}, wireTag = WireTag {getWireTag = 216}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.attacker_team\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"attacker_team\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 28}, wireTag = WireTag {getWireTag = 224}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.target_team\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"target_team\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 29}, wireTag = WireTag {getWireTag = 232}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.obs_wards_placed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"obs_wards_placed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 30}, wireTag = WireTag {getWireTag = 240}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.assist_player0\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"assist_player0\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 31}, wireTag = WireTag {getWireTag = 248}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.assist_player1\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"assist_player1\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 32}, wireTag = WireTag {getWireTag = 256}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.assist_player2\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"assist_player2\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 33}, wireTag = WireTag {getWireTag = 264}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.assist_player3\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"assist_player3\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 34}, wireTag = WireTag {getWireTag = 272}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.stack_count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"stack_count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 35}, wireTag = WireTag {getWireTag = 280}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.hidden_modifier\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"hidden_modifier\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 36}, wireTag = WireTag {getWireTag = 288}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_target_building\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_target_building\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 37}, wireTag = WireTag {getWireTag = 296}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.neutral_camp_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"neutral_camp_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 38}, wireTag = WireTag {getWireTag = 304}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.rune_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"rune_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 39}, wireTag = WireTag {getWireTag = 312}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.assist_players\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"assist_players\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 40}, wireTag = WireTag {getWireTag = 320}, packedTag = Just (WireTag {getWireTag = 320},WireTag {getWireTag = 322}), wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_heal_save\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_heal_save\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 41}, wireTag = WireTag {getWireTag = 328}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.is_ultimate_ability\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"is_ultimate_ability\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 42}, wireTag = WireTag {getWireTag = 336}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.attacker_hero_level\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"attacker_hero_level\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 43}, wireTag = WireTag {getWireTag = 344}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.target_hero_level\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"target_hero_level\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 44}, wireTag = WireTag {getWireTag = 352}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.xpm\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"xpm\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 45}, wireTag = WireTag {getWireTag = 360}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.gpm\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"gpm\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 46}, wireTag = WireTag {getWireTag = 368}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.event_location\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"event_location\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 47}, wireTag = WireTag {getWireTag = 376}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.target_is_self\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"target_is_self\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 48}, wireTag = WireTag {getWireTag = 384}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.damage_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"damage_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 49}, wireTag = WireTag {getWireTag = 392}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.invisibility_modifier\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"invisibility_modifier\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 50}, wireTag = WireTag {getWireTag = 400}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.damage_category\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"damage_category\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 51}, wireTag = WireTag {getWireTag = 408}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.networth\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"networth\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 52}, wireTag = WireTag {getWireTag = 416}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.building_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"building_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 53}, wireTag = WireTag {getWireTag = 424}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.modifier_elapsed_duration\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"modifier_elapsed_duration\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 54}, wireTag = WireTag {getWireTag = 437}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.silence_modifier\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"silence_modifier\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 55}, wireTag = WireTag {getWireTag = 440}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.heal_from_lifesteal\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"heal_from_lifesteal\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 56}, wireTag = WireTag {getWireTag = 448}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.modifier_purged\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"modifier_purged\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 57}, wireTag = WireTag {getWireTag = 456}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.spell_evaded\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"spell_evaded\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 58}, wireTag = WireTag {getWireTag = 464}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.motion_controller_modifier\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"motion_controller_modifier\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 59}, wireTag = WireTag {getWireTag = 472}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.long_range_kill\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"long_range_kill\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 60}, wireTag = WireTag {getWireTag = 480}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.modifier_purge_ability\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"modifier_purge_ability\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 61}, wireTag = WireTag {getWireTag = 488}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.modifier_purge_npc\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"modifier_purge_npc\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 62}, wireTag = WireTag {getWireTag = 496}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.root_modifier\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"root_modifier\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 63}, wireTag = WireTag {getWireTag = 504}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.total_unit_death_count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"total_unit_death_count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 64}, wireTag = WireTag {getWireTag = 512}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.aura_modifier\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"aura_modifier\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 65}, wireTag = WireTag {getWireTag = 520}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.armor_debuff_modifier\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"armor_debuff_modifier\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 66}, wireTag = WireTag {getWireTag = 528}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CMsgDOTACombatLogEntry.no_physical_damage_modifier\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CMsgDOTACombatLogEntry\"], baseName' = FName \"no_physical_damage_modifier\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 67}, wireTag = WireTag {getWireTag = 536}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgDOTACombatLogEntry where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgDOTACombatLogEntry where
  textPut msg
   = do
       P'.tellT "type" (type' msg)
       P'.tellT "target_name" (target_name msg)
       P'.tellT "target_source_name" (target_source_name msg)
       P'.tellT "attacker_name" (attacker_name msg)
       P'.tellT "damage_source_name" (damage_source_name msg)
       P'.tellT "inflictor_name" (inflictor_name msg)
       P'.tellT "is_attacker_illusion" (is_attacker_illusion msg)
       P'.tellT "is_attacker_hero" (is_attacker_hero msg)
       P'.tellT "is_target_illusion" (is_target_illusion msg)
       P'.tellT "is_target_hero" (is_target_hero msg)
       P'.tellT "is_visible_radiant" (is_visible_radiant msg)
       P'.tellT "is_visible_dire" (is_visible_dire msg)
       P'.tellT "value" (value msg)
       P'.tellT "health" (health msg)
       P'.tellT "timestamp" (timestamp msg)
       P'.tellT "stun_duration" (stun_duration msg)
       P'.tellT "slow_duration" (slow_duration msg)
       P'.tellT "is_ability_toggle_on" (is_ability_toggle_on msg)
       P'.tellT "is_ability_toggle_off" (is_ability_toggle_off msg)
       P'.tellT "ability_level" (ability_level msg)
       P'.tellT "location_x" (location_x msg)
       P'.tellT "location_y" (location_y msg)
       P'.tellT "gold_reason" (gold_reason msg)
       P'.tellT "timestamp_raw" (timestamp_raw msg)
       P'.tellT "modifier_duration" (modifier_duration msg)
       P'.tellT "xp_reason" (xp_reason msg)
       P'.tellT "last_hits" (last_hits msg)
       P'.tellT "attacker_team" (attacker_team msg)
       P'.tellT "target_team" (target_team msg)
       P'.tellT "obs_wards_placed" (obs_wards_placed msg)
       P'.tellT "assist_player0" (assist_player0 msg)
       P'.tellT "assist_player1" (assist_player1 msg)
       P'.tellT "assist_player2" (assist_player2 msg)
       P'.tellT "assist_player3" (assist_player3 msg)
       P'.tellT "stack_count" (stack_count msg)
       P'.tellT "hidden_modifier" (hidden_modifier msg)
       P'.tellT "is_target_building" (is_target_building msg)
       P'.tellT "neutral_camp_type" (neutral_camp_type msg)
       P'.tellT "rune_type" (rune_type msg)
       P'.tellT "assist_players" (assist_players msg)
       P'.tellT "is_heal_save" (is_heal_save msg)
       P'.tellT "is_ultimate_ability" (is_ultimate_ability msg)
       P'.tellT "attacker_hero_level" (attacker_hero_level msg)
       P'.tellT "target_hero_level" (target_hero_level msg)
       P'.tellT "xpm" (xpm msg)
       P'.tellT "gpm" (gpm msg)
       P'.tellT "event_location" (event_location msg)
       P'.tellT "target_is_self" (target_is_self msg)
       P'.tellT "damage_type" (damage_type msg)
       P'.tellT "invisibility_modifier" (invisibility_modifier msg)
       P'.tellT "damage_category" (damage_category msg)
       P'.tellT "networth" (networth msg)
       P'.tellT "building_type" (building_type msg)
       P'.tellT "modifier_elapsed_duration" (modifier_elapsed_duration msg)
       P'.tellT "silence_modifier" (silence_modifier msg)
       P'.tellT "heal_from_lifesteal" (heal_from_lifesteal msg)
       P'.tellT "modifier_purged" (modifier_purged msg)
       P'.tellT "spell_evaded" (spell_evaded msg)
       P'.tellT "motion_controller_modifier" (motion_controller_modifier msg)
       P'.tellT "long_range_kill" (long_range_kill msg)
       P'.tellT "modifier_purge_ability" (modifier_purge_ability msg)
       P'.tellT "modifier_purge_npc" (modifier_purge_npc msg)
       P'.tellT "root_modifier" (root_modifier msg)
       P'.tellT "total_unit_death_count" (total_unit_death_count msg)
       P'.tellT "aura_modifier" (aura_modifier msg)
       P'.tellT "armor_debuff_modifier" (armor_debuff_modifier msg)
       P'.tellT "no_physical_damage_modifier" (no_physical_damage_modifier msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'type', parse'target_name, parse'target_source_name, parse'attacker_name, parse'damage_source_name,
                   parse'inflictor_name, parse'is_attacker_illusion, parse'is_attacker_hero, parse'is_target_illusion,
                   parse'is_target_hero, parse'is_visible_radiant, parse'is_visible_dire, parse'value, parse'health,
                   parse'timestamp, parse'stun_duration, parse'slow_duration, parse'is_ability_toggle_on,
                   parse'is_ability_toggle_off, parse'ability_level, parse'location_x, parse'location_y, parse'gold_reason,
                   parse'timestamp_raw, parse'modifier_duration, parse'xp_reason, parse'last_hits, parse'attacker_team,
                   parse'target_team, parse'obs_wards_placed, parse'assist_player0, parse'assist_player1, parse'assist_player2,
                   parse'assist_player3, parse'stack_count, parse'hidden_modifier, parse'is_target_building,
                   parse'neutral_camp_type, parse'rune_type, parse'assist_players, parse'is_heal_save, parse'is_ultimate_ability,
                   parse'attacker_hero_level, parse'target_hero_level, parse'xpm, parse'gpm, parse'event_location,
                   parse'target_is_self, parse'damage_type, parse'invisibility_modifier, parse'damage_category, parse'networth,
                   parse'building_type, parse'modifier_elapsed_duration, parse'silence_modifier, parse'heal_from_lifesteal,
                   parse'modifier_purged, parse'spell_evaded, parse'motion_controller_modifier, parse'long_range_kill,
                   parse'modifier_purge_ability, parse'modifier_purge_npc, parse'root_modifier, parse'total_unit_death_count,
                   parse'aura_modifier, parse'armor_debuff_modifier, parse'no_physical_damage_modifier])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'type'
         = P'.try
            (do
               v <- P'.getT "type"
               Prelude'.return (\ o -> o{type' = v}))
        parse'target_name
         = P'.try
            (do
               v <- P'.getT "target_name"
               Prelude'.return (\ o -> o{target_name = v}))
        parse'target_source_name
         = P'.try
            (do
               v <- P'.getT "target_source_name"
               Prelude'.return (\ o -> o{target_source_name = v}))
        parse'attacker_name
         = P'.try
            (do
               v <- P'.getT "attacker_name"
               Prelude'.return (\ o -> o{attacker_name = v}))
        parse'damage_source_name
         = P'.try
            (do
               v <- P'.getT "damage_source_name"
               Prelude'.return (\ o -> o{damage_source_name = v}))
        parse'inflictor_name
         = P'.try
            (do
               v <- P'.getT "inflictor_name"
               Prelude'.return (\ o -> o{inflictor_name = v}))
        parse'is_attacker_illusion
         = P'.try
            (do
               v <- P'.getT "is_attacker_illusion"
               Prelude'.return (\ o -> o{is_attacker_illusion = v}))
        parse'is_attacker_hero
         = P'.try
            (do
               v <- P'.getT "is_attacker_hero"
               Prelude'.return (\ o -> o{is_attacker_hero = v}))
        parse'is_target_illusion
         = P'.try
            (do
               v <- P'.getT "is_target_illusion"
               Prelude'.return (\ o -> o{is_target_illusion = v}))
        parse'is_target_hero
         = P'.try
            (do
               v <- P'.getT "is_target_hero"
               Prelude'.return (\ o -> o{is_target_hero = v}))
        parse'is_visible_radiant
         = P'.try
            (do
               v <- P'.getT "is_visible_radiant"
               Prelude'.return (\ o -> o{is_visible_radiant = v}))
        parse'is_visible_dire
         = P'.try
            (do
               v <- P'.getT "is_visible_dire"
               Prelude'.return (\ o -> o{is_visible_dire = v}))
        parse'value
         = P'.try
            (do
               v <- P'.getT "value"
               Prelude'.return (\ o -> o{value = v}))
        parse'health
         = P'.try
            (do
               v <- P'.getT "health"
               Prelude'.return (\ o -> o{health = v}))
        parse'timestamp
         = P'.try
            (do
               v <- P'.getT "timestamp"
               Prelude'.return (\ o -> o{timestamp = v}))
        parse'stun_duration
         = P'.try
            (do
               v <- P'.getT "stun_duration"
               Prelude'.return (\ o -> o{stun_duration = v}))
        parse'slow_duration
         = P'.try
            (do
               v <- P'.getT "slow_duration"
               Prelude'.return (\ o -> o{slow_duration = v}))
        parse'is_ability_toggle_on
         = P'.try
            (do
               v <- P'.getT "is_ability_toggle_on"
               Prelude'.return (\ o -> o{is_ability_toggle_on = v}))
        parse'is_ability_toggle_off
         = P'.try
            (do
               v <- P'.getT "is_ability_toggle_off"
               Prelude'.return (\ o -> o{is_ability_toggle_off = v}))
        parse'ability_level
         = P'.try
            (do
               v <- P'.getT "ability_level"
               Prelude'.return (\ o -> o{ability_level = v}))
        parse'location_x
         = P'.try
            (do
               v <- P'.getT "location_x"
               Prelude'.return (\ o -> o{location_x = v}))
        parse'location_y
         = P'.try
            (do
               v <- P'.getT "location_y"
               Prelude'.return (\ o -> o{location_y = v}))
        parse'gold_reason
         = P'.try
            (do
               v <- P'.getT "gold_reason"
               Prelude'.return (\ o -> o{gold_reason = v}))
        parse'timestamp_raw
         = P'.try
            (do
               v <- P'.getT "timestamp_raw"
               Prelude'.return (\ o -> o{timestamp_raw = v}))
        parse'modifier_duration
         = P'.try
            (do
               v <- P'.getT "modifier_duration"
               Prelude'.return (\ o -> o{modifier_duration = v}))
        parse'xp_reason
         = P'.try
            (do
               v <- P'.getT "xp_reason"
               Prelude'.return (\ o -> o{xp_reason = v}))
        parse'last_hits
         = P'.try
            (do
               v <- P'.getT "last_hits"
               Prelude'.return (\ o -> o{last_hits = v}))
        parse'attacker_team
         = P'.try
            (do
               v <- P'.getT "attacker_team"
               Prelude'.return (\ o -> o{attacker_team = v}))
        parse'target_team
         = P'.try
            (do
               v <- P'.getT "target_team"
               Prelude'.return (\ o -> o{target_team = v}))
        parse'obs_wards_placed
         = P'.try
            (do
               v <- P'.getT "obs_wards_placed"
               Prelude'.return (\ o -> o{obs_wards_placed = v}))
        parse'assist_player0
         = P'.try
            (do
               v <- P'.getT "assist_player0"
               Prelude'.return (\ o -> o{assist_player0 = v}))
        parse'assist_player1
         = P'.try
            (do
               v <- P'.getT "assist_player1"
               Prelude'.return (\ o -> o{assist_player1 = v}))
        parse'assist_player2
         = P'.try
            (do
               v <- P'.getT "assist_player2"
               Prelude'.return (\ o -> o{assist_player2 = v}))
        parse'assist_player3
         = P'.try
            (do
               v <- P'.getT "assist_player3"
               Prelude'.return (\ o -> o{assist_player3 = v}))
        parse'stack_count
         = P'.try
            (do
               v <- P'.getT "stack_count"
               Prelude'.return (\ o -> o{stack_count = v}))
        parse'hidden_modifier
         = P'.try
            (do
               v <- P'.getT "hidden_modifier"
               Prelude'.return (\ o -> o{hidden_modifier = v}))
        parse'is_target_building
         = P'.try
            (do
               v <- P'.getT "is_target_building"
               Prelude'.return (\ o -> o{is_target_building = v}))
        parse'neutral_camp_type
         = P'.try
            (do
               v <- P'.getT "neutral_camp_type"
               Prelude'.return (\ o -> o{neutral_camp_type = v}))
        parse'rune_type
         = P'.try
            (do
               v <- P'.getT "rune_type"
               Prelude'.return (\ o -> o{rune_type = v}))
        parse'assist_players
         = P'.try
            (do
               v <- P'.getT "assist_players"
               Prelude'.return (\ o -> o{assist_players = P'.append (assist_players o) v}))
        parse'is_heal_save
         = P'.try
            (do
               v <- P'.getT "is_heal_save"
               Prelude'.return (\ o -> o{is_heal_save = v}))
        parse'is_ultimate_ability
         = P'.try
            (do
               v <- P'.getT "is_ultimate_ability"
               Prelude'.return (\ o -> o{is_ultimate_ability = v}))
        parse'attacker_hero_level
         = P'.try
            (do
               v <- P'.getT "attacker_hero_level"
               Prelude'.return (\ o -> o{attacker_hero_level = v}))
        parse'target_hero_level
         = P'.try
            (do
               v <- P'.getT "target_hero_level"
               Prelude'.return (\ o -> o{target_hero_level = v}))
        parse'xpm
         = P'.try
            (do
               v <- P'.getT "xpm"
               Prelude'.return (\ o -> o{xpm = v}))
        parse'gpm
         = P'.try
            (do
               v <- P'.getT "gpm"
               Prelude'.return (\ o -> o{gpm = v}))
        parse'event_location
         = P'.try
            (do
               v <- P'.getT "event_location"
               Prelude'.return (\ o -> o{event_location = v}))
        parse'target_is_self
         = P'.try
            (do
               v <- P'.getT "target_is_self"
               Prelude'.return (\ o -> o{target_is_self = v}))
        parse'damage_type
         = P'.try
            (do
               v <- P'.getT "damage_type"
               Prelude'.return (\ o -> o{damage_type = v}))
        parse'invisibility_modifier
         = P'.try
            (do
               v <- P'.getT "invisibility_modifier"
               Prelude'.return (\ o -> o{invisibility_modifier = v}))
        parse'damage_category
         = P'.try
            (do
               v <- P'.getT "damage_category"
               Prelude'.return (\ o -> o{damage_category = v}))
        parse'networth
         = P'.try
            (do
               v <- P'.getT "networth"
               Prelude'.return (\ o -> o{networth = v}))
        parse'building_type
         = P'.try
            (do
               v <- P'.getT "building_type"
               Prelude'.return (\ o -> o{building_type = v}))
        parse'modifier_elapsed_duration
         = P'.try
            (do
               v <- P'.getT "modifier_elapsed_duration"
               Prelude'.return (\ o -> o{modifier_elapsed_duration = v}))
        parse'silence_modifier
         = P'.try
            (do
               v <- P'.getT "silence_modifier"
               Prelude'.return (\ o -> o{silence_modifier = v}))
        parse'heal_from_lifesteal
         = P'.try
            (do
               v <- P'.getT "heal_from_lifesteal"
               Prelude'.return (\ o -> o{heal_from_lifesteal = v}))
        parse'modifier_purged
         = P'.try
            (do
               v <- P'.getT "modifier_purged"
               Prelude'.return (\ o -> o{modifier_purged = v}))
        parse'spell_evaded
         = P'.try
            (do
               v <- P'.getT "spell_evaded"
               Prelude'.return (\ o -> o{spell_evaded = v}))
        parse'motion_controller_modifier
         = P'.try
            (do
               v <- P'.getT "motion_controller_modifier"
               Prelude'.return (\ o -> o{motion_controller_modifier = v}))
        parse'long_range_kill
         = P'.try
            (do
               v <- P'.getT "long_range_kill"
               Prelude'.return (\ o -> o{long_range_kill = v}))
        parse'modifier_purge_ability
         = P'.try
            (do
               v <- P'.getT "modifier_purge_ability"
               Prelude'.return (\ o -> o{modifier_purge_ability = v}))
        parse'modifier_purge_npc
         = P'.try
            (do
               v <- P'.getT "modifier_purge_npc"
               Prelude'.return (\ o -> o{modifier_purge_npc = v}))
        parse'root_modifier
         = P'.try
            (do
               v <- P'.getT "root_modifier"
               Prelude'.return (\ o -> o{root_modifier = v}))
        parse'total_unit_death_count
         = P'.try
            (do
               v <- P'.getT "total_unit_death_count"
               Prelude'.return (\ o -> o{total_unit_death_count = v}))
        parse'aura_modifier
         = P'.try
            (do
               v <- P'.getT "aura_modifier"
               Prelude'.return (\ o -> o{aura_modifier = v}))
        parse'armor_debuff_modifier
         = P'.try
            (do
               v <- P'.getT "armor_debuff_modifier"
               Prelude'.return (\ o -> o{armor_debuff_modifier = v}))
        parse'no_physical_damage_modifier
         = P'.try
            (do
               v <- P'.getT "no_physical_damage_modifier"
               Prelude'.return (\ o -> o{no_physical_damage_modifier = v}))