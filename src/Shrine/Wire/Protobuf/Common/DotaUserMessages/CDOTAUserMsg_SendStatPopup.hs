{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SendStatPopup (CDOTAUserMsg_SendStatPopup(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaCommonMessages.CDOTAMsg_SendStatPopup as DotaCommonMessages
       (CDOTAMsg_SendStatPopup)

data CDOTAUserMsg_SendStatPopup = CDOTAUserMsg_SendStatPopup{player_id :: !(P'.Maybe P'.Int32),
                                                             statpopup :: !(P'.Maybe DotaCommonMessages.CDOTAMsg_SendStatPopup)}
                                deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                          Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_SendStatPopup where
  mergeAppend (CDOTAUserMsg_SendStatPopup x'1 x'2) (CDOTAUserMsg_SendStatPopup y'1 y'2)
   = CDOTAUserMsg_SendStatPopup (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CDOTAUserMsg_SendStatPopup where
  defaultValue = CDOTAUserMsg_SendStatPopup P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAUserMsg_SendStatPopup where
  wireSize ft' self'@(CDOTAUserMsg_SendStatPopup x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 11 x'2)
  wirePut ft' self'@(CDOTAUserMsg_SendStatPopup x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 18 11 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{player_id = Prelude'.Just new'Field}) (P'.wireGet 5)
             18 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{statpopup = P'.mergeAppend (statpopup old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_SendStatPopup) CDOTAUserMsg_SendStatPopup where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_SendStatPopup

instance P'.ReflectDescriptor CDOTAUserMsg_SendStatPopup where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_SendStatPopup\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_SendStatPopup\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_SendStatPopup.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SendStatPopup.player_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SendStatPopup\"], baseName' = FName \"player_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SendStatPopup.statpopup\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SendStatPopup\"], baseName' = FName \"statpopup\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Commonmessages.CDOTAMsg_SendStatPopup\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaCommonMessages\"], baseName = MName \"CDOTAMsg_SendStatPopup\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_SendStatPopup where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_SendStatPopup where
  textPut msg
   = do
       P'.tellT "player_id" (player_id msg)
       P'.tellT "statpopup" (statpopup msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'player_id, parse'statpopup]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'player_id
         = P'.try
            (do
               v <- P'.getT "player_id"
               Prelude'.return (\ o -> o{player_id = v}))
        parse'statpopup
         = P'.try
            (do
               v <- P'.getT "statpopup"
               Prelude'.return (\ o -> o{statpopup = v}))