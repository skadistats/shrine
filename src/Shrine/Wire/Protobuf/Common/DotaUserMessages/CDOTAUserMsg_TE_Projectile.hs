{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_TE_Projectile (CDOTAUserMsg_TE_Projectile(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTAUserMsg_TE_Projectile = CDOTAUserMsg_TE_Projectile{hSource :: !(P'.Maybe P'.Int32), hTarget :: !(P'.Maybe P'.Int32),
                                                             moveSpeed :: !(P'.Maybe P'.Int32),
                                                             sourceAttachment :: !(P'.Maybe P'.Int32),
                                                             particleSystemHandle :: !(P'.Maybe P'.Int64),
                                                             dodgeable :: !(P'.Maybe P'.Bool), isAttack :: !(P'.Maybe P'.Bool),
                                                             isEvaded :: !(P'.Maybe P'.Bool), expireTime :: !(P'.Maybe P'.Float),
                                                             maximpacttime :: !(P'.Maybe P'.Float),
                                                             colorgemcolor :: !(P'.Maybe P'.Word32),
                                                             launch_tick :: !(P'.Maybe P'.Int32)}
                                deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                          Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_TE_Projectile where
  mergeAppend (CDOTAUserMsg_TE_Projectile x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12)
   (CDOTAUserMsg_TE_Projectile y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12)
   = CDOTAUserMsg_TE_Projectile (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)

instance P'.Default CDOTAUserMsg_TE_Projectile where
  defaultValue
   = CDOTAUserMsg_TE_Projectile P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAUserMsg_TE_Projectile where
  wireSize ft' self'@(CDOTAUserMsg_TE_Projectile x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeOpt 1 5 x'4 +
             P'.wireSizeOpt 1 3 x'5
             + P'.wireSizeOpt 1 8 x'6
             + P'.wireSizeOpt 1 8 x'7
             + P'.wireSizeOpt 1 8 x'8
             + P'.wireSizeOpt 1 2 x'9
             + P'.wireSizeOpt 1 2 x'10
             + P'.wireSizeOpt 1 7 x'11
             + P'.wireSizeOpt 1 5 x'12)
  wirePut ft' self'@(CDOTAUserMsg_TE_Projectile x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutOpt 32 5 x'4
             P'.wirePutOpt 40 3 x'5
             P'.wirePutOpt 48 8 x'6
             P'.wirePutOpt 56 8 x'7
             P'.wirePutOpt 64 8 x'8
             P'.wirePutOpt 77 2 x'9
             P'.wirePutOpt 85 2 x'10
             P'.wirePutOpt 93 7 x'11
             P'.wirePutOpt 96 5 x'12
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{hSource = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{hTarget = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{moveSpeed = Prelude'.Just new'Field}) (P'.wireGet 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{sourceAttachment = Prelude'.Just new'Field}) (P'.wireGet 5)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{particleSystemHandle = Prelude'.Just new'Field}) (P'.wireGet 3)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{dodgeable = Prelude'.Just new'Field}) (P'.wireGet 8)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{isAttack = Prelude'.Just new'Field}) (P'.wireGet 8)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{isEvaded = Prelude'.Just new'Field}) (P'.wireGet 8)
             77 -> Prelude'.fmap (\ !new'Field -> old'Self{expireTime = Prelude'.Just new'Field}) (P'.wireGet 2)
             85 -> Prelude'.fmap (\ !new'Field -> old'Self{maximpacttime = Prelude'.Just new'Field}) (P'.wireGet 2)
             93 -> Prelude'.fmap (\ !new'Field -> old'Self{colorgemcolor = Prelude'.Just new'Field}) (P'.wireGet 7)
             96 -> Prelude'.fmap (\ !new'Field -> old'Self{launch_tick = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_TE_Projectile) CDOTAUserMsg_TE_Projectile where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_TE_Projectile

instance P'.ReflectDescriptor CDOTAUserMsg_TE_Projectile where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 32, 40, 48, 56, 64, 77, 85, 93, 96])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_TE_Projectile\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_TE_Projectile.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.hSource\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"hSource\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.hTarget\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"hTarget\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.moveSpeed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"moveSpeed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.sourceAttachment\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"sourceAttachment\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.particleSystemHandle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"particleSystemHandle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 3}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.dodgeable\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"dodgeable\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.isAttack\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"isAttack\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.isEvaded\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"isEvaded\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.expireTime\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"expireTime\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 77}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.maximpacttime\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"maximpacttime\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 85}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.colorgemcolor\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"colorgemcolor\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 93}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_Projectile.launch_tick\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_Projectile\"], baseName' = FName \"launch_tick\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 96}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_TE_Projectile where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_TE_Projectile where
  textPut msg
   = do
       P'.tellT "hSource" (hSource msg)
       P'.tellT "hTarget" (hTarget msg)
       P'.tellT "moveSpeed" (moveSpeed msg)
       P'.tellT "sourceAttachment" (sourceAttachment msg)
       P'.tellT "particleSystemHandle" (particleSystemHandle msg)
       P'.tellT "dodgeable" (dodgeable msg)
       P'.tellT "isAttack" (isAttack msg)
       P'.tellT "isEvaded" (isEvaded msg)
       P'.tellT "expireTime" (expireTime msg)
       P'.tellT "maximpacttime" (maximpacttime msg)
       P'.tellT "colorgemcolor" (colorgemcolor msg)
       P'.tellT "launch_tick" (launch_tick msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'hSource, parse'hTarget, parse'moveSpeed, parse'sourceAttachment, parse'particleSystemHandle,
                   parse'dodgeable, parse'isAttack, parse'isEvaded, parse'expireTime, parse'maximpacttime, parse'colorgemcolor,
                   parse'launch_tick])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'hSource
         = P'.try
            (do
               v <- P'.getT "hSource"
               Prelude'.return (\ o -> o{hSource = v}))
        parse'hTarget
         = P'.try
            (do
               v <- P'.getT "hTarget"
               Prelude'.return (\ o -> o{hTarget = v}))
        parse'moveSpeed
         = P'.try
            (do
               v <- P'.getT "moveSpeed"
               Prelude'.return (\ o -> o{moveSpeed = v}))
        parse'sourceAttachment
         = P'.try
            (do
               v <- P'.getT "sourceAttachment"
               Prelude'.return (\ o -> o{sourceAttachment = v}))
        parse'particleSystemHandle
         = P'.try
            (do
               v <- P'.getT "particleSystemHandle"
               Prelude'.return (\ o -> o{particleSystemHandle = v}))
        parse'dodgeable
         = P'.try
            (do
               v <- P'.getT "dodgeable"
               Prelude'.return (\ o -> o{dodgeable = v}))
        parse'isAttack
         = P'.try
            (do
               v <- P'.getT "isAttack"
               Prelude'.return (\ o -> o{isAttack = v}))
        parse'isEvaded
         = P'.try
            (do
               v <- P'.getT "isEvaded"
               Prelude'.return (\ o -> o{isEvaded = v}))
        parse'expireTime
         = P'.try
            (do
               v <- P'.getT "expireTime"
               Prelude'.return (\ o -> o{expireTime = v}))
        parse'maximpacttime
         = P'.try
            (do
               v <- P'.getT "maximpacttime"
               Prelude'.return (\ o -> o{maximpacttime = v}))
        parse'colorgemcolor
         = P'.try
            (do
               v <- P'.getT "colorgemcolor"
               Prelude'.return (\ o -> o{colorgemcolor = v}))
        parse'launch_tick
         = P'.try
            (do
               v <- P'.getT "launch_tick"
               Prelude'.return (\ o -> o{launch_tick = v}))