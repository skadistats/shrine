{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_TE_UnitAnimation (CDOTAUserMsg_TE_UnitAnimation(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTAUserMsg_TE_UnitAnimation = CDOTAUserMsg_TE_UnitAnimation{entity :: !(P'.Maybe P'.Int32),
                                                                   sequenceVariant :: !(P'.Maybe P'.Int32),
                                                                   playbackrate :: !(P'.Maybe P'.Float),
                                                                   castpoint :: !(P'.Maybe P'.Float), type' :: !(P'.Maybe P'.Int32),
                                                                   activity :: !(P'.Maybe P'.Int32)}
                                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                             Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_TE_UnitAnimation where
  mergeAppend (CDOTAUserMsg_TE_UnitAnimation x'1 x'2 x'3 x'4 x'5 x'6) (CDOTAUserMsg_TE_UnitAnimation y'1 y'2 y'3 y'4 y'5 y'6)
   = CDOTAUserMsg_TE_UnitAnimation (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)
      (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CDOTAUserMsg_TE_UnitAnimation where
  defaultValue
   = CDOTAUserMsg_TE_UnitAnimation P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAUserMsg_TE_UnitAnimation where
  wireSize ft' self'@(CDOTAUserMsg_TE_UnitAnimation x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 2 x'3 + P'.wireSizeOpt 1 2 x'4 +
             P'.wireSizeOpt 1 5 x'5
             + P'.wireSizeOpt 1 5 x'6)
  wirePut ft' self'@(CDOTAUserMsg_TE_UnitAnimation x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 29 2 x'3
             P'.wirePutOpt 37 2 x'4
             P'.wirePutOpt 40 5 x'5
             P'.wirePutOpt 48 5 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{entity = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{sequenceVariant = Prelude'.Just new'Field}) (P'.wireGet 5)
             29 -> Prelude'.fmap (\ !new'Field -> old'Self{playbackrate = Prelude'.Just new'Field}) (P'.wireGet 2)
             37 -> Prelude'.fmap (\ !new'Field -> old'Self{castpoint = Prelude'.Just new'Field}) (P'.wireGet 2)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{type' = Prelude'.Just new'Field}) (P'.wireGet 5)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{activity = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_TE_UnitAnimation) CDOTAUserMsg_TE_UnitAnimation where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_TE_UnitAnimation

instance P'.ReflectDescriptor CDOTAUserMsg_TE_UnitAnimation where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 29, 37, 40, 48])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_UnitAnimation\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_TE_UnitAnimation\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_TE_UnitAnimation.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_UnitAnimation.entity\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_UnitAnimation\"], baseName' = FName \"entity\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_UnitAnimation.sequenceVariant\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_UnitAnimation\"], baseName' = FName \"sequenceVariant\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_UnitAnimation.playbackrate\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_UnitAnimation\"], baseName' = FName \"playbackrate\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 29}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_UnitAnimation.castpoint\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_UnitAnimation\"], baseName' = FName \"castpoint\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 37}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_UnitAnimation.type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_UnitAnimation\"], baseName' = FName \"type'\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_UnitAnimation.activity\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_UnitAnimation\"], baseName' = FName \"activity\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_TE_UnitAnimation where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_TE_UnitAnimation where
  textPut msg
   = do
       P'.tellT "entity" (entity msg)
       P'.tellT "sequenceVariant" (sequenceVariant msg)
       P'.tellT "playbackrate" (playbackrate msg)
       P'.tellT "castpoint" (castpoint msg)
       P'.tellT "type" (type' msg)
       P'.tellT "activity" (activity msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice [parse'entity, parse'sequenceVariant, parse'playbackrate, parse'castpoint, parse'type', parse'activity])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'entity
         = P'.try
            (do
               v <- P'.getT "entity"
               Prelude'.return (\ o -> o{entity = v}))
        parse'sequenceVariant
         = P'.try
            (do
               v <- P'.getT "sequenceVariant"
               Prelude'.return (\ o -> o{sequenceVariant = v}))
        parse'playbackrate
         = P'.try
            (do
               v <- P'.getT "playbackrate"
               Prelude'.return (\ o -> o{playbackrate = v}))
        parse'castpoint
         = P'.try
            (do
               v <- P'.getT "castpoint"
               Prelude'.return (\ o -> o{castpoint = v}))
        parse'type'
         = P'.try
            (do
               v <- P'.getT "type"
               Prelude'.return (\ o -> o{type' = v}))
        parse'activity
         = P'.try
            (do
               v <- P'.getT "activity"
               Prelude'.return (\ o -> o{activity = v}))