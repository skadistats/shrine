{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_UnitEvent (CDOTAUserMsg_UnitEvent(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTASpeechMatchOnClient as DotaUserMessages
       (CDOTASpeechMatchOnClient)
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_UnitEvent.AddGesture
       as DotaUserMessages.CDOTAUserMsg_UnitEvent (AddGesture)
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_UnitEvent.BloodImpact
       as DotaUserMessages.CDOTAUserMsg_UnitEvent (BloodImpact)
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_UnitEvent.FadeGesture
       as DotaUserMessages.CDOTAUserMsg_UnitEvent (FadeGesture)
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_UnitEvent.RemoveGesture
       as DotaUserMessages.CDOTAUserMsg_UnitEvent (RemoveGesture)
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_UnitEvent.Speech
       as DotaUserMessages.CDOTAUserMsg_UnitEvent (Speech)
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_UnitEvent.SpeechMute
       as DotaUserMessages.CDOTAUserMsg_UnitEvent (SpeechMute)
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.EDotaEntityMessages as DotaUserMessages (EDotaEntityMessages)

data CDOTAUserMsg_UnitEvent = CDOTAUserMsg_UnitEvent{msg_type :: !(DotaUserMessages.EDotaEntityMessages),
                                                     entity_index :: !(P'.Int32),
                                                     speech :: !(P'.Maybe DotaUserMessages.CDOTAUserMsg_UnitEvent.Speech),
                                                     speech_mute :: !(P'.Maybe DotaUserMessages.CDOTAUserMsg_UnitEvent.SpeechMute),
                                                     add_gesture :: !(P'.Maybe DotaUserMessages.CDOTAUserMsg_UnitEvent.AddGesture),
                                                     remove_gesture ::
                                                     !(P'.Maybe DotaUserMessages.CDOTAUserMsg_UnitEvent.RemoveGesture),
                                                     blood_impact ::
                                                     !(P'.Maybe DotaUserMessages.CDOTAUserMsg_UnitEvent.BloodImpact),
                                                     fade_gesture ::
                                                     !(P'.Maybe DotaUserMessages.CDOTAUserMsg_UnitEvent.FadeGesture),
                                                     speech_match_on_client ::
                                                     !(P'.Maybe DotaUserMessages.CDOTASpeechMatchOnClient)}
                            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_UnitEvent where
  mergeAppend (CDOTAUserMsg_UnitEvent x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9)
   (CDOTAUserMsg_UnitEvent y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9)
   = CDOTAUserMsg_UnitEvent (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)

instance P'.Default CDOTAUserMsg_UnitEvent where
  defaultValue
   = CDOTAUserMsg_UnitEvent (Prelude'.read "DOTA_UNIT_SPEECH") P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAUserMsg_UnitEvent where
  wireSize ft' self'@(CDOTAUserMsg_UnitEvent x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeReq 1 14 x'1 + P'.wireSizeReq 1 5 x'2 + P'.wireSizeOpt 1 11 x'3 + P'.wireSizeOpt 1 11 x'4 +
             P'.wireSizeOpt 1 11 x'5
             + P'.wireSizeOpt 1 11 x'6
             + P'.wireSizeOpt 1 11 x'7
             + P'.wireSizeOpt 1 11 x'8
             + P'.wireSizeOpt 1 11 x'9)
  wirePut ft' self'@(CDOTAUserMsg_UnitEvent x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutReq 8 14 x'1
             P'.wirePutReq 16 5 x'2
             P'.wirePutOpt 26 11 x'3
             P'.wirePutOpt 34 11 x'4
             P'.wirePutOpt 42 11 x'5
             P'.wirePutOpt 50 11 x'6
             P'.wirePutOpt 58 11 x'7
             P'.wirePutOpt 66 11 x'8
             P'.wirePutOpt 74 11 x'9
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{msg_type = new'Field}) (P'.wireGet 14)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{entity_index = new'Field}) (P'.wireGet 5)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{speech = P'.mergeAppend (speech old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             34 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{speech_mute = P'.mergeAppend (speech_mute old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             42 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{add_gesture = P'.mergeAppend (add_gesture old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             50 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{remove_gesture = P'.mergeAppend (remove_gesture old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             58 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{blood_impact = P'.mergeAppend (blood_impact old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             66 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{fade_gesture = P'.mergeAppend (fade_gesture old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             74 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{speech_match_on_client = P'.mergeAppend (speech_match_on_client old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_UnitEvent) CDOTAUserMsg_UnitEvent where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_UnitEvent

instance P'.ReflectDescriptor CDOTAUserMsg_UnitEvent where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList [8, 16]) (P'.fromDistinctAscList [8, 16, 26, 34, 42, 50, 58, 66, 74])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_UnitEvent\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_UnitEvent.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.msg_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName' = FName \"msg_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.EDotaEntityMessages\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"EDotaEntityMessages\"}), hsRawDefault = Just \"DOTA_UNIT_SPEECH\", hsDefault = Just (HsDef'Enum \"DOTA_UNIT_SPEECH\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.entity_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName' = FName \"entity_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.speech\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName' = FName \"speech\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.Speech\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName = MName \"Speech\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.speech_mute\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName' = FName \"speech_mute\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 34}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.SpeechMute\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName = MName \"SpeechMute\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.add_gesture\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName' = FName \"add_gesture\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.AddGesture\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName = MName \"AddGesture\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.remove_gesture\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName' = FName \"remove_gesture\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 50}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.RemoveGesture\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName = MName \"RemoveGesture\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.blood_impact\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName' = FName \"blood_impact\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 58}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.BloodImpact\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName = MName \"BloodImpact\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.fade_gesture\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName' = FName \"fade_gesture\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 66}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.FadeGesture\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName = MName \"FadeGesture\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_UnitEvent.speech_match_on_client\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_UnitEvent\"], baseName' = FName \"speech_match_on_client\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 74}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTASpeechMatchOnClient\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTASpeechMatchOnClient\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_UnitEvent where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_UnitEvent where
  textPut msg
   = do
       P'.tellT "msg_type" (msg_type msg)
       P'.tellT "entity_index" (entity_index msg)
       P'.tellT "speech" (speech msg)
       P'.tellT "speech_mute" (speech_mute msg)
       P'.tellT "add_gesture" (add_gesture msg)
       P'.tellT "remove_gesture" (remove_gesture msg)
       P'.tellT "blood_impact" (blood_impact msg)
       P'.tellT "fade_gesture" (fade_gesture msg)
       P'.tellT "speech_match_on_client" (speech_match_on_client msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'msg_type, parse'entity_index, parse'speech, parse'speech_mute, parse'add_gesture, parse'remove_gesture,
                   parse'blood_impact, parse'fade_gesture, parse'speech_match_on_client])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'msg_type
         = P'.try
            (do
               v <- P'.getT "msg_type"
               Prelude'.return (\ o -> o{msg_type = v}))
        parse'entity_index
         = P'.try
            (do
               v <- P'.getT "entity_index"
               Prelude'.return (\ o -> o{entity_index = v}))
        parse'speech
         = P'.try
            (do
               v <- P'.getT "speech"
               Prelude'.return (\ o -> o{speech = v}))
        parse'speech_mute
         = P'.try
            (do
               v <- P'.getT "speech_mute"
               Prelude'.return (\ o -> o{speech_mute = v}))
        parse'add_gesture
         = P'.try
            (do
               v <- P'.getT "add_gesture"
               Prelude'.return (\ o -> o{add_gesture = v}))
        parse'remove_gesture
         = P'.try
            (do
               v <- P'.getT "remove_gesture"
               Prelude'.return (\ o -> o{remove_gesture = v}))
        parse'blood_impact
         = P'.try
            (do
               v <- P'.getT "blood_impact"
               Prelude'.return (\ o -> o{blood_impact = v}))
        parse'fade_gesture
         = P'.try
            (do
               v <- P'.getT "fade_gesture"
               Prelude'.return (\ o -> o{fade_gesture = v}))
        parse'speech_match_on_client
         = P'.try
            (do
               v <- P'.getT "speech_match_on_client"
               Prelude'.return (\ o -> o{speech_match_on_client = v}))