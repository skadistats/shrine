{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMessage_TeamCaptainChanged (CDOTAUserMessage_TeamCaptainChanged(..))
       where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTAUserMessage_TeamCaptainChanged = CDOTAUserMessage_TeamCaptainChanged{team :: !(P'.Maybe P'.Word32),
                                                                               captain_player_id :: !(P'.Maybe P'.Word32)}
                                         deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                   Prelude'.Generic)

instance P'.Mergeable CDOTAUserMessage_TeamCaptainChanged where
  mergeAppend (CDOTAUserMessage_TeamCaptainChanged x'1 x'2) (CDOTAUserMessage_TeamCaptainChanged y'1 y'2)
   = CDOTAUserMessage_TeamCaptainChanged (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CDOTAUserMessage_TeamCaptainChanged where
  defaultValue = CDOTAUserMessage_TeamCaptainChanged P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAUserMessage_TeamCaptainChanged where
  wireSize ft' self'@(CDOTAUserMessage_TeamCaptainChanged x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 13 x'2)
  wirePut ft' self'@(CDOTAUserMessage_TeamCaptainChanged x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 13 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{team = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{captain_player_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMessage_TeamCaptainChanged) CDOTAUserMessage_TeamCaptainChanged where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMessage_TeamCaptainChanged

instance P'.ReflectDescriptor CDOTAUserMessage_TeamCaptainChanged where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMessage_TeamCaptainChanged\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMessage_TeamCaptainChanged\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMessage_TeamCaptainChanged.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMessage_TeamCaptainChanged.team\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMessage_TeamCaptainChanged\"], baseName' = FName \"team\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMessage_TeamCaptainChanged.captain_player_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMessage_TeamCaptainChanged\"], baseName' = FName \"captain_player_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMessage_TeamCaptainChanged where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMessage_TeamCaptainChanged where
  textPut msg
   = do
       P'.tellT "team" (team msg)
       P'.tellT "captain_player_id" (captain_player_id msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'team, parse'captain_player_id]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'team
         = P'.try
            (do
               v <- P'.getT "team"
               Prelude'.return (\ o -> o{team = v}))
        parse'captain_player_id
         = P'.try
            (do
               v <- P'.getT "captain_player_id"
               Prelude'.return (\ o -> o{captain_player_id = v}))