{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_SendRoshanSpectatorPhase
       (CDOTAUserMsg_SendRoshanSpectatorPhase(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ROSHAN_PHASE as DotaUserMessages (DOTA_ROSHAN_PHASE)

data CDOTAUserMsg_SendRoshanSpectatorPhase = CDOTAUserMsg_SendRoshanSpectatorPhase{phase ::
                                                                                   !(P'.Maybe DotaUserMessages.DOTA_ROSHAN_PHASE),
                                                                                   phase_start_time :: !(P'.Maybe P'.Int32),
                                                                                   phase_length :: !(P'.Maybe P'.Int32)}
                                           deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                     Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_SendRoshanSpectatorPhase where
  mergeAppend (CDOTAUserMsg_SendRoshanSpectatorPhase x'1 x'2 x'3) (CDOTAUserMsg_SendRoshanSpectatorPhase y'1 y'2 y'3)
   = CDOTAUserMsg_SendRoshanSpectatorPhase (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default CDOTAUserMsg_SendRoshanSpectatorPhase where
  defaultValue
   = CDOTAUserMsg_SendRoshanSpectatorPhase (Prelude'.Just (Prelude'.read "K_SRSP_ROSHAN_ALIVE")) P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAUserMsg_SendRoshanSpectatorPhase where
  wireSize ft' self'@(CDOTAUserMsg_SendRoshanSpectatorPhase x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 14 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 5 x'3)
  wirePut ft' self'@(CDOTAUserMsg_SendRoshanSpectatorPhase x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 14 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 5 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{phase = Prelude'.Just new'Field}) (P'.wireGet 14)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{phase_start_time = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{phase_length = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_SendRoshanSpectatorPhase) CDOTAUserMsg_SendRoshanSpectatorPhase where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_SendRoshanSpectatorPhase

instance P'.ReflectDescriptor CDOTAUserMsg_SendRoshanSpectatorPhase where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_SendRoshanSpectatorPhase\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_SendRoshanSpectatorPhase\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_SendRoshanSpectatorPhase.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SendRoshanSpectatorPhase.phase\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SendRoshanSpectatorPhase\"], baseName' = FName \"phase\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.DOTA_ROSHAN_PHASE\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"DOTA_ROSHAN_PHASE\"}), hsRawDefault = Just \"k_SRSP_ROSHAN_ALIVE\", hsDefault = Just (HsDef'Enum \"K_SRSP_ROSHAN_ALIVE\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SendRoshanSpectatorPhase.phase_start_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SendRoshanSpectatorPhase\"], baseName' = FName \"phase_start_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_SendRoshanSpectatorPhase.phase_length\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_SendRoshanSpectatorPhase\"], baseName' = FName \"phase_length\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_SendRoshanSpectatorPhase where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_SendRoshanSpectatorPhase where
  textPut msg
   = do
       P'.tellT "phase" (phase msg)
       P'.tellT "phase_start_time" (phase_start_time msg)
       P'.tellT "phase_length" (phase_length msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'phase, parse'phase_start_time, parse'phase_length]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'phase
         = P'.try
            (do
               v <- P'.getT "phase"
               Prelude'.return (\ o -> o{phase = v}))
        parse'phase_start_time
         = P'.try
            (do
               v <- P'.getT "phase_start_time"
               Prelude'.return (\ o -> o{phase_start_time = v}))
        parse'phase_length
         = P'.try
            (do
               v <- P'.getT "phase_length"
               Prelude'.return (\ o -> o{phase_length = v}))