{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_ItemPurchased (CDOTAUserMsg_ItemPurchased(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTAUserMsg_ItemPurchased = CDOTAUserMsg_ItemPurchased{item_index :: !(P'.Maybe P'.Int32)}
                                deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                          Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_ItemPurchased where
  mergeAppend (CDOTAUserMsg_ItemPurchased x'1) (CDOTAUserMsg_ItemPurchased y'1)
   = CDOTAUserMsg_ItemPurchased (P'.mergeAppend x'1 y'1)

instance P'.Default CDOTAUserMsg_ItemPurchased where
  defaultValue = CDOTAUserMsg_ItemPurchased P'.defaultValue

instance P'.Wire CDOTAUserMsg_ItemPurchased where
  wireSize ft' self'@(CDOTAUserMsg_ItemPurchased x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1)
  wirePut ft' self'@(CDOTAUserMsg_ItemPurchased x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{item_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_ItemPurchased) CDOTAUserMsg_ItemPurchased where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_ItemPurchased

instance P'.ReflectDescriptor CDOTAUserMsg_ItemPurchased where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_ItemPurchased\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_ItemPurchased\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_ItemPurchased.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_ItemPurchased.item_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_ItemPurchased\"], baseName' = FName \"item_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_ItemPurchased where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_ItemPurchased where
  textPut msg
   = do
       P'.tellT "item_index" (item_index msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'item_index]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'item_index
         = P'.try
            (do
               v <- P'.getT "item_index"
               Prelude'.return (\ o -> o{item_index = v}))