{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.EProjectionEvent (EProjectionEvent(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EProjectionEvent = EPE_FirstBlood
                      | EPE_Killstreak_godlike
                      deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                Prelude'.Generic)

instance P'.Mergeable EProjectionEvent

instance Prelude'.Bounded EProjectionEvent where
  minBound = EPE_FirstBlood
  maxBound = EPE_Killstreak_godlike

instance P'.Default EProjectionEvent where
  defaultValue = EPE_FirstBlood

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EProjectionEvent
toMaybe'Enum 0 = Prelude'.Just EPE_FirstBlood
toMaybe'Enum 1 = Prelude'.Just EPE_Killstreak_godlike
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EProjectionEvent where
  fromEnum EPE_FirstBlood = 0
  fromEnum EPE_Killstreak_godlike = 1
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EProjectionEvent")
      . toMaybe'Enum
  succ EPE_FirstBlood = EPE_Killstreak_godlike
  succ _
   = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EProjectionEvent"
  pred EPE_Killstreak_godlike = EPE_FirstBlood
  pred _
   = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.EProjectionEvent"

instance P'.Wire EProjectionEvent where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EProjectionEvent

instance P'.MessageAPI msg' (msg' -> EProjectionEvent) EProjectionEvent where
  getVal m' f' = f' m'

instance P'.ReflectEnum EProjectionEvent where
  reflectEnum = [(0, "EPE_FirstBlood", EPE_FirstBlood), (1, "EPE_Killstreak_godlike", EPE_Killstreak_godlike)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.EProjectionEvent") ["Shrine", "Wire", "Protobuf", "Common"] ["DotaUserMessages"]
        "EProjectionEvent")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "EProjectionEvent.hs"]
      [(0, "EPE_FirstBlood"), (1, "EPE_Killstreak_godlike")]

instance P'.TextType EProjectionEvent where
  tellT = P'.tellShow
  getT = P'.getRead