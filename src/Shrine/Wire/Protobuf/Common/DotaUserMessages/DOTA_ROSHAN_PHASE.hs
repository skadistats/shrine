{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ROSHAN_PHASE (DOTA_ROSHAN_PHASE(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data DOTA_ROSHAN_PHASE = K_SRSP_ROSHAN_ALIVE
                       | K_SRSP_ROSHAN_BASE_TIMER
                       | K_SRSP_ROSHAN_VISIBLE_TIMER
                       deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                 Prelude'.Generic)

instance P'.Mergeable DOTA_ROSHAN_PHASE

instance Prelude'.Bounded DOTA_ROSHAN_PHASE where
  minBound = K_SRSP_ROSHAN_ALIVE
  maxBound = K_SRSP_ROSHAN_VISIBLE_TIMER

instance P'.Default DOTA_ROSHAN_PHASE where
  defaultValue = K_SRSP_ROSHAN_ALIVE

toMaybe'Enum :: Prelude'.Int -> P'.Maybe DOTA_ROSHAN_PHASE
toMaybe'Enum 0 = Prelude'.Just K_SRSP_ROSHAN_ALIVE
toMaybe'Enum 1 = Prelude'.Just K_SRSP_ROSHAN_BASE_TIMER
toMaybe'Enum 2 = Prelude'.Just K_SRSP_ROSHAN_VISIBLE_TIMER
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum DOTA_ROSHAN_PHASE where
  fromEnum K_SRSP_ROSHAN_ALIVE = 0
  fromEnum K_SRSP_ROSHAN_BASE_TIMER = 1
  fromEnum K_SRSP_ROSHAN_VISIBLE_TIMER = 2
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ROSHAN_PHASE")
      . toMaybe'Enum
  succ K_SRSP_ROSHAN_ALIVE = K_SRSP_ROSHAN_BASE_TIMER
  succ K_SRSP_ROSHAN_BASE_TIMER = K_SRSP_ROSHAN_VISIBLE_TIMER
  succ _
   = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ROSHAN_PHASE"
  pred K_SRSP_ROSHAN_BASE_TIMER = K_SRSP_ROSHAN_ALIVE
  pred K_SRSP_ROSHAN_VISIBLE_TIMER = K_SRSP_ROSHAN_BASE_TIMER
  pred _
   = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_ROSHAN_PHASE"

instance P'.Wire DOTA_ROSHAN_PHASE where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB DOTA_ROSHAN_PHASE

instance P'.MessageAPI msg' (msg' -> DOTA_ROSHAN_PHASE) DOTA_ROSHAN_PHASE where
  getVal m' f' = f' m'

instance P'.ReflectEnum DOTA_ROSHAN_PHASE where
  reflectEnum
   = [(0, "K_SRSP_ROSHAN_ALIVE", K_SRSP_ROSHAN_ALIVE), (1, "K_SRSP_ROSHAN_BASE_TIMER", K_SRSP_ROSHAN_BASE_TIMER),
      (2, "K_SRSP_ROSHAN_VISIBLE_TIMER", K_SRSP_ROSHAN_VISIBLE_TIMER)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.DOTA_ROSHAN_PHASE") ["Shrine", "Wire", "Protobuf", "Common"] ["DotaUserMessages"]
        "DOTA_ROSHAN_PHASE")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "DOTA_ROSHAN_PHASE.hs"]
      [(0, "K_SRSP_ROSHAN_ALIVE"), (1, "K_SRSP_ROSHAN_BASE_TIMER"), (2, "K_SRSP_ROSHAN_VISIBLE_TIMER")]

instance P'.TextType DOTA_ROSHAN_PHASE where
  tellT = P'.tellShow
  getT = P'.getRead