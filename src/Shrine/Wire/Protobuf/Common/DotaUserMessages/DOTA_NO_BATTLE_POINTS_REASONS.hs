{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_NO_BATTLE_POINTS_REASONS (DOTA_NO_BATTLE_POINTS_REASONS(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data DOTA_NO_BATTLE_POINTS_REASONS = NO_BATTLE_POINTS_WRONG_LOBBY_TYPE
                                   | NO_BATTLE_POINTS_PRACTICE_BOTS
                                   | NO_BATTLE_POINTS_CHEATS_ENABLED
                                   | NO_BATTLE_POINTS_LOW_PRIORITY
                                   deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable,
                                             Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable DOTA_NO_BATTLE_POINTS_REASONS

instance Prelude'.Bounded DOTA_NO_BATTLE_POINTS_REASONS where
  minBound = NO_BATTLE_POINTS_WRONG_LOBBY_TYPE
  maxBound = NO_BATTLE_POINTS_LOW_PRIORITY

instance P'.Default DOTA_NO_BATTLE_POINTS_REASONS where
  defaultValue = NO_BATTLE_POINTS_WRONG_LOBBY_TYPE

toMaybe'Enum :: Prelude'.Int -> P'.Maybe DOTA_NO_BATTLE_POINTS_REASONS
toMaybe'Enum 1 = Prelude'.Just NO_BATTLE_POINTS_WRONG_LOBBY_TYPE
toMaybe'Enum 2 = Prelude'.Just NO_BATTLE_POINTS_PRACTICE_BOTS
toMaybe'Enum 3 = Prelude'.Just NO_BATTLE_POINTS_CHEATS_ENABLED
toMaybe'Enum 4 = Prelude'.Just NO_BATTLE_POINTS_LOW_PRIORITY
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum DOTA_NO_BATTLE_POINTS_REASONS where
  fromEnum NO_BATTLE_POINTS_WRONG_LOBBY_TYPE = 1
  fromEnum NO_BATTLE_POINTS_PRACTICE_BOTS = 2
  fromEnum NO_BATTLE_POINTS_CHEATS_ENABLED = 3
  fromEnum NO_BATTLE_POINTS_LOW_PRIORITY = 4
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_NO_BATTLE_POINTS_REASONS")
      . toMaybe'Enum
  succ NO_BATTLE_POINTS_WRONG_LOBBY_TYPE = NO_BATTLE_POINTS_PRACTICE_BOTS
  succ NO_BATTLE_POINTS_PRACTICE_BOTS = NO_BATTLE_POINTS_CHEATS_ENABLED
  succ NO_BATTLE_POINTS_CHEATS_ENABLED = NO_BATTLE_POINTS_LOW_PRIORITY
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_NO_BATTLE_POINTS_REASONS"
  pred NO_BATTLE_POINTS_PRACTICE_BOTS = NO_BATTLE_POINTS_WRONG_LOBBY_TYPE
  pred NO_BATTLE_POINTS_CHEATS_ENABLED = NO_BATTLE_POINTS_PRACTICE_BOTS
  pred NO_BATTLE_POINTS_LOW_PRIORITY = NO_BATTLE_POINTS_CHEATS_ENABLED
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaUserMessages.DOTA_NO_BATTLE_POINTS_REASONS"

instance P'.Wire DOTA_NO_BATTLE_POINTS_REASONS where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB DOTA_NO_BATTLE_POINTS_REASONS

instance P'.MessageAPI msg' (msg' -> DOTA_NO_BATTLE_POINTS_REASONS) DOTA_NO_BATTLE_POINTS_REASONS where
  getVal m' f' = f' m'

instance P'.ReflectEnum DOTA_NO_BATTLE_POINTS_REASONS where
  reflectEnum
   = [(1, "NO_BATTLE_POINTS_WRONG_LOBBY_TYPE", NO_BATTLE_POINTS_WRONG_LOBBY_TYPE),
      (2, "NO_BATTLE_POINTS_PRACTICE_BOTS", NO_BATTLE_POINTS_PRACTICE_BOTS),
      (3, "NO_BATTLE_POINTS_CHEATS_ENABLED", NO_BATTLE_POINTS_CHEATS_ENABLED),
      (4, "NO_BATTLE_POINTS_LOW_PRIORITY", NO_BATTLE_POINTS_LOW_PRIORITY)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Usermessages.DOTA_NO_BATTLE_POINTS_REASONS") ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaUserMessages"]
        "DOTA_NO_BATTLE_POINTS_REASONS")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaUserMessages", "DOTA_NO_BATTLE_POINTS_REASONS.hs"]
      [(1, "NO_BATTLE_POINTS_WRONG_LOBBY_TYPE"), (2, "NO_BATTLE_POINTS_PRACTICE_BOTS"), (3, "NO_BATTLE_POINTS_CHEATS_ENABLED"),
       (4, "NO_BATTLE_POINTS_LOW_PRIORITY")]

instance P'.TextType DOTA_NO_BATTLE_POINTS_REASONS where
  tellT = P'.tellShow
  getT = P'.getRead