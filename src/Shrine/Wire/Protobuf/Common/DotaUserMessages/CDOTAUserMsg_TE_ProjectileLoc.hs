{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_TE_ProjectileLoc (CDOTAUserMsg_TE_ProjectileLoc(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CDOTAUserMsg_TE_ProjectileLoc = CDOTAUserMsg_TE_ProjectileLoc{vSourceLoc :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                                                   hTarget :: !(P'.Maybe P'.Int32),
                                                                   moveSpeed :: !(P'.Maybe P'.Int32),
                                                                   particleSystemHandle :: !(P'.Maybe P'.Int64),
                                                                   dodgeable :: !(P'.Maybe P'.Bool),
                                                                   isAttack :: !(P'.Maybe P'.Bool), isEvaded :: !(P'.Maybe P'.Bool),
                                                                   expireTime :: !(P'.Maybe P'.Float),
                                                                   vTargetLoc :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                                                   colorgemcolor :: !(P'.Maybe P'.Word32),
                                                                   launch_tick :: !(P'.Maybe P'.Int32)}
                                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                             Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_TE_ProjectileLoc where
  mergeAppend (CDOTAUserMsg_TE_ProjectileLoc x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11)
   (CDOTAUserMsg_TE_ProjectileLoc y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11)
   = CDOTAUserMsg_TE_ProjectileLoc (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)
      (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)

instance P'.Default CDOTAUserMsg_TE_ProjectileLoc where
  defaultValue
   = CDOTAUserMsg_TE_ProjectileLoc P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAUserMsg_TE_ProjectileLoc where
  wireSize ft' self'@(CDOTAUserMsg_TE_ProjectileLoc x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 11 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeOpt 1 3 x'4 +
             P'.wireSizeOpt 1 8 x'5
             + P'.wireSizeOpt 1 8 x'6
             + P'.wireSizeOpt 1 8 x'7
             + P'.wireSizeOpt 1 2 x'8
             + P'.wireSizeOpt 1 11 x'9
             + P'.wireSizeOpt 1 7 x'10
             + P'.wireSizeOpt 1 5 x'11)
  wirePut ft' self'@(CDOTAUserMsg_TE_ProjectileLoc x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 11 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutOpt 32 3 x'4
             P'.wirePutOpt 40 8 x'5
             P'.wirePutOpt 48 8 x'6
             P'.wirePutOpt 64 8 x'7
             P'.wirePutOpt 77 2 x'8
             P'.wirePutOpt 82 11 x'9
             P'.wirePutOpt 93 7 x'10
             P'.wirePutOpt 96 5 x'11
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{vSourceLoc = P'.mergeAppend (vSourceLoc old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{hTarget = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{moveSpeed = Prelude'.Just new'Field}) (P'.wireGet 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{particleSystemHandle = Prelude'.Just new'Field}) (P'.wireGet 3)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{dodgeable = Prelude'.Just new'Field}) (P'.wireGet 8)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{isAttack = Prelude'.Just new'Field}) (P'.wireGet 8)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{isEvaded = Prelude'.Just new'Field}) (P'.wireGet 8)
             77 -> Prelude'.fmap (\ !new'Field -> old'Self{expireTime = Prelude'.Just new'Field}) (P'.wireGet 2)
             82 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{vTargetLoc = P'.mergeAppend (vTargetLoc old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             93 -> Prelude'.fmap (\ !new'Field -> old'Self{colorgemcolor = Prelude'.Just new'Field}) (P'.wireGet 7)
             96 -> Prelude'.fmap (\ !new'Field -> old'Self{launch_tick = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_TE_ProjectileLoc) CDOTAUserMsg_TE_ProjectileLoc where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_TE_ProjectileLoc

instance P'.ReflectDescriptor CDOTAUserMsg_TE_ProjectileLoc where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 16, 24, 32, 40, 48, 64, 77, 82, 93, 96])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_TE_ProjectileLoc\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_TE_ProjectileLoc.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.vSourceLoc\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"vSourceLoc\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.hTarget\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"hTarget\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.moveSpeed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"moveSpeed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.particleSystemHandle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"particleSystemHandle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 3}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.dodgeable\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"dodgeable\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.isAttack\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"isAttack\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.isEvaded\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"isEvaded\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.expireTime\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"expireTime\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 77}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.vTargetLoc\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"vTargetLoc\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 82}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.colorgemcolor\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"colorgemcolor\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 93}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_TE_ProjectileLoc.launch_tick\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_TE_ProjectileLoc\"], baseName' = FName \"launch_tick\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 96}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_TE_ProjectileLoc where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_TE_ProjectileLoc where
  textPut msg
   = do
       P'.tellT "vSourceLoc" (vSourceLoc msg)
       P'.tellT "hTarget" (hTarget msg)
       P'.tellT "moveSpeed" (moveSpeed msg)
       P'.tellT "particleSystemHandle" (particleSystemHandle msg)
       P'.tellT "dodgeable" (dodgeable msg)
       P'.tellT "isAttack" (isAttack msg)
       P'.tellT "isEvaded" (isEvaded msg)
       P'.tellT "expireTime" (expireTime msg)
       P'.tellT "vTargetLoc" (vTargetLoc msg)
       P'.tellT "colorgemcolor" (colorgemcolor msg)
       P'.tellT "launch_tick" (launch_tick msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'vSourceLoc, parse'hTarget, parse'moveSpeed, parse'particleSystemHandle, parse'dodgeable, parse'isAttack,
                   parse'isEvaded, parse'expireTime, parse'vTargetLoc, parse'colorgemcolor, parse'launch_tick])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'vSourceLoc
         = P'.try
            (do
               v <- P'.getT "vSourceLoc"
               Prelude'.return (\ o -> o{vSourceLoc = v}))
        parse'hTarget
         = P'.try
            (do
               v <- P'.getT "hTarget"
               Prelude'.return (\ o -> o{hTarget = v}))
        parse'moveSpeed
         = P'.try
            (do
               v <- P'.getT "moveSpeed"
               Prelude'.return (\ o -> o{moveSpeed = v}))
        parse'particleSystemHandle
         = P'.try
            (do
               v <- P'.getT "particleSystemHandle"
               Prelude'.return (\ o -> o{particleSystemHandle = v}))
        parse'dodgeable
         = P'.try
            (do
               v <- P'.getT "dodgeable"
               Prelude'.return (\ o -> o{dodgeable = v}))
        parse'isAttack
         = P'.try
            (do
               v <- P'.getT "isAttack"
               Prelude'.return (\ o -> o{isAttack = v}))
        parse'isEvaded
         = P'.try
            (do
               v <- P'.getT "isEvaded"
               Prelude'.return (\ o -> o{isEvaded = v}))
        parse'expireTime
         = P'.try
            (do
               v <- P'.getT "expireTime"
               Prelude'.return (\ o -> o{expireTime = v}))
        parse'vTargetLoc
         = P'.try
            (do
               v <- P'.getT "vTargetLoc"
               Prelude'.return (\ o -> o{vTargetLoc = v}))
        parse'colorgemcolor
         = P'.try
            (do
               v <- P'.getT "colorgemcolor"
               Prelude'.return (\ o -> o{colorgemcolor = v}))
        parse'launch_tick
         = P'.try
            (do
               v <- P'.getT "launch_tick"
               Prelude'.return (\ o -> o{launch_tick = v}))