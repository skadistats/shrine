{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_StatsPlayerKillShare (CDOTAUserMsg_StatsPlayerKillShare(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTAUserMsg_StatsPlayerKillShare = CDOTAUserMsg_StatsPlayerKillShare{player_id :: !(P'.Maybe P'.Int32),
                                                                           kill_share_percent :: !(P'.Maybe P'.Float),
                                                                           player_loc_x :: !(P'.Maybe P'.Float),
                                                                           player_loc_y :: !(P'.Maybe P'.Float),
                                                                           health_percent :: !(P'.Maybe P'.Float),
                                                                           mana_percent :: !(P'.Maybe P'.Float)}
                                       deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                 Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_StatsPlayerKillShare where
  mergeAppend (CDOTAUserMsg_StatsPlayerKillShare x'1 x'2 x'3 x'4 x'5 x'6)
   (CDOTAUserMsg_StatsPlayerKillShare y'1 y'2 y'3 y'4 y'5 y'6)
   = CDOTAUserMsg_StatsPlayerKillShare (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)
      (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CDOTAUserMsg_StatsPlayerKillShare where
  defaultValue
   = CDOTAUserMsg_StatsPlayerKillShare P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAUserMsg_StatsPlayerKillShare where
  wireSize ft' self'@(CDOTAUserMsg_StatsPlayerKillShare x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 2 x'2 + P'.wireSizeOpt 1 2 x'3 + P'.wireSizeOpt 1 2 x'4 +
             P'.wireSizeOpt 1 2 x'5
             + P'.wireSizeOpt 1 2 x'6)
  wirePut ft' self'@(CDOTAUserMsg_StatsPlayerKillShare x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 21 2 x'2
             P'.wirePutOpt 29 2 x'3
             P'.wirePutOpt 37 2 x'4
             P'.wirePutOpt 45 2 x'5
             P'.wirePutOpt 53 2 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{player_id = Prelude'.Just new'Field}) (P'.wireGet 5)
             21 -> Prelude'.fmap (\ !new'Field -> old'Self{kill_share_percent = Prelude'.Just new'Field}) (P'.wireGet 2)
             29 -> Prelude'.fmap (\ !new'Field -> old'Self{player_loc_x = Prelude'.Just new'Field}) (P'.wireGet 2)
             37 -> Prelude'.fmap (\ !new'Field -> old'Self{player_loc_y = Prelude'.Just new'Field}) (P'.wireGet 2)
             45 -> Prelude'.fmap (\ !new'Field -> old'Self{health_percent = Prelude'.Just new'Field}) (P'.wireGet 2)
             53 -> Prelude'.fmap (\ !new'Field -> old'Self{mana_percent = Prelude'.Just new'Field}) (P'.wireGet 2)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_StatsPlayerKillShare) CDOTAUserMsg_StatsPlayerKillShare where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_StatsPlayerKillShare

instance P'.ReflectDescriptor CDOTAUserMsg_StatsPlayerKillShare where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 21, 29, 37, 45, 53])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsPlayerKillShare\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_StatsPlayerKillShare\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_StatsPlayerKillShare.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsPlayerKillShare.player_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsPlayerKillShare\"], baseName' = FName \"player_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsPlayerKillShare.kill_share_percent\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsPlayerKillShare\"], baseName' = FName \"kill_share_percent\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 21}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsPlayerKillShare.player_loc_x\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsPlayerKillShare\"], baseName' = FName \"player_loc_x\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 29}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsPlayerKillShare.player_loc_y\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsPlayerKillShare\"], baseName' = FName \"player_loc_y\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 37}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsPlayerKillShare.health_percent\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsPlayerKillShare\"], baseName' = FName \"health_percent\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 45}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsPlayerKillShare.mana_percent\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsPlayerKillShare\"], baseName' = FName \"mana_percent\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 53}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_StatsPlayerKillShare where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_StatsPlayerKillShare where
  textPut msg
   = do
       P'.tellT "player_id" (player_id msg)
       P'.tellT "kill_share_percent" (kill_share_percent msg)
       P'.tellT "player_loc_x" (player_loc_x msg)
       P'.tellT "player_loc_y" (player_loc_y msg)
       P'.tellT "health_percent" (health_percent msg)
       P'.tellT "mana_percent" (mana_percent msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'player_id, parse'kill_share_percent, parse'player_loc_x, parse'player_loc_y, parse'health_percent,
                   parse'mana_percent])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'player_id
         = P'.try
            (do
               v <- P'.getT "player_id"
               Prelude'.return (\ o -> o{player_id = v}))
        parse'kill_share_percent
         = P'.try
            (do
               v <- P'.getT "kill_share_percent"
               Prelude'.return (\ o -> o{kill_share_percent = v}))
        parse'player_loc_x
         = P'.try
            (do
               v <- P'.getT "player_loc_x"
               Prelude'.return (\ o -> o{player_loc_x = v}))
        parse'player_loc_y
         = P'.try
            (do
               v <- P'.getT "player_loc_y"
               Prelude'.return (\ o -> o{player_loc_y = v}))
        parse'health_percent
         = P'.try
            (do
               v <- P'.getT "health_percent"
               Prelude'.return (\ o -> o{health_percent = v}))
        parse'mana_percent
         = P'.try
            (do
               v <- P'.getT "mana_percent"
               Prelude'.return (\ o -> o{mana_percent = v}))