{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_HalloweenDrops (CDOTAUserMsg_HalloweenDrops(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTAUserMsg_HalloweenDrops = CDOTAUserMsg_HalloweenDrops{item_defs :: !(P'.Seq P'.Word32), player_ids :: !(P'.Seq P'.Word32),
                                                               prize_list :: !(P'.Maybe P'.Word32)}
                                 deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                           Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_HalloweenDrops where
  mergeAppend (CDOTAUserMsg_HalloweenDrops x'1 x'2 x'3) (CDOTAUserMsg_HalloweenDrops y'1 y'2 y'3)
   = CDOTAUserMsg_HalloweenDrops (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default CDOTAUserMsg_HalloweenDrops where
  defaultValue = CDOTAUserMsg_HalloweenDrops P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAUserMsg_HalloweenDrops where
  wireSize ft' self'@(CDOTAUserMsg_HalloweenDrops x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 13 x'1 + P'.wireSizeRep 1 13 x'2 + P'.wireSizeOpt 1 13 x'3)
  wirePut ft' self'@(CDOTAUserMsg_HalloweenDrops x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 8 13 x'1
             P'.wirePutRep 16 13 x'2
             P'.wirePutOpt 24 13 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{item_defs = P'.append (item_defs old'Self) new'Field}) (P'.wireGet 13)
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{item_defs = P'.mergeAppend (item_defs old'Self) new'Field})
                    (P'.wireGetPacked 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{player_ids = P'.append (player_ids old'Self) new'Field}) (P'.wireGet 13)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{player_ids = P'.mergeAppend (player_ids old'Self) new'Field})
                    (P'.wireGetPacked 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{prize_list = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_HalloweenDrops) CDOTAUserMsg_HalloweenDrops where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_HalloweenDrops

instance P'.ReflectDescriptor CDOTAUserMsg_HalloweenDrops where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 10, 16, 18, 24])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_HalloweenDrops\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_HalloweenDrops\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_HalloweenDrops.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_HalloweenDrops.item_defs\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_HalloweenDrops\"], baseName' = FName \"item_defs\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Just (WireTag {getWireTag = 8},WireTag {getWireTag = 10}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_HalloweenDrops.player_ids\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_HalloweenDrops\"], baseName' = FName \"player_ids\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Just (WireTag {getWireTag = 16},WireTag {getWireTag = 18}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_HalloweenDrops.prize_list\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_HalloweenDrops\"], baseName' = FName \"prize_list\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_HalloweenDrops where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_HalloweenDrops where
  textPut msg
   = do
       P'.tellT "item_defs" (item_defs msg)
       P'.tellT "player_ids" (player_ids msg)
       P'.tellT "prize_list" (prize_list msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'item_defs, parse'player_ids, parse'prize_list]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'item_defs
         = P'.try
            (do
               v <- P'.getT "item_defs"
               Prelude'.return (\ o -> o{item_defs = P'.append (item_defs o) v}))
        parse'player_ids
         = P'.try
            (do
               v <- P'.getT "player_ids"
               Prelude'.return (\ o -> o{player_ids = P'.append (player_ids o) v}))
        parse'prize_list
         = P'.try
            (do
               v <- P'.getT "prize_list"
               Prelude'.return (\ o -> o{prize_list = v}))