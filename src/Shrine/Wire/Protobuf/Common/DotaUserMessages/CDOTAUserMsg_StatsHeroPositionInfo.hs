{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_StatsHeroPositionInfo (CDOTAUserMsg_StatsHeroPositionInfo(..))
       where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_StatsHeroPositionInfo.PositionPair
       as DotaUserMessages.CDOTAUserMsg_StatsHeroPositionInfo (PositionPair)

data CDOTAUserMsg_StatsHeroPositionInfo = CDOTAUserMsg_StatsHeroPositionInfo{average_position :: !(P'.Maybe P'.Float),
                                                                             position_details ::
                                                                             !(P'.Seq
                                                                                DotaUserMessages.CDOTAUserMsg_StatsHeroPositionInfo.PositionPair)}
                                        deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                  Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_StatsHeroPositionInfo where
  mergeAppend (CDOTAUserMsg_StatsHeroPositionInfo x'1 x'2) (CDOTAUserMsg_StatsHeroPositionInfo y'1 y'2)
   = CDOTAUserMsg_StatsHeroPositionInfo (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CDOTAUserMsg_StatsHeroPositionInfo where
  defaultValue = CDOTAUserMsg_StatsHeroPositionInfo P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAUserMsg_StatsHeroPositionInfo where
  wireSize ft' self'@(CDOTAUserMsg_StatsHeroPositionInfo x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 2 x'1 + P'.wireSizeRep 1 11 x'2)
  wirePut ft' self'@(CDOTAUserMsg_StatsHeroPositionInfo x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 13 2 x'1
             P'.wirePutRep 18 11 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             13 -> Prelude'.fmap (\ !new'Field -> old'Self{average_position = Prelude'.Just new'Field}) (P'.wireGet 2)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{position_details = P'.append (position_details old'Self) new'Field})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_StatsHeroPositionInfo) CDOTAUserMsg_StatsHeroPositionInfo where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_StatsHeroPositionInfo

instance P'.ReflectDescriptor CDOTAUserMsg_StatsHeroPositionInfo where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [13, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsHeroPositionInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_StatsHeroPositionInfo\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_StatsHeroPositionInfo.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsHeroPositionInfo.average_position\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsHeroPositionInfo\"], baseName' = FName \"average_position\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 13}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsHeroPositionInfo.position_details\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsHeroPositionInfo\"], baseName' = FName \"position_details\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_StatsHeroPositionInfo.PositionPair\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_StatsHeroPositionInfo\"], baseName = MName \"PositionPair\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_StatsHeroPositionInfo where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_StatsHeroPositionInfo where
  textPut msg
   = do
       P'.tellT "average_position" (average_position msg)
       P'.tellT "position_details" (position_details msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'average_position, parse'position_details]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'average_position
         = P'.try
            (do
               v <- P'.getT "average_position"
               Prelude'.return (\ o -> o{average_position = v}))
        parse'position_details
         = P'.try
            (do
               v <- P'.getT "position_details"
               Prelude'.return (\ o -> o{position_details = P'.append (position_details o) v}))