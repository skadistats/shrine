{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_MiniKillCamInfo (CDOTAUserMsg_MiniKillCamInfo(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaUserMessages.CDOTAUserMsg_MiniKillCamInfo.Attacker
       as DotaUserMessages.CDOTAUserMsg_MiniKillCamInfo (Attacker)

data CDOTAUserMsg_MiniKillCamInfo = CDOTAUserMsg_MiniKillCamInfo{attackers ::
                                                                 !(P'.Seq DotaUserMessages.CDOTAUserMsg_MiniKillCamInfo.Attacker)}
                                  deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                            Prelude'.Generic)

instance P'.Mergeable CDOTAUserMsg_MiniKillCamInfo where
  mergeAppend (CDOTAUserMsg_MiniKillCamInfo x'1) (CDOTAUserMsg_MiniKillCamInfo y'1)
   = CDOTAUserMsg_MiniKillCamInfo (P'.mergeAppend x'1 y'1)

instance P'.Default CDOTAUserMsg_MiniKillCamInfo where
  defaultValue = CDOTAUserMsg_MiniKillCamInfo P'.defaultValue

instance P'.Wire CDOTAUserMsg_MiniKillCamInfo where
  wireSize ft' self'@(CDOTAUserMsg_MiniKillCamInfo x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 11 x'1)
  wirePut ft' self'@(CDOTAUserMsg_MiniKillCamInfo x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 10 11 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{attackers = P'.append (attackers old'Self) new'Field}) (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAUserMsg_MiniKillCamInfo) CDOTAUserMsg_MiniKillCamInfo where
  getVal m' f' = f' m'

instance P'.GPB CDOTAUserMsg_MiniKillCamInfo

instance P'.ReflectDescriptor CDOTAUserMsg_MiniKillCamInfo where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_MiniKillCamInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\"], baseName = MName \"CDOTAUserMsg_MiniKillCamInfo\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaUserMessages\",\"CDOTAUserMsg_MiniKillCamInfo.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Usermessages.CDOTAUserMsg_MiniKillCamInfo.attackers\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_MiniKillCamInfo\"], baseName' = FName \"attackers\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Usermessages.CDOTAUserMsg_MiniKillCamInfo.Attacker\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaUserMessages\",MName \"CDOTAUserMsg_MiniKillCamInfo\"], baseName = MName \"Attacker\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAUserMsg_MiniKillCamInfo where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAUserMsg_MiniKillCamInfo where
  textPut msg
   = do
       P'.tellT "attackers" (attackers msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'attackers]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'attackers
         = P'.try
            (do
               v <- P'.getT "attackers"
               Prelude'.return (\ o -> o{attackers = P'.append (attackers o) v}))