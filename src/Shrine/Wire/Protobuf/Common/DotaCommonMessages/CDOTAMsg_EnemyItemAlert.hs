{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaCommonMessages.CDOTAMsg_EnemyItemAlert (CDOTAMsg_EnemyItemAlert(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CDOTAMsg_EnemyItemAlert = CDOTAMsg_EnemyItemAlert{player_id :: !(P'.Maybe P'.Word32), itemid :: !(P'.Maybe P'.Int32)}
                             deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CDOTAMsg_EnemyItemAlert where
  mergeAppend (CDOTAMsg_EnemyItemAlert x'1 x'2) (CDOTAMsg_EnemyItemAlert y'1 y'2)
   = CDOTAMsg_EnemyItemAlert (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CDOTAMsg_EnemyItemAlert where
  defaultValue = CDOTAMsg_EnemyItemAlert P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAMsg_EnemyItemAlert where
  wireSize ft' self'@(CDOTAMsg_EnemyItemAlert x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 5 x'2)
  wirePut ft' self'@(CDOTAMsg_EnemyItemAlert x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 5 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{player_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{itemid = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAMsg_EnemyItemAlert) CDOTAMsg_EnemyItemAlert where
  getVal m' f' = f' m'

instance P'.GPB CDOTAMsg_EnemyItemAlert

instance P'.ReflectDescriptor CDOTAMsg_EnemyItemAlert where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Commonmessages.CDOTAMsg_EnemyItemAlert\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaCommonMessages\"], baseName = MName \"CDOTAMsg_EnemyItemAlert\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaCommonMessages\",\"CDOTAMsg_EnemyItemAlert.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Commonmessages.CDOTAMsg_EnemyItemAlert.player_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaCommonMessages\",MName \"CDOTAMsg_EnemyItemAlert\"], baseName' = FName \"player_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Commonmessages.CDOTAMsg_EnemyItemAlert.itemid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaCommonMessages\",MName \"CDOTAMsg_EnemyItemAlert\"], baseName' = FName \"itemid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAMsg_EnemyItemAlert where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAMsg_EnemyItemAlert where
  textPut msg
   = do
       P'.tellT "player_id" (player_id msg)
       P'.tellT "itemid" (itemid msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'player_id, parse'itemid]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'player_id
         = P'.try
            (do
               v <- P'.getT "player_id"
               Prelude'.return (\ o -> o{player_id = v}))
        parse'itemid
         = P'.try
            (do
               v <- P'.getT "itemid"
               Prelude'.return (\ o -> o{itemid = v}))