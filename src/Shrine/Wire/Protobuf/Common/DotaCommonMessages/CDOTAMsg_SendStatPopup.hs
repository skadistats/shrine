{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaCommonMessages.CDOTAMsg_SendStatPopup (CDOTAMsg_SendStatPopup(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.DotaCommonMessages.EDOTAStatPopupTypes as DotaCommonMessages (EDOTAStatPopupTypes)

data CDOTAMsg_SendStatPopup = CDOTAMsg_SendStatPopup{style :: !(P'.Maybe DotaCommonMessages.EDOTAStatPopupTypes),
                                                     stat_strings :: !(P'.Seq P'.Utf8), stat_images :: !(P'.Seq P'.Int32),
                                                     stat_image_types :: !(P'.Seq P'.Int32), duration :: !(P'.Maybe P'.Float)}
                            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CDOTAMsg_SendStatPopup where
  mergeAppend (CDOTAMsg_SendStatPopup x'1 x'2 x'3 x'4 x'5) (CDOTAMsg_SendStatPopup y'1 y'2 y'3 y'4 y'5)
   = CDOTAMsg_SendStatPopup (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)

instance P'.Default CDOTAMsg_SendStatPopup where
  defaultValue
   = CDOTAMsg_SendStatPopup (Prelude'.Just (Prelude'.read "K_EDOTA_SPT_Textline")) P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue

instance P'.Wire CDOTAMsg_SendStatPopup where
  wireSize ft' self'@(CDOTAMsg_SendStatPopup x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 14 x'1 + P'.wireSizeRep 1 9 x'2 + P'.wireSizeRep 1 5 x'3 + P'.wireSizeRep 1 5 x'4 +
             P'.wireSizeOpt 1 2 x'5)
  wirePut ft' self'@(CDOTAMsg_SendStatPopup x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 14 x'1
             P'.wirePutRep 18 9 x'2
             P'.wirePutRep 24 5 x'3
             P'.wirePutRep 32 5 x'4
             P'.wirePutOpt 45 2 x'5
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{style = Prelude'.Just new'Field}) (P'.wireGet 14)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{stat_strings = P'.append (stat_strings old'Self) new'Field})
                    (P'.wireGet 9)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{stat_images = P'.append (stat_images old'Self) new'Field}) (P'.wireGet 5)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{stat_images = P'.mergeAppend (stat_images old'Self) new'Field})
                    (P'.wireGetPacked 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{stat_image_types = P'.append (stat_image_types old'Self) new'Field})
                    (P'.wireGet 5)
             34 -> Prelude'.fmap (\ !new'Field -> old'Self{stat_image_types = P'.mergeAppend (stat_image_types old'Self) new'Field})
                    (P'.wireGetPacked 5)
             45 -> Prelude'.fmap (\ !new'Field -> old'Self{duration = Prelude'.Just new'Field}) (P'.wireGet 2)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAMsg_SendStatPopup) CDOTAMsg_SendStatPopup where
  getVal m' f' = f' m'

instance P'.GPB CDOTAMsg_SendStatPopup

instance P'.ReflectDescriptor CDOTAMsg_SendStatPopup where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18, 24, 26, 32, 34, 45])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".Dota_Commonmessages.CDOTAMsg_SendStatPopup\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaCommonMessages\"], baseName = MName \"CDOTAMsg_SendStatPopup\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"Common\",\"DotaCommonMessages\",\"CDOTAMsg_SendStatPopup.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Commonmessages.CDOTAMsg_SendStatPopup.style\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaCommonMessages\",MName \"CDOTAMsg_SendStatPopup\"], baseName' = FName \"style\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".Dota_Commonmessages.EDOTAStatPopupTypes\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule = [MName \"DotaCommonMessages\"], baseName = MName \"EDOTAStatPopupTypes\"}), hsRawDefault = Just \"k_EDOTA_SPT_Textline\", hsDefault = Just (HsDef'Enum \"K_EDOTA_SPT_Textline\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Commonmessages.CDOTAMsg_SendStatPopup.stat_strings\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaCommonMessages\",MName \"CDOTAMsg_SendStatPopup\"], baseName' = FName \"stat_strings\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Commonmessages.CDOTAMsg_SendStatPopup.stat_images\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaCommonMessages\",MName \"CDOTAMsg_SendStatPopup\"], baseName' = FName \"stat_images\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Just (WireTag {getWireTag = 24},WireTag {getWireTag = 26}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Commonmessages.CDOTAMsg_SendStatPopup.stat_image_types\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaCommonMessages\",MName \"CDOTAMsg_SendStatPopup\"], baseName' = FName \"stat_image_types\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Just (WireTag {getWireTag = 32},WireTag {getWireTag = 34}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".Dota_Commonmessages.CDOTAMsg_SendStatPopup.duration\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"Common\"], parentModule' = [MName \"DotaCommonMessages\",MName \"CDOTAMsg_SendStatPopup\"], baseName' = FName \"duration\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 45}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAMsg_SendStatPopup where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAMsg_SendStatPopup where
  textPut msg
   = do
       P'.tellT "style" (style msg)
       P'.tellT "stat_strings" (stat_strings msg)
       P'.tellT "stat_images" (stat_images msg)
       P'.tellT "stat_image_types" (stat_image_types msg)
       P'.tellT "duration" (duration msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'style, parse'stat_strings, parse'stat_images, parse'stat_image_types, parse'duration])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'style
         = P'.try
            (do
               v <- P'.getT "style"
               Prelude'.return (\ o -> o{style = v}))
        parse'stat_strings
         = P'.try
            (do
               v <- P'.getT "stat_strings"
               Prelude'.return (\ o -> o{stat_strings = P'.append (stat_strings o) v}))
        parse'stat_images
         = P'.try
            (do
               v <- P'.getT "stat_images"
               Prelude'.return (\ o -> o{stat_images = P'.append (stat_images o) v}))
        parse'stat_image_types
         = P'.try
            (do
               v <- P'.getT "stat_image_types"
               Prelude'.return (\ o -> o{stat_image_types = P'.append (stat_image_types o) v}))
        parse'duration
         = P'.try
            (do
               v <- P'.getT "duration"
               Prelude'.return (\ o -> o{duration = v}))