{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaCommonMessages.EDOTAChatWheelMessage (EDOTAChatWheelMessage(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EDOTAChatWheelMessage = K_EDOTA_CW_Ok
                           | K_EDOTA_CW_Care
                           | K_EDOTA_CW_GetBack
                           | K_EDOTA_CW_NeedWards
                           | K_EDOTA_CW_Stun
                           | K_EDOTA_CW_Help
                           | K_EDOTA_CW_Push
                           | K_EDOTA_CW_GoodJob
                           | K_EDOTA_CW_Missing
                           | K_EDOTA_CW_Missing_Top
                           | K_EDOTA_CW_Missing_Mid
                           | K_EDOTA_CW_Missing_Bottom
                           | K_EDOTA_CW_Go
                           | K_EDOTA_CW_Initiate
                           | K_EDOTA_CW_Follow
                           | K_EDOTA_CW_Group_Up
                           | K_EDOTA_CW_Spread_Out
                           | K_EDOTA_CW_Split_Farm
                           | K_EDOTA_CW_Attack
                           | K_EDOTA_CW_BRB
                           | K_EDOTA_CW_Dive
                           | K_EDOTA_CW_OMW
                           | K_EDOTA_CW_Get_Ready
                           | K_EDOTA_CW_Bait
                           | K_EDOTA_CW_Heal
                           | K_EDOTA_CW_Mana
                           | K_EDOTA_CW_OOM
                           | K_EDOTA_CW_Skill_Cooldown
                           | K_EDOTA_CW_Ulti_Ready
                           | K_EDOTA_CW_Enemy_Returned
                           | K_EDOTA_CW_All_Missing
                           | K_EDOTA_CW_Enemy_Incoming
                           | K_EDOTA_CW_Invis_Enemy
                           | K_EDOTA_CW_Enemy_Had_Rune
                           | K_EDOTA_CW_Split_Push
                           | K_EDOTA_CW_Coming_To_Gank
                           | K_EDOTA_CW_Request_Gank
                           | K_EDOTA_CW_Fight_Under_Tower
                           | K_EDOTA_CW_Deny_Tower
                           | K_EDOTA_CW_Buy_Courier
                           | K_EDOTA_CW_Upgrade_Courier
                           | K_EDOTA_CW_Need_Detection
                           | K_EDOTA_CW_They_Have_Detection
                           | K_EDOTA_CW_Buy_TP
                           | K_EDOTA_CW_Reuse_Courier
                           | K_EDOTA_CW_Deward
                           | K_EDOTA_CW_Building_Mek
                           | K_EDOTA_CW_Building_Pipe
                           | K_EDOTA_CW_Stack_And_Pull
                           | K_EDOTA_CW_Pull
                           | K_EDOTA_CW_Pulling
                           | K_EDOTA_CW_Stack
                           | K_EDOTA_CW_Jungling
                           | K_EDOTA_CW_Roshan
                           | K_EDOTA_CW_Affirmative
                           | K_EDOTA_CW_Wait
                           | K_EDOTA_CW_Pause
                           | K_EDOTA_CW_Current_Time
                           | K_EDOTA_CW_Check_Runes
                           | K_EDOTA_CW_Smoke_Gank
                           | K_EDOTA_CW_GLHF
                           | K_EDOTA_CW_Nice
                           | K_EDOTA_CW_Thanks
                           | K_EDOTA_CW_Sorry
                           | K_EDOTA_CW_No_Give_Up
                           | K_EDOTA_CW_Just_Happened
                           | K_EDOTA_CW_Game_Is_Hard
                           | K_EDOTA_CW_New_Meta
                           | K_EDOTA_CW_My_Bad
                           | K_EDOTA_CW_Regret
                           | K_EDOTA_CW_Relax
                           | K_EDOTA_CW_MissingHero
                           | K_EDOTA_CW_ReturnedHero
                           | K_EDOTA_CW_GG
                           | K_EDOTA_CW_GGWP
                           | K_EDOTA_CW_All_GG
                           | K_EDOTA_CW_All_GGWP
                           | K_EDOTA_CW_What_To_Buy
                           | K_EDOTA_CW_Im_Retreating
                           | K_EDOTA_CW_Space_Created
                           | K_EDOTA_CW_Whoops
                           | K_EDOTA_CW_Tower_then_Back
                           | K_EDOTA_CW_Barracks_then_Back
                           | K_EDOTA_CW_Ward_Bottom_Rune
                           | K_EDOTA_CW_Ward_Top_Rune
                           | K_EDOTA_CW_Zeus_Ult
                           deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                     Prelude'.Generic)

instance P'.Mergeable EDOTAChatWheelMessage

instance Prelude'.Bounded EDOTAChatWheelMessage where
  minBound = K_EDOTA_CW_Ok
  maxBound = K_EDOTA_CW_Zeus_Ult

instance P'.Default EDOTAChatWheelMessage where
  defaultValue = K_EDOTA_CW_Ok

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EDOTAChatWheelMessage
toMaybe'Enum 0 = Prelude'.Just K_EDOTA_CW_Ok
toMaybe'Enum 1 = Prelude'.Just K_EDOTA_CW_Care
toMaybe'Enum 2 = Prelude'.Just K_EDOTA_CW_GetBack
toMaybe'Enum 3 = Prelude'.Just K_EDOTA_CW_NeedWards
toMaybe'Enum 4 = Prelude'.Just K_EDOTA_CW_Stun
toMaybe'Enum 5 = Prelude'.Just K_EDOTA_CW_Help
toMaybe'Enum 6 = Prelude'.Just K_EDOTA_CW_Push
toMaybe'Enum 7 = Prelude'.Just K_EDOTA_CW_GoodJob
toMaybe'Enum 8 = Prelude'.Just K_EDOTA_CW_Missing
toMaybe'Enum 9 = Prelude'.Just K_EDOTA_CW_Missing_Top
toMaybe'Enum 10 = Prelude'.Just K_EDOTA_CW_Missing_Mid
toMaybe'Enum 11 = Prelude'.Just K_EDOTA_CW_Missing_Bottom
toMaybe'Enum 12 = Prelude'.Just K_EDOTA_CW_Go
toMaybe'Enum 13 = Prelude'.Just K_EDOTA_CW_Initiate
toMaybe'Enum 14 = Prelude'.Just K_EDOTA_CW_Follow
toMaybe'Enum 15 = Prelude'.Just K_EDOTA_CW_Group_Up
toMaybe'Enum 16 = Prelude'.Just K_EDOTA_CW_Spread_Out
toMaybe'Enum 17 = Prelude'.Just K_EDOTA_CW_Split_Farm
toMaybe'Enum 18 = Prelude'.Just K_EDOTA_CW_Attack
toMaybe'Enum 19 = Prelude'.Just K_EDOTA_CW_BRB
toMaybe'Enum 20 = Prelude'.Just K_EDOTA_CW_Dive
toMaybe'Enum 21 = Prelude'.Just K_EDOTA_CW_OMW
toMaybe'Enum 22 = Prelude'.Just K_EDOTA_CW_Get_Ready
toMaybe'Enum 23 = Prelude'.Just K_EDOTA_CW_Bait
toMaybe'Enum 24 = Prelude'.Just K_EDOTA_CW_Heal
toMaybe'Enum 25 = Prelude'.Just K_EDOTA_CW_Mana
toMaybe'Enum 26 = Prelude'.Just K_EDOTA_CW_OOM
toMaybe'Enum 27 = Prelude'.Just K_EDOTA_CW_Skill_Cooldown
toMaybe'Enum 28 = Prelude'.Just K_EDOTA_CW_Ulti_Ready
toMaybe'Enum 29 = Prelude'.Just K_EDOTA_CW_Enemy_Returned
toMaybe'Enum 30 = Prelude'.Just K_EDOTA_CW_All_Missing
toMaybe'Enum 31 = Prelude'.Just K_EDOTA_CW_Enemy_Incoming
toMaybe'Enum 32 = Prelude'.Just K_EDOTA_CW_Invis_Enemy
toMaybe'Enum 33 = Prelude'.Just K_EDOTA_CW_Enemy_Had_Rune
toMaybe'Enum 34 = Prelude'.Just K_EDOTA_CW_Split_Push
toMaybe'Enum 35 = Prelude'.Just K_EDOTA_CW_Coming_To_Gank
toMaybe'Enum 36 = Prelude'.Just K_EDOTA_CW_Request_Gank
toMaybe'Enum 37 = Prelude'.Just K_EDOTA_CW_Fight_Under_Tower
toMaybe'Enum 38 = Prelude'.Just K_EDOTA_CW_Deny_Tower
toMaybe'Enum 39 = Prelude'.Just K_EDOTA_CW_Buy_Courier
toMaybe'Enum 40 = Prelude'.Just K_EDOTA_CW_Upgrade_Courier
toMaybe'Enum 41 = Prelude'.Just K_EDOTA_CW_Need_Detection
toMaybe'Enum 42 = Prelude'.Just K_EDOTA_CW_They_Have_Detection
toMaybe'Enum 43 = Prelude'.Just K_EDOTA_CW_Buy_TP
toMaybe'Enum 44 = Prelude'.Just K_EDOTA_CW_Reuse_Courier
toMaybe'Enum 45 = Prelude'.Just K_EDOTA_CW_Deward
toMaybe'Enum 46 = Prelude'.Just K_EDOTA_CW_Building_Mek
toMaybe'Enum 47 = Prelude'.Just K_EDOTA_CW_Building_Pipe
toMaybe'Enum 48 = Prelude'.Just K_EDOTA_CW_Stack_And_Pull
toMaybe'Enum 49 = Prelude'.Just K_EDOTA_CW_Pull
toMaybe'Enum 50 = Prelude'.Just K_EDOTA_CW_Pulling
toMaybe'Enum 51 = Prelude'.Just K_EDOTA_CW_Stack
toMaybe'Enum 52 = Prelude'.Just K_EDOTA_CW_Jungling
toMaybe'Enum 53 = Prelude'.Just K_EDOTA_CW_Roshan
toMaybe'Enum 54 = Prelude'.Just K_EDOTA_CW_Affirmative
toMaybe'Enum 55 = Prelude'.Just K_EDOTA_CW_Wait
toMaybe'Enum 56 = Prelude'.Just K_EDOTA_CW_Pause
toMaybe'Enum 57 = Prelude'.Just K_EDOTA_CW_Current_Time
toMaybe'Enum 58 = Prelude'.Just K_EDOTA_CW_Check_Runes
toMaybe'Enum 59 = Prelude'.Just K_EDOTA_CW_Smoke_Gank
toMaybe'Enum 60 = Prelude'.Just K_EDOTA_CW_GLHF
toMaybe'Enum 61 = Prelude'.Just K_EDOTA_CW_Nice
toMaybe'Enum 62 = Prelude'.Just K_EDOTA_CW_Thanks
toMaybe'Enum 63 = Prelude'.Just K_EDOTA_CW_Sorry
toMaybe'Enum 64 = Prelude'.Just K_EDOTA_CW_No_Give_Up
toMaybe'Enum 65 = Prelude'.Just K_EDOTA_CW_Just_Happened
toMaybe'Enum 66 = Prelude'.Just K_EDOTA_CW_Game_Is_Hard
toMaybe'Enum 67 = Prelude'.Just K_EDOTA_CW_New_Meta
toMaybe'Enum 68 = Prelude'.Just K_EDOTA_CW_My_Bad
toMaybe'Enum 69 = Prelude'.Just K_EDOTA_CW_Regret
toMaybe'Enum 70 = Prelude'.Just K_EDOTA_CW_Relax
toMaybe'Enum 71 = Prelude'.Just K_EDOTA_CW_MissingHero
toMaybe'Enum 72 = Prelude'.Just K_EDOTA_CW_ReturnedHero
toMaybe'Enum 73 = Prelude'.Just K_EDOTA_CW_GG
toMaybe'Enum 74 = Prelude'.Just K_EDOTA_CW_GGWP
toMaybe'Enum 75 = Prelude'.Just K_EDOTA_CW_All_GG
toMaybe'Enum 76 = Prelude'.Just K_EDOTA_CW_All_GGWP
toMaybe'Enum 77 = Prelude'.Just K_EDOTA_CW_What_To_Buy
toMaybe'Enum 78 = Prelude'.Just K_EDOTA_CW_Im_Retreating
toMaybe'Enum 79 = Prelude'.Just K_EDOTA_CW_Space_Created
toMaybe'Enum 80 = Prelude'.Just K_EDOTA_CW_Whoops
toMaybe'Enum 81 = Prelude'.Just K_EDOTA_CW_Tower_then_Back
toMaybe'Enum 82 = Prelude'.Just K_EDOTA_CW_Barracks_then_Back
toMaybe'Enum 83 = Prelude'.Just K_EDOTA_CW_Ward_Bottom_Rune
toMaybe'Enum 84 = Prelude'.Just K_EDOTA_CW_Ward_Top_Rune
toMaybe'Enum 85 = Prelude'.Just K_EDOTA_CW_Zeus_Ult
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EDOTAChatWheelMessage where
  fromEnum K_EDOTA_CW_Ok = 0
  fromEnum K_EDOTA_CW_Care = 1
  fromEnum K_EDOTA_CW_GetBack = 2
  fromEnum K_EDOTA_CW_NeedWards = 3
  fromEnum K_EDOTA_CW_Stun = 4
  fromEnum K_EDOTA_CW_Help = 5
  fromEnum K_EDOTA_CW_Push = 6
  fromEnum K_EDOTA_CW_GoodJob = 7
  fromEnum K_EDOTA_CW_Missing = 8
  fromEnum K_EDOTA_CW_Missing_Top = 9
  fromEnum K_EDOTA_CW_Missing_Mid = 10
  fromEnum K_EDOTA_CW_Missing_Bottom = 11
  fromEnum K_EDOTA_CW_Go = 12
  fromEnum K_EDOTA_CW_Initiate = 13
  fromEnum K_EDOTA_CW_Follow = 14
  fromEnum K_EDOTA_CW_Group_Up = 15
  fromEnum K_EDOTA_CW_Spread_Out = 16
  fromEnum K_EDOTA_CW_Split_Farm = 17
  fromEnum K_EDOTA_CW_Attack = 18
  fromEnum K_EDOTA_CW_BRB = 19
  fromEnum K_EDOTA_CW_Dive = 20
  fromEnum K_EDOTA_CW_OMW = 21
  fromEnum K_EDOTA_CW_Get_Ready = 22
  fromEnum K_EDOTA_CW_Bait = 23
  fromEnum K_EDOTA_CW_Heal = 24
  fromEnum K_EDOTA_CW_Mana = 25
  fromEnum K_EDOTA_CW_OOM = 26
  fromEnum K_EDOTA_CW_Skill_Cooldown = 27
  fromEnum K_EDOTA_CW_Ulti_Ready = 28
  fromEnum K_EDOTA_CW_Enemy_Returned = 29
  fromEnum K_EDOTA_CW_All_Missing = 30
  fromEnum K_EDOTA_CW_Enemy_Incoming = 31
  fromEnum K_EDOTA_CW_Invis_Enemy = 32
  fromEnum K_EDOTA_CW_Enemy_Had_Rune = 33
  fromEnum K_EDOTA_CW_Split_Push = 34
  fromEnum K_EDOTA_CW_Coming_To_Gank = 35
  fromEnum K_EDOTA_CW_Request_Gank = 36
  fromEnum K_EDOTA_CW_Fight_Under_Tower = 37
  fromEnum K_EDOTA_CW_Deny_Tower = 38
  fromEnum K_EDOTA_CW_Buy_Courier = 39
  fromEnum K_EDOTA_CW_Upgrade_Courier = 40
  fromEnum K_EDOTA_CW_Need_Detection = 41
  fromEnum K_EDOTA_CW_They_Have_Detection = 42
  fromEnum K_EDOTA_CW_Buy_TP = 43
  fromEnum K_EDOTA_CW_Reuse_Courier = 44
  fromEnum K_EDOTA_CW_Deward = 45
  fromEnum K_EDOTA_CW_Building_Mek = 46
  fromEnum K_EDOTA_CW_Building_Pipe = 47
  fromEnum K_EDOTA_CW_Stack_And_Pull = 48
  fromEnum K_EDOTA_CW_Pull = 49
  fromEnum K_EDOTA_CW_Pulling = 50
  fromEnum K_EDOTA_CW_Stack = 51
  fromEnum K_EDOTA_CW_Jungling = 52
  fromEnum K_EDOTA_CW_Roshan = 53
  fromEnum K_EDOTA_CW_Affirmative = 54
  fromEnum K_EDOTA_CW_Wait = 55
  fromEnum K_EDOTA_CW_Pause = 56
  fromEnum K_EDOTA_CW_Current_Time = 57
  fromEnum K_EDOTA_CW_Check_Runes = 58
  fromEnum K_EDOTA_CW_Smoke_Gank = 59
  fromEnum K_EDOTA_CW_GLHF = 60
  fromEnum K_EDOTA_CW_Nice = 61
  fromEnum K_EDOTA_CW_Thanks = 62
  fromEnum K_EDOTA_CW_Sorry = 63
  fromEnum K_EDOTA_CW_No_Give_Up = 64
  fromEnum K_EDOTA_CW_Just_Happened = 65
  fromEnum K_EDOTA_CW_Game_Is_Hard = 66
  fromEnum K_EDOTA_CW_New_Meta = 67
  fromEnum K_EDOTA_CW_My_Bad = 68
  fromEnum K_EDOTA_CW_Regret = 69
  fromEnum K_EDOTA_CW_Relax = 70
  fromEnum K_EDOTA_CW_MissingHero = 71
  fromEnum K_EDOTA_CW_ReturnedHero = 72
  fromEnum K_EDOTA_CW_GG = 73
  fromEnum K_EDOTA_CW_GGWP = 74
  fromEnum K_EDOTA_CW_All_GG = 75
  fromEnum K_EDOTA_CW_All_GGWP = 76
  fromEnum K_EDOTA_CW_What_To_Buy = 77
  fromEnum K_EDOTA_CW_Im_Retreating = 78
  fromEnum K_EDOTA_CW_Space_Created = 79
  fromEnum K_EDOTA_CW_Whoops = 80
  fromEnum K_EDOTA_CW_Tower_then_Back = 81
  fromEnum K_EDOTA_CW_Barracks_then_Back = 82
  fromEnum K_EDOTA_CW_Ward_Bottom_Rune = 83
  fromEnum K_EDOTA_CW_Ward_Top_Rune = 84
  fromEnum K_EDOTA_CW_Zeus_Ult = 85
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaCommonMessages.EDOTAChatWheelMessage")
      . toMaybe'Enum
  succ K_EDOTA_CW_Ok = K_EDOTA_CW_Care
  succ K_EDOTA_CW_Care = K_EDOTA_CW_GetBack
  succ K_EDOTA_CW_GetBack = K_EDOTA_CW_NeedWards
  succ K_EDOTA_CW_NeedWards = K_EDOTA_CW_Stun
  succ K_EDOTA_CW_Stun = K_EDOTA_CW_Help
  succ K_EDOTA_CW_Help = K_EDOTA_CW_Push
  succ K_EDOTA_CW_Push = K_EDOTA_CW_GoodJob
  succ K_EDOTA_CW_GoodJob = K_EDOTA_CW_Missing
  succ K_EDOTA_CW_Missing = K_EDOTA_CW_Missing_Top
  succ K_EDOTA_CW_Missing_Top = K_EDOTA_CW_Missing_Mid
  succ K_EDOTA_CW_Missing_Mid = K_EDOTA_CW_Missing_Bottom
  succ K_EDOTA_CW_Missing_Bottom = K_EDOTA_CW_Go
  succ K_EDOTA_CW_Go = K_EDOTA_CW_Initiate
  succ K_EDOTA_CW_Initiate = K_EDOTA_CW_Follow
  succ K_EDOTA_CW_Follow = K_EDOTA_CW_Group_Up
  succ K_EDOTA_CW_Group_Up = K_EDOTA_CW_Spread_Out
  succ K_EDOTA_CW_Spread_Out = K_EDOTA_CW_Split_Farm
  succ K_EDOTA_CW_Split_Farm = K_EDOTA_CW_Attack
  succ K_EDOTA_CW_Attack = K_EDOTA_CW_BRB
  succ K_EDOTA_CW_BRB = K_EDOTA_CW_Dive
  succ K_EDOTA_CW_Dive = K_EDOTA_CW_OMW
  succ K_EDOTA_CW_OMW = K_EDOTA_CW_Get_Ready
  succ K_EDOTA_CW_Get_Ready = K_EDOTA_CW_Bait
  succ K_EDOTA_CW_Bait = K_EDOTA_CW_Heal
  succ K_EDOTA_CW_Heal = K_EDOTA_CW_Mana
  succ K_EDOTA_CW_Mana = K_EDOTA_CW_OOM
  succ K_EDOTA_CW_OOM = K_EDOTA_CW_Skill_Cooldown
  succ K_EDOTA_CW_Skill_Cooldown = K_EDOTA_CW_Ulti_Ready
  succ K_EDOTA_CW_Ulti_Ready = K_EDOTA_CW_Enemy_Returned
  succ K_EDOTA_CW_Enemy_Returned = K_EDOTA_CW_All_Missing
  succ K_EDOTA_CW_All_Missing = K_EDOTA_CW_Enemy_Incoming
  succ K_EDOTA_CW_Enemy_Incoming = K_EDOTA_CW_Invis_Enemy
  succ K_EDOTA_CW_Invis_Enemy = K_EDOTA_CW_Enemy_Had_Rune
  succ K_EDOTA_CW_Enemy_Had_Rune = K_EDOTA_CW_Split_Push
  succ K_EDOTA_CW_Split_Push = K_EDOTA_CW_Coming_To_Gank
  succ K_EDOTA_CW_Coming_To_Gank = K_EDOTA_CW_Request_Gank
  succ K_EDOTA_CW_Request_Gank = K_EDOTA_CW_Fight_Under_Tower
  succ K_EDOTA_CW_Fight_Under_Tower = K_EDOTA_CW_Deny_Tower
  succ K_EDOTA_CW_Deny_Tower = K_EDOTA_CW_Buy_Courier
  succ K_EDOTA_CW_Buy_Courier = K_EDOTA_CW_Upgrade_Courier
  succ K_EDOTA_CW_Upgrade_Courier = K_EDOTA_CW_Need_Detection
  succ K_EDOTA_CW_Need_Detection = K_EDOTA_CW_They_Have_Detection
  succ K_EDOTA_CW_They_Have_Detection = K_EDOTA_CW_Buy_TP
  succ K_EDOTA_CW_Buy_TP = K_EDOTA_CW_Reuse_Courier
  succ K_EDOTA_CW_Reuse_Courier = K_EDOTA_CW_Deward
  succ K_EDOTA_CW_Deward = K_EDOTA_CW_Building_Mek
  succ K_EDOTA_CW_Building_Mek = K_EDOTA_CW_Building_Pipe
  succ K_EDOTA_CW_Building_Pipe = K_EDOTA_CW_Stack_And_Pull
  succ K_EDOTA_CW_Stack_And_Pull = K_EDOTA_CW_Pull
  succ K_EDOTA_CW_Pull = K_EDOTA_CW_Pulling
  succ K_EDOTA_CW_Pulling = K_EDOTA_CW_Stack
  succ K_EDOTA_CW_Stack = K_EDOTA_CW_Jungling
  succ K_EDOTA_CW_Jungling = K_EDOTA_CW_Roshan
  succ K_EDOTA_CW_Roshan = K_EDOTA_CW_Affirmative
  succ K_EDOTA_CW_Affirmative = K_EDOTA_CW_Wait
  succ K_EDOTA_CW_Wait = K_EDOTA_CW_Pause
  succ K_EDOTA_CW_Pause = K_EDOTA_CW_Current_Time
  succ K_EDOTA_CW_Current_Time = K_EDOTA_CW_Check_Runes
  succ K_EDOTA_CW_Check_Runes = K_EDOTA_CW_Smoke_Gank
  succ K_EDOTA_CW_Smoke_Gank = K_EDOTA_CW_GLHF
  succ K_EDOTA_CW_GLHF = K_EDOTA_CW_Nice
  succ K_EDOTA_CW_Nice = K_EDOTA_CW_Thanks
  succ K_EDOTA_CW_Thanks = K_EDOTA_CW_Sorry
  succ K_EDOTA_CW_Sorry = K_EDOTA_CW_No_Give_Up
  succ K_EDOTA_CW_No_Give_Up = K_EDOTA_CW_Just_Happened
  succ K_EDOTA_CW_Just_Happened = K_EDOTA_CW_Game_Is_Hard
  succ K_EDOTA_CW_Game_Is_Hard = K_EDOTA_CW_New_Meta
  succ K_EDOTA_CW_New_Meta = K_EDOTA_CW_My_Bad
  succ K_EDOTA_CW_My_Bad = K_EDOTA_CW_Regret
  succ K_EDOTA_CW_Regret = K_EDOTA_CW_Relax
  succ K_EDOTA_CW_Relax = K_EDOTA_CW_MissingHero
  succ K_EDOTA_CW_MissingHero = K_EDOTA_CW_ReturnedHero
  succ K_EDOTA_CW_ReturnedHero = K_EDOTA_CW_GG
  succ K_EDOTA_CW_GG = K_EDOTA_CW_GGWP
  succ K_EDOTA_CW_GGWP = K_EDOTA_CW_All_GG
  succ K_EDOTA_CW_All_GG = K_EDOTA_CW_All_GGWP
  succ K_EDOTA_CW_All_GGWP = K_EDOTA_CW_What_To_Buy
  succ K_EDOTA_CW_What_To_Buy = K_EDOTA_CW_Im_Retreating
  succ K_EDOTA_CW_Im_Retreating = K_EDOTA_CW_Space_Created
  succ K_EDOTA_CW_Space_Created = K_EDOTA_CW_Whoops
  succ K_EDOTA_CW_Whoops = K_EDOTA_CW_Tower_then_Back
  succ K_EDOTA_CW_Tower_then_Back = K_EDOTA_CW_Barracks_then_Back
  succ K_EDOTA_CW_Barracks_then_Back = K_EDOTA_CW_Ward_Bottom_Rune
  succ K_EDOTA_CW_Ward_Bottom_Rune = K_EDOTA_CW_Ward_Top_Rune
  succ K_EDOTA_CW_Ward_Top_Rune = K_EDOTA_CW_Zeus_Ult
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaCommonMessages.EDOTAChatWheelMessage"
  pred K_EDOTA_CW_Care = K_EDOTA_CW_Ok
  pred K_EDOTA_CW_GetBack = K_EDOTA_CW_Care
  pred K_EDOTA_CW_NeedWards = K_EDOTA_CW_GetBack
  pred K_EDOTA_CW_Stun = K_EDOTA_CW_NeedWards
  pred K_EDOTA_CW_Help = K_EDOTA_CW_Stun
  pred K_EDOTA_CW_Push = K_EDOTA_CW_Help
  pred K_EDOTA_CW_GoodJob = K_EDOTA_CW_Push
  pred K_EDOTA_CW_Missing = K_EDOTA_CW_GoodJob
  pred K_EDOTA_CW_Missing_Top = K_EDOTA_CW_Missing
  pred K_EDOTA_CW_Missing_Mid = K_EDOTA_CW_Missing_Top
  pred K_EDOTA_CW_Missing_Bottom = K_EDOTA_CW_Missing_Mid
  pred K_EDOTA_CW_Go = K_EDOTA_CW_Missing_Bottom
  pred K_EDOTA_CW_Initiate = K_EDOTA_CW_Go
  pred K_EDOTA_CW_Follow = K_EDOTA_CW_Initiate
  pred K_EDOTA_CW_Group_Up = K_EDOTA_CW_Follow
  pred K_EDOTA_CW_Spread_Out = K_EDOTA_CW_Group_Up
  pred K_EDOTA_CW_Split_Farm = K_EDOTA_CW_Spread_Out
  pred K_EDOTA_CW_Attack = K_EDOTA_CW_Split_Farm
  pred K_EDOTA_CW_BRB = K_EDOTA_CW_Attack
  pred K_EDOTA_CW_Dive = K_EDOTA_CW_BRB
  pred K_EDOTA_CW_OMW = K_EDOTA_CW_Dive
  pred K_EDOTA_CW_Get_Ready = K_EDOTA_CW_OMW
  pred K_EDOTA_CW_Bait = K_EDOTA_CW_Get_Ready
  pred K_EDOTA_CW_Heal = K_EDOTA_CW_Bait
  pred K_EDOTA_CW_Mana = K_EDOTA_CW_Heal
  pred K_EDOTA_CW_OOM = K_EDOTA_CW_Mana
  pred K_EDOTA_CW_Skill_Cooldown = K_EDOTA_CW_OOM
  pred K_EDOTA_CW_Ulti_Ready = K_EDOTA_CW_Skill_Cooldown
  pred K_EDOTA_CW_Enemy_Returned = K_EDOTA_CW_Ulti_Ready
  pred K_EDOTA_CW_All_Missing = K_EDOTA_CW_Enemy_Returned
  pred K_EDOTA_CW_Enemy_Incoming = K_EDOTA_CW_All_Missing
  pred K_EDOTA_CW_Invis_Enemy = K_EDOTA_CW_Enemy_Incoming
  pred K_EDOTA_CW_Enemy_Had_Rune = K_EDOTA_CW_Invis_Enemy
  pred K_EDOTA_CW_Split_Push = K_EDOTA_CW_Enemy_Had_Rune
  pred K_EDOTA_CW_Coming_To_Gank = K_EDOTA_CW_Split_Push
  pred K_EDOTA_CW_Request_Gank = K_EDOTA_CW_Coming_To_Gank
  pred K_EDOTA_CW_Fight_Under_Tower = K_EDOTA_CW_Request_Gank
  pred K_EDOTA_CW_Deny_Tower = K_EDOTA_CW_Fight_Under_Tower
  pred K_EDOTA_CW_Buy_Courier = K_EDOTA_CW_Deny_Tower
  pred K_EDOTA_CW_Upgrade_Courier = K_EDOTA_CW_Buy_Courier
  pred K_EDOTA_CW_Need_Detection = K_EDOTA_CW_Upgrade_Courier
  pred K_EDOTA_CW_They_Have_Detection = K_EDOTA_CW_Need_Detection
  pred K_EDOTA_CW_Buy_TP = K_EDOTA_CW_They_Have_Detection
  pred K_EDOTA_CW_Reuse_Courier = K_EDOTA_CW_Buy_TP
  pred K_EDOTA_CW_Deward = K_EDOTA_CW_Reuse_Courier
  pred K_EDOTA_CW_Building_Mek = K_EDOTA_CW_Deward
  pred K_EDOTA_CW_Building_Pipe = K_EDOTA_CW_Building_Mek
  pred K_EDOTA_CW_Stack_And_Pull = K_EDOTA_CW_Building_Pipe
  pred K_EDOTA_CW_Pull = K_EDOTA_CW_Stack_And_Pull
  pred K_EDOTA_CW_Pulling = K_EDOTA_CW_Pull
  pred K_EDOTA_CW_Stack = K_EDOTA_CW_Pulling
  pred K_EDOTA_CW_Jungling = K_EDOTA_CW_Stack
  pred K_EDOTA_CW_Roshan = K_EDOTA_CW_Jungling
  pred K_EDOTA_CW_Affirmative = K_EDOTA_CW_Roshan
  pred K_EDOTA_CW_Wait = K_EDOTA_CW_Affirmative
  pred K_EDOTA_CW_Pause = K_EDOTA_CW_Wait
  pred K_EDOTA_CW_Current_Time = K_EDOTA_CW_Pause
  pred K_EDOTA_CW_Check_Runes = K_EDOTA_CW_Current_Time
  pred K_EDOTA_CW_Smoke_Gank = K_EDOTA_CW_Check_Runes
  pred K_EDOTA_CW_GLHF = K_EDOTA_CW_Smoke_Gank
  pred K_EDOTA_CW_Nice = K_EDOTA_CW_GLHF
  pred K_EDOTA_CW_Thanks = K_EDOTA_CW_Nice
  pred K_EDOTA_CW_Sorry = K_EDOTA_CW_Thanks
  pred K_EDOTA_CW_No_Give_Up = K_EDOTA_CW_Sorry
  pred K_EDOTA_CW_Just_Happened = K_EDOTA_CW_No_Give_Up
  pred K_EDOTA_CW_Game_Is_Hard = K_EDOTA_CW_Just_Happened
  pred K_EDOTA_CW_New_Meta = K_EDOTA_CW_Game_Is_Hard
  pred K_EDOTA_CW_My_Bad = K_EDOTA_CW_New_Meta
  pred K_EDOTA_CW_Regret = K_EDOTA_CW_My_Bad
  pred K_EDOTA_CW_Relax = K_EDOTA_CW_Regret
  pred K_EDOTA_CW_MissingHero = K_EDOTA_CW_Relax
  pred K_EDOTA_CW_ReturnedHero = K_EDOTA_CW_MissingHero
  pred K_EDOTA_CW_GG = K_EDOTA_CW_ReturnedHero
  pred K_EDOTA_CW_GGWP = K_EDOTA_CW_GG
  pred K_EDOTA_CW_All_GG = K_EDOTA_CW_GGWP
  pred K_EDOTA_CW_All_GGWP = K_EDOTA_CW_All_GG
  pred K_EDOTA_CW_What_To_Buy = K_EDOTA_CW_All_GGWP
  pred K_EDOTA_CW_Im_Retreating = K_EDOTA_CW_What_To_Buy
  pred K_EDOTA_CW_Space_Created = K_EDOTA_CW_Im_Retreating
  pred K_EDOTA_CW_Whoops = K_EDOTA_CW_Space_Created
  pred K_EDOTA_CW_Tower_then_Back = K_EDOTA_CW_Whoops
  pred K_EDOTA_CW_Barracks_then_Back = K_EDOTA_CW_Tower_then_Back
  pred K_EDOTA_CW_Ward_Bottom_Rune = K_EDOTA_CW_Barracks_then_Back
  pred K_EDOTA_CW_Ward_Top_Rune = K_EDOTA_CW_Ward_Bottom_Rune
  pred K_EDOTA_CW_Zeus_Ult = K_EDOTA_CW_Ward_Top_Rune
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaCommonMessages.EDOTAChatWheelMessage"

instance P'.Wire EDOTAChatWheelMessage where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EDOTAChatWheelMessage

instance P'.MessageAPI msg' (msg' -> EDOTAChatWheelMessage) EDOTAChatWheelMessage where
  getVal m' f' = f' m'

instance P'.ReflectEnum EDOTAChatWheelMessage where
  reflectEnum
   = [(0, "K_EDOTA_CW_Ok", K_EDOTA_CW_Ok), (1, "K_EDOTA_CW_Care", K_EDOTA_CW_Care), (2, "K_EDOTA_CW_GetBack", K_EDOTA_CW_GetBack),
      (3, "K_EDOTA_CW_NeedWards", K_EDOTA_CW_NeedWards), (4, "K_EDOTA_CW_Stun", K_EDOTA_CW_Stun),
      (5, "K_EDOTA_CW_Help", K_EDOTA_CW_Help), (6, "K_EDOTA_CW_Push", K_EDOTA_CW_Push),
      (7, "K_EDOTA_CW_GoodJob", K_EDOTA_CW_GoodJob), (8, "K_EDOTA_CW_Missing", K_EDOTA_CW_Missing),
      (9, "K_EDOTA_CW_Missing_Top", K_EDOTA_CW_Missing_Top), (10, "K_EDOTA_CW_Missing_Mid", K_EDOTA_CW_Missing_Mid),
      (11, "K_EDOTA_CW_Missing_Bottom", K_EDOTA_CW_Missing_Bottom), (12, "K_EDOTA_CW_Go", K_EDOTA_CW_Go),
      (13, "K_EDOTA_CW_Initiate", K_EDOTA_CW_Initiate), (14, "K_EDOTA_CW_Follow", K_EDOTA_CW_Follow),
      (15, "K_EDOTA_CW_Group_Up", K_EDOTA_CW_Group_Up), (16, "K_EDOTA_CW_Spread_Out", K_EDOTA_CW_Spread_Out),
      (17, "K_EDOTA_CW_Split_Farm", K_EDOTA_CW_Split_Farm), (18, "K_EDOTA_CW_Attack", K_EDOTA_CW_Attack),
      (19, "K_EDOTA_CW_BRB", K_EDOTA_CW_BRB), (20, "K_EDOTA_CW_Dive", K_EDOTA_CW_Dive), (21, "K_EDOTA_CW_OMW", K_EDOTA_CW_OMW),
      (22, "K_EDOTA_CW_Get_Ready", K_EDOTA_CW_Get_Ready), (23, "K_EDOTA_CW_Bait", K_EDOTA_CW_Bait),
      (24, "K_EDOTA_CW_Heal", K_EDOTA_CW_Heal), (25, "K_EDOTA_CW_Mana", K_EDOTA_CW_Mana), (26, "K_EDOTA_CW_OOM", K_EDOTA_CW_OOM),
      (27, "K_EDOTA_CW_Skill_Cooldown", K_EDOTA_CW_Skill_Cooldown), (28, "K_EDOTA_CW_Ulti_Ready", K_EDOTA_CW_Ulti_Ready),
      (29, "K_EDOTA_CW_Enemy_Returned", K_EDOTA_CW_Enemy_Returned), (30, "K_EDOTA_CW_All_Missing", K_EDOTA_CW_All_Missing),
      (31, "K_EDOTA_CW_Enemy_Incoming", K_EDOTA_CW_Enemy_Incoming), (32, "K_EDOTA_CW_Invis_Enemy", K_EDOTA_CW_Invis_Enemy),
      (33, "K_EDOTA_CW_Enemy_Had_Rune", K_EDOTA_CW_Enemy_Had_Rune), (34, "K_EDOTA_CW_Split_Push", K_EDOTA_CW_Split_Push),
      (35, "K_EDOTA_CW_Coming_To_Gank", K_EDOTA_CW_Coming_To_Gank), (36, "K_EDOTA_CW_Request_Gank", K_EDOTA_CW_Request_Gank),
      (37, "K_EDOTA_CW_Fight_Under_Tower", K_EDOTA_CW_Fight_Under_Tower), (38, "K_EDOTA_CW_Deny_Tower", K_EDOTA_CW_Deny_Tower),
      (39, "K_EDOTA_CW_Buy_Courier", K_EDOTA_CW_Buy_Courier), (40, "K_EDOTA_CW_Upgrade_Courier", K_EDOTA_CW_Upgrade_Courier),
      (41, "K_EDOTA_CW_Need_Detection", K_EDOTA_CW_Need_Detection),
      (42, "K_EDOTA_CW_They_Have_Detection", K_EDOTA_CW_They_Have_Detection), (43, "K_EDOTA_CW_Buy_TP", K_EDOTA_CW_Buy_TP),
      (44, "K_EDOTA_CW_Reuse_Courier", K_EDOTA_CW_Reuse_Courier), (45, "K_EDOTA_CW_Deward", K_EDOTA_CW_Deward),
      (46, "K_EDOTA_CW_Building_Mek", K_EDOTA_CW_Building_Mek), (47, "K_EDOTA_CW_Building_Pipe", K_EDOTA_CW_Building_Pipe),
      (48, "K_EDOTA_CW_Stack_And_Pull", K_EDOTA_CW_Stack_And_Pull), (49, "K_EDOTA_CW_Pull", K_EDOTA_CW_Pull),
      (50, "K_EDOTA_CW_Pulling", K_EDOTA_CW_Pulling), (51, "K_EDOTA_CW_Stack", K_EDOTA_CW_Stack),
      (52, "K_EDOTA_CW_Jungling", K_EDOTA_CW_Jungling), (53, "K_EDOTA_CW_Roshan", K_EDOTA_CW_Roshan),
      (54, "K_EDOTA_CW_Affirmative", K_EDOTA_CW_Affirmative), (55, "K_EDOTA_CW_Wait", K_EDOTA_CW_Wait),
      (56, "K_EDOTA_CW_Pause", K_EDOTA_CW_Pause), (57, "K_EDOTA_CW_Current_Time", K_EDOTA_CW_Current_Time),
      (58, "K_EDOTA_CW_Check_Runes", K_EDOTA_CW_Check_Runes), (59, "K_EDOTA_CW_Smoke_Gank", K_EDOTA_CW_Smoke_Gank),
      (60, "K_EDOTA_CW_GLHF", K_EDOTA_CW_GLHF), (61, "K_EDOTA_CW_Nice", K_EDOTA_CW_Nice),
      (62, "K_EDOTA_CW_Thanks", K_EDOTA_CW_Thanks), (63, "K_EDOTA_CW_Sorry", K_EDOTA_CW_Sorry),
      (64, "K_EDOTA_CW_No_Give_Up", K_EDOTA_CW_No_Give_Up), (65, "K_EDOTA_CW_Just_Happened", K_EDOTA_CW_Just_Happened),
      (66, "K_EDOTA_CW_Game_Is_Hard", K_EDOTA_CW_Game_Is_Hard), (67, "K_EDOTA_CW_New_Meta", K_EDOTA_CW_New_Meta),
      (68, "K_EDOTA_CW_My_Bad", K_EDOTA_CW_My_Bad), (69, "K_EDOTA_CW_Regret", K_EDOTA_CW_Regret),
      (70, "K_EDOTA_CW_Relax", K_EDOTA_CW_Relax), (71, "K_EDOTA_CW_MissingHero", K_EDOTA_CW_MissingHero),
      (72, "K_EDOTA_CW_ReturnedHero", K_EDOTA_CW_ReturnedHero), (73, "K_EDOTA_CW_GG", K_EDOTA_CW_GG),
      (74, "K_EDOTA_CW_GGWP", K_EDOTA_CW_GGWP), (75, "K_EDOTA_CW_All_GG", K_EDOTA_CW_All_GG),
      (76, "K_EDOTA_CW_All_GGWP", K_EDOTA_CW_All_GGWP), (77, "K_EDOTA_CW_What_To_Buy", K_EDOTA_CW_What_To_Buy),
      (78, "K_EDOTA_CW_Im_Retreating", K_EDOTA_CW_Im_Retreating), (79, "K_EDOTA_CW_Space_Created", K_EDOTA_CW_Space_Created),
      (80, "K_EDOTA_CW_Whoops", K_EDOTA_CW_Whoops), (81, "K_EDOTA_CW_Tower_then_Back", K_EDOTA_CW_Tower_then_Back),
      (82, "K_EDOTA_CW_Barracks_then_Back", K_EDOTA_CW_Barracks_then_Back),
      (83, "K_EDOTA_CW_Ward_Bottom_Rune", K_EDOTA_CW_Ward_Bottom_Rune), (84, "K_EDOTA_CW_Ward_Top_Rune", K_EDOTA_CW_Ward_Top_Rune),
      (85, "K_EDOTA_CW_Zeus_Ult", K_EDOTA_CW_Zeus_Ult)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Commonmessages.EDOTAChatWheelMessage") ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaCommonMessages"]
        "EDOTAChatWheelMessage")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaCommonMessages", "EDOTAChatWheelMessage.hs"]
      [(0, "K_EDOTA_CW_Ok"), (1, "K_EDOTA_CW_Care"), (2, "K_EDOTA_CW_GetBack"), (3, "K_EDOTA_CW_NeedWards"), (4, "K_EDOTA_CW_Stun"),
       (5, "K_EDOTA_CW_Help"), (6, "K_EDOTA_CW_Push"), (7, "K_EDOTA_CW_GoodJob"), (8, "K_EDOTA_CW_Missing"),
       (9, "K_EDOTA_CW_Missing_Top"), (10, "K_EDOTA_CW_Missing_Mid"), (11, "K_EDOTA_CW_Missing_Bottom"), (12, "K_EDOTA_CW_Go"),
       (13, "K_EDOTA_CW_Initiate"), (14, "K_EDOTA_CW_Follow"), (15, "K_EDOTA_CW_Group_Up"), (16, "K_EDOTA_CW_Spread_Out"),
       (17, "K_EDOTA_CW_Split_Farm"), (18, "K_EDOTA_CW_Attack"), (19, "K_EDOTA_CW_BRB"), (20, "K_EDOTA_CW_Dive"),
       (21, "K_EDOTA_CW_OMW"), (22, "K_EDOTA_CW_Get_Ready"), (23, "K_EDOTA_CW_Bait"), (24, "K_EDOTA_CW_Heal"),
       (25, "K_EDOTA_CW_Mana"), (26, "K_EDOTA_CW_OOM"), (27, "K_EDOTA_CW_Skill_Cooldown"), (28, "K_EDOTA_CW_Ulti_Ready"),
       (29, "K_EDOTA_CW_Enemy_Returned"), (30, "K_EDOTA_CW_All_Missing"), (31, "K_EDOTA_CW_Enemy_Incoming"),
       (32, "K_EDOTA_CW_Invis_Enemy"), (33, "K_EDOTA_CW_Enemy_Had_Rune"), (34, "K_EDOTA_CW_Split_Push"),
       (35, "K_EDOTA_CW_Coming_To_Gank"), (36, "K_EDOTA_CW_Request_Gank"), (37, "K_EDOTA_CW_Fight_Under_Tower"),
       (38, "K_EDOTA_CW_Deny_Tower"), (39, "K_EDOTA_CW_Buy_Courier"), (40, "K_EDOTA_CW_Upgrade_Courier"),
       (41, "K_EDOTA_CW_Need_Detection"), (42, "K_EDOTA_CW_They_Have_Detection"), (43, "K_EDOTA_CW_Buy_TP"),
       (44, "K_EDOTA_CW_Reuse_Courier"), (45, "K_EDOTA_CW_Deward"), (46, "K_EDOTA_CW_Building_Mek"),
       (47, "K_EDOTA_CW_Building_Pipe"), (48, "K_EDOTA_CW_Stack_And_Pull"), (49, "K_EDOTA_CW_Pull"), (50, "K_EDOTA_CW_Pulling"),
       (51, "K_EDOTA_CW_Stack"), (52, "K_EDOTA_CW_Jungling"), (53, "K_EDOTA_CW_Roshan"), (54, "K_EDOTA_CW_Affirmative"),
       (55, "K_EDOTA_CW_Wait"), (56, "K_EDOTA_CW_Pause"), (57, "K_EDOTA_CW_Current_Time"), (58, "K_EDOTA_CW_Check_Runes"),
       (59, "K_EDOTA_CW_Smoke_Gank"), (60, "K_EDOTA_CW_GLHF"), (61, "K_EDOTA_CW_Nice"), (62, "K_EDOTA_CW_Thanks"),
       (63, "K_EDOTA_CW_Sorry"), (64, "K_EDOTA_CW_No_Give_Up"), (65, "K_EDOTA_CW_Just_Happened"), (66, "K_EDOTA_CW_Game_Is_Hard"),
       (67, "K_EDOTA_CW_New_Meta"), (68, "K_EDOTA_CW_My_Bad"), (69, "K_EDOTA_CW_Regret"), (70, "K_EDOTA_CW_Relax"),
       (71, "K_EDOTA_CW_MissingHero"), (72, "K_EDOTA_CW_ReturnedHero"), (73, "K_EDOTA_CW_GG"), (74, "K_EDOTA_CW_GGWP"),
       (75, "K_EDOTA_CW_All_GG"), (76, "K_EDOTA_CW_All_GGWP"), (77, "K_EDOTA_CW_What_To_Buy"), (78, "K_EDOTA_CW_Im_Retreating"),
       (79, "K_EDOTA_CW_Space_Created"), (80, "K_EDOTA_CW_Whoops"), (81, "K_EDOTA_CW_Tower_then_Back"),
       (82, "K_EDOTA_CW_Barracks_then_Back"), (83, "K_EDOTA_CW_Ward_Bottom_Rune"), (84, "K_EDOTA_CW_Ward_Top_Rune"),
       (85, "K_EDOTA_CW_Zeus_Ult")]

instance P'.TextType EDOTAChatWheelMessage where
  tellT = P'.tellShow
  getT = P'.getRead