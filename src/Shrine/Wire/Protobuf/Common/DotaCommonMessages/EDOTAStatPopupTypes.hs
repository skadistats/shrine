{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.Common.DotaCommonMessages.EDOTAStatPopupTypes (EDOTAStatPopupTypes(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EDOTAStatPopupTypes = K_EDOTA_SPT_Textline
                         | K_EDOTA_SPT_Basic
                         | K_EDOTA_SPT_Poll
                         | K_EDOTA_SPT_Grid
                         | K_EDOTA_SPT_DualImage
                         deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                   Prelude'.Generic)

instance P'.Mergeable EDOTAStatPopupTypes

instance Prelude'.Bounded EDOTAStatPopupTypes where
  minBound = K_EDOTA_SPT_Textline
  maxBound = K_EDOTA_SPT_DualImage

instance P'.Default EDOTAStatPopupTypes where
  defaultValue = K_EDOTA_SPT_Textline

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EDOTAStatPopupTypes
toMaybe'Enum 0 = Prelude'.Just K_EDOTA_SPT_Textline
toMaybe'Enum 1 = Prelude'.Just K_EDOTA_SPT_Basic
toMaybe'Enum 2 = Prelude'.Just K_EDOTA_SPT_Poll
toMaybe'Enum 3 = Prelude'.Just K_EDOTA_SPT_Grid
toMaybe'Enum 4 = Prelude'.Just K_EDOTA_SPT_DualImage
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EDOTAStatPopupTypes where
  fromEnum K_EDOTA_SPT_Textline = 0
  fromEnum K_EDOTA_SPT_Basic = 1
  fromEnum K_EDOTA_SPT_Poll = 2
  fromEnum K_EDOTA_SPT_Grid = 3
  fromEnum K_EDOTA_SPT_DualImage = 4
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.Common.DotaCommonMessages.EDOTAStatPopupTypes")
      . toMaybe'Enum
  succ K_EDOTA_SPT_Textline = K_EDOTA_SPT_Basic
  succ K_EDOTA_SPT_Basic = K_EDOTA_SPT_Poll
  succ K_EDOTA_SPT_Poll = K_EDOTA_SPT_Grid
  succ K_EDOTA_SPT_Grid = K_EDOTA_SPT_DualImage
  succ _
   = Prelude'.error
      "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.Common.DotaCommonMessages.EDOTAStatPopupTypes"
  pred K_EDOTA_SPT_Basic = K_EDOTA_SPT_Textline
  pred K_EDOTA_SPT_Poll = K_EDOTA_SPT_Basic
  pred K_EDOTA_SPT_Grid = K_EDOTA_SPT_Poll
  pred K_EDOTA_SPT_DualImage = K_EDOTA_SPT_Grid
  pred _
   = Prelude'.error
      "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.Common.DotaCommonMessages.EDOTAStatPopupTypes"

instance P'.Wire EDOTAStatPopupTypes where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EDOTAStatPopupTypes

instance P'.MessageAPI msg' (msg' -> EDOTAStatPopupTypes) EDOTAStatPopupTypes where
  getVal m' f' = f' m'

instance P'.ReflectEnum EDOTAStatPopupTypes where
  reflectEnum
   = [(0, "K_EDOTA_SPT_Textline", K_EDOTA_SPT_Textline), (1, "K_EDOTA_SPT_Basic", K_EDOTA_SPT_Basic),
      (2, "K_EDOTA_SPT_Poll", K_EDOTA_SPT_Poll), (3, "K_EDOTA_SPT_Grid", K_EDOTA_SPT_Grid),
      (4, "K_EDOTA_SPT_DualImage", K_EDOTA_SPT_DualImage)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".Dota_Commonmessages.EDOTAStatPopupTypes") ["Shrine", "Wire", "Protobuf", "Common"]
        ["DotaCommonMessages"]
        "EDOTAStatPopupTypes")
      ["Shrine", "Wire", "Protobuf", "Common", "DotaCommonMessages", "EDOTAStatPopupTypes.hs"]
      [(0, "K_EDOTA_SPT_Textline"), (1, "K_EDOTA_SPT_Basic"), (2, "K_EDOTA_SPT_Poll"), (3, "K_EDOTA_SPT_Grid"),
       (4, "K_EDOTA_SPT_DualImage")]

instance P'.TextType EDOTAStatPopupTypes where
  tellT = P'.tellShow
  getT = P'.getRead