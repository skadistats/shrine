{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.Player (Player(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMatchAdditionalUnitInventory as S2DotaGcCommon
       (CMatchAdditionalUnitInventory)
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMatchPlayerAbilityUpgrade as S2DotaGcCommon (CMatchPlayerAbilityUpgrade)
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMatchPlayerPermanentBuff as S2DotaGcCommon (CMatchPlayerPermanentBuff)
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.Player.CustomGameData as S2DotaGcCommon.CMsgDOTAMatch.Player
       (CustomGameData)

data Player = Player{account_id :: !(P'.Maybe P'.Word32), player_slot :: !(P'.Maybe P'.Word32), hero_id :: !(P'.Maybe P'.Word32),
                     item_0 :: !(P'.Maybe P'.Word32), item_1 :: !(P'.Maybe P'.Word32), item_2 :: !(P'.Maybe P'.Word32),
                     item_3 :: !(P'.Maybe P'.Word32), item_4 :: !(P'.Maybe P'.Word32), item_5 :: !(P'.Maybe P'.Word32),
                     expected_team_contribution :: !(P'.Maybe P'.Float), scaled_metric :: !(P'.Maybe P'.Float),
                     previous_rank :: !(P'.Maybe P'.Word32), rank_change :: !(P'.Maybe P'.Int32), solo_rank :: !(P'.Maybe P'.Bool),
                     kills :: !(P'.Maybe P'.Word32), deaths :: !(P'.Maybe P'.Word32), assists :: !(P'.Maybe P'.Word32),
                     leaver_status :: !(P'.Maybe P'.Word32), gold :: !(P'.Maybe P'.Word32), last_hits :: !(P'.Maybe P'.Word32),
                     denies :: !(P'.Maybe P'.Word32), gold_per_min :: !(P'.Maybe P'.Word32), xP_per_min :: !(P'.Maybe P'.Word32),
                     gold_spent :: !(P'.Maybe P'.Word32), hero_damage :: !(P'.Maybe P'.Word32),
                     tower_damage :: !(P'.Maybe P'.Word32), hero_healing :: !(P'.Maybe P'.Word32), level :: !(P'.Maybe P'.Word32),
                     time_last_seen :: !(P'.Maybe P'.Word32), player_name :: !(P'.Maybe P'.Utf8),
                     support_ability_value :: !(P'.Maybe P'.Word32), feeding_detected :: !(P'.Maybe P'.Bool),
                     search_rank :: !(P'.Maybe P'.Word32), search_rank_uncertainty :: !(P'.Maybe P'.Word32),
                     rank_uncertainty_change :: !(P'.Maybe P'.Int32), hero_play_count :: !(P'.Maybe P'.Word32),
                     party_id :: !(P'.Maybe P'.Word64), scaled_hero_damage :: !(P'.Maybe P'.Word32),
                     scaled_tower_damage :: !(P'.Maybe P'.Word32), scaled_hero_healing :: !(P'.Maybe P'.Word32),
                     scaled_kills :: !(P'.Maybe P'.Float), scaled_deaths :: !(P'.Maybe P'.Float),
                     scaled_assists :: !(P'.Maybe P'.Float), claimed_farm_gold :: !(P'.Maybe P'.Word32),
                     support_gold :: !(P'.Maybe P'.Word32), claimed_denies :: !(P'.Maybe P'.Word32),
                     claimed_misses :: !(P'.Maybe P'.Word32), misses :: !(P'.Maybe P'.Word32),
                     ability_upgrades :: !(P'.Seq S2DotaGcCommon.CMatchPlayerAbilityUpgrade),
                     additional_units_inventory :: !(P'.Seq S2DotaGcCommon.CMatchAdditionalUnitInventory),
                     permanent_buffs :: !(P'.Seq S2DotaGcCommon.CMatchPlayerPermanentBuff),
                     custom_game_data :: !(P'.Maybe S2DotaGcCommon.CMsgDOTAMatch.Player.CustomGameData),
                     active_battle_pass :: !(P'.Maybe P'.Bool), net_worth :: !(P'.Maybe P'.Word32),
                     bot_difficulty :: !(P'.Maybe P'.Word32)}
            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Player where
  mergeAppend
   (Player x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23 x'24 x'25 x'26
     x'27 x'28 x'29 x'30 x'31 x'32 x'33 x'34 x'35 x'36 x'37 x'38 x'39 x'40 x'41 x'42 x'43 x'44 x'45 x'46 x'47 x'48 x'49 x'50 x'51
     x'52 x'53 x'54 x'55)
   (Player y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18 y'19 y'20 y'21 y'22 y'23 y'24 y'25 y'26
     y'27 y'28 y'29 y'30 y'31 y'32 y'33 y'34 y'35 y'36 y'37 y'38 y'39 y'40 y'41 y'42 y'43 y'44 y'45 y'46 y'47 y'48 y'49 y'50 y'51
     y'52 y'53 y'54 y'55)
   = Player (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)
      (P'.mergeAppend x'19 y'19)
      (P'.mergeAppend x'20 y'20)
      (P'.mergeAppend x'21 y'21)
      (P'.mergeAppend x'22 y'22)
      (P'.mergeAppend x'23 y'23)
      (P'.mergeAppend x'24 y'24)
      (P'.mergeAppend x'25 y'25)
      (P'.mergeAppend x'26 y'26)
      (P'.mergeAppend x'27 y'27)
      (P'.mergeAppend x'28 y'28)
      (P'.mergeAppend x'29 y'29)
      (P'.mergeAppend x'30 y'30)
      (P'.mergeAppend x'31 y'31)
      (P'.mergeAppend x'32 y'32)
      (P'.mergeAppend x'33 y'33)
      (P'.mergeAppend x'34 y'34)
      (P'.mergeAppend x'35 y'35)
      (P'.mergeAppend x'36 y'36)
      (P'.mergeAppend x'37 y'37)
      (P'.mergeAppend x'38 y'38)
      (P'.mergeAppend x'39 y'39)
      (P'.mergeAppend x'40 y'40)
      (P'.mergeAppend x'41 y'41)
      (P'.mergeAppend x'42 y'42)
      (P'.mergeAppend x'43 y'43)
      (P'.mergeAppend x'44 y'44)
      (P'.mergeAppend x'45 y'45)
      (P'.mergeAppend x'46 y'46)
      (P'.mergeAppend x'47 y'47)
      (P'.mergeAppend x'48 y'48)
      (P'.mergeAppend x'49 y'49)
      (P'.mergeAppend x'50 y'50)
      (P'.mergeAppend x'51 y'51)
      (P'.mergeAppend x'52 y'52)
      (P'.mergeAppend x'53 y'53)
      (P'.mergeAppend x'54 y'54)
      (P'.mergeAppend x'55 y'55)

instance P'.Default Player where
  defaultValue
   = Player P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire Player where
  wireSize ft'
   self'@(Player x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23 x'24 x'25
           x'26 x'27 x'28 x'29 x'30 x'31 x'32 x'33 x'34 x'35 x'36 x'37 x'38 x'39 x'40 x'41 x'42 x'43 x'44 x'45 x'46 x'47 x'48 x'49
           x'50 x'51 x'52 x'53 x'54 x'55)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 13 x'3 + P'.wireSizeOpt 1 13 x'4 +
             P'.wireSizeOpt 1 13 x'5
             + P'.wireSizeOpt 1 13 x'6
             + P'.wireSizeOpt 1 13 x'7
             + P'.wireSizeOpt 1 13 x'8
             + P'.wireSizeOpt 1 13 x'9
             + P'.wireSizeOpt 1 2 x'10
             + P'.wireSizeOpt 1 2 x'11
             + P'.wireSizeOpt 1 13 x'12
             + P'.wireSizeOpt 1 17 x'13
             + P'.wireSizeOpt 2 8 x'14
             + P'.wireSizeOpt 1 13 x'15
             + P'.wireSizeOpt 1 13 x'16
             + P'.wireSizeOpt 2 13 x'17
             + P'.wireSizeOpt 2 13 x'18
             + P'.wireSizeOpt 2 13 x'19
             + P'.wireSizeOpt 2 13 x'20
             + P'.wireSizeOpt 2 13 x'21
             + P'.wireSizeOpt 2 13 x'22
             + P'.wireSizeOpt 2 13 x'23
             + P'.wireSizeOpt 2 13 x'24
             + P'.wireSizeOpt 2 13 x'25
             + P'.wireSizeOpt 2 13 x'26
             + P'.wireSizeOpt 2 13 x'27
             + P'.wireSizeOpt 2 13 x'28
             + P'.wireSizeOpt 2 13 x'29
             + P'.wireSizeOpt 2 9 x'30
             + P'.wireSizeOpt 2 13 x'31
             + P'.wireSizeOpt 2 8 x'32
             + P'.wireSizeOpt 2 13 x'33
             + P'.wireSizeOpt 2 13 x'34
             + P'.wireSizeOpt 2 5 x'35
             + P'.wireSizeOpt 2 13 x'36
             + P'.wireSizeOpt 2 6 x'37
             + P'.wireSizeOpt 2 13 x'38
             + P'.wireSizeOpt 2 13 x'39
             + P'.wireSizeOpt 2 13 x'40
             + P'.wireSizeOpt 2 2 x'41
             + P'.wireSizeOpt 2 2 x'42
             + P'.wireSizeOpt 2 2 x'43
             + P'.wireSizeOpt 2 13 x'44
             + P'.wireSizeOpt 2 13 x'45
             + P'.wireSizeOpt 2 13 x'46
             + P'.wireSizeOpt 2 13 x'47
             + P'.wireSizeOpt 2 13 x'48
             + P'.wireSizeRep 2 11 x'49
             + P'.wireSizeRep 2 11 x'50
             + P'.wireSizeRep 2 11 x'51
             + P'.wireSizeOpt 2 11 x'52
             + P'.wireSizeOpt 2 8 x'53
             + P'.wireSizeOpt 2 13 x'54
             + P'.wireSizeOpt 2 13 x'55)
  wirePut ft'
   self'@(Player x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23 x'24 x'25
           x'26 x'27 x'28 x'29 x'30 x'31 x'32 x'33 x'34 x'35 x'36 x'37 x'38 x'39 x'40 x'41 x'42 x'43 x'44 x'45 x'46 x'47 x'48 x'49
           x'50 x'51 x'52 x'53 x'54 x'55)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 13 x'3
             P'.wirePutOpt 32 13 x'4
             P'.wirePutOpt 40 13 x'5
             P'.wirePutOpt 48 13 x'6
             P'.wirePutOpt 56 13 x'7
             P'.wirePutOpt 64 13 x'8
             P'.wirePutOpt 72 13 x'9
             P'.wirePutOpt 85 2 x'10
             P'.wirePutOpt 93 2 x'11
             P'.wirePutOpt 96 13 x'12
             P'.wirePutOpt 104 17 x'13
             P'.wirePutOpt 112 13 x'15
             P'.wirePutOpt 120 13 x'16
             P'.wirePutOpt 128 13 x'17
             P'.wirePutOpt 136 13 x'18
             P'.wirePutOpt 144 13 x'19
             P'.wirePutOpt 152 13 x'20
             P'.wirePutOpt 160 13 x'21
             P'.wirePutOpt 168 13 x'22
             P'.wirePutOpt 176 13 x'23
             P'.wirePutOpt 184 13 x'24
             P'.wirePutOpt 192 13 x'25
             P'.wirePutOpt 200 13 x'26
             P'.wirePutOpt 208 13 x'27
             P'.wirePutOpt 216 13 x'28
             P'.wirePutOpt 224 13 x'29
             P'.wirePutOpt 234 9 x'30
             P'.wirePutOpt 240 13 x'31
             P'.wirePutOpt 256 8 x'32
             P'.wirePutOpt 272 13 x'33
             P'.wirePutOpt 280 13 x'34
             P'.wirePutOpt 288 5 x'35
             P'.wirePutOpt 296 13 x'36
             P'.wirePutOpt 305 6 x'37
             P'.wirePutOpt 317 2 x'41
             P'.wirePutOpt 325 2 x'42
             P'.wirePutOpt 333 2 x'43
             P'.wirePutOpt 336 13 x'44
             P'.wirePutOpt 344 13 x'45
             P'.wirePutOpt 352 13 x'46
             P'.wirePutOpt 360 13 x'47
             P'.wirePutOpt 368 13 x'48
             P'.wirePutRep 378 11 x'49
             P'.wirePutRep 386 11 x'50
             P'.wirePutOpt 392 8 x'14
             P'.wirePutOpt 402 11 x'52
             P'.wirePutOpt 408 8 x'53
             P'.wirePutOpt 416 13 x'54
             P'.wirePutOpt 432 13 x'38
             P'.wirePutOpt 440 13 x'39
             P'.wirePutOpt 448 13 x'40
             P'.wirePutRep 458 11 x'51
             P'.wirePutOpt 464 13 x'55
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{account_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{player_slot = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{hero_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{item_0 = Prelude'.Just new'Field}) (P'.wireGet 13)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{item_1 = Prelude'.Just new'Field}) (P'.wireGet 13)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{item_2 = Prelude'.Just new'Field}) (P'.wireGet 13)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{item_3 = Prelude'.Just new'Field}) (P'.wireGet 13)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{item_4 = Prelude'.Just new'Field}) (P'.wireGet 13)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{item_5 = Prelude'.Just new'Field}) (P'.wireGet 13)
             85 -> Prelude'.fmap (\ !new'Field -> old'Self{expected_team_contribution = Prelude'.Just new'Field}) (P'.wireGet 2)
             93 -> Prelude'.fmap (\ !new'Field -> old'Self{scaled_metric = Prelude'.Just new'Field}) (P'.wireGet 2)
             96 -> Prelude'.fmap (\ !new'Field -> old'Self{previous_rank = Prelude'.Just new'Field}) (P'.wireGet 13)
             104 -> Prelude'.fmap (\ !new'Field -> old'Self{rank_change = Prelude'.Just new'Field}) (P'.wireGet 17)
             392 -> Prelude'.fmap (\ !new'Field -> old'Self{solo_rank = Prelude'.Just new'Field}) (P'.wireGet 8)
             112 -> Prelude'.fmap (\ !new'Field -> old'Self{kills = Prelude'.Just new'Field}) (P'.wireGet 13)
             120 -> Prelude'.fmap (\ !new'Field -> old'Self{deaths = Prelude'.Just new'Field}) (P'.wireGet 13)
             128 -> Prelude'.fmap (\ !new'Field -> old'Self{assists = Prelude'.Just new'Field}) (P'.wireGet 13)
             136 -> Prelude'.fmap (\ !new'Field -> old'Self{leaver_status = Prelude'.Just new'Field}) (P'.wireGet 13)
             144 -> Prelude'.fmap (\ !new'Field -> old'Self{gold = Prelude'.Just new'Field}) (P'.wireGet 13)
             152 -> Prelude'.fmap (\ !new'Field -> old'Self{last_hits = Prelude'.Just new'Field}) (P'.wireGet 13)
             160 -> Prelude'.fmap (\ !new'Field -> old'Self{denies = Prelude'.Just new'Field}) (P'.wireGet 13)
             168 -> Prelude'.fmap (\ !new'Field -> old'Self{gold_per_min = Prelude'.Just new'Field}) (P'.wireGet 13)
             176 -> Prelude'.fmap (\ !new'Field -> old'Self{xP_per_min = Prelude'.Just new'Field}) (P'.wireGet 13)
             184 -> Prelude'.fmap (\ !new'Field -> old'Self{gold_spent = Prelude'.Just new'Field}) (P'.wireGet 13)
             192 -> Prelude'.fmap (\ !new'Field -> old'Self{hero_damage = Prelude'.Just new'Field}) (P'.wireGet 13)
             200 -> Prelude'.fmap (\ !new'Field -> old'Self{tower_damage = Prelude'.Just new'Field}) (P'.wireGet 13)
             208 -> Prelude'.fmap (\ !new'Field -> old'Self{hero_healing = Prelude'.Just new'Field}) (P'.wireGet 13)
             216 -> Prelude'.fmap (\ !new'Field -> old'Self{level = Prelude'.Just new'Field}) (P'.wireGet 13)
             224 -> Prelude'.fmap (\ !new'Field -> old'Self{time_last_seen = Prelude'.Just new'Field}) (P'.wireGet 13)
             234 -> Prelude'.fmap (\ !new'Field -> old'Self{player_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             240 -> Prelude'.fmap (\ !new'Field -> old'Self{support_ability_value = Prelude'.Just new'Field}) (P'.wireGet 13)
             256 -> Prelude'.fmap (\ !new'Field -> old'Self{feeding_detected = Prelude'.Just new'Field}) (P'.wireGet 8)
             272 -> Prelude'.fmap (\ !new'Field -> old'Self{search_rank = Prelude'.Just new'Field}) (P'.wireGet 13)
             280 -> Prelude'.fmap (\ !new'Field -> old'Self{search_rank_uncertainty = Prelude'.Just new'Field}) (P'.wireGet 13)
             288 -> Prelude'.fmap (\ !new'Field -> old'Self{rank_uncertainty_change = Prelude'.Just new'Field}) (P'.wireGet 5)
             296 -> Prelude'.fmap (\ !new'Field -> old'Self{hero_play_count = Prelude'.Just new'Field}) (P'.wireGet 13)
             305 -> Prelude'.fmap (\ !new'Field -> old'Self{party_id = Prelude'.Just new'Field}) (P'.wireGet 6)
             432 -> Prelude'.fmap (\ !new'Field -> old'Self{scaled_hero_damage = Prelude'.Just new'Field}) (P'.wireGet 13)
             440 -> Prelude'.fmap (\ !new'Field -> old'Self{scaled_tower_damage = Prelude'.Just new'Field}) (P'.wireGet 13)
             448 -> Prelude'.fmap (\ !new'Field -> old'Self{scaled_hero_healing = Prelude'.Just new'Field}) (P'.wireGet 13)
             317 -> Prelude'.fmap (\ !new'Field -> old'Self{scaled_kills = Prelude'.Just new'Field}) (P'.wireGet 2)
             325 -> Prelude'.fmap (\ !new'Field -> old'Self{scaled_deaths = Prelude'.Just new'Field}) (P'.wireGet 2)
             333 -> Prelude'.fmap (\ !new'Field -> old'Self{scaled_assists = Prelude'.Just new'Field}) (P'.wireGet 2)
             336 -> Prelude'.fmap (\ !new'Field -> old'Self{claimed_farm_gold = Prelude'.Just new'Field}) (P'.wireGet 13)
             344 -> Prelude'.fmap (\ !new'Field -> old'Self{support_gold = Prelude'.Just new'Field}) (P'.wireGet 13)
             352 -> Prelude'.fmap (\ !new'Field -> old'Self{claimed_denies = Prelude'.Just new'Field}) (P'.wireGet 13)
             360 -> Prelude'.fmap (\ !new'Field -> old'Self{claimed_misses = Prelude'.Just new'Field}) (P'.wireGet 13)
             368 -> Prelude'.fmap (\ !new'Field -> old'Self{misses = Prelude'.Just new'Field}) (P'.wireGet 13)
             378 -> Prelude'.fmap (\ !new'Field -> old'Self{ability_upgrades = P'.append (ability_upgrades old'Self) new'Field})
                     (P'.wireGet 11)
             386 -> Prelude'.fmap
                     (\ !new'Field ->
                       old'Self{additional_units_inventory = P'.append (additional_units_inventory old'Self) new'Field})
                     (P'.wireGet 11)
             458 -> Prelude'.fmap (\ !new'Field -> old'Self{permanent_buffs = P'.append (permanent_buffs old'Self) new'Field})
                     (P'.wireGet 11)
             402 -> Prelude'.fmap
                     (\ !new'Field ->
                       old'Self{custom_game_data = P'.mergeAppend (custom_game_data old'Self) (Prelude'.Just new'Field)})
                     (P'.wireGet 11)
             408 -> Prelude'.fmap (\ !new'Field -> old'Self{active_battle_pass = Prelude'.Just new'Field}) (P'.wireGet 8)
             416 -> Prelude'.fmap (\ !new'Field -> old'Self{net_worth = Prelude'.Just new'Field}) (P'.wireGet 13)
             464 -> Prelude'.fmap (\ !new'Field -> old'Self{bot_difficulty = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Player) Player where
  getVal m' f' = f' m'

instance P'.GPB Player

instance P'.ReflectDescriptor Player where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList
        [8, 16, 24, 32, 40, 48, 56, 64, 72, 85, 93, 96, 104, 112, 120, 128, 136, 144, 152, 160, 168, 176, 184, 192, 200, 208, 216,
         224, 234, 240, 256, 272, 280, 288, 296, 305, 317, 325, 333, 336, 344, 352, 360, 368, 378, 386, 392, 402, 408, 416, 432,
         440, 448, 458, 464])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName = MName \"Player\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaGcCommon\",\"CMsgDOTAMatch\",\"Player.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.account_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"account_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.player_slot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"player_slot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.hero_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"hero_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.item_0\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"item_0\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.item_1\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"item_1\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.item_2\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"item_2\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.item_3\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"item_3\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.item_4\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"item_4\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.item_5\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"item_5\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.expected_team_contribution\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"expected_team_contribution\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 85}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.scaled_metric\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"scaled_metric\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 93}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.previous_rank\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"previous_rank\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 96}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.rank_change\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"rank_change\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 104}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 17}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.solo_rank\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"solo_rank\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 49}, wireTag = WireTag {getWireTag = 392}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.kills\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"kills\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 112}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.deaths\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"deaths\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 120}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.assists\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"assists\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 128}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.leaver_status\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"leaver_status\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 136}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.gold\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"gold\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 144}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.last_hits\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"last_hits\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 19}, wireTag = WireTag {getWireTag = 152}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.denies\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"denies\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 20}, wireTag = WireTag {getWireTag = 160}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.gold_per_min\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"gold_per_min\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 21}, wireTag = WireTag {getWireTag = 168}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.XP_per_min\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"xP_per_min\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 22}, wireTag = WireTag {getWireTag = 176}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.gold_spent\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"gold_spent\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 23}, wireTag = WireTag {getWireTag = 184}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.hero_damage\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"hero_damage\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 24}, wireTag = WireTag {getWireTag = 192}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.tower_damage\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"tower_damage\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 25}, wireTag = WireTag {getWireTag = 200}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.hero_healing\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"hero_healing\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 26}, wireTag = WireTag {getWireTag = 208}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.level\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"level\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 27}, wireTag = WireTag {getWireTag = 216}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.time_last_seen\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"time_last_seen\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 28}, wireTag = WireTag {getWireTag = 224}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.player_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"player_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 29}, wireTag = WireTag {getWireTag = 234}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.support_ability_value\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"support_ability_value\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 30}, wireTag = WireTag {getWireTag = 240}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.feeding_detected\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"feeding_detected\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 32}, wireTag = WireTag {getWireTag = 256}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.search_rank\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"search_rank\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 34}, wireTag = WireTag {getWireTag = 272}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.search_rank_uncertainty\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"search_rank_uncertainty\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 35}, wireTag = WireTag {getWireTag = 280}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.rank_uncertainty_change\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"rank_uncertainty_change\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 36}, wireTag = WireTag {getWireTag = 288}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.hero_play_count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"hero_play_count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 37}, wireTag = WireTag {getWireTag = 296}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.party_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"party_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 38}, wireTag = WireTag {getWireTag = 305}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.scaled_hero_damage\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"scaled_hero_damage\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 54}, wireTag = WireTag {getWireTag = 432}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.scaled_tower_damage\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"scaled_tower_damage\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 55}, wireTag = WireTag {getWireTag = 440}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.scaled_hero_healing\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"scaled_hero_healing\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 56}, wireTag = WireTag {getWireTag = 448}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.scaled_kills\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"scaled_kills\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 39}, wireTag = WireTag {getWireTag = 317}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.scaled_deaths\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"scaled_deaths\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 40}, wireTag = WireTag {getWireTag = 325}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.scaled_assists\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"scaled_assists\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 41}, wireTag = WireTag {getWireTag = 333}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.claimed_farm_gold\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"claimed_farm_gold\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 42}, wireTag = WireTag {getWireTag = 336}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.support_gold\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"support_gold\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 43}, wireTag = WireTag {getWireTag = 344}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.claimed_denies\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"claimed_denies\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 44}, wireTag = WireTag {getWireTag = 352}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.claimed_misses\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"claimed_misses\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 45}, wireTag = WireTag {getWireTag = 360}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.misses\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"misses\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 46}, wireTag = WireTag {getWireTag = 368}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.ability_upgrades\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"ability_upgrades\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 47}, wireTag = WireTag {getWireTag = 378}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMatchPlayerAbilityUpgrade\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\"], baseName = MName \"CMatchPlayerAbilityUpgrade\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.additional_units_inventory\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"additional_units_inventory\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 48}, wireTag = WireTag {getWireTag = 386}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMatchAdditionalUnitInventory\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\"], baseName = MName \"CMatchAdditionalUnitInventory\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.permanent_buffs\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"permanent_buffs\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 57}, wireTag = WireTag {getWireTag = 458}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMatchPlayerPermanentBuff\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\"], baseName = MName \"CMatchPlayerPermanentBuff\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.custom_game_data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"custom_game_data\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 50}, wireTag = WireTag {getWireTag = 402}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.CustomGameData\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName = MName \"CustomGameData\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.active_battle_pass\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"active_battle_pass\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 51}, wireTag = WireTag {getWireTag = 408}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.net_worth\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"net_worth\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 52}, wireTag = WireTag {getWireTag = 416}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.bot_difficulty\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName' = FName \"bot_difficulty\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 58}, wireTag = WireTag {getWireTag = 464}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Player where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Player where
  textPut msg
   = do
       P'.tellT "account_id" (account_id msg)
       P'.tellT "player_slot" (player_slot msg)
       P'.tellT "hero_id" (hero_id msg)
       P'.tellT "item_0" (item_0 msg)
       P'.tellT "item_1" (item_1 msg)
       P'.tellT "item_2" (item_2 msg)
       P'.tellT "item_3" (item_3 msg)
       P'.tellT "item_4" (item_4 msg)
       P'.tellT "item_5" (item_5 msg)
       P'.tellT "expected_team_contribution" (expected_team_contribution msg)
       P'.tellT "scaled_metric" (scaled_metric msg)
       P'.tellT "previous_rank" (previous_rank msg)
       P'.tellT "rank_change" (rank_change msg)
       P'.tellT "solo_rank" (solo_rank msg)
       P'.tellT "kills" (kills msg)
       P'.tellT "deaths" (deaths msg)
       P'.tellT "assists" (assists msg)
       P'.tellT "leaver_status" (leaver_status msg)
       P'.tellT "gold" (gold msg)
       P'.tellT "last_hits" (last_hits msg)
       P'.tellT "denies" (denies msg)
       P'.tellT "gold_per_min" (gold_per_min msg)
       P'.tellT "XP_per_min" (xP_per_min msg)
       P'.tellT "gold_spent" (gold_spent msg)
       P'.tellT "hero_damage" (hero_damage msg)
       P'.tellT "tower_damage" (tower_damage msg)
       P'.tellT "hero_healing" (hero_healing msg)
       P'.tellT "level" (level msg)
       P'.tellT "time_last_seen" (time_last_seen msg)
       P'.tellT "player_name" (player_name msg)
       P'.tellT "support_ability_value" (support_ability_value msg)
       P'.tellT "feeding_detected" (feeding_detected msg)
       P'.tellT "search_rank" (search_rank msg)
       P'.tellT "search_rank_uncertainty" (search_rank_uncertainty msg)
       P'.tellT "rank_uncertainty_change" (rank_uncertainty_change msg)
       P'.tellT "hero_play_count" (hero_play_count msg)
       P'.tellT "party_id" (party_id msg)
       P'.tellT "scaled_hero_damage" (scaled_hero_damage msg)
       P'.tellT "scaled_tower_damage" (scaled_tower_damage msg)
       P'.tellT "scaled_hero_healing" (scaled_hero_healing msg)
       P'.tellT "scaled_kills" (scaled_kills msg)
       P'.tellT "scaled_deaths" (scaled_deaths msg)
       P'.tellT "scaled_assists" (scaled_assists msg)
       P'.tellT "claimed_farm_gold" (claimed_farm_gold msg)
       P'.tellT "support_gold" (support_gold msg)
       P'.tellT "claimed_denies" (claimed_denies msg)
       P'.tellT "claimed_misses" (claimed_misses msg)
       P'.tellT "misses" (misses msg)
       P'.tellT "ability_upgrades" (ability_upgrades msg)
       P'.tellT "additional_units_inventory" (additional_units_inventory msg)
       P'.tellT "permanent_buffs" (permanent_buffs msg)
       P'.tellT "custom_game_data" (custom_game_data msg)
       P'.tellT "active_battle_pass" (active_battle_pass msg)
       P'.tellT "net_worth" (net_worth msg)
       P'.tellT "bot_difficulty" (bot_difficulty msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'account_id, parse'player_slot, parse'hero_id, parse'item_0, parse'item_1, parse'item_2, parse'item_3,
                   parse'item_4, parse'item_5, parse'expected_team_contribution, parse'scaled_metric, parse'previous_rank,
                   parse'rank_change, parse'solo_rank, parse'kills, parse'deaths, parse'assists, parse'leaver_status, parse'gold,
                   parse'last_hits, parse'denies, parse'gold_per_min, parse'xP_per_min, parse'gold_spent, parse'hero_damage,
                   parse'tower_damage, parse'hero_healing, parse'level, parse'time_last_seen, parse'player_name,
                   parse'support_ability_value, parse'feeding_detected, parse'search_rank, parse'search_rank_uncertainty,
                   parse'rank_uncertainty_change, parse'hero_play_count, parse'party_id, parse'scaled_hero_damage,
                   parse'scaled_tower_damage, parse'scaled_hero_healing, parse'scaled_kills, parse'scaled_deaths,
                   parse'scaled_assists, parse'claimed_farm_gold, parse'support_gold, parse'claimed_denies, parse'claimed_misses,
                   parse'misses, parse'ability_upgrades, parse'additional_units_inventory, parse'permanent_buffs,
                   parse'custom_game_data, parse'active_battle_pass, parse'net_worth, parse'bot_difficulty])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'account_id
         = P'.try
            (do
               v <- P'.getT "account_id"
               Prelude'.return (\ o -> o{account_id = v}))
        parse'player_slot
         = P'.try
            (do
               v <- P'.getT "player_slot"
               Prelude'.return (\ o -> o{player_slot = v}))
        parse'hero_id
         = P'.try
            (do
               v <- P'.getT "hero_id"
               Prelude'.return (\ o -> o{hero_id = v}))
        parse'item_0
         = P'.try
            (do
               v <- P'.getT "item_0"
               Prelude'.return (\ o -> o{item_0 = v}))
        parse'item_1
         = P'.try
            (do
               v <- P'.getT "item_1"
               Prelude'.return (\ o -> o{item_1 = v}))
        parse'item_2
         = P'.try
            (do
               v <- P'.getT "item_2"
               Prelude'.return (\ o -> o{item_2 = v}))
        parse'item_3
         = P'.try
            (do
               v <- P'.getT "item_3"
               Prelude'.return (\ o -> o{item_3 = v}))
        parse'item_4
         = P'.try
            (do
               v <- P'.getT "item_4"
               Prelude'.return (\ o -> o{item_4 = v}))
        parse'item_5
         = P'.try
            (do
               v <- P'.getT "item_5"
               Prelude'.return (\ o -> o{item_5 = v}))
        parse'expected_team_contribution
         = P'.try
            (do
               v <- P'.getT "expected_team_contribution"
               Prelude'.return (\ o -> o{expected_team_contribution = v}))
        parse'scaled_metric
         = P'.try
            (do
               v <- P'.getT "scaled_metric"
               Prelude'.return (\ o -> o{scaled_metric = v}))
        parse'previous_rank
         = P'.try
            (do
               v <- P'.getT "previous_rank"
               Prelude'.return (\ o -> o{previous_rank = v}))
        parse'rank_change
         = P'.try
            (do
               v <- P'.getT "rank_change"
               Prelude'.return (\ o -> o{rank_change = v}))
        parse'solo_rank
         = P'.try
            (do
               v <- P'.getT "solo_rank"
               Prelude'.return (\ o -> o{solo_rank = v}))
        parse'kills
         = P'.try
            (do
               v <- P'.getT "kills"
               Prelude'.return (\ o -> o{kills = v}))
        parse'deaths
         = P'.try
            (do
               v <- P'.getT "deaths"
               Prelude'.return (\ o -> o{deaths = v}))
        parse'assists
         = P'.try
            (do
               v <- P'.getT "assists"
               Prelude'.return (\ o -> o{assists = v}))
        parse'leaver_status
         = P'.try
            (do
               v <- P'.getT "leaver_status"
               Prelude'.return (\ o -> o{leaver_status = v}))
        parse'gold
         = P'.try
            (do
               v <- P'.getT "gold"
               Prelude'.return (\ o -> o{gold = v}))
        parse'last_hits
         = P'.try
            (do
               v <- P'.getT "last_hits"
               Prelude'.return (\ o -> o{last_hits = v}))
        parse'denies
         = P'.try
            (do
               v <- P'.getT "denies"
               Prelude'.return (\ o -> o{denies = v}))
        parse'gold_per_min
         = P'.try
            (do
               v <- P'.getT "gold_per_min"
               Prelude'.return (\ o -> o{gold_per_min = v}))
        parse'xP_per_min
         = P'.try
            (do
               v <- P'.getT "XP_per_min"
               Prelude'.return (\ o -> o{xP_per_min = v}))
        parse'gold_spent
         = P'.try
            (do
               v <- P'.getT "gold_spent"
               Prelude'.return (\ o -> o{gold_spent = v}))
        parse'hero_damage
         = P'.try
            (do
               v <- P'.getT "hero_damage"
               Prelude'.return (\ o -> o{hero_damage = v}))
        parse'tower_damage
         = P'.try
            (do
               v <- P'.getT "tower_damage"
               Prelude'.return (\ o -> o{tower_damage = v}))
        parse'hero_healing
         = P'.try
            (do
               v <- P'.getT "hero_healing"
               Prelude'.return (\ o -> o{hero_healing = v}))
        parse'level
         = P'.try
            (do
               v <- P'.getT "level"
               Prelude'.return (\ o -> o{level = v}))
        parse'time_last_seen
         = P'.try
            (do
               v <- P'.getT "time_last_seen"
               Prelude'.return (\ o -> o{time_last_seen = v}))
        parse'player_name
         = P'.try
            (do
               v <- P'.getT "player_name"
               Prelude'.return (\ o -> o{player_name = v}))
        parse'support_ability_value
         = P'.try
            (do
               v <- P'.getT "support_ability_value"
               Prelude'.return (\ o -> o{support_ability_value = v}))
        parse'feeding_detected
         = P'.try
            (do
               v <- P'.getT "feeding_detected"
               Prelude'.return (\ o -> o{feeding_detected = v}))
        parse'search_rank
         = P'.try
            (do
               v <- P'.getT "search_rank"
               Prelude'.return (\ o -> o{search_rank = v}))
        parse'search_rank_uncertainty
         = P'.try
            (do
               v <- P'.getT "search_rank_uncertainty"
               Prelude'.return (\ o -> o{search_rank_uncertainty = v}))
        parse'rank_uncertainty_change
         = P'.try
            (do
               v <- P'.getT "rank_uncertainty_change"
               Prelude'.return (\ o -> o{rank_uncertainty_change = v}))
        parse'hero_play_count
         = P'.try
            (do
               v <- P'.getT "hero_play_count"
               Prelude'.return (\ o -> o{hero_play_count = v}))
        parse'party_id
         = P'.try
            (do
               v <- P'.getT "party_id"
               Prelude'.return (\ o -> o{party_id = v}))
        parse'scaled_hero_damage
         = P'.try
            (do
               v <- P'.getT "scaled_hero_damage"
               Prelude'.return (\ o -> o{scaled_hero_damage = v}))
        parse'scaled_tower_damage
         = P'.try
            (do
               v <- P'.getT "scaled_tower_damage"
               Prelude'.return (\ o -> o{scaled_tower_damage = v}))
        parse'scaled_hero_healing
         = P'.try
            (do
               v <- P'.getT "scaled_hero_healing"
               Prelude'.return (\ o -> o{scaled_hero_healing = v}))
        parse'scaled_kills
         = P'.try
            (do
               v <- P'.getT "scaled_kills"
               Prelude'.return (\ o -> o{scaled_kills = v}))
        parse'scaled_deaths
         = P'.try
            (do
               v <- P'.getT "scaled_deaths"
               Prelude'.return (\ o -> o{scaled_deaths = v}))
        parse'scaled_assists
         = P'.try
            (do
               v <- P'.getT "scaled_assists"
               Prelude'.return (\ o -> o{scaled_assists = v}))
        parse'claimed_farm_gold
         = P'.try
            (do
               v <- P'.getT "claimed_farm_gold"
               Prelude'.return (\ o -> o{claimed_farm_gold = v}))
        parse'support_gold
         = P'.try
            (do
               v <- P'.getT "support_gold"
               Prelude'.return (\ o -> o{support_gold = v}))
        parse'claimed_denies
         = P'.try
            (do
               v <- P'.getT "claimed_denies"
               Prelude'.return (\ o -> o{claimed_denies = v}))
        parse'claimed_misses
         = P'.try
            (do
               v <- P'.getT "claimed_misses"
               Prelude'.return (\ o -> o{claimed_misses = v}))
        parse'misses
         = P'.try
            (do
               v <- P'.getT "misses"
               Prelude'.return (\ o -> o{misses = v}))
        parse'ability_upgrades
         = P'.try
            (do
               v <- P'.getT "ability_upgrades"
               Prelude'.return (\ o -> o{ability_upgrades = P'.append (ability_upgrades o) v}))
        parse'additional_units_inventory
         = P'.try
            (do
               v <- P'.getT "additional_units_inventory"
               Prelude'.return (\ o -> o{additional_units_inventory = P'.append (additional_units_inventory o) v}))
        parse'permanent_buffs
         = P'.try
            (do
               v <- P'.getT "permanent_buffs"
               Prelude'.return (\ o -> o{permanent_buffs = P'.append (permanent_buffs o) v}))
        parse'custom_game_data
         = P'.try
            (do
               v <- P'.getT "custom_game_data"
               Prelude'.return (\ o -> o{custom_game_data = v}))
        parse'active_battle_pass
         = P'.try
            (do
               v <- P'.getT "active_battle_pass"
               Prelude'.return (\ o -> o{active_battle_pass = v}))
        parse'net_worth
         = P'.try
            (do
               v <- P'.getT "net_worth"
               Prelude'.return (\ o -> o{net_worth = v}))
        parse'bot_difficulty
         = P'.try
            (do
               v <- P'.getT "bot_difficulty"
               Prelude'.return (\ o -> o{bot_difficulty = v}))