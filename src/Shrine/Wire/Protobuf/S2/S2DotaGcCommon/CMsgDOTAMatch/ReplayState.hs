{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.ReplayState (ReplayState(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data ReplayState = REPLAY_AVAILABLE
                 | REPLAY_NOT_RECORDED
                 | REPLAY_EXPIRED
                 deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                           Prelude'.Generic)

instance P'.Mergeable ReplayState

instance Prelude'.Bounded ReplayState where
  minBound = REPLAY_AVAILABLE
  maxBound = REPLAY_EXPIRED

instance P'.Default ReplayState where
  defaultValue = REPLAY_AVAILABLE

toMaybe'Enum :: Prelude'.Int -> P'.Maybe ReplayState
toMaybe'Enum 0 = Prelude'.Just REPLAY_AVAILABLE
toMaybe'Enum 1 = Prelude'.Just REPLAY_NOT_RECORDED
toMaybe'Enum 2 = Prelude'.Just REPLAY_EXPIRED
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum ReplayState where
  fromEnum REPLAY_AVAILABLE = 0
  fromEnum REPLAY_NOT_RECORDED = 1
  fromEnum REPLAY_EXPIRED = 2
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.ReplayState")
      . toMaybe'Enum
  succ REPLAY_AVAILABLE = REPLAY_NOT_RECORDED
  succ REPLAY_NOT_RECORDED = REPLAY_EXPIRED
  succ _
   = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.ReplayState"
  pred REPLAY_NOT_RECORDED = REPLAY_AVAILABLE
  pred REPLAY_EXPIRED = REPLAY_NOT_RECORDED
  pred _
   = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.ReplayState"

instance P'.Wire ReplayState where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB ReplayState

instance P'.MessageAPI msg' (msg' -> ReplayState) ReplayState where
  getVal m' f' = f' m'

instance P'.ReflectEnum ReplayState where
  reflectEnum
   = [(0, "REPLAY_AVAILABLE", REPLAY_AVAILABLE), (1, "REPLAY_NOT_RECORDED", REPLAY_NOT_RECORDED),
      (2, "REPLAY_EXPIRED", REPLAY_EXPIRED)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.ReplayState") ["Shrine", "Wire", "Protobuf", "S2"]
        ["S2DotaGcCommon", "CMsgDOTAMatch"]
        "ReplayState")
      ["Shrine", "Wire", "Protobuf", "S2", "S2DotaGcCommon", "CMsgDOTAMatch", "ReplayState.hs"]
      [(0, "REPLAY_AVAILABLE"), (1, "REPLAY_NOT_RECORDED"), (2, "REPLAY_EXPIRED")]

instance P'.TextType ReplayState where
  tellT = P'.tellShow
  getT = P'.getRead