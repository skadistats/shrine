{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.BroadcasterChannel (BroadcasterChannel(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.BroadcasterInfo as S2DotaGcCommon.CMsgDOTAMatch
       (BroadcasterInfo)

data BroadcasterChannel = BroadcasterChannel{country_code :: !(P'.Maybe P'.Utf8), description :: !(P'.Maybe P'.Utf8),
                                             broadcaster_infos :: !(P'.Seq S2DotaGcCommon.CMsgDOTAMatch.BroadcasterInfo),
                                             language_code :: !(P'.Maybe P'.Utf8)}
                        deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable BroadcasterChannel where
  mergeAppend (BroadcasterChannel x'1 x'2 x'3 x'4) (BroadcasterChannel y'1 y'2 y'3 y'4)
   = BroadcasterChannel (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)

instance P'.Default BroadcasterChannel where
  defaultValue = BroadcasterChannel P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire BroadcasterChannel where
  wireSize ft' self'@(BroadcasterChannel x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 9 x'1 + P'.wireSizeOpt 1 9 x'2 + P'.wireSizeRep 1 11 x'3 + P'.wireSizeOpt 1 9 x'4)
  wirePut ft' self'@(BroadcasterChannel x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
             P'.wirePutOpt 18 9 x'2
             P'.wirePutRep 26 11 x'3
             P'.wirePutOpt 34 9 x'4
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{country_code = Prelude'.Just new'Field}) (P'.wireGet 9)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{description = Prelude'.Just new'Field}) (P'.wireGet 9)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{broadcaster_infos = P'.append (broadcaster_infos old'Self) new'Field})
                    (P'.wireGet 11)
             34 -> Prelude'.fmap (\ !new'Field -> old'Self{language_code = Prelude'.Just new'Field}) (P'.wireGet 9)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> BroadcasterChannel) BroadcasterChannel where
  getVal m' f' = f' m'

instance P'.GPB BroadcasterChannel

instance P'.ReflectDescriptor BroadcasterChannel where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18, 26, 34])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.BroadcasterChannel\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName = MName \"BroadcasterChannel\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaGcCommon\",\"CMsgDOTAMatch\",\"BroadcasterChannel.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.BroadcasterChannel.country_code\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"BroadcasterChannel\"], baseName' = FName \"country_code\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.BroadcasterChannel.description\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"BroadcasterChannel\"], baseName' = FName \"description\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.BroadcasterChannel.broadcaster_infos\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"BroadcasterChannel\"], baseName' = FName \"broadcaster_infos\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.BroadcasterInfo\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName = MName \"BroadcasterInfo\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.BroadcasterChannel.language_code\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"BroadcasterChannel\"], baseName' = FName \"language_code\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 34}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType BroadcasterChannel where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg BroadcasterChannel where
  textPut msg
   = do
       P'.tellT "country_code" (country_code msg)
       P'.tellT "description" (description msg)
       P'.tellT "broadcaster_infos" (broadcaster_infos msg)
       P'.tellT "language_code" (language_code msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'country_code, parse'description, parse'broadcaster_infos, parse'language_code])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'country_code
         = P'.try
            (do
               v <- P'.getT "country_code"
               Prelude'.return (\ o -> o{country_code = v}))
        parse'description
         = P'.try
            (do
               v <- P'.getT "description"
               Prelude'.return (\ o -> o{description = v}))
        parse'broadcaster_infos
         = P'.try
            (do
               v <- P'.getT "broadcaster_infos"
               Prelude'.return (\ o -> o{broadcaster_infos = P'.append (broadcaster_infos o) v}))
        parse'language_code
         = P'.try
            (do
               v <- P'.getT "language_code"
               Prelude'.return (\ o -> o{language_code = v}))