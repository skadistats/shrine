{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.Player.CustomGameData (CustomGameData(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CustomGameData = CustomGameData{dota_team :: !(P'.Maybe P'.Word32), winner :: !(P'.Maybe P'.Bool)}
                    deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CustomGameData where
  mergeAppend (CustomGameData x'1 x'2) (CustomGameData y'1 y'2) = CustomGameData (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CustomGameData where
  defaultValue = CustomGameData P'.defaultValue P'.defaultValue

instance P'.Wire CustomGameData where
  wireSize ft' self'@(CustomGameData x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 8 x'2)
  wirePut ft' self'@(CustomGameData x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 8 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{dota_team = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{winner = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CustomGameData) CustomGameData where
  getVal m' f' = f' m'

instance P'.GPB CustomGameData

instance P'.ReflectDescriptor CustomGameData where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.CustomGameData\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\"], baseName = MName \"CustomGameData\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaGcCommon\",\"CMsgDOTAMatch\",\"Player\",\"CustomGameData.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.CustomGameData.dota_team\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\",MName \"CustomGameData\"], baseName' = FName \"dota_team\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player.CustomGameData.winner\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\",MName \"Player\",MName \"CustomGameData\"], baseName' = FName \"winner\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CustomGameData where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CustomGameData where
  textPut msg
   = do
       P'.tellT "dota_team" (dota_team msg)
       P'.tellT "winner" (winner msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'dota_team, parse'winner]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'dota_team
         = P'.try
            (do
               v <- P'.getT "dota_team"
               Prelude'.return (\ o -> o{dota_team = v}))
        parse'winner
         = P'.try
            (do
               v <- P'.getT "winner"
               Prelude'.return (\ o -> o{winner = v}))