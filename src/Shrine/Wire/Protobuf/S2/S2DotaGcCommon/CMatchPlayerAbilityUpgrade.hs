{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMatchPlayerAbilityUpgrade (CMatchPlayerAbilityUpgrade(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CMatchPlayerAbilityUpgrade = CMatchPlayerAbilityUpgrade{ability :: !(P'.Maybe P'.Word32), time :: !(P'.Maybe P'.Word32)}
                                deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                          Prelude'.Generic)

instance P'.Mergeable CMatchPlayerAbilityUpgrade where
  mergeAppend (CMatchPlayerAbilityUpgrade x'1 x'2) (CMatchPlayerAbilityUpgrade y'1 y'2)
   = CMatchPlayerAbilityUpgrade (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMatchPlayerAbilityUpgrade where
  defaultValue = CMatchPlayerAbilityUpgrade P'.defaultValue P'.defaultValue

instance P'.Wire CMatchPlayerAbilityUpgrade where
  wireSize ft' self'@(CMatchPlayerAbilityUpgrade x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 13 x'2)
  wirePut ft' self'@(CMatchPlayerAbilityUpgrade x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 13 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{ability = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{time = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMatchPlayerAbilityUpgrade) CMatchPlayerAbilityUpgrade where
  getVal m' f' = f' m'

instance P'.GPB CMatchPlayerAbilityUpgrade

instance P'.ReflectDescriptor CMatchPlayerAbilityUpgrade where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMatchPlayerAbilityUpgrade\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\"], baseName = MName \"CMatchPlayerAbilityUpgrade\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaGcCommon\",\"CMatchPlayerAbilityUpgrade.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMatchPlayerAbilityUpgrade.ability\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMatchPlayerAbilityUpgrade\"], baseName' = FName \"ability\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMatchPlayerAbilityUpgrade.time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMatchPlayerAbilityUpgrade\"], baseName' = FName \"time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMatchPlayerAbilityUpgrade where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMatchPlayerAbilityUpgrade where
  textPut msg
   = do
       P'.tellT "ability" (ability msg)
       P'.tellT "time" (time msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'ability, parse'time]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'ability
         = P'.try
            (do
               v <- P'.getT "ability"
               Prelude'.return (\ o -> o{ability = v}))
        parse'time
         = P'.try
            (do
               v <- P'.getT "time"
               Prelude'.return (\ o -> o{time = v}))