{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaGcCommon.EMatchOutcome (EMatchOutcome(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EMatchOutcome = K_EMatchOutcome_Unknown
                   | K_EMatchOutcome_RadVictory
                   | K_EMatchOutcome_DireVictory
                   | K_EMatchOutcome_NotScored_PoorNetworkConditions
                   | K_EMatchOutcome_NotScored_Leaver
                   | K_EMatchOutcome_NotScored_ServerCrash
                   | K_EMatchOutcome_NotScored_NeverStarted
                   | K_EMatchOutcome_NotScored_Canceled
                   deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                             Prelude'.Generic)

instance P'.Mergeable EMatchOutcome

instance Prelude'.Bounded EMatchOutcome where
  minBound = K_EMatchOutcome_Unknown
  maxBound = K_EMatchOutcome_NotScored_Canceled

instance P'.Default EMatchOutcome where
  defaultValue = K_EMatchOutcome_Unknown

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EMatchOutcome
toMaybe'Enum 0 = Prelude'.Just K_EMatchOutcome_Unknown
toMaybe'Enum 2 = Prelude'.Just K_EMatchOutcome_RadVictory
toMaybe'Enum 3 = Prelude'.Just K_EMatchOutcome_DireVictory
toMaybe'Enum 64 = Prelude'.Just K_EMatchOutcome_NotScored_PoorNetworkConditions
toMaybe'Enum 65 = Prelude'.Just K_EMatchOutcome_NotScored_Leaver
toMaybe'Enum 66 = Prelude'.Just K_EMatchOutcome_NotScored_ServerCrash
toMaybe'Enum 67 = Prelude'.Just K_EMatchOutcome_NotScored_NeverStarted
toMaybe'Enum 68 = Prelude'.Just K_EMatchOutcome_NotScored_Canceled
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EMatchOutcome where
  fromEnum K_EMatchOutcome_Unknown = 0
  fromEnum K_EMatchOutcome_RadVictory = 2
  fromEnum K_EMatchOutcome_DireVictory = 3
  fromEnum K_EMatchOutcome_NotScored_PoorNetworkConditions = 64
  fromEnum K_EMatchOutcome_NotScored_Leaver = 65
  fromEnum K_EMatchOutcome_NotScored_ServerCrash = 66
  fromEnum K_EMatchOutcome_NotScored_NeverStarted = 67
  fromEnum K_EMatchOutcome_NotScored_Canceled = 68
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2DotaGcCommon.EMatchOutcome")
      . toMaybe'Enum
  succ K_EMatchOutcome_Unknown = K_EMatchOutcome_RadVictory
  succ K_EMatchOutcome_RadVictory = K_EMatchOutcome_DireVictory
  succ K_EMatchOutcome_DireVictory = K_EMatchOutcome_NotScored_PoorNetworkConditions
  succ K_EMatchOutcome_NotScored_PoorNetworkConditions = K_EMatchOutcome_NotScored_Leaver
  succ K_EMatchOutcome_NotScored_Leaver = K_EMatchOutcome_NotScored_ServerCrash
  succ K_EMatchOutcome_NotScored_ServerCrash = K_EMatchOutcome_NotScored_NeverStarted
  succ K_EMatchOutcome_NotScored_NeverStarted = K_EMatchOutcome_NotScored_Canceled
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2DotaGcCommon.EMatchOutcome"
  pred K_EMatchOutcome_RadVictory = K_EMatchOutcome_Unknown
  pred K_EMatchOutcome_DireVictory = K_EMatchOutcome_RadVictory
  pred K_EMatchOutcome_NotScored_PoorNetworkConditions = K_EMatchOutcome_DireVictory
  pred K_EMatchOutcome_NotScored_Leaver = K_EMatchOutcome_NotScored_PoorNetworkConditions
  pred K_EMatchOutcome_NotScored_ServerCrash = K_EMatchOutcome_NotScored_Leaver
  pred K_EMatchOutcome_NotScored_NeverStarted = K_EMatchOutcome_NotScored_ServerCrash
  pred K_EMatchOutcome_NotScored_Canceled = K_EMatchOutcome_NotScored_NeverStarted
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2DotaGcCommon.EMatchOutcome"

instance P'.Wire EMatchOutcome where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EMatchOutcome

instance P'.MessageAPI msg' (msg' -> EMatchOutcome) EMatchOutcome where
  getVal m' f' = f' m'

instance P'.ReflectEnum EMatchOutcome where
  reflectEnum
   = [(0, "K_EMatchOutcome_Unknown", K_EMatchOutcome_Unknown), (2, "K_EMatchOutcome_RadVictory", K_EMatchOutcome_RadVictory),
      (3, "K_EMatchOutcome_DireVictory", K_EMatchOutcome_DireVictory),
      (64, "K_EMatchOutcome_NotScored_PoorNetworkConditions", K_EMatchOutcome_NotScored_PoorNetworkConditions),
      (65, "K_EMatchOutcome_NotScored_Leaver", K_EMatchOutcome_NotScored_Leaver),
      (66, "K_EMatchOutcome_NotScored_ServerCrash", K_EMatchOutcome_NotScored_ServerCrash),
      (67, "K_EMatchOutcome_NotScored_NeverStarted", K_EMatchOutcome_NotScored_NeverStarted),
      (68, "K_EMatchOutcome_NotScored_Canceled", K_EMatchOutcome_NotScored_Canceled)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Dota_Gcmessages_Common.EMatchOutcome") ["Shrine", "Wire", "Protobuf", "S2"] ["S2DotaGcCommon"]
        "EMatchOutcome")
      ["Shrine", "Wire", "Protobuf", "S2", "S2DotaGcCommon", "EMatchOutcome.hs"]
      [(0, "K_EMatchOutcome_Unknown"), (2, "K_EMatchOutcome_RadVictory"), (3, "K_EMatchOutcome_DireVictory"),
       (64, "K_EMatchOutcome_NotScored_PoorNetworkConditions"), (65, "K_EMatchOutcome_NotScored_Leaver"),
       (66, "K_EMatchOutcome_NotScored_ServerCrash"), (67, "K_EMatchOutcome_NotScored_NeverStarted"),
       (68, "K_EMatchOutcome_NotScored_Canceled")]

instance P'.TextType EMatchOutcome where
  tellT = P'.tellShow
  getT = P'.getRead