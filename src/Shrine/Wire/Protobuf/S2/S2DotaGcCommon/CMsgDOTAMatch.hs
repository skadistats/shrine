{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch (CMsgDOTAMatch(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMatchHeroSelectEvent as S2DotaGcCommon (CMatchHeroSelectEvent)
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.BroadcasterChannel as S2DotaGcCommon.CMsgDOTAMatch
       (BroadcasterChannel)
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.CustomGameData as S2DotaGcCommon.CMsgDOTAMatch
       (CustomGameData)
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.Player as S2DotaGcCommon.CMsgDOTAMatch (Player)
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMsgDOTAMatch.ReplayState as S2DotaGcCommon.CMsgDOTAMatch (ReplayState)
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.DOTA_GameMode as S2DotaGcCommon (DOTA_GameMode)
import qualified Shrine.Wire.Protobuf.S2.S2DotaGcCommon.EMatchOutcome as S2DotaGcCommon (EMatchOutcome)

data CMsgDOTAMatch = CMsgDOTAMatch{good_guys_win :: !(P'.Maybe P'.Bool), duration :: !(P'.Maybe P'.Word32),
                                   startTime :: !(P'.Maybe P'.Word32), players :: !(P'.Seq S2DotaGcCommon.CMsgDOTAMatch.Player),
                                   match_id :: !(P'.Maybe P'.Word64), tower_status :: !(P'.Seq P'.Word32),
                                   barracks_status :: !(P'.Seq P'.Word32), cluster :: !(P'.Maybe P'.Word32),
                                   first_blood_time :: !(P'.Maybe P'.Word32), replay_salt :: !(P'.Maybe P'.Word32),
                                   server_ip :: !(P'.Maybe P'.Word32), server_port :: !(P'.Maybe P'.Word32),
                                   lobby_type :: !(P'.Maybe P'.Word32), human_players :: !(P'.Maybe P'.Word32),
                                   average_skill :: !(P'.Maybe P'.Word32), game_balance :: !(P'.Maybe P'.Float),
                                   radiant_team_id :: !(P'.Maybe P'.Word32), dire_team_id :: !(P'.Maybe P'.Word32),
                                   leagueid :: !(P'.Maybe P'.Word32), radiant_team_name :: !(P'.Maybe P'.Utf8),
                                   dire_team_name :: !(P'.Maybe P'.Utf8), radiant_team_logo :: !(P'.Maybe P'.Word64),
                                   dire_team_logo :: !(P'.Maybe P'.Word64), radiant_team_complete :: !(P'.Maybe P'.Word32),
                                   dire_team_complete :: !(P'.Maybe P'.Word32), positive_votes :: !(P'.Maybe P'.Word32),
                                   negative_votes :: !(P'.Maybe P'.Word32), game_mode :: !(P'.Maybe S2DotaGcCommon.DOTA_GameMode),
                                   picks_bans :: !(P'.Seq S2DotaGcCommon.CMatchHeroSelectEvent),
                                   match_seq_num :: !(P'.Maybe P'.Word64),
                                   replay_state :: !(P'.Maybe S2DotaGcCommon.CMsgDOTAMatch.ReplayState),
                                   radiant_guild_id :: !(P'.Maybe P'.Word32), dire_guild_id :: !(P'.Maybe P'.Word32),
                                   radiant_team_tag :: !(P'.Maybe P'.Utf8), dire_team_tag :: !(P'.Maybe P'.Utf8),
                                   series_id :: !(P'.Maybe P'.Word32), series_type :: !(P'.Maybe P'.Word32),
                                   broadcaster_channels :: !(P'.Seq S2DotaGcCommon.CMsgDOTAMatch.BroadcasterChannel),
                                   engine :: !(P'.Maybe P'.Word32),
                                   custom_game_data :: !(P'.Maybe S2DotaGcCommon.CMsgDOTAMatch.CustomGameData),
                                   match_flags :: !(P'.Maybe P'.Word32), private_metadata_key :: !(P'.Maybe P'.Word32),
                                   radiant_team_score :: !(P'.Maybe P'.Word32), dire_team_score :: !(P'.Maybe P'.Word32),
                                   match_outcome :: !(P'.Maybe S2DotaGcCommon.EMatchOutcome),
                                   tournament_id :: !(P'.Maybe P'.Word32), tournament_round :: !(P'.Maybe P'.Word32),
                                   pre_game_duration :: !(P'.Maybe P'.Word32)}
                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgDOTAMatch where
  mergeAppend
   (CMsgDOTAMatch x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23 x'24
     x'25 x'26 x'27 x'28 x'29 x'30 x'31 x'32 x'33 x'34 x'35 x'36 x'37 x'38 x'39 x'40 x'41 x'42 x'43 x'44 x'45 x'46 x'47 x'48)
   (CMsgDOTAMatch y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18 y'19 y'20 y'21 y'22 y'23 y'24
     y'25 y'26 y'27 y'28 y'29 y'30 y'31 y'32 y'33 y'34 y'35 y'36 y'37 y'38 y'39 y'40 y'41 y'42 y'43 y'44 y'45 y'46 y'47 y'48)
   = CMsgDOTAMatch (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)
      (P'.mergeAppend x'19 y'19)
      (P'.mergeAppend x'20 y'20)
      (P'.mergeAppend x'21 y'21)
      (P'.mergeAppend x'22 y'22)
      (P'.mergeAppend x'23 y'23)
      (P'.mergeAppend x'24 y'24)
      (P'.mergeAppend x'25 y'25)
      (P'.mergeAppend x'26 y'26)
      (P'.mergeAppend x'27 y'27)
      (P'.mergeAppend x'28 y'28)
      (P'.mergeAppend x'29 y'29)
      (P'.mergeAppend x'30 y'30)
      (P'.mergeAppend x'31 y'31)
      (P'.mergeAppend x'32 y'32)
      (P'.mergeAppend x'33 y'33)
      (P'.mergeAppend x'34 y'34)
      (P'.mergeAppend x'35 y'35)
      (P'.mergeAppend x'36 y'36)
      (P'.mergeAppend x'37 y'37)
      (P'.mergeAppend x'38 y'38)
      (P'.mergeAppend x'39 y'39)
      (P'.mergeAppend x'40 y'40)
      (P'.mergeAppend x'41 y'41)
      (P'.mergeAppend x'42 y'42)
      (P'.mergeAppend x'43 y'43)
      (P'.mergeAppend x'44 y'44)
      (P'.mergeAppend x'45 y'45)
      (P'.mergeAppend x'46 y'46)
      (P'.mergeAppend x'47 y'47)
      (P'.mergeAppend x'48 y'48)

instance P'.Default CMsgDOTAMatch where
  defaultValue
   = CMsgDOTAMatch P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      (Prelude'.Just (Prelude'.read "DOTA_GAMEMODE_NONE"))
      P'.defaultValue
      P'.defaultValue
      (Prelude'.Just (Prelude'.read "REPLAY_AVAILABLE"))
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      (Prelude'.Just (Prelude'.read "K_EMatchOutcome_Unknown"))
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CMsgDOTAMatch where
  wireSize ft'
   self'@(CMsgDOTAMatch x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23
           x'24 x'25 x'26 x'27 x'28 x'29 x'30 x'31 x'32 x'33 x'34 x'35 x'36 x'37 x'38 x'39 x'40 x'41 x'42 x'43 x'44 x'45 x'46 x'47
           x'48)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 8 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 7 x'3 + P'.wireSizeRep 1 11 x'4 +
             P'.wireSizeOpt 1 4 x'5
             + P'.wireSizeRep 1 13 x'6
             + P'.wireSizeRep 1 13 x'7
             + P'.wireSizeOpt 1 13 x'8
             + P'.wireSizeOpt 1 13 x'9
             + P'.wireSizeOpt 1 7 x'10
             + P'.wireSizeOpt 1 7 x'11
             + P'.wireSizeOpt 1 13 x'12
             + P'.wireSizeOpt 2 13 x'13
             + P'.wireSizeOpt 2 13 x'14
             + P'.wireSizeOpt 2 13 x'15
             + P'.wireSizeOpt 2 2 x'16
             + P'.wireSizeOpt 2 13 x'17
             + P'.wireSizeOpt 2 13 x'18
             + P'.wireSizeOpt 2 13 x'19
             + P'.wireSizeOpt 2 9 x'20
             + P'.wireSizeOpt 2 9 x'21
             + P'.wireSizeOpt 2 4 x'22
             + P'.wireSizeOpt 2 4 x'23
             + P'.wireSizeOpt 2 13 x'24
             + P'.wireSizeOpt 2 13 x'25
             + P'.wireSizeOpt 2 13 x'26
             + P'.wireSizeOpt 2 13 x'27
             + P'.wireSizeOpt 2 14 x'28
             + P'.wireSizeRep 2 11 x'29
             + P'.wireSizeOpt 2 4 x'30
             + P'.wireSizeOpt 2 14 x'31
             + P'.wireSizeOpt 2 13 x'32
             + P'.wireSizeOpt 2 13 x'33
             + P'.wireSizeOpt 2 9 x'34
             + P'.wireSizeOpt 2 9 x'35
             + P'.wireSizeOpt 2 13 x'36
             + P'.wireSizeOpt 2 13 x'37
             + P'.wireSizeRep 2 11 x'38
             + P'.wireSizeOpt 2 13 x'39
             + P'.wireSizeOpt 2 11 x'40
             + P'.wireSizeOpt 2 13 x'41
             + P'.wireSizeOpt 2 7 x'42
             + P'.wireSizeOpt 2 13 x'43
             + P'.wireSizeOpt 2 13 x'44
             + P'.wireSizeOpt 2 14 x'45
             + P'.wireSizeOpt 2 13 x'46
             + P'.wireSizeOpt 2 13 x'47
             + P'.wireSizeOpt 2 13 x'48)
  wirePut ft'
   self'@(CMsgDOTAMatch x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23
           x'24 x'25 x'26 x'27 x'28 x'29 x'30 x'31 x'32 x'33 x'34 x'35 x'36 x'37 x'38 x'39 x'40 x'41 x'42 x'43 x'44 x'45 x'46 x'47
           x'48)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 16 8 x'1
             P'.wirePutOpt 24 13 x'2
             P'.wirePutOpt 37 7 x'3
             P'.wirePutRep 42 11 x'4
             P'.wirePutOpt 48 4 x'5
             P'.wirePutRep 64 13 x'6
             P'.wirePutRep 72 13 x'7
             P'.wirePutOpt 80 13 x'8
             P'.wirePutOpt 96 13 x'9
             P'.wirePutOpt 109 7 x'10
             P'.wirePutOpt 117 7 x'11
             P'.wirePutOpt 120 13 x'12
             P'.wirePutOpt 128 13 x'13
             P'.wirePutOpt 136 13 x'14
             P'.wirePutOpt 144 13 x'15
             P'.wirePutOpt 157 2 x'16
             P'.wirePutOpt 160 13 x'17
             P'.wirePutOpt 168 13 x'18
             P'.wirePutOpt 176 13 x'19
             P'.wirePutOpt 186 9 x'20
             P'.wirePutOpt 194 9 x'21
             P'.wirePutOpt 200 4 x'22
             P'.wirePutOpt 208 4 x'23
             P'.wirePutOpt 216 13 x'24
             P'.wirePutOpt 224 13 x'25
             P'.wirePutOpt 232 13 x'26
             P'.wirePutOpt 240 13 x'27
             P'.wirePutOpt 248 14 x'28
             P'.wirePutRep 258 11 x'29
             P'.wirePutOpt 264 4 x'30
             P'.wirePutOpt 272 14 x'31
             P'.wirePutOpt 280 13 x'32
             P'.wirePutOpt 288 13 x'33
             P'.wirePutOpt 298 9 x'34
             P'.wirePutOpt 306 9 x'35
             P'.wirePutOpt 312 13 x'36
             P'.wirePutOpt 320 13 x'37
             P'.wirePutRep 346 11 x'38
             P'.wirePutOpt 352 13 x'39
             P'.wirePutOpt 362 11 x'40
             P'.wirePutOpt 368 13 x'41
             P'.wirePutOpt 381 7 x'42
             P'.wirePutOpt 384 13 x'43
             P'.wirePutOpt 392 13 x'44
             P'.wirePutOpt 400 14 x'45
             P'.wirePutOpt 408 13 x'46
             P'.wirePutOpt 416 13 x'47
             P'.wirePutOpt 424 13 x'48
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{good_guys_win = Prelude'.Just new'Field}) (P'.wireGet 8)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{duration = Prelude'.Just new'Field}) (P'.wireGet 13)
             37 -> Prelude'.fmap (\ !new'Field -> old'Self{startTime = Prelude'.Just new'Field}) (P'.wireGet 7)
             42 -> Prelude'.fmap (\ !new'Field -> old'Self{players = P'.append (players old'Self) new'Field}) (P'.wireGet 11)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{match_id = Prelude'.Just new'Field}) (P'.wireGet 4)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{tower_status = P'.append (tower_status old'Self) new'Field})
                    (P'.wireGet 13)
             66 -> Prelude'.fmap (\ !new'Field -> old'Self{tower_status = P'.mergeAppend (tower_status old'Self) new'Field})
                    (P'.wireGetPacked 13)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{barracks_status = P'.append (barracks_status old'Self) new'Field})
                    (P'.wireGet 13)
             74 -> Prelude'.fmap (\ !new'Field -> old'Self{barracks_status = P'.mergeAppend (barracks_status old'Self) new'Field})
                    (P'.wireGetPacked 13)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{cluster = Prelude'.Just new'Field}) (P'.wireGet 13)
             96 -> Prelude'.fmap (\ !new'Field -> old'Self{first_blood_time = Prelude'.Just new'Field}) (P'.wireGet 13)
             109 -> Prelude'.fmap (\ !new'Field -> old'Self{replay_salt = Prelude'.Just new'Field}) (P'.wireGet 7)
             117 -> Prelude'.fmap (\ !new'Field -> old'Self{server_ip = Prelude'.Just new'Field}) (P'.wireGet 7)
             120 -> Prelude'.fmap (\ !new'Field -> old'Self{server_port = Prelude'.Just new'Field}) (P'.wireGet 13)
             128 -> Prelude'.fmap (\ !new'Field -> old'Self{lobby_type = Prelude'.Just new'Field}) (P'.wireGet 13)
             136 -> Prelude'.fmap (\ !new'Field -> old'Self{human_players = Prelude'.Just new'Field}) (P'.wireGet 13)
             144 -> Prelude'.fmap (\ !new'Field -> old'Self{average_skill = Prelude'.Just new'Field}) (P'.wireGet 13)
             157 -> Prelude'.fmap (\ !new'Field -> old'Self{game_balance = Prelude'.Just new'Field}) (P'.wireGet 2)
             160 -> Prelude'.fmap (\ !new'Field -> old'Self{radiant_team_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             168 -> Prelude'.fmap (\ !new'Field -> old'Self{dire_team_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             176 -> Prelude'.fmap (\ !new'Field -> old'Self{leagueid = Prelude'.Just new'Field}) (P'.wireGet 13)
             186 -> Prelude'.fmap (\ !new'Field -> old'Self{radiant_team_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             194 -> Prelude'.fmap (\ !new'Field -> old'Self{dire_team_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             200 -> Prelude'.fmap (\ !new'Field -> old'Self{radiant_team_logo = Prelude'.Just new'Field}) (P'.wireGet 4)
             208 -> Prelude'.fmap (\ !new'Field -> old'Self{dire_team_logo = Prelude'.Just new'Field}) (P'.wireGet 4)
             216 -> Prelude'.fmap (\ !new'Field -> old'Self{radiant_team_complete = Prelude'.Just new'Field}) (P'.wireGet 13)
             224 -> Prelude'.fmap (\ !new'Field -> old'Self{dire_team_complete = Prelude'.Just new'Field}) (P'.wireGet 13)
             232 -> Prelude'.fmap (\ !new'Field -> old'Self{positive_votes = Prelude'.Just new'Field}) (P'.wireGet 13)
             240 -> Prelude'.fmap (\ !new'Field -> old'Self{negative_votes = Prelude'.Just new'Field}) (P'.wireGet 13)
             248 -> Prelude'.fmap (\ !new'Field -> old'Self{game_mode = Prelude'.Just new'Field}) (P'.wireGet 14)
             258 -> Prelude'.fmap (\ !new'Field -> old'Self{picks_bans = P'.append (picks_bans old'Self) new'Field}) (P'.wireGet 11)
             264 -> Prelude'.fmap (\ !new'Field -> old'Self{match_seq_num = Prelude'.Just new'Field}) (P'.wireGet 4)
             272 -> Prelude'.fmap (\ !new'Field -> old'Self{replay_state = Prelude'.Just new'Field}) (P'.wireGet 14)
             280 -> Prelude'.fmap (\ !new'Field -> old'Self{radiant_guild_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             288 -> Prelude'.fmap (\ !new'Field -> old'Self{dire_guild_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             298 -> Prelude'.fmap (\ !new'Field -> old'Self{radiant_team_tag = Prelude'.Just new'Field}) (P'.wireGet 9)
             306 -> Prelude'.fmap (\ !new'Field -> old'Self{dire_team_tag = Prelude'.Just new'Field}) (P'.wireGet 9)
             312 -> Prelude'.fmap (\ !new'Field -> old'Self{series_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             320 -> Prelude'.fmap (\ !new'Field -> old'Self{series_type = Prelude'.Just new'Field}) (P'.wireGet 13)
             346 -> Prelude'.fmap
                     (\ !new'Field -> old'Self{broadcaster_channels = P'.append (broadcaster_channels old'Self) new'Field})
                     (P'.wireGet 11)
             352 -> Prelude'.fmap (\ !new'Field -> old'Self{engine = Prelude'.Just new'Field}) (P'.wireGet 13)
             362 -> Prelude'.fmap
                     (\ !new'Field ->
                       old'Self{custom_game_data = P'.mergeAppend (custom_game_data old'Self) (Prelude'.Just new'Field)})
                     (P'.wireGet 11)
             368 -> Prelude'.fmap (\ !new'Field -> old'Self{match_flags = Prelude'.Just new'Field}) (P'.wireGet 13)
             381 -> Prelude'.fmap (\ !new'Field -> old'Self{private_metadata_key = Prelude'.Just new'Field}) (P'.wireGet 7)
             384 -> Prelude'.fmap (\ !new'Field -> old'Self{radiant_team_score = Prelude'.Just new'Field}) (P'.wireGet 13)
             392 -> Prelude'.fmap (\ !new'Field -> old'Self{dire_team_score = Prelude'.Just new'Field}) (P'.wireGet 13)
             400 -> Prelude'.fmap (\ !new'Field -> old'Self{match_outcome = Prelude'.Just new'Field}) (P'.wireGet 14)
             408 -> Prelude'.fmap (\ !new'Field -> old'Self{tournament_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             416 -> Prelude'.fmap (\ !new'Field -> old'Self{tournament_round = Prelude'.Just new'Field}) (P'.wireGet 13)
             424 -> Prelude'.fmap (\ !new'Field -> old'Self{pre_game_duration = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgDOTAMatch) CMsgDOTAMatch where
  getVal m' f' = f' m'

instance P'.GPB CMsgDOTAMatch

instance P'.ReflectDescriptor CMsgDOTAMatch where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList
        [16, 24, 37, 42, 48, 64, 66, 72, 74, 80, 96, 109, 117, 120, 128, 136, 144, 157, 160, 168, 176, 186, 194, 200, 208, 216, 224,
         232, 240, 248, 258, 264, 272, 280, 288, 298, 306, 312, 320, 346, 352, 362, 368, 381, 384, 392, 400, 408, 416, 424])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\"], baseName = MName \"CMsgDOTAMatch\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaGcCommon\",\"CMsgDOTAMatch.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.good_guys_win\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"good_guys_win\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.duration\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"duration\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.startTime\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"startTime\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 37}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.players\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"players\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.Player\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName = MName \"Player\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.match_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"match_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.tower_status\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"tower_status\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Just (WireTag {getWireTag = 64},WireTag {getWireTag = 66}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.barracks_status\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"barracks_status\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Just (WireTag {getWireTag = 72},WireTag {getWireTag = 74}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.cluster\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"cluster\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.first_blood_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"first_blood_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 96}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.replay_salt\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"replay_salt\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 109}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.server_ip\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"server_ip\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 117}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.server_port\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"server_port\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 120}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.lobby_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"lobby_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 128}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.human_players\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"human_players\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 136}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.average_skill\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"average_skill\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 144}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.game_balance\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"game_balance\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 19}, wireTag = WireTag {getWireTag = 157}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.radiant_team_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"radiant_team_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 20}, wireTag = WireTag {getWireTag = 160}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.dire_team_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"dire_team_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 21}, wireTag = WireTag {getWireTag = 168}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.leagueid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"leagueid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 22}, wireTag = WireTag {getWireTag = 176}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.radiant_team_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"radiant_team_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 23}, wireTag = WireTag {getWireTag = 186}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.dire_team_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"dire_team_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 24}, wireTag = WireTag {getWireTag = 194}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.radiant_team_logo\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"radiant_team_logo\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 25}, wireTag = WireTag {getWireTag = 200}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.dire_team_logo\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"dire_team_logo\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 26}, wireTag = WireTag {getWireTag = 208}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.radiant_team_complete\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"radiant_team_complete\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 27}, wireTag = WireTag {getWireTag = 216}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.dire_team_complete\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"dire_team_complete\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 28}, wireTag = WireTag {getWireTag = 224}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.positive_votes\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"positive_votes\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 29}, wireTag = WireTag {getWireTag = 232}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.negative_votes\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"negative_votes\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 30}, wireTag = WireTag {getWireTag = 240}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.game_mode\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"game_mode\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 31}, wireTag = WireTag {getWireTag = 248}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.DOTA_GameMode\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\"], baseName = MName \"DOTA_GameMode\"}), hsRawDefault = Just \"DOTA_GAMEMODE_NONE\", hsDefault = Just (HsDef'Enum \"DOTA_GAMEMODE_NONE\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.picks_bans\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"picks_bans\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 32}, wireTag = WireTag {getWireTag = 258}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMatchHeroSelectEvent\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\"], baseName = MName \"CMatchHeroSelectEvent\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.match_seq_num\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"match_seq_num\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 33}, wireTag = WireTag {getWireTag = 264}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.replay_state\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"replay_state\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 34}, wireTag = WireTag {getWireTag = 272}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.ReplayState\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName = MName \"ReplayState\"}), hsRawDefault = Just \"REPLAY_AVAILABLE\", hsDefault = Just (HsDef'Enum \"REPLAY_AVAILABLE\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.radiant_guild_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"radiant_guild_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 35}, wireTag = WireTag {getWireTag = 280}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.dire_guild_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"dire_guild_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 36}, wireTag = WireTag {getWireTag = 288}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.radiant_team_tag\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"radiant_team_tag\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 37}, wireTag = WireTag {getWireTag = 298}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.dire_team_tag\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"dire_team_tag\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 38}, wireTag = WireTag {getWireTag = 306}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.series_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"series_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 39}, wireTag = WireTag {getWireTag = 312}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.series_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"series_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 40}, wireTag = WireTag {getWireTag = 320}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.broadcaster_channels\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"broadcaster_channels\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 43}, wireTag = WireTag {getWireTag = 346}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.BroadcasterChannel\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName = MName \"BroadcasterChannel\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.engine\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"engine\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 44}, wireTag = WireTag {getWireTag = 352}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.custom_game_data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"custom_game_data\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 45}, wireTag = WireTag {getWireTag = 362}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.CustomGameData\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName = MName \"CustomGameData\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.match_flags\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"match_flags\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 46}, wireTag = WireTag {getWireTag = 368}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.private_metadata_key\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"private_metadata_key\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 47}, wireTag = WireTag {getWireTag = 381}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.radiant_team_score\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"radiant_team_score\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 48}, wireTag = WireTag {getWireTag = 384}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.dire_team_score\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"dire_team_score\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 49}, wireTag = WireTag {getWireTag = 392}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.match_outcome\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"match_outcome\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 50}, wireTag = WireTag {getWireTag = 400}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.EMatchOutcome\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\"], baseName = MName \"EMatchOutcome\"}), hsRawDefault = Just \"k_EMatchOutcome_Unknown\", hsDefault = Just (HsDef'Enum \"K_EMatchOutcome_Unknown\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.tournament_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"tournament_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 51}, wireTag = WireTag {getWireTag = 408}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.tournament_round\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"tournament_round\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 52}, wireTag = WireTag {getWireTag = 416}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMsgDOTAMatch.pre_game_duration\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMsgDOTAMatch\"], baseName' = FName \"pre_game_duration\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 53}, wireTag = WireTag {getWireTag = 424}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgDOTAMatch where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgDOTAMatch where
  textPut msg
   = do
       P'.tellT "good_guys_win" (good_guys_win msg)
       P'.tellT "duration" (duration msg)
       P'.tellT "startTime" (startTime msg)
       P'.tellT "players" (players msg)
       P'.tellT "match_id" (match_id msg)
       P'.tellT "tower_status" (tower_status msg)
       P'.tellT "barracks_status" (barracks_status msg)
       P'.tellT "cluster" (cluster msg)
       P'.tellT "first_blood_time" (first_blood_time msg)
       P'.tellT "replay_salt" (replay_salt msg)
       P'.tellT "server_ip" (server_ip msg)
       P'.tellT "server_port" (server_port msg)
       P'.tellT "lobby_type" (lobby_type msg)
       P'.tellT "human_players" (human_players msg)
       P'.tellT "average_skill" (average_skill msg)
       P'.tellT "game_balance" (game_balance msg)
       P'.tellT "radiant_team_id" (radiant_team_id msg)
       P'.tellT "dire_team_id" (dire_team_id msg)
       P'.tellT "leagueid" (leagueid msg)
       P'.tellT "radiant_team_name" (radiant_team_name msg)
       P'.tellT "dire_team_name" (dire_team_name msg)
       P'.tellT "radiant_team_logo" (radiant_team_logo msg)
       P'.tellT "dire_team_logo" (dire_team_logo msg)
       P'.tellT "radiant_team_complete" (radiant_team_complete msg)
       P'.tellT "dire_team_complete" (dire_team_complete msg)
       P'.tellT "positive_votes" (positive_votes msg)
       P'.tellT "negative_votes" (negative_votes msg)
       P'.tellT "game_mode" (game_mode msg)
       P'.tellT "picks_bans" (picks_bans msg)
       P'.tellT "match_seq_num" (match_seq_num msg)
       P'.tellT "replay_state" (replay_state msg)
       P'.tellT "radiant_guild_id" (radiant_guild_id msg)
       P'.tellT "dire_guild_id" (dire_guild_id msg)
       P'.tellT "radiant_team_tag" (radiant_team_tag msg)
       P'.tellT "dire_team_tag" (dire_team_tag msg)
       P'.tellT "series_id" (series_id msg)
       P'.tellT "series_type" (series_type msg)
       P'.tellT "broadcaster_channels" (broadcaster_channels msg)
       P'.tellT "engine" (engine msg)
       P'.tellT "custom_game_data" (custom_game_data msg)
       P'.tellT "match_flags" (match_flags msg)
       P'.tellT "private_metadata_key" (private_metadata_key msg)
       P'.tellT "radiant_team_score" (radiant_team_score msg)
       P'.tellT "dire_team_score" (dire_team_score msg)
       P'.tellT "match_outcome" (match_outcome msg)
       P'.tellT "tournament_id" (tournament_id msg)
       P'.tellT "tournament_round" (tournament_round msg)
       P'.tellT "pre_game_duration" (pre_game_duration msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'good_guys_win, parse'duration, parse'startTime, parse'players, parse'match_id, parse'tower_status,
                   parse'barracks_status, parse'cluster, parse'first_blood_time, parse'replay_salt, parse'server_ip,
                   parse'server_port, parse'lobby_type, parse'human_players, parse'average_skill, parse'game_balance,
                   parse'radiant_team_id, parse'dire_team_id, parse'leagueid, parse'radiant_team_name, parse'dire_team_name,
                   parse'radiant_team_logo, parse'dire_team_logo, parse'radiant_team_complete, parse'dire_team_complete,
                   parse'positive_votes, parse'negative_votes, parse'game_mode, parse'picks_bans, parse'match_seq_num,
                   parse'replay_state, parse'radiant_guild_id, parse'dire_guild_id, parse'radiant_team_tag, parse'dire_team_tag,
                   parse'series_id, parse'series_type, parse'broadcaster_channels, parse'engine, parse'custom_game_data,
                   parse'match_flags, parse'private_metadata_key, parse'radiant_team_score, parse'dire_team_score,
                   parse'match_outcome, parse'tournament_id, parse'tournament_round, parse'pre_game_duration])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'good_guys_win
         = P'.try
            (do
               v <- P'.getT "good_guys_win"
               Prelude'.return (\ o -> o{good_guys_win = v}))
        parse'duration
         = P'.try
            (do
               v <- P'.getT "duration"
               Prelude'.return (\ o -> o{duration = v}))
        parse'startTime
         = P'.try
            (do
               v <- P'.getT "startTime"
               Prelude'.return (\ o -> o{startTime = v}))
        parse'players
         = P'.try
            (do
               v <- P'.getT "players"
               Prelude'.return (\ o -> o{players = P'.append (players o) v}))
        parse'match_id
         = P'.try
            (do
               v <- P'.getT "match_id"
               Prelude'.return (\ o -> o{match_id = v}))
        parse'tower_status
         = P'.try
            (do
               v <- P'.getT "tower_status"
               Prelude'.return (\ o -> o{tower_status = P'.append (tower_status o) v}))
        parse'barracks_status
         = P'.try
            (do
               v <- P'.getT "barracks_status"
               Prelude'.return (\ o -> o{barracks_status = P'.append (barracks_status o) v}))
        parse'cluster
         = P'.try
            (do
               v <- P'.getT "cluster"
               Prelude'.return (\ o -> o{cluster = v}))
        parse'first_blood_time
         = P'.try
            (do
               v <- P'.getT "first_blood_time"
               Prelude'.return (\ o -> o{first_blood_time = v}))
        parse'replay_salt
         = P'.try
            (do
               v <- P'.getT "replay_salt"
               Prelude'.return (\ o -> o{replay_salt = v}))
        parse'server_ip
         = P'.try
            (do
               v <- P'.getT "server_ip"
               Prelude'.return (\ o -> o{server_ip = v}))
        parse'server_port
         = P'.try
            (do
               v <- P'.getT "server_port"
               Prelude'.return (\ o -> o{server_port = v}))
        parse'lobby_type
         = P'.try
            (do
               v <- P'.getT "lobby_type"
               Prelude'.return (\ o -> o{lobby_type = v}))
        parse'human_players
         = P'.try
            (do
               v <- P'.getT "human_players"
               Prelude'.return (\ o -> o{human_players = v}))
        parse'average_skill
         = P'.try
            (do
               v <- P'.getT "average_skill"
               Prelude'.return (\ o -> o{average_skill = v}))
        parse'game_balance
         = P'.try
            (do
               v <- P'.getT "game_balance"
               Prelude'.return (\ o -> o{game_balance = v}))
        parse'radiant_team_id
         = P'.try
            (do
               v <- P'.getT "radiant_team_id"
               Prelude'.return (\ o -> o{radiant_team_id = v}))
        parse'dire_team_id
         = P'.try
            (do
               v <- P'.getT "dire_team_id"
               Prelude'.return (\ o -> o{dire_team_id = v}))
        parse'leagueid
         = P'.try
            (do
               v <- P'.getT "leagueid"
               Prelude'.return (\ o -> o{leagueid = v}))
        parse'radiant_team_name
         = P'.try
            (do
               v <- P'.getT "radiant_team_name"
               Prelude'.return (\ o -> o{radiant_team_name = v}))
        parse'dire_team_name
         = P'.try
            (do
               v <- P'.getT "dire_team_name"
               Prelude'.return (\ o -> o{dire_team_name = v}))
        parse'radiant_team_logo
         = P'.try
            (do
               v <- P'.getT "radiant_team_logo"
               Prelude'.return (\ o -> o{radiant_team_logo = v}))
        parse'dire_team_logo
         = P'.try
            (do
               v <- P'.getT "dire_team_logo"
               Prelude'.return (\ o -> o{dire_team_logo = v}))
        parse'radiant_team_complete
         = P'.try
            (do
               v <- P'.getT "radiant_team_complete"
               Prelude'.return (\ o -> o{radiant_team_complete = v}))
        parse'dire_team_complete
         = P'.try
            (do
               v <- P'.getT "dire_team_complete"
               Prelude'.return (\ o -> o{dire_team_complete = v}))
        parse'positive_votes
         = P'.try
            (do
               v <- P'.getT "positive_votes"
               Prelude'.return (\ o -> o{positive_votes = v}))
        parse'negative_votes
         = P'.try
            (do
               v <- P'.getT "negative_votes"
               Prelude'.return (\ o -> o{negative_votes = v}))
        parse'game_mode
         = P'.try
            (do
               v <- P'.getT "game_mode"
               Prelude'.return (\ o -> o{game_mode = v}))
        parse'picks_bans
         = P'.try
            (do
               v <- P'.getT "picks_bans"
               Prelude'.return (\ o -> o{picks_bans = P'.append (picks_bans o) v}))
        parse'match_seq_num
         = P'.try
            (do
               v <- P'.getT "match_seq_num"
               Prelude'.return (\ o -> o{match_seq_num = v}))
        parse'replay_state
         = P'.try
            (do
               v <- P'.getT "replay_state"
               Prelude'.return (\ o -> o{replay_state = v}))
        parse'radiant_guild_id
         = P'.try
            (do
               v <- P'.getT "radiant_guild_id"
               Prelude'.return (\ o -> o{radiant_guild_id = v}))
        parse'dire_guild_id
         = P'.try
            (do
               v <- P'.getT "dire_guild_id"
               Prelude'.return (\ o -> o{dire_guild_id = v}))
        parse'radiant_team_tag
         = P'.try
            (do
               v <- P'.getT "radiant_team_tag"
               Prelude'.return (\ o -> o{radiant_team_tag = v}))
        parse'dire_team_tag
         = P'.try
            (do
               v <- P'.getT "dire_team_tag"
               Prelude'.return (\ o -> o{dire_team_tag = v}))
        parse'series_id
         = P'.try
            (do
               v <- P'.getT "series_id"
               Prelude'.return (\ o -> o{series_id = v}))
        parse'series_type
         = P'.try
            (do
               v <- P'.getT "series_type"
               Prelude'.return (\ o -> o{series_type = v}))
        parse'broadcaster_channels
         = P'.try
            (do
               v <- P'.getT "broadcaster_channels"
               Prelude'.return (\ o -> o{broadcaster_channels = P'.append (broadcaster_channels o) v}))
        parse'engine
         = P'.try
            (do
               v <- P'.getT "engine"
               Prelude'.return (\ o -> o{engine = v}))
        parse'custom_game_data
         = P'.try
            (do
               v <- P'.getT "custom_game_data"
               Prelude'.return (\ o -> o{custom_game_data = v}))
        parse'match_flags
         = P'.try
            (do
               v <- P'.getT "match_flags"
               Prelude'.return (\ o -> o{match_flags = v}))
        parse'private_metadata_key
         = P'.try
            (do
               v <- P'.getT "private_metadata_key"
               Prelude'.return (\ o -> o{private_metadata_key = v}))
        parse'radiant_team_score
         = P'.try
            (do
               v <- P'.getT "radiant_team_score"
               Prelude'.return (\ o -> o{radiant_team_score = v}))
        parse'dire_team_score
         = P'.try
            (do
               v <- P'.getT "dire_team_score"
               Prelude'.return (\ o -> o{dire_team_score = v}))
        parse'match_outcome
         = P'.try
            (do
               v <- P'.getT "match_outcome"
               Prelude'.return (\ o -> o{match_outcome = v}))
        parse'tournament_id
         = P'.try
            (do
               v <- P'.getT "tournament_id"
               Prelude'.return (\ o -> o{tournament_id = v}))
        parse'tournament_round
         = P'.try
            (do
               v <- P'.getT "tournament_round"
               Prelude'.return (\ o -> o{tournament_round = v}))
        parse'pre_game_duration
         = P'.try
            (do
               v <- P'.getT "pre_game_duration"
               Prelude'.return (\ o -> o{pre_game_duration = v}))