{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaGcCommon.CMatchAdditionalUnitInventory (CMatchAdditionalUnitInventory(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CMatchAdditionalUnitInventory = CMatchAdditionalUnitInventory{unit_name :: !(P'.Maybe P'.Utf8), items :: !(P'.Seq P'.Word32)}
                                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                             Prelude'.Generic)

instance P'.Mergeable CMatchAdditionalUnitInventory where
  mergeAppend (CMatchAdditionalUnitInventory x'1 x'2) (CMatchAdditionalUnitInventory y'1 y'2)
   = CMatchAdditionalUnitInventory (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMatchAdditionalUnitInventory where
  defaultValue = CMatchAdditionalUnitInventory P'.defaultValue P'.defaultValue

instance P'.Wire CMatchAdditionalUnitInventory where
  wireSize ft' self'@(CMatchAdditionalUnitInventory x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 9 x'1 + P'.wireSizeRep 1 13 x'2)
  wirePut ft' self'@(CMatchAdditionalUnitInventory x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
             P'.wirePutRep 16 13 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{unit_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{items = P'.append (items old'Self) new'Field}) (P'.wireGet 13)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{items = P'.mergeAppend (items old'Self) new'Field}) (P'.wireGetPacked 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMatchAdditionalUnitInventory) CMatchAdditionalUnitInventory where
  getVal m' f' = f' m'

instance P'.GPB CMatchAdditionalUnitInventory

instance P'.ReflectDescriptor CMatchAdditionalUnitInventory where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 16, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Gcmessages_Common.CMatchAdditionalUnitInventory\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaGcCommon\"], baseName = MName \"CMatchAdditionalUnitInventory\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaGcCommon\",\"CMatchAdditionalUnitInventory.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMatchAdditionalUnitInventory.unit_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMatchAdditionalUnitInventory\"], baseName' = FName \"unit_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Gcmessages_Common.CMatchAdditionalUnitInventory.items\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaGcCommon\",MName \"CMatchAdditionalUnitInventory\"], baseName' = FName \"items\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Just (WireTag {getWireTag = 16},WireTag {getWireTag = 18}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMatchAdditionalUnitInventory where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMatchAdditionalUnitInventory where
  textPut msg
   = do
       P'.tellT "unit_name" (unit_name msg)
       P'.tellT "items" (items msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'unit_name, parse'items]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'unit_name
         = P'.try
            (do
               v <- P'.getT "unit_name"
               Prelude'.return (\ o -> o{unit_name = v}))
        parse'items
         = P'.try
            (do
               v <- P'.getT "items"
               Prelude'.return (\ o -> o{items = P'.append (items o) v}))