{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaGcCommon.DOTA_GameMode (DOTA_GameMode(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data DOTA_GameMode = DOTA_GAMEMODE_NONE
                   | DOTA_GAMEMODE_AP
                   | DOTA_GAMEMODE_CM
                   | DOTA_GAMEMODE_RD
                   | DOTA_GAMEMODE_SD
                   | DOTA_GAMEMODE_AR
                   | DOTA_GAMEMODE_INTRO
                   | DOTA_GAMEMODE_HW
                   | DOTA_GAMEMODE_REVERSE_CM
                   | DOTA_GAMEMODE_XMAS
                   | DOTA_GAMEMODE_TUTORIAL
                   | DOTA_GAMEMODE_MO
                   | DOTA_GAMEMODE_LP
                   | DOTA_GAMEMODE_POOL1
                   | DOTA_GAMEMODE_FH
                   | DOTA_GAMEMODE_CUSTOM
                   | DOTA_GAMEMODE_CD
                   | DOTA_GAMEMODE_BD
                   | DOTA_GAMEMODE_ABILITY_DRAFT
                   | DOTA_GAMEMODE_EVENT
                   | DOTA_GAMEMODE_ARDM
                   | DOTA_GAMEMODE_1V1MID
                   | DOTA_GAMEMODE_ALL_DRAFT
                   deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                             Prelude'.Generic)

instance P'.Mergeable DOTA_GameMode

instance Prelude'.Bounded DOTA_GameMode where
  minBound = DOTA_GAMEMODE_NONE
  maxBound = DOTA_GAMEMODE_ALL_DRAFT

instance P'.Default DOTA_GameMode where
  defaultValue = DOTA_GAMEMODE_NONE

toMaybe'Enum :: Prelude'.Int -> P'.Maybe DOTA_GameMode
toMaybe'Enum 0 = Prelude'.Just DOTA_GAMEMODE_NONE
toMaybe'Enum 1 = Prelude'.Just DOTA_GAMEMODE_AP
toMaybe'Enum 2 = Prelude'.Just DOTA_GAMEMODE_CM
toMaybe'Enum 3 = Prelude'.Just DOTA_GAMEMODE_RD
toMaybe'Enum 4 = Prelude'.Just DOTA_GAMEMODE_SD
toMaybe'Enum 5 = Prelude'.Just DOTA_GAMEMODE_AR
toMaybe'Enum 6 = Prelude'.Just DOTA_GAMEMODE_INTRO
toMaybe'Enum 7 = Prelude'.Just DOTA_GAMEMODE_HW
toMaybe'Enum 8 = Prelude'.Just DOTA_GAMEMODE_REVERSE_CM
toMaybe'Enum 9 = Prelude'.Just DOTA_GAMEMODE_XMAS
toMaybe'Enum 10 = Prelude'.Just DOTA_GAMEMODE_TUTORIAL
toMaybe'Enum 11 = Prelude'.Just DOTA_GAMEMODE_MO
toMaybe'Enum 12 = Prelude'.Just DOTA_GAMEMODE_LP
toMaybe'Enum 13 = Prelude'.Just DOTA_GAMEMODE_POOL1
toMaybe'Enum 14 = Prelude'.Just DOTA_GAMEMODE_FH
toMaybe'Enum 15 = Prelude'.Just DOTA_GAMEMODE_CUSTOM
toMaybe'Enum 16 = Prelude'.Just DOTA_GAMEMODE_CD
toMaybe'Enum 17 = Prelude'.Just DOTA_GAMEMODE_BD
toMaybe'Enum 18 = Prelude'.Just DOTA_GAMEMODE_ABILITY_DRAFT
toMaybe'Enum 19 = Prelude'.Just DOTA_GAMEMODE_EVENT
toMaybe'Enum 20 = Prelude'.Just DOTA_GAMEMODE_ARDM
toMaybe'Enum 21 = Prelude'.Just DOTA_GAMEMODE_1V1MID
toMaybe'Enum 22 = Prelude'.Just DOTA_GAMEMODE_ALL_DRAFT
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum DOTA_GameMode where
  fromEnum DOTA_GAMEMODE_NONE = 0
  fromEnum DOTA_GAMEMODE_AP = 1
  fromEnum DOTA_GAMEMODE_CM = 2
  fromEnum DOTA_GAMEMODE_RD = 3
  fromEnum DOTA_GAMEMODE_SD = 4
  fromEnum DOTA_GAMEMODE_AR = 5
  fromEnum DOTA_GAMEMODE_INTRO = 6
  fromEnum DOTA_GAMEMODE_HW = 7
  fromEnum DOTA_GAMEMODE_REVERSE_CM = 8
  fromEnum DOTA_GAMEMODE_XMAS = 9
  fromEnum DOTA_GAMEMODE_TUTORIAL = 10
  fromEnum DOTA_GAMEMODE_MO = 11
  fromEnum DOTA_GAMEMODE_LP = 12
  fromEnum DOTA_GAMEMODE_POOL1 = 13
  fromEnum DOTA_GAMEMODE_FH = 14
  fromEnum DOTA_GAMEMODE_CUSTOM = 15
  fromEnum DOTA_GAMEMODE_CD = 16
  fromEnum DOTA_GAMEMODE_BD = 17
  fromEnum DOTA_GAMEMODE_ABILITY_DRAFT = 18
  fromEnum DOTA_GAMEMODE_EVENT = 19
  fromEnum DOTA_GAMEMODE_ARDM = 20
  fromEnum DOTA_GAMEMODE_1V1MID = 21
  fromEnum DOTA_GAMEMODE_ALL_DRAFT = 22
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2DotaGcCommon.DOTA_GameMode")
      . toMaybe'Enum
  succ DOTA_GAMEMODE_NONE = DOTA_GAMEMODE_AP
  succ DOTA_GAMEMODE_AP = DOTA_GAMEMODE_CM
  succ DOTA_GAMEMODE_CM = DOTA_GAMEMODE_RD
  succ DOTA_GAMEMODE_RD = DOTA_GAMEMODE_SD
  succ DOTA_GAMEMODE_SD = DOTA_GAMEMODE_AR
  succ DOTA_GAMEMODE_AR = DOTA_GAMEMODE_INTRO
  succ DOTA_GAMEMODE_INTRO = DOTA_GAMEMODE_HW
  succ DOTA_GAMEMODE_HW = DOTA_GAMEMODE_REVERSE_CM
  succ DOTA_GAMEMODE_REVERSE_CM = DOTA_GAMEMODE_XMAS
  succ DOTA_GAMEMODE_XMAS = DOTA_GAMEMODE_TUTORIAL
  succ DOTA_GAMEMODE_TUTORIAL = DOTA_GAMEMODE_MO
  succ DOTA_GAMEMODE_MO = DOTA_GAMEMODE_LP
  succ DOTA_GAMEMODE_LP = DOTA_GAMEMODE_POOL1
  succ DOTA_GAMEMODE_POOL1 = DOTA_GAMEMODE_FH
  succ DOTA_GAMEMODE_FH = DOTA_GAMEMODE_CUSTOM
  succ DOTA_GAMEMODE_CUSTOM = DOTA_GAMEMODE_CD
  succ DOTA_GAMEMODE_CD = DOTA_GAMEMODE_BD
  succ DOTA_GAMEMODE_BD = DOTA_GAMEMODE_ABILITY_DRAFT
  succ DOTA_GAMEMODE_ABILITY_DRAFT = DOTA_GAMEMODE_EVENT
  succ DOTA_GAMEMODE_EVENT = DOTA_GAMEMODE_ARDM
  succ DOTA_GAMEMODE_ARDM = DOTA_GAMEMODE_1V1MID
  succ DOTA_GAMEMODE_1V1MID = DOTA_GAMEMODE_ALL_DRAFT
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2DotaGcCommon.DOTA_GameMode"
  pred DOTA_GAMEMODE_AP = DOTA_GAMEMODE_NONE
  pred DOTA_GAMEMODE_CM = DOTA_GAMEMODE_AP
  pred DOTA_GAMEMODE_RD = DOTA_GAMEMODE_CM
  pred DOTA_GAMEMODE_SD = DOTA_GAMEMODE_RD
  pred DOTA_GAMEMODE_AR = DOTA_GAMEMODE_SD
  pred DOTA_GAMEMODE_INTRO = DOTA_GAMEMODE_AR
  pred DOTA_GAMEMODE_HW = DOTA_GAMEMODE_INTRO
  pred DOTA_GAMEMODE_REVERSE_CM = DOTA_GAMEMODE_HW
  pred DOTA_GAMEMODE_XMAS = DOTA_GAMEMODE_REVERSE_CM
  pred DOTA_GAMEMODE_TUTORIAL = DOTA_GAMEMODE_XMAS
  pred DOTA_GAMEMODE_MO = DOTA_GAMEMODE_TUTORIAL
  pred DOTA_GAMEMODE_LP = DOTA_GAMEMODE_MO
  pred DOTA_GAMEMODE_POOL1 = DOTA_GAMEMODE_LP
  pred DOTA_GAMEMODE_FH = DOTA_GAMEMODE_POOL1
  pred DOTA_GAMEMODE_CUSTOM = DOTA_GAMEMODE_FH
  pred DOTA_GAMEMODE_CD = DOTA_GAMEMODE_CUSTOM
  pred DOTA_GAMEMODE_BD = DOTA_GAMEMODE_CD
  pred DOTA_GAMEMODE_ABILITY_DRAFT = DOTA_GAMEMODE_BD
  pred DOTA_GAMEMODE_EVENT = DOTA_GAMEMODE_ABILITY_DRAFT
  pred DOTA_GAMEMODE_ARDM = DOTA_GAMEMODE_EVENT
  pred DOTA_GAMEMODE_1V1MID = DOTA_GAMEMODE_ARDM
  pred DOTA_GAMEMODE_ALL_DRAFT = DOTA_GAMEMODE_1V1MID
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2DotaGcCommon.DOTA_GameMode"

instance P'.Wire DOTA_GameMode where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB DOTA_GameMode

instance P'.MessageAPI msg' (msg' -> DOTA_GameMode) DOTA_GameMode where
  getVal m' f' = f' m'

instance P'.ReflectEnum DOTA_GameMode where
  reflectEnum
   = [(0, "DOTA_GAMEMODE_NONE", DOTA_GAMEMODE_NONE), (1, "DOTA_GAMEMODE_AP", DOTA_GAMEMODE_AP),
      (2, "DOTA_GAMEMODE_CM", DOTA_GAMEMODE_CM), (3, "DOTA_GAMEMODE_RD", DOTA_GAMEMODE_RD),
      (4, "DOTA_GAMEMODE_SD", DOTA_GAMEMODE_SD), (5, "DOTA_GAMEMODE_AR", DOTA_GAMEMODE_AR),
      (6, "DOTA_GAMEMODE_INTRO", DOTA_GAMEMODE_INTRO), (7, "DOTA_GAMEMODE_HW", DOTA_GAMEMODE_HW),
      (8, "DOTA_GAMEMODE_REVERSE_CM", DOTA_GAMEMODE_REVERSE_CM), (9, "DOTA_GAMEMODE_XMAS", DOTA_GAMEMODE_XMAS),
      (10, "DOTA_GAMEMODE_TUTORIAL", DOTA_GAMEMODE_TUTORIAL), (11, "DOTA_GAMEMODE_MO", DOTA_GAMEMODE_MO),
      (12, "DOTA_GAMEMODE_LP", DOTA_GAMEMODE_LP), (13, "DOTA_GAMEMODE_POOL1", DOTA_GAMEMODE_POOL1),
      (14, "DOTA_GAMEMODE_FH", DOTA_GAMEMODE_FH), (15, "DOTA_GAMEMODE_CUSTOM", DOTA_GAMEMODE_CUSTOM),
      (16, "DOTA_GAMEMODE_CD", DOTA_GAMEMODE_CD), (17, "DOTA_GAMEMODE_BD", DOTA_GAMEMODE_BD),
      (18, "DOTA_GAMEMODE_ABILITY_DRAFT", DOTA_GAMEMODE_ABILITY_DRAFT), (19, "DOTA_GAMEMODE_EVENT", DOTA_GAMEMODE_EVENT),
      (20, "DOTA_GAMEMODE_ARDM", DOTA_GAMEMODE_ARDM), (21, "DOTA_GAMEMODE_1V1MID", DOTA_GAMEMODE_1V1MID),
      (22, "DOTA_GAMEMODE_ALL_DRAFT", DOTA_GAMEMODE_ALL_DRAFT)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Dota_Gcmessages_Common.DOTA_GameMode") ["Shrine", "Wire", "Protobuf", "S2"] ["S2DotaGcCommon"]
        "DOTA_GameMode")
      ["Shrine", "Wire", "Protobuf", "S2", "S2DotaGcCommon", "DOTA_GameMode.hs"]
      [(0, "DOTA_GAMEMODE_NONE"), (1, "DOTA_GAMEMODE_AP"), (2, "DOTA_GAMEMODE_CM"), (3, "DOTA_GAMEMODE_RD"),
       (4, "DOTA_GAMEMODE_SD"), (5, "DOTA_GAMEMODE_AR"), (6, "DOTA_GAMEMODE_INTRO"), (7, "DOTA_GAMEMODE_HW"),
       (8, "DOTA_GAMEMODE_REVERSE_CM"), (9, "DOTA_GAMEMODE_XMAS"), (10, "DOTA_GAMEMODE_TUTORIAL"), (11, "DOTA_GAMEMODE_MO"),
       (12, "DOTA_GAMEMODE_LP"), (13, "DOTA_GAMEMODE_POOL1"), (14, "DOTA_GAMEMODE_FH"), (15, "DOTA_GAMEMODE_CUSTOM"),
       (16, "DOTA_GAMEMODE_CD"), (17, "DOTA_GAMEMODE_BD"), (18, "DOTA_GAMEMODE_ABILITY_DRAFT"), (19, "DOTA_GAMEMODE_EVENT"),
       (20, "DOTA_GAMEMODE_ARDM"), (21, "DOTA_GAMEMODE_1V1MID"), (22, "DOTA_GAMEMODE_ALL_DRAFT")]

instance P'.TextType DOTA_GameMode where
  tellT = P'.tellShow
  getT = P'.getRead