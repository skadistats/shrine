{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaUserMessages.EDotaUserMessages (EDotaUserMessages(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EDotaUserMessages = DOTA_UM_AddUnitToSelection
                       | DOTA_UM_AIDebugLine
                       | DOTA_UM_ChatEvent
                       | DOTA_UM_CombatHeroPositions
                       | DOTA_UM_CombatLogData
                       | DOTA_UM_CombatLogShowDeath
                       | DOTA_UM_CreateLinearProjectile
                       | DOTA_UM_DestroyLinearProjectile
                       | DOTA_UM_DodgeTrackingProjectiles
                       | DOTA_UM_GlobalLightColor
                       | DOTA_UM_GlobalLightDirection
                       | DOTA_UM_InvalidCommand
                       | DOTA_UM_LocationPing
                       | DOTA_UM_MapLine
                       | DOTA_UM_MiniKillCamInfo
                       | DOTA_UM_MinimapDebugPoint
                       | DOTA_UM_MinimapEvent
                       | DOTA_UM_NevermoreRequiem
                       | DOTA_UM_OverheadEvent
                       | DOTA_UM_SetNextAutobuyItem
                       | DOTA_UM_SharedCooldown
                       | DOTA_UM_SpectatorPlayerClick
                       | DOTA_UM_TutorialTipInfo
                       | DOTA_UM_UnitEvent
                       | DOTA_UM_ParticleManager
                       | DOTA_UM_BotChat
                       | DOTA_UM_HudError
                       | DOTA_UM_ItemPurchased
                       | DOTA_UM_Ping
                       | DOTA_UM_ItemFound
                       | DOTA_UM_CharacterSpeakConcept
                       | DOTA_UM_SwapVerify
                       | DOTA_UM_WorldLine
                       | DOTA_UM_TournamentDrop
                       | DOTA_UM_ItemAlert
                       | DOTA_UM_HalloweenDrops
                       | DOTA_UM_ChatWheel
                       | DOTA_UM_ReceivedXmasGift
                       | DOTA_UM_UpdateSharedContent
                       | DOTA_UM_TutorialRequestExp
                       | DOTA_UM_TutorialPingMinimap
                       | DOTA_UM_GamerulesStateChanged
                       | DOTA_UM_ShowSurvey
                       | DOTA_UM_TutorialFade
                       | DOTA_UM_AddQuestLogEntry
                       | DOTA_UM_SendStatPopup
                       | DOTA_UM_TutorialFinish
                       | DOTA_UM_SendRoshanPopup
                       | DOTA_UM_SendGenericToolTip
                       | DOTA_UM_SendFinalGold
                       | DOTA_UM_CustomMsg
                       | DOTA_UM_CoachHUDPing
                       | DOTA_UM_ClientLoadGridNav
                       | DOTA_UM_TE_Projectile
                       | DOTA_UM_TE_ProjectileLoc
                       | DOTA_UM_TE_DotaBloodImpact
                       | DOTA_UM_TE_UnitAnimation
                       | DOTA_UM_TE_UnitAnimationEnd
                       | DOTA_UM_AbilityPing
                       | DOTA_UM_ShowGenericPopup
                       | DOTA_UM_VoteStart
                       | DOTA_UM_VoteUpdate
                       | DOTA_UM_VoteEnd
                       | DOTA_UM_BoosterState
                       | DOTA_UM_WillPurchaseAlert
                       | DOTA_UM_TutorialMinimapPosition
                       | DOTA_UM_PlayerMMR
                       | DOTA_UM_AbilitySteal
                       | DOTA_UM_CourierKilledAlert
                       | DOTA_UM_EnemyItemAlert
                       | DOTA_UM_StatsMatchDetails
                       | DOTA_UM_MiniTaunt
                       | DOTA_UM_BuyBackStateAlert
                       | DOTA_UM_SpeechBubble
                       | DOTA_UM_CustomHeaderMessage
                       | DOTA_UM_QuickBuyAlert
                       | DOTA_UM_StatsHeroDetails
                       | DOTA_UM_PredictionResult
                       | DOTA_UM_ModifierAlert
                       | DOTA_UM_HPManaAlert
                       | DOTA_UM_GlyphAlert
                       | DOTA_UM_BeastChat
                       | DOTA_UM_SpectatorPlayerUnitOrders
                       | DOTA_UM_CustomHudElement_Create
                       | DOTA_UM_CustomHudElement_Modify
                       | DOTA_UM_CustomHudElement_Destroy
                       | DOTA_UM_CompendiumState
                       | DOTA_UM_ProjectionAbility
                       | DOTA_UM_ProjectionEvent
                       | DOTA_UM_CombatLogDataHLTV
                       | DOTA_UM_XPAlert
                       | DOTA_UM_UpdateQuestProgress
                       | DOTA_UM_MatchMetadata
                       | DOTA_UM_MatchDetails
                       | DOTA_UM_QuestStatus
                       | DOTA_UM_SuggestHeroPick
                       | DOTA_UM_SuggestHeroRole
                       | DOTA_UM_KillcamDamageTaken
                       | DOTA_UM_SelectPenaltyGold
                       | DOTA_UM_RollDiceResult
                       | DOTA_UM_FlipCoinResult
                       | DOTA_UM_RequestItemSuggestions
                       | DOTA_UM_TeamCaptainChanged
                       | DOTA_UM_SendRoshanSpectatorPhase
                       | DOTA_UM_ChatWheelCooldown
                       deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                 Prelude'.Generic)

instance P'.Mergeable EDotaUserMessages

instance Prelude'.Bounded EDotaUserMessages where
  minBound = DOTA_UM_AddUnitToSelection
  maxBound = DOTA_UM_ChatWheelCooldown

instance P'.Default EDotaUserMessages where
  defaultValue = DOTA_UM_AddUnitToSelection

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EDotaUserMessages
toMaybe'Enum 464 = Prelude'.Just DOTA_UM_AddUnitToSelection
toMaybe'Enum 465 = Prelude'.Just DOTA_UM_AIDebugLine
toMaybe'Enum 466 = Prelude'.Just DOTA_UM_ChatEvent
toMaybe'Enum 467 = Prelude'.Just DOTA_UM_CombatHeroPositions
toMaybe'Enum 468 = Prelude'.Just DOTA_UM_CombatLogData
toMaybe'Enum 470 = Prelude'.Just DOTA_UM_CombatLogShowDeath
toMaybe'Enum 471 = Prelude'.Just DOTA_UM_CreateLinearProjectile
toMaybe'Enum 472 = Prelude'.Just DOTA_UM_DestroyLinearProjectile
toMaybe'Enum 473 = Prelude'.Just DOTA_UM_DodgeTrackingProjectiles
toMaybe'Enum 474 = Prelude'.Just DOTA_UM_GlobalLightColor
toMaybe'Enum 475 = Prelude'.Just DOTA_UM_GlobalLightDirection
toMaybe'Enum 476 = Prelude'.Just DOTA_UM_InvalidCommand
toMaybe'Enum 477 = Prelude'.Just DOTA_UM_LocationPing
toMaybe'Enum 478 = Prelude'.Just DOTA_UM_MapLine
toMaybe'Enum 479 = Prelude'.Just DOTA_UM_MiniKillCamInfo
toMaybe'Enum 480 = Prelude'.Just DOTA_UM_MinimapDebugPoint
toMaybe'Enum 481 = Prelude'.Just DOTA_UM_MinimapEvent
toMaybe'Enum 482 = Prelude'.Just DOTA_UM_NevermoreRequiem
toMaybe'Enum 483 = Prelude'.Just DOTA_UM_OverheadEvent
toMaybe'Enum 484 = Prelude'.Just DOTA_UM_SetNextAutobuyItem
toMaybe'Enum 485 = Prelude'.Just DOTA_UM_SharedCooldown
toMaybe'Enum 486 = Prelude'.Just DOTA_UM_SpectatorPlayerClick
toMaybe'Enum 487 = Prelude'.Just DOTA_UM_TutorialTipInfo
toMaybe'Enum 488 = Prelude'.Just DOTA_UM_UnitEvent
toMaybe'Enum 489 = Prelude'.Just DOTA_UM_ParticleManager
toMaybe'Enum 490 = Prelude'.Just DOTA_UM_BotChat
toMaybe'Enum 491 = Prelude'.Just DOTA_UM_HudError
toMaybe'Enum 492 = Prelude'.Just DOTA_UM_ItemPurchased
toMaybe'Enum 493 = Prelude'.Just DOTA_UM_Ping
toMaybe'Enum 494 = Prelude'.Just DOTA_UM_ItemFound
toMaybe'Enum 495 = Prelude'.Just DOTA_UM_CharacterSpeakConcept
toMaybe'Enum 496 = Prelude'.Just DOTA_UM_SwapVerify
toMaybe'Enum 497 = Prelude'.Just DOTA_UM_WorldLine
toMaybe'Enum 498 = Prelude'.Just DOTA_UM_TournamentDrop
toMaybe'Enum 499 = Prelude'.Just DOTA_UM_ItemAlert
toMaybe'Enum 500 = Prelude'.Just DOTA_UM_HalloweenDrops
toMaybe'Enum 501 = Prelude'.Just DOTA_UM_ChatWheel
toMaybe'Enum 502 = Prelude'.Just DOTA_UM_ReceivedXmasGift
toMaybe'Enum 503 = Prelude'.Just DOTA_UM_UpdateSharedContent
toMaybe'Enum 504 = Prelude'.Just DOTA_UM_TutorialRequestExp
toMaybe'Enum 505 = Prelude'.Just DOTA_UM_TutorialPingMinimap
toMaybe'Enum 506 = Prelude'.Just DOTA_UM_GamerulesStateChanged
toMaybe'Enum 507 = Prelude'.Just DOTA_UM_ShowSurvey
toMaybe'Enum 508 = Prelude'.Just DOTA_UM_TutorialFade
toMaybe'Enum 509 = Prelude'.Just DOTA_UM_AddQuestLogEntry
toMaybe'Enum 510 = Prelude'.Just DOTA_UM_SendStatPopup
toMaybe'Enum 511 = Prelude'.Just DOTA_UM_TutorialFinish
toMaybe'Enum 512 = Prelude'.Just DOTA_UM_SendRoshanPopup
toMaybe'Enum 513 = Prelude'.Just DOTA_UM_SendGenericToolTip
toMaybe'Enum 514 = Prelude'.Just DOTA_UM_SendFinalGold
toMaybe'Enum 515 = Prelude'.Just DOTA_UM_CustomMsg
toMaybe'Enum 516 = Prelude'.Just DOTA_UM_CoachHUDPing
toMaybe'Enum 517 = Prelude'.Just DOTA_UM_ClientLoadGridNav
toMaybe'Enum 518 = Prelude'.Just DOTA_UM_TE_Projectile
toMaybe'Enum 519 = Prelude'.Just DOTA_UM_TE_ProjectileLoc
toMaybe'Enum 520 = Prelude'.Just DOTA_UM_TE_DotaBloodImpact
toMaybe'Enum 521 = Prelude'.Just DOTA_UM_TE_UnitAnimation
toMaybe'Enum 522 = Prelude'.Just DOTA_UM_TE_UnitAnimationEnd
toMaybe'Enum 523 = Prelude'.Just DOTA_UM_AbilityPing
toMaybe'Enum 524 = Prelude'.Just DOTA_UM_ShowGenericPopup
toMaybe'Enum 525 = Prelude'.Just DOTA_UM_VoteStart
toMaybe'Enum 526 = Prelude'.Just DOTA_UM_VoteUpdate
toMaybe'Enum 527 = Prelude'.Just DOTA_UM_VoteEnd
toMaybe'Enum 528 = Prelude'.Just DOTA_UM_BoosterState
toMaybe'Enum 529 = Prelude'.Just DOTA_UM_WillPurchaseAlert
toMaybe'Enum 530 = Prelude'.Just DOTA_UM_TutorialMinimapPosition
toMaybe'Enum 531 = Prelude'.Just DOTA_UM_PlayerMMR
toMaybe'Enum 532 = Prelude'.Just DOTA_UM_AbilitySteal
toMaybe'Enum 533 = Prelude'.Just DOTA_UM_CourierKilledAlert
toMaybe'Enum 534 = Prelude'.Just DOTA_UM_EnemyItemAlert
toMaybe'Enum 535 = Prelude'.Just DOTA_UM_StatsMatchDetails
toMaybe'Enum 536 = Prelude'.Just DOTA_UM_MiniTaunt
toMaybe'Enum 537 = Prelude'.Just DOTA_UM_BuyBackStateAlert
toMaybe'Enum 538 = Prelude'.Just DOTA_UM_SpeechBubble
toMaybe'Enum 539 = Prelude'.Just DOTA_UM_CustomHeaderMessage
toMaybe'Enum 540 = Prelude'.Just DOTA_UM_QuickBuyAlert
toMaybe'Enum 541 = Prelude'.Just DOTA_UM_StatsHeroDetails
toMaybe'Enum 542 = Prelude'.Just DOTA_UM_PredictionResult
toMaybe'Enum 543 = Prelude'.Just DOTA_UM_ModifierAlert
toMaybe'Enum 544 = Prelude'.Just DOTA_UM_HPManaAlert
toMaybe'Enum 545 = Prelude'.Just DOTA_UM_GlyphAlert
toMaybe'Enum 546 = Prelude'.Just DOTA_UM_BeastChat
toMaybe'Enum 547 = Prelude'.Just DOTA_UM_SpectatorPlayerUnitOrders
toMaybe'Enum 548 = Prelude'.Just DOTA_UM_CustomHudElement_Create
toMaybe'Enum 549 = Prelude'.Just DOTA_UM_CustomHudElement_Modify
toMaybe'Enum 550 = Prelude'.Just DOTA_UM_CustomHudElement_Destroy
toMaybe'Enum 551 = Prelude'.Just DOTA_UM_CompendiumState
toMaybe'Enum 552 = Prelude'.Just DOTA_UM_ProjectionAbility
toMaybe'Enum 553 = Prelude'.Just DOTA_UM_ProjectionEvent
toMaybe'Enum 554 = Prelude'.Just DOTA_UM_CombatLogDataHLTV
toMaybe'Enum 555 = Prelude'.Just DOTA_UM_XPAlert
toMaybe'Enum 556 = Prelude'.Just DOTA_UM_UpdateQuestProgress
toMaybe'Enum 557 = Prelude'.Just DOTA_UM_MatchMetadata
toMaybe'Enum 558 = Prelude'.Just DOTA_UM_MatchDetails
toMaybe'Enum 559 = Prelude'.Just DOTA_UM_QuestStatus
toMaybe'Enum 560 = Prelude'.Just DOTA_UM_SuggestHeroPick
toMaybe'Enum 561 = Prelude'.Just DOTA_UM_SuggestHeroRole
toMaybe'Enum 562 = Prelude'.Just DOTA_UM_KillcamDamageTaken
toMaybe'Enum 563 = Prelude'.Just DOTA_UM_SelectPenaltyGold
toMaybe'Enum 564 = Prelude'.Just DOTA_UM_RollDiceResult
toMaybe'Enum 565 = Prelude'.Just DOTA_UM_FlipCoinResult
toMaybe'Enum 566 = Prelude'.Just DOTA_UM_RequestItemSuggestions
toMaybe'Enum 567 = Prelude'.Just DOTA_UM_TeamCaptainChanged
toMaybe'Enum 568 = Prelude'.Just DOTA_UM_SendRoshanSpectatorPhase
toMaybe'Enum 569 = Prelude'.Just DOTA_UM_ChatWheelCooldown
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EDotaUserMessages where
  fromEnum DOTA_UM_AddUnitToSelection = 464
  fromEnum DOTA_UM_AIDebugLine = 465
  fromEnum DOTA_UM_ChatEvent = 466
  fromEnum DOTA_UM_CombatHeroPositions = 467
  fromEnum DOTA_UM_CombatLogData = 468
  fromEnum DOTA_UM_CombatLogShowDeath = 470
  fromEnum DOTA_UM_CreateLinearProjectile = 471
  fromEnum DOTA_UM_DestroyLinearProjectile = 472
  fromEnum DOTA_UM_DodgeTrackingProjectiles = 473
  fromEnum DOTA_UM_GlobalLightColor = 474
  fromEnum DOTA_UM_GlobalLightDirection = 475
  fromEnum DOTA_UM_InvalidCommand = 476
  fromEnum DOTA_UM_LocationPing = 477
  fromEnum DOTA_UM_MapLine = 478
  fromEnum DOTA_UM_MiniKillCamInfo = 479
  fromEnum DOTA_UM_MinimapDebugPoint = 480
  fromEnum DOTA_UM_MinimapEvent = 481
  fromEnum DOTA_UM_NevermoreRequiem = 482
  fromEnum DOTA_UM_OverheadEvent = 483
  fromEnum DOTA_UM_SetNextAutobuyItem = 484
  fromEnum DOTA_UM_SharedCooldown = 485
  fromEnum DOTA_UM_SpectatorPlayerClick = 486
  fromEnum DOTA_UM_TutorialTipInfo = 487
  fromEnum DOTA_UM_UnitEvent = 488
  fromEnum DOTA_UM_ParticleManager = 489
  fromEnum DOTA_UM_BotChat = 490
  fromEnum DOTA_UM_HudError = 491
  fromEnum DOTA_UM_ItemPurchased = 492
  fromEnum DOTA_UM_Ping = 493
  fromEnum DOTA_UM_ItemFound = 494
  fromEnum DOTA_UM_CharacterSpeakConcept = 495
  fromEnum DOTA_UM_SwapVerify = 496
  fromEnum DOTA_UM_WorldLine = 497
  fromEnum DOTA_UM_TournamentDrop = 498
  fromEnum DOTA_UM_ItemAlert = 499
  fromEnum DOTA_UM_HalloweenDrops = 500
  fromEnum DOTA_UM_ChatWheel = 501
  fromEnum DOTA_UM_ReceivedXmasGift = 502
  fromEnum DOTA_UM_UpdateSharedContent = 503
  fromEnum DOTA_UM_TutorialRequestExp = 504
  fromEnum DOTA_UM_TutorialPingMinimap = 505
  fromEnum DOTA_UM_GamerulesStateChanged = 506
  fromEnum DOTA_UM_ShowSurvey = 507
  fromEnum DOTA_UM_TutorialFade = 508
  fromEnum DOTA_UM_AddQuestLogEntry = 509
  fromEnum DOTA_UM_SendStatPopup = 510
  fromEnum DOTA_UM_TutorialFinish = 511
  fromEnum DOTA_UM_SendRoshanPopup = 512
  fromEnum DOTA_UM_SendGenericToolTip = 513
  fromEnum DOTA_UM_SendFinalGold = 514
  fromEnum DOTA_UM_CustomMsg = 515
  fromEnum DOTA_UM_CoachHUDPing = 516
  fromEnum DOTA_UM_ClientLoadGridNav = 517
  fromEnum DOTA_UM_TE_Projectile = 518
  fromEnum DOTA_UM_TE_ProjectileLoc = 519
  fromEnum DOTA_UM_TE_DotaBloodImpact = 520
  fromEnum DOTA_UM_TE_UnitAnimation = 521
  fromEnum DOTA_UM_TE_UnitAnimationEnd = 522
  fromEnum DOTA_UM_AbilityPing = 523
  fromEnum DOTA_UM_ShowGenericPopup = 524
  fromEnum DOTA_UM_VoteStart = 525
  fromEnum DOTA_UM_VoteUpdate = 526
  fromEnum DOTA_UM_VoteEnd = 527
  fromEnum DOTA_UM_BoosterState = 528
  fromEnum DOTA_UM_WillPurchaseAlert = 529
  fromEnum DOTA_UM_TutorialMinimapPosition = 530
  fromEnum DOTA_UM_PlayerMMR = 531
  fromEnum DOTA_UM_AbilitySteal = 532
  fromEnum DOTA_UM_CourierKilledAlert = 533
  fromEnum DOTA_UM_EnemyItemAlert = 534
  fromEnum DOTA_UM_StatsMatchDetails = 535
  fromEnum DOTA_UM_MiniTaunt = 536
  fromEnum DOTA_UM_BuyBackStateAlert = 537
  fromEnum DOTA_UM_SpeechBubble = 538
  fromEnum DOTA_UM_CustomHeaderMessage = 539
  fromEnum DOTA_UM_QuickBuyAlert = 540
  fromEnum DOTA_UM_StatsHeroDetails = 541
  fromEnum DOTA_UM_PredictionResult = 542
  fromEnum DOTA_UM_ModifierAlert = 543
  fromEnum DOTA_UM_HPManaAlert = 544
  fromEnum DOTA_UM_GlyphAlert = 545
  fromEnum DOTA_UM_BeastChat = 546
  fromEnum DOTA_UM_SpectatorPlayerUnitOrders = 547
  fromEnum DOTA_UM_CustomHudElement_Create = 548
  fromEnum DOTA_UM_CustomHudElement_Modify = 549
  fromEnum DOTA_UM_CustomHudElement_Destroy = 550
  fromEnum DOTA_UM_CompendiumState = 551
  fromEnum DOTA_UM_ProjectionAbility = 552
  fromEnum DOTA_UM_ProjectionEvent = 553
  fromEnum DOTA_UM_CombatLogDataHLTV = 554
  fromEnum DOTA_UM_XPAlert = 555
  fromEnum DOTA_UM_UpdateQuestProgress = 556
  fromEnum DOTA_UM_MatchMetadata = 557
  fromEnum DOTA_UM_MatchDetails = 558
  fromEnum DOTA_UM_QuestStatus = 559
  fromEnum DOTA_UM_SuggestHeroPick = 560
  fromEnum DOTA_UM_SuggestHeroRole = 561
  fromEnum DOTA_UM_KillcamDamageTaken = 562
  fromEnum DOTA_UM_SelectPenaltyGold = 563
  fromEnum DOTA_UM_RollDiceResult = 564
  fromEnum DOTA_UM_FlipCoinResult = 565
  fromEnum DOTA_UM_RequestItemSuggestions = 566
  fromEnum DOTA_UM_TeamCaptainChanged = 567
  fromEnum DOTA_UM_SendRoshanSpectatorPhase = 568
  fromEnum DOTA_UM_ChatWheelCooldown = 569
  toEnum
   = P'.fromMaybe
      (Prelude'.error
        "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2DotaUserMessages.EDotaUserMessages")
      . toMaybe'Enum
  succ DOTA_UM_AddUnitToSelection = DOTA_UM_AIDebugLine
  succ DOTA_UM_AIDebugLine = DOTA_UM_ChatEvent
  succ DOTA_UM_ChatEvent = DOTA_UM_CombatHeroPositions
  succ DOTA_UM_CombatHeroPositions = DOTA_UM_CombatLogData
  succ DOTA_UM_CombatLogData = DOTA_UM_CombatLogShowDeath
  succ DOTA_UM_CombatLogShowDeath = DOTA_UM_CreateLinearProjectile
  succ DOTA_UM_CreateLinearProjectile = DOTA_UM_DestroyLinearProjectile
  succ DOTA_UM_DestroyLinearProjectile = DOTA_UM_DodgeTrackingProjectiles
  succ DOTA_UM_DodgeTrackingProjectiles = DOTA_UM_GlobalLightColor
  succ DOTA_UM_GlobalLightColor = DOTA_UM_GlobalLightDirection
  succ DOTA_UM_GlobalLightDirection = DOTA_UM_InvalidCommand
  succ DOTA_UM_InvalidCommand = DOTA_UM_LocationPing
  succ DOTA_UM_LocationPing = DOTA_UM_MapLine
  succ DOTA_UM_MapLine = DOTA_UM_MiniKillCamInfo
  succ DOTA_UM_MiniKillCamInfo = DOTA_UM_MinimapDebugPoint
  succ DOTA_UM_MinimapDebugPoint = DOTA_UM_MinimapEvent
  succ DOTA_UM_MinimapEvent = DOTA_UM_NevermoreRequiem
  succ DOTA_UM_NevermoreRequiem = DOTA_UM_OverheadEvent
  succ DOTA_UM_OverheadEvent = DOTA_UM_SetNextAutobuyItem
  succ DOTA_UM_SetNextAutobuyItem = DOTA_UM_SharedCooldown
  succ DOTA_UM_SharedCooldown = DOTA_UM_SpectatorPlayerClick
  succ DOTA_UM_SpectatorPlayerClick = DOTA_UM_TutorialTipInfo
  succ DOTA_UM_TutorialTipInfo = DOTA_UM_UnitEvent
  succ DOTA_UM_UnitEvent = DOTA_UM_ParticleManager
  succ DOTA_UM_ParticleManager = DOTA_UM_BotChat
  succ DOTA_UM_BotChat = DOTA_UM_HudError
  succ DOTA_UM_HudError = DOTA_UM_ItemPurchased
  succ DOTA_UM_ItemPurchased = DOTA_UM_Ping
  succ DOTA_UM_Ping = DOTA_UM_ItemFound
  succ DOTA_UM_ItemFound = DOTA_UM_CharacterSpeakConcept
  succ DOTA_UM_CharacterSpeakConcept = DOTA_UM_SwapVerify
  succ DOTA_UM_SwapVerify = DOTA_UM_WorldLine
  succ DOTA_UM_WorldLine = DOTA_UM_TournamentDrop
  succ DOTA_UM_TournamentDrop = DOTA_UM_ItemAlert
  succ DOTA_UM_ItemAlert = DOTA_UM_HalloweenDrops
  succ DOTA_UM_HalloweenDrops = DOTA_UM_ChatWheel
  succ DOTA_UM_ChatWheel = DOTA_UM_ReceivedXmasGift
  succ DOTA_UM_ReceivedXmasGift = DOTA_UM_UpdateSharedContent
  succ DOTA_UM_UpdateSharedContent = DOTA_UM_TutorialRequestExp
  succ DOTA_UM_TutorialRequestExp = DOTA_UM_TutorialPingMinimap
  succ DOTA_UM_TutorialPingMinimap = DOTA_UM_GamerulesStateChanged
  succ DOTA_UM_GamerulesStateChanged = DOTA_UM_ShowSurvey
  succ DOTA_UM_ShowSurvey = DOTA_UM_TutorialFade
  succ DOTA_UM_TutorialFade = DOTA_UM_AddQuestLogEntry
  succ DOTA_UM_AddQuestLogEntry = DOTA_UM_SendStatPopup
  succ DOTA_UM_SendStatPopup = DOTA_UM_TutorialFinish
  succ DOTA_UM_TutorialFinish = DOTA_UM_SendRoshanPopup
  succ DOTA_UM_SendRoshanPopup = DOTA_UM_SendGenericToolTip
  succ DOTA_UM_SendGenericToolTip = DOTA_UM_SendFinalGold
  succ DOTA_UM_SendFinalGold = DOTA_UM_CustomMsg
  succ DOTA_UM_CustomMsg = DOTA_UM_CoachHUDPing
  succ DOTA_UM_CoachHUDPing = DOTA_UM_ClientLoadGridNav
  succ DOTA_UM_ClientLoadGridNav = DOTA_UM_TE_Projectile
  succ DOTA_UM_TE_Projectile = DOTA_UM_TE_ProjectileLoc
  succ DOTA_UM_TE_ProjectileLoc = DOTA_UM_TE_DotaBloodImpact
  succ DOTA_UM_TE_DotaBloodImpact = DOTA_UM_TE_UnitAnimation
  succ DOTA_UM_TE_UnitAnimation = DOTA_UM_TE_UnitAnimationEnd
  succ DOTA_UM_TE_UnitAnimationEnd = DOTA_UM_AbilityPing
  succ DOTA_UM_AbilityPing = DOTA_UM_ShowGenericPopup
  succ DOTA_UM_ShowGenericPopup = DOTA_UM_VoteStart
  succ DOTA_UM_VoteStart = DOTA_UM_VoteUpdate
  succ DOTA_UM_VoteUpdate = DOTA_UM_VoteEnd
  succ DOTA_UM_VoteEnd = DOTA_UM_BoosterState
  succ DOTA_UM_BoosterState = DOTA_UM_WillPurchaseAlert
  succ DOTA_UM_WillPurchaseAlert = DOTA_UM_TutorialMinimapPosition
  succ DOTA_UM_TutorialMinimapPosition = DOTA_UM_PlayerMMR
  succ DOTA_UM_PlayerMMR = DOTA_UM_AbilitySteal
  succ DOTA_UM_AbilitySteal = DOTA_UM_CourierKilledAlert
  succ DOTA_UM_CourierKilledAlert = DOTA_UM_EnemyItemAlert
  succ DOTA_UM_EnemyItemAlert = DOTA_UM_StatsMatchDetails
  succ DOTA_UM_StatsMatchDetails = DOTA_UM_MiniTaunt
  succ DOTA_UM_MiniTaunt = DOTA_UM_BuyBackStateAlert
  succ DOTA_UM_BuyBackStateAlert = DOTA_UM_SpeechBubble
  succ DOTA_UM_SpeechBubble = DOTA_UM_CustomHeaderMessage
  succ DOTA_UM_CustomHeaderMessage = DOTA_UM_QuickBuyAlert
  succ DOTA_UM_QuickBuyAlert = DOTA_UM_StatsHeroDetails
  succ DOTA_UM_StatsHeroDetails = DOTA_UM_PredictionResult
  succ DOTA_UM_PredictionResult = DOTA_UM_ModifierAlert
  succ DOTA_UM_ModifierAlert = DOTA_UM_HPManaAlert
  succ DOTA_UM_HPManaAlert = DOTA_UM_GlyphAlert
  succ DOTA_UM_GlyphAlert = DOTA_UM_BeastChat
  succ DOTA_UM_BeastChat = DOTA_UM_SpectatorPlayerUnitOrders
  succ DOTA_UM_SpectatorPlayerUnitOrders = DOTA_UM_CustomHudElement_Create
  succ DOTA_UM_CustomHudElement_Create = DOTA_UM_CustomHudElement_Modify
  succ DOTA_UM_CustomHudElement_Modify = DOTA_UM_CustomHudElement_Destroy
  succ DOTA_UM_CustomHudElement_Destroy = DOTA_UM_CompendiumState
  succ DOTA_UM_CompendiumState = DOTA_UM_ProjectionAbility
  succ DOTA_UM_ProjectionAbility = DOTA_UM_ProjectionEvent
  succ DOTA_UM_ProjectionEvent = DOTA_UM_CombatLogDataHLTV
  succ DOTA_UM_CombatLogDataHLTV = DOTA_UM_XPAlert
  succ DOTA_UM_XPAlert = DOTA_UM_UpdateQuestProgress
  succ DOTA_UM_UpdateQuestProgress = DOTA_UM_MatchMetadata
  succ DOTA_UM_MatchMetadata = DOTA_UM_MatchDetails
  succ DOTA_UM_MatchDetails = DOTA_UM_QuestStatus
  succ DOTA_UM_QuestStatus = DOTA_UM_SuggestHeroPick
  succ DOTA_UM_SuggestHeroPick = DOTA_UM_SuggestHeroRole
  succ DOTA_UM_SuggestHeroRole = DOTA_UM_KillcamDamageTaken
  succ DOTA_UM_KillcamDamageTaken = DOTA_UM_SelectPenaltyGold
  succ DOTA_UM_SelectPenaltyGold = DOTA_UM_RollDiceResult
  succ DOTA_UM_RollDiceResult = DOTA_UM_FlipCoinResult
  succ DOTA_UM_FlipCoinResult = DOTA_UM_RequestItemSuggestions
  succ DOTA_UM_RequestItemSuggestions = DOTA_UM_TeamCaptainChanged
  succ DOTA_UM_TeamCaptainChanged = DOTA_UM_SendRoshanSpectatorPhase
  succ DOTA_UM_SendRoshanSpectatorPhase = DOTA_UM_ChatWheelCooldown
  succ _
   = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2DotaUserMessages.EDotaUserMessages"
  pred DOTA_UM_AIDebugLine = DOTA_UM_AddUnitToSelection
  pred DOTA_UM_ChatEvent = DOTA_UM_AIDebugLine
  pred DOTA_UM_CombatHeroPositions = DOTA_UM_ChatEvent
  pred DOTA_UM_CombatLogData = DOTA_UM_CombatHeroPositions
  pred DOTA_UM_CombatLogShowDeath = DOTA_UM_CombatLogData
  pred DOTA_UM_CreateLinearProjectile = DOTA_UM_CombatLogShowDeath
  pred DOTA_UM_DestroyLinearProjectile = DOTA_UM_CreateLinearProjectile
  pred DOTA_UM_DodgeTrackingProjectiles = DOTA_UM_DestroyLinearProjectile
  pred DOTA_UM_GlobalLightColor = DOTA_UM_DodgeTrackingProjectiles
  pred DOTA_UM_GlobalLightDirection = DOTA_UM_GlobalLightColor
  pred DOTA_UM_InvalidCommand = DOTA_UM_GlobalLightDirection
  pred DOTA_UM_LocationPing = DOTA_UM_InvalidCommand
  pred DOTA_UM_MapLine = DOTA_UM_LocationPing
  pred DOTA_UM_MiniKillCamInfo = DOTA_UM_MapLine
  pred DOTA_UM_MinimapDebugPoint = DOTA_UM_MiniKillCamInfo
  pred DOTA_UM_MinimapEvent = DOTA_UM_MinimapDebugPoint
  pred DOTA_UM_NevermoreRequiem = DOTA_UM_MinimapEvent
  pred DOTA_UM_OverheadEvent = DOTA_UM_NevermoreRequiem
  pred DOTA_UM_SetNextAutobuyItem = DOTA_UM_OverheadEvent
  pred DOTA_UM_SharedCooldown = DOTA_UM_SetNextAutobuyItem
  pred DOTA_UM_SpectatorPlayerClick = DOTA_UM_SharedCooldown
  pred DOTA_UM_TutorialTipInfo = DOTA_UM_SpectatorPlayerClick
  pred DOTA_UM_UnitEvent = DOTA_UM_TutorialTipInfo
  pred DOTA_UM_ParticleManager = DOTA_UM_UnitEvent
  pred DOTA_UM_BotChat = DOTA_UM_ParticleManager
  pred DOTA_UM_HudError = DOTA_UM_BotChat
  pred DOTA_UM_ItemPurchased = DOTA_UM_HudError
  pred DOTA_UM_Ping = DOTA_UM_ItemPurchased
  pred DOTA_UM_ItemFound = DOTA_UM_Ping
  pred DOTA_UM_CharacterSpeakConcept = DOTA_UM_ItemFound
  pred DOTA_UM_SwapVerify = DOTA_UM_CharacterSpeakConcept
  pred DOTA_UM_WorldLine = DOTA_UM_SwapVerify
  pred DOTA_UM_TournamentDrop = DOTA_UM_WorldLine
  pred DOTA_UM_ItemAlert = DOTA_UM_TournamentDrop
  pred DOTA_UM_HalloweenDrops = DOTA_UM_ItemAlert
  pred DOTA_UM_ChatWheel = DOTA_UM_HalloweenDrops
  pred DOTA_UM_ReceivedXmasGift = DOTA_UM_ChatWheel
  pred DOTA_UM_UpdateSharedContent = DOTA_UM_ReceivedXmasGift
  pred DOTA_UM_TutorialRequestExp = DOTA_UM_UpdateSharedContent
  pred DOTA_UM_TutorialPingMinimap = DOTA_UM_TutorialRequestExp
  pred DOTA_UM_GamerulesStateChanged = DOTA_UM_TutorialPingMinimap
  pred DOTA_UM_ShowSurvey = DOTA_UM_GamerulesStateChanged
  pred DOTA_UM_TutorialFade = DOTA_UM_ShowSurvey
  pred DOTA_UM_AddQuestLogEntry = DOTA_UM_TutorialFade
  pred DOTA_UM_SendStatPopup = DOTA_UM_AddQuestLogEntry
  pred DOTA_UM_TutorialFinish = DOTA_UM_SendStatPopup
  pred DOTA_UM_SendRoshanPopup = DOTA_UM_TutorialFinish
  pred DOTA_UM_SendGenericToolTip = DOTA_UM_SendRoshanPopup
  pred DOTA_UM_SendFinalGold = DOTA_UM_SendGenericToolTip
  pred DOTA_UM_CustomMsg = DOTA_UM_SendFinalGold
  pred DOTA_UM_CoachHUDPing = DOTA_UM_CustomMsg
  pred DOTA_UM_ClientLoadGridNav = DOTA_UM_CoachHUDPing
  pred DOTA_UM_TE_Projectile = DOTA_UM_ClientLoadGridNav
  pred DOTA_UM_TE_ProjectileLoc = DOTA_UM_TE_Projectile
  pred DOTA_UM_TE_DotaBloodImpact = DOTA_UM_TE_ProjectileLoc
  pred DOTA_UM_TE_UnitAnimation = DOTA_UM_TE_DotaBloodImpact
  pred DOTA_UM_TE_UnitAnimationEnd = DOTA_UM_TE_UnitAnimation
  pred DOTA_UM_AbilityPing = DOTA_UM_TE_UnitAnimationEnd
  pred DOTA_UM_ShowGenericPopup = DOTA_UM_AbilityPing
  pred DOTA_UM_VoteStart = DOTA_UM_ShowGenericPopup
  pred DOTA_UM_VoteUpdate = DOTA_UM_VoteStart
  pred DOTA_UM_VoteEnd = DOTA_UM_VoteUpdate
  pred DOTA_UM_BoosterState = DOTA_UM_VoteEnd
  pred DOTA_UM_WillPurchaseAlert = DOTA_UM_BoosterState
  pred DOTA_UM_TutorialMinimapPosition = DOTA_UM_WillPurchaseAlert
  pred DOTA_UM_PlayerMMR = DOTA_UM_TutorialMinimapPosition
  pred DOTA_UM_AbilitySteal = DOTA_UM_PlayerMMR
  pred DOTA_UM_CourierKilledAlert = DOTA_UM_AbilitySteal
  pred DOTA_UM_EnemyItemAlert = DOTA_UM_CourierKilledAlert
  pred DOTA_UM_StatsMatchDetails = DOTA_UM_EnemyItemAlert
  pred DOTA_UM_MiniTaunt = DOTA_UM_StatsMatchDetails
  pred DOTA_UM_BuyBackStateAlert = DOTA_UM_MiniTaunt
  pred DOTA_UM_SpeechBubble = DOTA_UM_BuyBackStateAlert
  pred DOTA_UM_CustomHeaderMessage = DOTA_UM_SpeechBubble
  pred DOTA_UM_QuickBuyAlert = DOTA_UM_CustomHeaderMessage
  pred DOTA_UM_StatsHeroDetails = DOTA_UM_QuickBuyAlert
  pred DOTA_UM_PredictionResult = DOTA_UM_StatsHeroDetails
  pred DOTA_UM_ModifierAlert = DOTA_UM_PredictionResult
  pred DOTA_UM_HPManaAlert = DOTA_UM_ModifierAlert
  pred DOTA_UM_GlyphAlert = DOTA_UM_HPManaAlert
  pred DOTA_UM_BeastChat = DOTA_UM_GlyphAlert
  pred DOTA_UM_SpectatorPlayerUnitOrders = DOTA_UM_BeastChat
  pred DOTA_UM_CustomHudElement_Create = DOTA_UM_SpectatorPlayerUnitOrders
  pred DOTA_UM_CustomHudElement_Modify = DOTA_UM_CustomHudElement_Create
  pred DOTA_UM_CustomHudElement_Destroy = DOTA_UM_CustomHudElement_Modify
  pred DOTA_UM_CompendiumState = DOTA_UM_CustomHudElement_Destroy
  pred DOTA_UM_ProjectionAbility = DOTA_UM_CompendiumState
  pred DOTA_UM_ProjectionEvent = DOTA_UM_ProjectionAbility
  pred DOTA_UM_CombatLogDataHLTV = DOTA_UM_ProjectionEvent
  pred DOTA_UM_XPAlert = DOTA_UM_CombatLogDataHLTV
  pred DOTA_UM_UpdateQuestProgress = DOTA_UM_XPAlert
  pred DOTA_UM_MatchMetadata = DOTA_UM_UpdateQuestProgress
  pred DOTA_UM_MatchDetails = DOTA_UM_MatchMetadata
  pred DOTA_UM_QuestStatus = DOTA_UM_MatchDetails
  pred DOTA_UM_SuggestHeroPick = DOTA_UM_QuestStatus
  pred DOTA_UM_SuggestHeroRole = DOTA_UM_SuggestHeroPick
  pred DOTA_UM_KillcamDamageTaken = DOTA_UM_SuggestHeroRole
  pred DOTA_UM_SelectPenaltyGold = DOTA_UM_KillcamDamageTaken
  pred DOTA_UM_RollDiceResult = DOTA_UM_SelectPenaltyGold
  pred DOTA_UM_FlipCoinResult = DOTA_UM_RollDiceResult
  pred DOTA_UM_RequestItemSuggestions = DOTA_UM_FlipCoinResult
  pred DOTA_UM_TeamCaptainChanged = DOTA_UM_RequestItemSuggestions
  pred DOTA_UM_SendRoshanSpectatorPhase = DOTA_UM_TeamCaptainChanged
  pred DOTA_UM_ChatWheelCooldown = DOTA_UM_SendRoshanSpectatorPhase
  pred _
   = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2DotaUserMessages.EDotaUserMessages"

instance P'.Wire EDotaUserMessages where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EDotaUserMessages

instance P'.MessageAPI msg' (msg' -> EDotaUserMessages) EDotaUserMessages where
  getVal m' f' = f' m'

instance P'.ReflectEnum EDotaUserMessages where
  reflectEnum
   = [(464, "DOTA_UM_AddUnitToSelection", DOTA_UM_AddUnitToSelection), (465, "DOTA_UM_AIDebugLine", DOTA_UM_AIDebugLine),
      (466, "DOTA_UM_ChatEvent", DOTA_UM_ChatEvent), (467, "DOTA_UM_CombatHeroPositions", DOTA_UM_CombatHeroPositions),
      (468, "DOTA_UM_CombatLogData", DOTA_UM_CombatLogData), (470, "DOTA_UM_CombatLogShowDeath", DOTA_UM_CombatLogShowDeath),
      (471, "DOTA_UM_CreateLinearProjectile", DOTA_UM_CreateLinearProjectile),
      (472, "DOTA_UM_DestroyLinearProjectile", DOTA_UM_DestroyLinearProjectile),
      (473, "DOTA_UM_DodgeTrackingProjectiles", DOTA_UM_DodgeTrackingProjectiles),
      (474, "DOTA_UM_GlobalLightColor", DOTA_UM_GlobalLightColor),
      (475, "DOTA_UM_GlobalLightDirection", DOTA_UM_GlobalLightDirection), (476, "DOTA_UM_InvalidCommand", DOTA_UM_InvalidCommand),
      (477, "DOTA_UM_LocationPing", DOTA_UM_LocationPing), (478, "DOTA_UM_MapLine", DOTA_UM_MapLine),
      (479, "DOTA_UM_MiniKillCamInfo", DOTA_UM_MiniKillCamInfo), (480, "DOTA_UM_MinimapDebugPoint", DOTA_UM_MinimapDebugPoint),
      (481, "DOTA_UM_MinimapEvent", DOTA_UM_MinimapEvent), (482, "DOTA_UM_NevermoreRequiem", DOTA_UM_NevermoreRequiem),
      (483, "DOTA_UM_OverheadEvent", DOTA_UM_OverheadEvent), (484, "DOTA_UM_SetNextAutobuyItem", DOTA_UM_SetNextAutobuyItem),
      (485, "DOTA_UM_SharedCooldown", DOTA_UM_SharedCooldown), (486, "DOTA_UM_SpectatorPlayerClick", DOTA_UM_SpectatorPlayerClick),
      (487, "DOTA_UM_TutorialTipInfo", DOTA_UM_TutorialTipInfo), (488, "DOTA_UM_UnitEvent", DOTA_UM_UnitEvent),
      (489, "DOTA_UM_ParticleManager", DOTA_UM_ParticleManager), (490, "DOTA_UM_BotChat", DOTA_UM_BotChat),
      (491, "DOTA_UM_HudError", DOTA_UM_HudError), (492, "DOTA_UM_ItemPurchased", DOTA_UM_ItemPurchased),
      (493, "DOTA_UM_Ping", DOTA_UM_Ping), (494, "DOTA_UM_ItemFound", DOTA_UM_ItemFound),
      (495, "DOTA_UM_CharacterSpeakConcept", DOTA_UM_CharacterSpeakConcept), (496, "DOTA_UM_SwapVerify", DOTA_UM_SwapVerify),
      (497, "DOTA_UM_WorldLine", DOTA_UM_WorldLine), (498, "DOTA_UM_TournamentDrop", DOTA_UM_TournamentDrop),
      (499, "DOTA_UM_ItemAlert", DOTA_UM_ItemAlert), (500, "DOTA_UM_HalloweenDrops", DOTA_UM_HalloweenDrops),
      (501, "DOTA_UM_ChatWheel", DOTA_UM_ChatWheel), (502, "DOTA_UM_ReceivedXmasGift", DOTA_UM_ReceivedXmasGift),
      (503, "DOTA_UM_UpdateSharedContent", DOTA_UM_UpdateSharedContent),
      (504, "DOTA_UM_TutorialRequestExp", DOTA_UM_TutorialRequestExp),
      (505, "DOTA_UM_TutorialPingMinimap", DOTA_UM_TutorialPingMinimap),
      (506, "DOTA_UM_GamerulesStateChanged", DOTA_UM_GamerulesStateChanged), (507, "DOTA_UM_ShowSurvey", DOTA_UM_ShowSurvey),
      (508, "DOTA_UM_TutorialFade", DOTA_UM_TutorialFade), (509, "DOTA_UM_AddQuestLogEntry", DOTA_UM_AddQuestLogEntry),
      (510, "DOTA_UM_SendStatPopup", DOTA_UM_SendStatPopup), (511, "DOTA_UM_TutorialFinish", DOTA_UM_TutorialFinish),
      (512, "DOTA_UM_SendRoshanPopup", DOTA_UM_SendRoshanPopup), (513, "DOTA_UM_SendGenericToolTip", DOTA_UM_SendGenericToolTip),
      (514, "DOTA_UM_SendFinalGold", DOTA_UM_SendFinalGold), (515, "DOTA_UM_CustomMsg", DOTA_UM_CustomMsg),
      (516, "DOTA_UM_CoachHUDPing", DOTA_UM_CoachHUDPing), (517, "DOTA_UM_ClientLoadGridNav", DOTA_UM_ClientLoadGridNav),
      (518, "DOTA_UM_TE_Projectile", DOTA_UM_TE_Projectile), (519, "DOTA_UM_TE_ProjectileLoc", DOTA_UM_TE_ProjectileLoc),
      (520, "DOTA_UM_TE_DotaBloodImpact", DOTA_UM_TE_DotaBloodImpact), (521, "DOTA_UM_TE_UnitAnimation", DOTA_UM_TE_UnitAnimation),
      (522, "DOTA_UM_TE_UnitAnimationEnd", DOTA_UM_TE_UnitAnimationEnd), (523, "DOTA_UM_AbilityPing", DOTA_UM_AbilityPing),
      (524, "DOTA_UM_ShowGenericPopup", DOTA_UM_ShowGenericPopup), (525, "DOTA_UM_VoteStart", DOTA_UM_VoteStart),
      (526, "DOTA_UM_VoteUpdate", DOTA_UM_VoteUpdate), (527, "DOTA_UM_VoteEnd", DOTA_UM_VoteEnd),
      (528, "DOTA_UM_BoosterState", DOTA_UM_BoosterState), (529, "DOTA_UM_WillPurchaseAlert", DOTA_UM_WillPurchaseAlert),
      (530, "DOTA_UM_TutorialMinimapPosition", DOTA_UM_TutorialMinimapPosition), (531, "DOTA_UM_PlayerMMR", DOTA_UM_PlayerMMR),
      (532, "DOTA_UM_AbilitySteal", DOTA_UM_AbilitySteal), (533, "DOTA_UM_CourierKilledAlert", DOTA_UM_CourierKilledAlert),
      (534, "DOTA_UM_EnemyItemAlert", DOTA_UM_EnemyItemAlert), (535, "DOTA_UM_StatsMatchDetails", DOTA_UM_StatsMatchDetails),
      (536, "DOTA_UM_MiniTaunt", DOTA_UM_MiniTaunt), (537, "DOTA_UM_BuyBackStateAlert", DOTA_UM_BuyBackStateAlert),
      (538, "DOTA_UM_SpeechBubble", DOTA_UM_SpeechBubble), (539, "DOTA_UM_CustomHeaderMessage", DOTA_UM_CustomHeaderMessage),
      (540, "DOTA_UM_QuickBuyAlert", DOTA_UM_QuickBuyAlert), (541, "DOTA_UM_StatsHeroDetails", DOTA_UM_StatsHeroDetails),
      (542, "DOTA_UM_PredictionResult", DOTA_UM_PredictionResult), (543, "DOTA_UM_ModifierAlert", DOTA_UM_ModifierAlert),
      (544, "DOTA_UM_HPManaAlert", DOTA_UM_HPManaAlert), (545, "DOTA_UM_GlyphAlert", DOTA_UM_GlyphAlert),
      (546, "DOTA_UM_BeastChat", DOTA_UM_BeastChat), (547, "DOTA_UM_SpectatorPlayerUnitOrders", DOTA_UM_SpectatorPlayerUnitOrders),
      (548, "DOTA_UM_CustomHudElement_Create", DOTA_UM_CustomHudElement_Create),
      (549, "DOTA_UM_CustomHudElement_Modify", DOTA_UM_CustomHudElement_Modify),
      (550, "DOTA_UM_CustomHudElement_Destroy", DOTA_UM_CustomHudElement_Destroy),
      (551, "DOTA_UM_CompendiumState", DOTA_UM_CompendiumState), (552, "DOTA_UM_ProjectionAbility", DOTA_UM_ProjectionAbility),
      (553, "DOTA_UM_ProjectionEvent", DOTA_UM_ProjectionEvent), (554, "DOTA_UM_CombatLogDataHLTV", DOTA_UM_CombatLogDataHLTV),
      (555, "DOTA_UM_XPAlert", DOTA_UM_XPAlert), (556, "DOTA_UM_UpdateQuestProgress", DOTA_UM_UpdateQuestProgress),
      (557, "DOTA_UM_MatchMetadata", DOTA_UM_MatchMetadata), (558, "DOTA_UM_MatchDetails", DOTA_UM_MatchDetails),
      (559, "DOTA_UM_QuestStatus", DOTA_UM_QuestStatus), (560, "DOTA_UM_SuggestHeroPick", DOTA_UM_SuggestHeroPick),
      (561, "DOTA_UM_SuggestHeroRole", DOTA_UM_SuggestHeroRole), (562, "DOTA_UM_KillcamDamageTaken", DOTA_UM_KillcamDamageTaken),
      (563, "DOTA_UM_SelectPenaltyGold", DOTA_UM_SelectPenaltyGold), (564, "DOTA_UM_RollDiceResult", DOTA_UM_RollDiceResult),
      (565, "DOTA_UM_FlipCoinResult", DOTA_UM_FlipCoinResult),
      (566, "DOTA_UM_RequestItemSuggestions", DOTA_UM_RequestItemSuggestions),
      (567, "DOTA_UM_TeamCaptainChanged", DOTA_UM_TeamCaptainChanged),
      (568, "DOTA_UM_SendRoshanSpectatorPhase", DOTA_UM_SendRoshanSpectatorPhase),
      (569, "DOTA_UM_ChatWheelCooldown", DOTA_UM_ChatWheelCooldown)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Dota_Usermessages.EDotaUserMessages") ["Shrine", "Wire", "Protobuf", "S2"] ["S2DotaUserMessages"]
        "EDotaUserMessages")
      ["Shrine", "Wire", "Protobuf", "S2", "S2DotaUserMessages", "EDotaUserMessages.hs"]
      [(464, "DOTA_UM_AddUnitToSelection"), (465, "DOTA_UM_AIDebugLine"), (466, "DOTA_UM_ChatEvent"),
       (467, "DOTA_UM_CombatHeroPositions"), (468, "DOTA_UM_CombatLogData"), (470, "DOTA_UM_CombatLogShowDeath"),
       (471, "DOTA_UM_CreateLinearProjectile"), (472, "DOTA_UM_DestroyLinearProjectile"), (473, "DOTA_UM_DodgeTrackingProjectiles"),
       (474, "DOTA_UM_GlobalLightColor"), (475, "DOTA_UM_GlobalLightDirection"), (476, "DOTA_UM_InvalidCommand"),
       (477, "DOTA_UM_LocationPing"), (478, "DOTA_UM_MapLine"), (479, "DOTA_UM_MiniKillCamInfo"),
       (480, "DOTA_UM_MinimapDebugPoint"), (481, "DOTA_UM_MinimapEvent"), (482, "DOTA_UM_NevermoreRequiem"),
       (483, "DOTA_UM_OverheadEvent"), (484, "DOTA_UM_SetNextAutobuyItem"), (485, "DOTA_UM_SharedCooldown"),
       (486, "DOTA_UM_SpectatorPlayerClick"), (487, "DOTA_UM_TutorialTipInfo"), (488, "DOTA_UM_UnitEvent"),
       (489, "DOTA_UM_ParticleManager"), (490, "DOTA_UM_BotChat"), (491, "DOTA_UM_HudError"), (492, "DOTA_UM_ItemPurchased"),
       (493, "DOTA_UM_Ping"), (494, "DOTA_UM_ItemFound"), (495, "DOTA_UM_CharacterSpeakConcept"), (496, "DOTA_UM_SwapVerify"),
       (497, "DOTA_UM_WorldLine"), (498, "DOTA_UM_TournamentDrop"), (499, "DOTA_UM_ItemAlert"), (500, "DOTA_UM_HalloweenDrops"),
       (501, "DOTA_UM_ChatWheel"), (502, "DOTA_UM_ReceivedXmasGift"), (503, "DOTA_UM_UpdateSharedContent"),
       (504, "DOTA_UM_TutorialRequestExp"), (505, "DOTA_UM_TutorialPingMinimap"), (506, "DOTA_UM_GamerulesStateChanged"),
       (507, "DOTA_UM_ShowSurvey"), (508, "DOTA_UM_TutorialFade"), (509, "DOTA_UM_AddQuestLogEntry"),
       (510, "DOTA_UM_SendStatPopup"), (511, "DOTA_UM_TutorialFinish"), (512, "DOTA_UM_SendRoshanPopup"),
       (513, "DOTA_UM_SendGenericToolTip"), (514, "DOTA_UM_SendFinalGold"), (515, "DOTA_UM_CustomMsg"),
       (516, "DOTA_UM_CoachHUDPing"), (517, "DOTA_UM_ClientLoadGridNav"), (518, "DOTA_UM_TE_Projectile"),
       (519, "DOTA_UM_TE_ProjectileLoc"), (520, "DOTA_UM_TE_DotaBloodImpact"), (521, "DOTA_UM_TE_UnitAnimation"),
       (522, "DOTA_UM_TE_UnitAnimationEnd"), (523, "DOTA_UM_AbilityPing"), (524, "DOTA_UM_ShowGenericPopup"),
       (525, "DOTA_UM_VoteStart"), (526, "DOTA_UM_VoteUpdate"), (527, "DOTA_UM_VoteEnd"), (528, "DOTA_UM_BoosterState"),
       (529, "DOTA_UM_WillPurchaseAlert"), (530, "DOTA_UM_TutorialMinimapPosition"), (531, "DOTA_UM_PlayerMMR"),
       (532, "DOTA_UM_AbilitySteal"), (533, "DOTA_UM_CourierKilledAlert"), (534, "DOTA_UM_EnemyItemAlert"),
       (535, "DOTA_UM_StatsMatchDetails"), (536, "DOTA_UM_MiniTaunt"), (537, "DOTA_UM_BuyBackStateAlert"),
       (538, "DOTA_UM_SpeechBubble"), (539, "DOTA_UM_CustomHeaderMessage"), (540, "DOTA_UM_QuickBuyAlert"),
       (541, "DOTA_UM_StatsHeroDetails"), (542, "DOTA_UM_PredictionResult"), (543, "DOTA_UM_ModifierAlert"),
       (544, "DOTA_UM_HPManaAlert"), (545, "DOTA_UM_GlyphAlert"), (546, "DOTA_UM_BeastChat"),
       (547, "DOTA_UM_SpectatorPlayerUnitOrders"), (548, "DOTA_UM_CustomHudElement_Create"),
       (549, "DOTA_UM_CustomHudElement_Modify"), (550, "DOTA_UM_CustomHudElement_Destroy"), (551, "DOTA_UM_CompendiumState"),
       (552, "DOTA_UM_ProjectionAbility"), (553, "DOTA_UM_ProjectionEvent"), (554, "DOTA_UM_CombatLogDataHLTV"),
       (555, "DOTA_UM_XPAlert"), (556, "DOTA_UM_UpdateQuestProgress"), (557, "DOTA_UM_MatchMetadata"),
       (558, "DOTA_UM_MatchDetails"), (559, "DOTA_UM_QuestStatus"), (560, "DOTA_UM_SuggestHeroPick"),
       (561, "DOTA_UM_SuggestHeroRole"), (562, "DOTA_UM_KillcamDamageTaken"), (563, "DOTA_UM_SelectPenaltyGold"),
       (564, "DOTA_UM_RollDiceResult"), (565, "DOTA_UM_FlipCoinResult"), (566, "DOTA_UM_RequestItemSuggestions"),
       (567, "DOTA_UM_TeamCaptainChanged"), (568, "DOTA_UM_SendRoshanSpectatorPhase"), (569, "DOTA_UM_ChatWheelCooldown")]

instance P'.TextType EDotaUserMessages where
  tellT = P'.tellShow
  getT = P'.getRead