{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2BaseGcMessages.CSOEconItemEquipped (CSOEconItemEquipped(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CSOEconItemEquipped = CSOEconItemEquipped{new_class :: !(P'.Maybe P'.Word32), new_slot :: !(P'.Maybe P'.Word32)}
                         deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CSOEconItemEquipped where
  mergeAppend (CSOEconItemEquipped x'1 x'2) (CSOEconItemEquipped y'1 y'2)
   = CSOEconItemEquipped (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CSOEconItemEquipped where
  defaultValue = CSOEconItemEquipped P'.defaultValue P'.defaultValue

instance P'.Wire CSOEconItemEquipped where
  wireSize ft' self'@(CSOEconItemEquipped x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 13 x'2)
  wirePut ft' self'@(CSOEconItemEquipped x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 13 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{new_class = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{new_slot = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSOEconItemEquipped) CSOEconItemEquipped where
  getVal m' f' = f' m'

instance P'.GPB CSOEconItemEquipped

instance P'.ReflectDescriptor CSOEconItemEquipped where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Base_Gcmessages.CSOEconItemEquipped\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2BaseGcMessages\"], baseName = MName \"CSOEconItemEquipped\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2BaseGcMessages\",\"CSOEconItemEquipped.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItemEquipped.new_class\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItemEquipped\"], baseName' = FName \"new_class\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItemEquipped.new_slot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItemEquipped\"], baseName' = FName \"new_slot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSOEconItemEquipped where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSOEconItemEquipped where
  textPut msg
   = do
       P'.tellT "new_class" (new_class msg)
       P'.tellT "new_slot" (new_slot msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'new_class, parse'new_slot]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'new_class
         = P'.try
            (do
               v <- P'.getT "new_class"
               Prelude'.return (\ o -> o{new_class = v}))
        parse'new_slot
         = P'.try
            (do
               v <- P'.getT "new_slot"
               Prelude'.return (\ o -> o{new_slot = v}))