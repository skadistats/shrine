{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2BaseGcMessages.CSOEconItem (CSOEconItem(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2BaseGcMessages.CSOEconItemAttribute as S2BaseGcMessages (CSOEconItemAttribute)
import qualified Shrine.Wire.Protobuf.S2.S2BaseGcMessages.CSOEconItemEquipped as S2BaseGcMessages (CSOEconItemEquipped)

data CSOEconItem = CSOEconItem{id :: !(P'.Maybe P'.Word64), account_id :: !(P'.Maybe P'.Word32), inventory :: !(P'.Maybe P'.Word32),
                               def_index :: !(P'.Maybe P'.Word32), quantity :: !(P'.Maybe P'.Word32),
                               level :: !(P'.Maybe P'.Word32), quality :: !(P'.Maybe P'.Word32), flags :: !(P'.Maybe P'.Word32),
                               origin :: !(P'.Maybe P'.Word32), attribute :: !(P'.Seq S2BaseGcMessages.CSOEconItemAttribute),
                               interior_item :: !(P'.Maybe CSOEconItem), in_use :: !(P'.Maybe P'.Bool),
                               style :: !(P'.Maybe P'.Word32), original_id :: !(P'.Maybe P'.Word64),
                               equipped_state :: !(P'.Seq S2BaseGcMessages.CSOEconItemEquipped)}
                 deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CSOEconItem where
  mergeAppend (CSOEconItem x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15)
   (CSOEconItem y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15)
   = CSOEconItem (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)

instance P'.Default CSOEconItem where
  defaultValue
   = CSOEconItem P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue (Prelude'.Just 1) (Prelude'.Just 1)
      (Prelude'.Just 4)
      (Prelude'.Just 0)
      (Prelude'.Just 0)
      P'.defaultValue
      P'.defaultValue
      (Prelude'.Just Prelude'.False)
      (Prelude'.Just 0)
      (Prelude'.Just 0)
      P'.defaultValue

instance P'.Wire CSOEconItem where
  wireSize ft' self'@(CSOEconItem x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 4 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 13 x'3 + P'.wireSizeOpt 1 13 x'4 +
             P'.wireSizeOpt 1 13 x'5
             + P'.wireSizeOpt 1 13 x'6
             + P'.wireSizeOpt 1 13 x'7
             + P'.wireSizeOpt 1 13 x'8
             + P'.wireSizeOpt 1 13 x'9
             + P'.wireSizeRep 1 11 x'10
             + P'.wireSizeOpt 1 11 x'11
             + P'.wireSizeOpt 1 8 x'12
             + P'.wireSizeOpt 1 13 x'13
             + P'.wireSizeOpt 2 4 x'14
             + P'.wireSizeRep 2 11 x'15)
  wirePut ft' self'@(CSOEconItem x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 4 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 13 x'3
             P'.wirePutOpt 32 13 x'4
             P'.wirePutOpt 40 13 x'5
             P'.wirePutOpt 48 13 x'6
             P'.wirePutOpt 56 13 x'7
             P'.wirePutOpt 64 13 x'8
             P'.wirePutOpt 72 13 x'9
             P'.wirePutRep 98 11 x'10
             P'.wirePutOpt 106 11 x'11
             P'.wirePutOpt 112 8 x'12
             P'.wirePutOpt 120 13 x'13
             P'.wirePutOpt 128 4 x'14
             P'.wirePutRep 146 11 x'15
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{id = Prelude'.Just new'Field}) (P'.wireGet 4)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{account_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{inventory = Prelude'.Just new'Field}) (P'.wireGet 13)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{def_index = Prelude'.Just new'Field}) (P'.wireGet 13)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{quantity = Prelude'.Just new'Field}) (P'.wireGet 13)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{level = Prelude'.Just new'Field}) (P'.wireGet 13)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{quality = Prelude'.Just new'Field}) (P'.wireGet 13)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{flags = Prelude'.Just new'Field}) (P'.wireGet 13)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{origin = Prelude'.Just new'Field}) (P'.wireGet 13)
             98 -> Prelude'.fmap (\ !new'Field -> old'Self{attribute = P'.append (attribute old'Self) new'Field}) (P'.wireGet 11)
             106 -> Prelude'.fmap
                     (\ !new'Field -> old'Self{interior_item = P'.mergeAppend (interior_item old'Self) (Prelude'.Just new'Field)})
                     (P'.wireGet 11)
             112 -> Prelude'.fmap (\ !new'Field -> old'Self{in_use = Prelude'.Just new'Field}) (P'.wireGet 8)
             120 -> Prelude'.fmap (\ !new'Field -> old'Self{style = Prelude'.Just new'Field}) (P'.wireGet 13)
             128 -> Prelude'.fmap (\ !new'Field -> old'Self{original_id = Prelude'.Just new'Field}) (P'.wireGet 4)
             146 -> Prelude'.fmap (\ !new'Field -> old'Self{equipped_state = P'.append (equipped_state old'Self) new'Field})
                     (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSOEconItem) CSOEconItem where
  getVal m' f' = f' m'

instance P'.GPB CSOEconItem

instance P'.ReflectDescriptor CSOEconItem where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList [8, 16, 24, 32, 40, 48, 56, 64, 72, 98, 106, 112, 120, 128, 146])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Base_Gcmessages.CSOEconItem\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2BaseGcMessages\"], baseName = MName \"CSOEconItem\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2BaseGcMessages\",\"CSOEconItem.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.account_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"account_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.inventory\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"inventory\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.def_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"def_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.quantity\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"quantity\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Just \"1\", hsDefault = Just (HsDef'Integer 1)},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.level\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"level\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Just \"1\", hsDefault = Just (HsDef'Integer 1)},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.quality\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"quality\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Just \"4\", hsDefault = Just (HsDef'Integer 4)},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.flags\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"flags\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Just \"0\", hsDefault = Just (HsDef'Integer 0)},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.origin\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"origin\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Just \"0\", hsDefault = Just (HsDef'Integer 0)},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.attribute\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"attribute\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 98}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Base_Gcmessages.CSOEconItemAttribute\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2BaseGcMessages\"], baseName = MName \"CSOEconItemAttribute\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.interior_item\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"interior_item\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 106}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Base_Gcmessages.CSOEconItem\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2BaseGcMessages\"], baseName = MName \"CSOEconItem\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.in_use\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"in_use\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 112}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Just \"false\", hsDefault = Just (HsDef'Bool False)},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.style\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"style\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 120}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Just \"0\", hsDefault = Just (HsDef'Integer 0)},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.original_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"original_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 128}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 4}, typeName = Nothing, hsRawDefault = Just \"0\", hsDefault = Just (HsDef'Integer 0)},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Base_Gcmessages.CSOEconItem.equipped_state\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2BaseGcMessages\",MName \"CSOEconItem\"], baseName' = FName \"equipped_state\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 146}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Base_Gcmessages.CSOEconItemEquipped\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2BaseGcMessages\"], baseName = MName \"CSOEconItemEquipped\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSOEconItem where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSOEconItem where
  textPut msg
   = do
       P'.tellT "id" (id msg)
       P'.tellT "account_id" (account_id msg)
       P'.tellT "inventory" (inventory msg)
       P'.tellT "def_index" (def_index msg)
       P'.tellT "quantity" (quantity msg)
       P'.tellT "level" (level msg)
       P'.tellT "quality" (quality msg)
       P'.tellT "flags" (flags msg)
       P'.tellT "origin" (origin msg)
       P'.tellT "attribute" (attribute msg)
       P'.tellT "interior_item" (interior_item msg)
       P'.tellT "in_use" (in_use msg)
       P'.tellT "style" (style msg)
       P'.tellT "original_id" (original_id msg)
       P'.tellT "equipped_state" (equipped_state msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'id, parse'account_id, parse'inventory, parse'def_index, parse'quantity, parse'level, parse'quality,
                   parse'flags, parse'origin, parse'attribute, parse'interior_item, parse'in_use, parse'style, parse'original_id,
                   parse'equipped_state])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'id
         = P'.try
            (do
               v <- P'.getT "id"
               Prelude'.return (\ o -> o{id = v}))
        parse'account_id
         = P'.try
            (do
               v <- P'.getT "account_id"
               Prelude'.return (\ o -> o{account_id = v}))
        parse'inventory
         = P'.try
            (do
               v <- P'.getT "inventory"
               Prelude'.return (\ o -> o{inventory = v}))
        parse'def_index
         = P'.try
            (do
               v <- P'.getT "def_index"
               Prelude'.return (\ o -> o{def_index = v}))
        parse'quantity
         = P'.try
            (do
               v <- P'.getT "quantity"
               Prelude'.return (\ o -> o{quantity = v}))
        parse'level
         = P'.try
            (do
               v <- P'.getT "level"
               Prelude'.return (\ o -> o{level = v}))
        parse'quality
         = P'.try
            (do
               v <- P'.getT "quality"
               Prelude'.return (\ o -> o{quality = v}))
        parse'flags
         = P'.try
            (do
               v <- P'.getT "flags"
               Prelude'.return (\ o -> o{flags = v}))
        parse'origin
         = P'.try
            (do
               v <- P'.getT "origin"
               Prelude'.return (\ o -> o{origin = v}))
        parse'attribute
         = P'.try
            (do
               v <- P'.getT "attribute"
               Prelude'.return (\ o -> o{attribute = P'.append (attribute o) v}))
        parse'interior_item
         = P'.try
            (do
               v <- P'.getT "interior_item"
               Prelude'.return (\ o -> o{interior_item = v}))
        parse'in_use
         = P'.try
            (do
               v <- P'.getT "in_use"
               Prelude'.return (\ o -> o{in_use = v}))
        parse'style
         = P'.try
            (do
               v <- P'.getT "style"
               Prelude'.return (\ o -> o{style = v}))
        parse'original_id
         = P'.try
            (do
               v <- P'.getT "original_id"
               Prelude'.return (\ o -> o{original_id = v}))
        parse'equipped_state
         = P'.try
            (do
               v <- P'.getT "equipped_state"
               Prelude'.return (\ o -> o{equipped_state = P'.append (equipped_state o) v}))