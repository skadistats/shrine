{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSosSetSoundEventParams (CMsgSosSetSoundEventParams(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CMsgSosSetSoundEventParams = CMsgSosSetSoundEventParams{soundevent_guid :: !(P'.Maybe P'.Int32),
                                                             packed_params :: !(P'.Maybe P'.ByteString)}
                                deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                          Prelude'.Generic)

instance P'.Mergeable CMsgSosSetSoundEventParams where
  mergeAppend (CMsgSosSetSoundEventParams x'1 x'2) (CMsgSosSetSoundEventParams y'1 y'2)
   = CMsgSosSetSoundEventParams (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMsgSosSetSoundEventParams where
  defaultValue = CMsgSosSetSoundEventParams P'.defaultValue P'.defaultValue

instance P'.Wire CMsgSosSetSoundEventParams where
  wireSize ft' self'@(CMsgSosSetSoundEventParams x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 12 x'2)
  wirePut ft' self'@(CMsgSosSetSoundEventParams x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 42 12 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{soundevent_guid = Prelude'.Just new'Field}) (P'.wireGet 5)
             42 -> Prelude'.fmap (\ !new'Field -> old'Self{packed_params = Prelude'.Just new'Field}) (P'.wireGet 12)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgSosSetSoundEventParams) CMsgSosSetSoundEventParams where
  getVal m' f' = f' m'

instance P'.GPB CMsgSosSetSoundEventParams

instance P'.ReflectDescriptor CMsgSosSetSoundEventParams where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 42])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Gameevents.CMsgSosSetSoundEventParams\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2GameEvents\"], baseName = MName \"CMsgSosSetSoundEventParams\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2GameEvents\",\"CMsgSosSetSoundEventParams.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosSetSoundEventParams.soundevent_guid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosSetSoundEventParams\"], baseName' = FName \"soundevent_guid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosSetSoundEventParams.packed_params\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosSetSoundEventParams\"], baseName' = FName \"packed_params\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgSosSetSoundEventParams where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgSosSetSoundEventParams where
  textPut msg
   = do
       P'.tellT "soundevent_guid" (soundevent_guid msg)
       P'.tellT "packed_params" (packed_params msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'soundevent_guid, parse'packed_params]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'soundevent_guid
         = P'.try
            (do
               v <- P'.getT "soundevent_guid"
               Prelude'.return (\ o -> o{soundevent_guid = v}))
        parse'packed_params
         = P'.try
            (do
               v <- P'.getT "packed_params"
               Prelude'.return (\ o -> o{packed_params = v}))