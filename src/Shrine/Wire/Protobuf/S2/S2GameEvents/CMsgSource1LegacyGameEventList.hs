{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSource1LegacyGameEventList (CMsgSource1LegacyGameEventList(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSource1LegacyGameEventList.Descriptor_t
       as S2GameEvents.CMsgSource1LegacyGameEventList (Descriptor_t)

data CMsgSource1LegacyGameEventList = CMsgSource1LegacyGameEventList{descriptors ::
                                                                     !(P'.Seq
                                                                        S2GameEvents.CMsgSource1LegacyGameEventList.Descriptor_t)}
                                    deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                              Prelude'.Generic)

instance P'.Mergeable CMsgSource1LegacyGameEventList where
  mergeAppend (CMsgSource1LegacyGameEventList x'1) (CMsgSource1LegacyGameEventList y'1)
   = CMsgSource1LegacyGameEventList (P'.mergeAppend x'1 y'1)

instance P'.Default CMsgSource1LegacyGameEventList where
  defaultValue = CMsgSource1LegacyGameEventList P'.defaultValue

instance P'.Wire CMsgSource1LegacyGameEventList where
  wireSize ft' self'@(CMsgSource1LegacyGameEventList x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 11 x'1)
  wirePut ft' self'@(CMsgSource1LegacyGameEventList x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 10 11 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{descriptors = P'.append (descriptors old'Self) new'Field})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgSource1LegacyGameEventList) CMsgSource1LegacyGameEventList where
  getVal m' f' = f' m'

instance P'.GPB CMsgSource1LegacyGameEventList

instance P'.ReflectDescriptor CMsgSource1LegacyGameEventList where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Gameevents.CMsgSource1LegacyGameEventList\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2GameEvents\"], baseName = MName \"CMsgSource1LegacyGameEventList\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2GameEvents\",\"CMsgSource1LegacyGameEventList.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSource1LegacyGameEventList.descriptors\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSource1LegacyGameEventList\"], baseName' = FName \"descriptors\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Gameevents.CMsgSource1LegacyGameEventList.descriptor_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2GameEvents\",MName \"CMsgSource1LegacyGameEventList\"], baseName = MName \"Descriptor_t\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgSource1LegacyGameEventList where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgSource1LegacyGameEventList where
  textPut msg
   = do
       P'.tellT "descriptors" (descriptors msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'descriptors]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'descriptors
         = P'.try
            (do
               v <- P'.getT "descriptors"
               Prelude'.return (\ o -> o{descriptors = P'.append (descriptors o) v}))