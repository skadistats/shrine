{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSosStartSoundEvent (CMsgSosStartSoundEvent(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CMsgSosStartSoundEvent = CMsgSosStartSoundEvent{soundevent_guid :: !(P'.Maybe P'.Int32),
                                                     soundevent_hash :: !(P'.Maybe P'.Word32),
                                                     source_entity_index :: !(P'.Maybe P'.Int32), seed :: !(P'.Maybe P'.Int32),
                                                     packed_params :: !(P'.Maybe P'.ByteString), start_time :: !(P'.Maybe P'.Float)}
                            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgSosStartSoundEvent where
  mergeAppend (CMsgSosStartSoundEvent x'1 x'2 x'3 x'4 x'5 x'6) (CMsgSosStartSoundEvent y'1 y'2 y'3 y'4 y'5 y'6)
   = CMsgSosStartSoundEvent (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CMsgSosStartSoundEvent where
  defaultValue
   = CMsgSosStartSoundEvent P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CMsgSosStartSoundEvent where
  wireSize ft' self'@(CMsgSosStartSoundEvent x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 7 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeOpt 1 5 x'4 +
             P'.wireSizeOpt 1 12 x'5
             + P'.wireSizeOpt 1 2 x'6)
  wirePut ft' self'@(CMsgSosStartSoundEvent x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 21 7 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutOpt 32 5 x'4
             P'.wirePutOpt 42 12 x'5
             P'.wirePutOpt 53 2 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{soundevent_guid = Prelude'.Just new'Field}) (P'.wireGet 5)
             21 -> Prelude'.fmap (\ !new'Field -> old'Self{soundevent_hash = Prelude'.Just new'Field}) (P'.wireGet 7)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{source_entity_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{seed = Prelude'.Just new'Field}) (P'.wireGet 5)
             42 -> Prelude'.fmap (\ !new'Field -> old'Self{packed_params = Prelude'.Just new'Field}) (P'.wireGet 12)
             53 -> Prelude'.fmap (\ !new'Field -> old'Self{start_time = Prelude'.Just new'Field}) (P'.wireGet 2)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgSosStartSoundEvent) CMsgSosStartSoundEvent where
  getVal m' f' = f' m'

instance P'.GPB CMsgSosStartSoundEvent

instance P'.ReflectDescriptor CMsgSosStartSoundEvent where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 21, 24, 32, 42, 53])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Gameevents.CMsgSosStartSoundEvent\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2GameEvents\"], baseName = MName \"CMsgSosStartSoundEvent\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2GameEvents\",\"CMsgSosStartSoundEvent.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosStartSoundEvent.soundevent_guid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosStartSoundEvent\"], baseName' = FName \"soundevent_guid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosStartSoundEvent.soundevent_hash\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosStartSoundEvent\"], baseName' = FName \"soundevent_hash\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 21}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosStartSoundEvent.source_entity_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosStartSoundEvent\"], baseName' = FName \"source_entity_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosStartSoundEvent.seed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosStartSoundEvent\"], baseName' = FName \"seed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosStartSoundEvent.packed_params\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosStartSoundEvent\"], baseName' = FName \"packed_params\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosStartSoundEvent.start_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosStartSoundEvent\"], baseName' = FName \"start_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 53}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgSosStartSoundEvent where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgSosStartSoundEvent where
  textPut msg
   = do
       P'.tellT "soundevent_guid" (soundevent_guid msg)
       P'.tellT "soundevent_hash" (soundevent_hash msg)
       P'.tellT "source_entity_index" (source_entity_index msg)
       P'.tellT "seed" (seed msg)
       P'.tellT "packed_params" (packed_params msg)
       P'.tellT "start_time" (start_time msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'soundevent_guid, parse'soundevent_hash, parse'source_entity_index, parse'seed, parse'packed_params,
                   parse'start_time])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'soundevent_guid
         = P'.try
            (do
               v <- P'.getT "soundevent_guid"
               Prelude'.return (\ o -> o{soundevent_guid = v}))
        parse'soundevent_hash
         = P'.try
            (do
               v <- P'.getT "soundevent_hash"
               Prelude'.return (\ o -> o{soundevent_hash = v}))
        parse'source_entity_index
         = P'.try
            (do
               v <- P'.getT "source_entity_index"
               Prelude'.return (\ o -> o{source_entity_index = v}))
        parse'seed
         = P'.try
            (do
               v <- P'.getT "seed"
               Prelude'.return (\ o -> o{seed = v}))
        parse'packed_params
         = P'.try
            (do
               v <- P'.getT "packed_params"
               Prelude'.return (\ o -> o{packed_params = v}))
        parse'start_time
         = P'.try
            (do
               v <- P'.getT "start_time"
               Prelude'.return (\ o -> o{start_time = v}))