{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSource1LegacyListenEvents (CMsgSource1LegacyListenEvents(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CMsgSource1LegacyListenEvents = CMsgSource1LegacyListenEvents{playerslot :: !(P'.Maybe P'.Int32),
                                                                   eventarraybits :: !(P'.Seq P'.Word32)}
                                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                             Prelude'.Generic)

instance P'.Mergeable CMsgSource1LegacyListenEvents where
  mergeAppend (CMsgSource1LegacyListenEvents x'1 x'2) (CMsgSource1LegacyListenEvents y'1 y'2)
   = CMsgSource1LegacyListenEvents (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMsgSource1LegacyListenEvents where
  defaultValue = CMsgSource1LegacyListenEvents P'.defaultValue P'.defaultValue

instance P'.Wire CMsgSource1LegacyListenEvents where
  wireSize ft' self'@(CMsgSource1LegacyListenEvents x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeRep 1 13 x'2)
  wirePut ft' self'@(CMsgSource1LegacyListenEvents x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutRep 16 13 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{playerslot = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{eventarraybits = P'.append (eventarraybits old'Self) new'Field})
                    (P'.wireGet 13)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{eventarraybits = P'.mergeAppend (eventarraybits old'Self) new'Field})
                    (P'.wireGetPacked 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgSource1LegacyListenEvents) CMsgSource1LegacyListenEvents where
  getVal m' f' = f' m'

instance P'.GPB CMsgSource1LegacyListenEvents

instance P'.ReflectDescriptor CMsgSource1LegacyListenEvents where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Gameevents.CMsgSource1LegacyListenEvents\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2GameEvents\"], baseName = MName \"CMsgSource1LegacyListenEvents\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2GameEvents\",\"CMsgSource1LegacyListenEvents.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSource1LegacyListenEvents.playerslot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSource1LegacyListenEvents\"], baseName' = FName \"playerslot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSource1LegacyListenEvents.eventarraybits\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSource1LegacyListenEvents\"], baseName' = FName \"eventarraybits\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Just (WireTag {getWireTag = 16},WireTag {getWireTag = 18}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgSource1LegacyListenEvents where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgSource1LegacyListenEvents where
  textPut msg
   = do
       P'.tellT "playerslot" (playerslot msg)
       P'.tellT "eventarraybits" (eventarraybits msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'playerslot, parse'eventarraybits]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'playerslot
         = P'.try
            (do
               v <- P'.getT "playerslot"
               Prelude'.return (\ o -> o{playerslot = v}))
        parse'eventarraybits
         = P'.try
            (do
               v <- P'.getT "eventarraybits"
               Prelude'.return (\ o -> o{eventarraybits = P'.append (eventarraybits o) v}))