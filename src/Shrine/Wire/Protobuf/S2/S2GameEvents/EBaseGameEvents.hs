{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2GameEvents.EBaseGameEvents (EBaseGameEvents(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EBaseGameEvents = GE_VDebugGameSessionIDEvent
                     | GE_PlaceDecalEvent
                     | GE_ClearWorldDecalsEvent
                     | GE_ClearEntityDecalsEvent
                     | GE_ClearDecalsForSkeletonInstanceEvent
                     | GE_Source1LegacyGameEventList
                     | GE_Source1LegacyListenEvents
                     | GE_Source1LegacyGameEvent
                     | GE_SosStartSoundEvent
                     | GE_SosStopSoundEvent
                     | GE_SosSetSoundEventParams
                     | GE_SosSetLibraryStackFields
                     | GE_SosStopSoundEventHash
                     deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                               Prelude'.Generic)

instance P'.Mergeable EBaseGameEvents

instance Prelude'.Bounded EBaseGameEvents where
  minBound = GE_VDebugGameSessionIDEvent
  maxBound = GE_SosStopSoundEventHash

instance P'.Default EBaseGameEvents where
  defaultValue = GE_VDebugGameSessionIDEvent

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EBaseGameEvents
toMaybe'Enum 200 = Prelude'.Just GE_VDebugGameSessionIDEvent
toMaybe'Enum 201 = Prelude'.Just GE_PlaceDecalEvent
toMaybe'Enum 202 = Prelude'.Just GE_ClearWorldDecalsEvent
toMaybe'Enum 203 = Prelude'.Just GE_ClearEntityDecalsEvent
toMaybe'Enum 204 = Prelude'.Just GE_ClearDecalsForSkeletonInstanceEvent
toMaybe'Enum 205 = Prelude'.Just GE_Source1LegacyGameEventList
toMaybe'Enum 206 = Prelude'.Just GE_Source1LegacyListenEvents
toMaybe'Enum 207 = Prelude'.Just GE_Source1LegacyGameEvent
toMaybe'Enum 208 = Prelude'.Just GE_SosStartSoundEvent
toMaybe'Enum 209 = Prelude'.Just GE_SosStopSoundEvent
toMaybe'Enum 210 = Prelude'.Just GE_SosSetSoundEventParams
toMaybe'Enum 211 = Prelude'.Just GE_SosSetLibraryStackFields
toMaybe'Enum 212 = Prelude'.Just GE_SosStopSoundEventHash
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EBaseGameEvents where
  fromEnum GE_VDebugGameSessionIDEvent = 200
  fromEnum GE_PlaceDecalEvent = 201
  fromEnum GE_ClearWorldDecalsEvent = 202
  fromEnum GE_ClearEntityDecalsEvent = 203
  fromEnum GE_ClearDecalsForSkeletonInstanceEvent = 204
  fromEnum GE_Source1LegacyGameEventList = 205
  fromEnum GE_Source1LegacyListenEvents = 206
  fromEnum GE_Source1LegacyGameEvent = 207
  fromEnum GE_SosStartSoundEvent = 208
  fromEnum GE_SosStopSoundEvent = 209
  fromEnum GE_SosSetSoundEventParams = 210
  fromEnum GE_SosSetLibraryStackFields = 211
  fromEnum GE_SosStopSoundEventHash = 212
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2GameEvents.EBaseGameEvents")
      . toMaybe'Enum
  succ GE_VDebugGameSessionIDEvent = GE_PlaceDecalEvent
  succ GE_PlaceDecalEvent = GE_ClearWorldDecalsEvent
  succ GE_ClearWorldDecalsEvent = GE_ClearEntityDecalsEvent
  succ GE_ClearEntityDecalsEvent = GE_ClearDecalsForSkeletonInstanceEvent
  succ GE_ClearDecalsForSkeletonInstanceEvent = GE_Source1LegacyGameEventList
  succ GE_Source1LegacyGameEventList = GE_Source1LegacyListenEvents
  succ GE_Source1LegacyListenEvents = GE_Source1LegacyGameEvent
  succ GE_Source1LegacyGameEvent = GE_SosStartSoundEvent
  succ GE_SosStartSoundEvent = GE_SosStopSoundEvent
  succ GE_SosStopSoundEvent = GE_SosSetSoundEventParams
  succ GE_SosSetSoundEventParams = GE_SosSetLibraryStackFields
  succ GE_SosSetLibraryStackFields = GE_SosStopSoundEventHash
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2GameEvents.EBaseGameEvents"
  pred GE_PlaceDecalEvent = GE_VDebugGameSessionIDEvent
  pred GE_ClearWorldDecalsEvent = GE_PlaceDecalEvent
  pred GE_ClearEntityDecalsEvent = GE_ClearWorldDecalsEvent
  pred GE_ClearDecalsForSkeletonInstanceEvent = GE_ClearEntityDecalsEvent
  pred GE_Source1LegacyGameEventList = GE_ClearDecalsForSkeletonInstanceEvent
  pred GE_Source1LegacyListenEvents = GE_Source1LegacyGameEventList
  pred GE_Source1LegacyGameEvent = GE_Source1LegacyListenEvents
  pred GE_SosStartSoundEvent = GE_Source1LegacyGameEvent
  pred GE_SosStopSoundEvent = GE_SosStartSoundEvent
  pred GE_SosSetSoundEventParams = GE_SosStopSoundEvent
  pred GE_SosSetLibraryStackFields = GE_SosSetSoundEventParams
  pred GE_SosStopSoundEventHash = GE_SosSetLibraryStackFields
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2GameEvents.EBaseGameEvents"

instance P'.Wire EBaseGameEvents where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EBaseGameEvents

instance P'.MessageAPI msg' (msg' -> EBaseGameEvents) EBaseGameEvents where
  getVal m' f' = f' m'

instance P'.ReflectEnum EBaseGameEvents where
  reflectEnum
   = [(200, "GE_VDebugGameSessionIDEvent", GE_VDebugGameSessionIDEvent), (201, "GE_PlaceDecalEvent", GE_PlaceDecalEvent),
      (202, "GE_ClearWorldDecalsEvent", GE_ClearWorldDecalsEvent), (203, "GE_ClearEntityDecalsEvent", GE_ClearEntityDecalsEvent),
      (204, "GE_ClearDecalsForSkeletonInstanceEvent", GE_ClearDecalsForSkeletonInstanceEvent),
      (205, "GE_Source1LegacyGameEventList", GE_Source1LegacyGameEventList),
      (206, "GE_Source1LegacyListenEvents", GE_Source1LegacyListenEvents),
      (207, "GE_Source1LegacyGameEvent", GE_Source1LegacyGameEvent), (208, "GE_SosStartSoundEvent", GE_SosStartSoundEvent),
      (209, "GE_SosStopSoundEvent", GE_SosStopSoundEvent), (210, "GE_SosSetSoundEventParams", GE_SosSetSoundEventParams),
      (211, "GE_SosSetLibraryStackFields", GE_SosSetLibraryStackFields),
      (212, "GE_SosStopSoundEventHash", GE_SosStopSoundEventHash)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Gameevents.EBaseGameEvents") ["Shrine", "Wire", "Protobuf", "S2"] ["S2GameEvents"]
        "EBaseGameEvents")
      ["Shrine", "Wire", "Protobuf", "S2", "S2GameEvents", "EBaseGameEvents.hs"]
      [(200, "GE_VDebugGameSessionIDEvent"), (201, "GE_PlaceDecalEvent"), (202, "GE_ClearWorldDecalsEvent"),
       (203, "GE_ClearEntityDecalsEvent"), (204, "GE_ClearDecalsForSkeletonInstanceEvent"), (205, "GE_Source1LegacyGameEventList"),
       (206, "GE_Source1LegacyListenEvents"), (207, "GE_Source1LegacyGameEvent"), (208, "GE_SosStartSoundEvent"),
       (209, "GE_SosStopSoundEvent"), (210, "GE_SosSetSoundEventParams"), (211, "GE_SosSetLibraryStackFields"),
       (212, "GE_SosStopSoundEventHash")]

instance P'.TextType EBaseGameEvents where
  tellT = P'.tellShow
  getT = P'.getRead