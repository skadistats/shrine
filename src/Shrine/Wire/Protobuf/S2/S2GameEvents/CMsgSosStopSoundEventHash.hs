{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2GameEvents.CMsgSosStopSoundEventHash (CMsgSosStopSoundEventHash(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CMsgSosStopSoundEventHash = CMsgSosStopSoundEventHash{soundevent_hash :: !(P'.Maybe P'.Word32),
                                                           source_entity_index :: !(P'.Maybe P'.Int32)}
                               deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                         Prelude'.Generic)

instance P'.Mergeable CMsgSosStopSoundEventHash where
  mergeAppend (CMsgSosStopSoundEventHash x'1 x'2) (CMsgSosStopSoundEventHash y'1 y'2)
   = CMsgSosStopSoundEventHash (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMsgSosStopSoundEventHash where
  defaultValue = CMsgSosStopSoundEventHash P'.defaultValue P'.defaultValue

instance P'.Wire CMsgSosStopSoundEventHash where
  wireSize ft' self'@(CMsgSosStopSoundEventHash x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 7 x'1 + P'.wireSizeOpt 1 5 x'2)
  wirePut ft' self'@(CMsgSosStopSoundEventHash x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 13 7 x'1
             P'.wirePutOpt 16 5 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             13 -> Prelude'.fmap (\ !new'Field -> old'Self{soundevent_hash = Prelude'.Just new'Field}) (P'.wireGet 7)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{source_entity_index = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgSosStopSoundEventHash) CMsgSosStopSoundEventHash where
  getVal m' f' = f' m'

instance P'.GPB CMsgSosStopSoundEventHash

instance P'.ReflectDescriptor CMsgSosStopSoundEventHash where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [13, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Gameevents.CMsgSosStopSoundEventHash\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2GameEvents\"], baseName = MName \"CMsgSosStopSoundEventHash\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2GameEvents\",\"CMsgSosStopSoundEventHash.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosStopSoundEventHash.soundevent_hash\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosStopSoundEventHash\"], baseName' = FName \"soundevent_hash\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 13}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Gameevents.CMsgSosStopSoundEventHash.source_entity_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2GameEvents\",MName \"CMsgSosStopSoundEventHash\"], baseName' = FName \"source_entity_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgSosStopSoundEventHash where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgSosStopSoundEventHash where
  textPut msg
   = do
       P'.tellT "soundevent_hash" (soundevent_hash msg)
       P'.tellT "source_entity_index" (source_entity_index msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'soundevent_hash, parse'source_entity_index]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'soundevent_hash
         = P'.try
            (do
               v <- P'.getT "soundevent_hash"
               Prelude'.return (\ o -> o{soundevent_hash = v}))
        parse'source_entity_index
         = P'.try
            (do
               v <- P'.getT "source_entity_index"
               Prelude'.return (\ o -> o{source_entity_index = v}))