{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchPrivateMetadata (CDOTAMatchPrivateMetadata(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team
       as S2DotaMatchMetadata.CDOTAMatchPrivateMetadata (Team)

data CDOTAMatchPrivateMetadata = CDOTAMatchPrivateMetadata{teams :: !(P'.Seq S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team)}
                               deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                         Prelude'.Generic)

instance P'.Mergeable CDOTAMatchPrivateMetadata where
  mergeAppend (CDOTAMatchPrivateMetadata x'1) (CDOTAMatchPrivateMetadata y'1) = CDOTAMatchPrivateMetadata (P'.mergeAppend x'1 y'1)

instance P'.Default CDOTAMatchPrivateMetadata where
  defaultValue = CDOTAMatchPrivateMetadata P'.defaultValue

instance P'.Wire CDOTAMatchPrivateMetadata where
  wireSize ft' self'@(CDOTAMatchPrivateMetadata x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 11 x'1)
  wirePut ft' self'@(CDOTAMatchPrivateMetadata x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 10 11 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{teams = P'.append (teams old'Self) new'Field}) (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAMatchPrivateMetadata) CDOTAMatchPrivateMetadata where
  getVal m' f' = f' m'

instance P'.GPB CDOTAMatchPrivateMetadata

instance P'.ReflectDescriptor CDOTAMatchPrivateMetadata where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\"], baseName = MName \"CDOTAMatchPrivateMetadata\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaMatchMetadata\",\"CDOTAMatchPrivateMetadata.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.teams\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\"], baseName' = FName \"teams\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\"], baseName = MName \"Team\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAMatchPrivateMetadata where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAMatchPrivateMetadata where
  textPut msg
   = do
       P'.tellT "teams" (teams msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'teams]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'teams
         = P'.try
            (do
               v <- P'.getT "teams"
               Prelude'.return (\ o -> o{teams = P'.append (teams o) v}))