{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadata.Team.Player (Player(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2BaseGcMessages.CSOEconItem as S2BaseGcMessages (CSOEconItem)
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadata.Team.AutoStyleCriteria
       as S2DotaMatchMetadata.CDOTAMatchMetadata.Team (AutoStyleCriteria)
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadata.Team.InventorySnapshot
       as S2DotaMatchMetadata.CDOTAMatchMetadata.Team (InventorySnapshot)
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadata.Team.ItemPurchase
       as S2DotaMatchMetadata.CDOTAMatchMetadata.Team (ItemPurchase)
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadata.Team.PlayerKill
       as S2DotaMatchMetadata.CDOTAMatchMetadata.Team (PlayerKill)

data Player = Player{account_id :: !(P'.Maybe P'.Word32), ability_upgrades :: !(P'.Seq P'.Word32),
                     player_slot :: !(P'.Maybe P'.Word32), equipped_econ_items :: !(P'.Seq S2BaseGcMessages.CSOEconItem),
                     kills :: !(P'.Seq S2DotaMatchMetadata.CDOTAMatchMetadata.Team.PlayerKill),
                     items :: !(P'.Seq S2DotaMatchMetadata.CDOTAMatchMetadata.Team.ItemPurchase),
                     avg_kills_x16 :: !(P'.Maybe P'.Word32), avg_deaths_x16 :: !(P'.Maybe P'.Word32),
                     avg_assists_x16 :: !(P'.Maybe P'.Word32), avg_gpm_x16 :: !(P'.Maybe P'.Word32),
                     avg_xpm_x16 :: !(P'.Maybe P'.Word32), best_kills_x16 :: !(P'.Maybe P'.Word32),
                     best_assists_x16 :: !(P'.Maybe P'.Word32), best_gpm_x16 :: !(P'.Maybe P'.Word32),
                     best_xpm_x16 :: !(P'.Maybe P'.Word32), win_streak :: !(P'.Maybe P'.Word32),
                     best_win_streak :: !(P'.Maybe P'.Word32), fight_score :: !(P'.Maybe P'.Float),
                     farm_score :: !(P'.Maybe P'.Float), support_score :: !(P'.Maybe P'.Float), push_score :: !(P'.Maybe P'.Float),
                     level_up_times :: !(P'.Seq P'.Word32), graph_net_worth :: !(P'.Seq P'.Float),
                     inventory_snapshot :: !(P'.Seq S2DotaMatchMetadata.CDOTAMatchMetadata.Team.InventorySnapshot),
                     avg_stats_calibrated :: !(P'.Maybe P'.Bool),
                     auto_style_criteria :: !(P'.Seq S2DotaMatchMetadata.CDOTAMatchMetadata.Team.AutoStyleCriteria),
                     event_id :: !(P'.Maybe P'.Word32), event_points :: !(P'.Maybe P'.Word32)}
            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Player where
  mergeAppend
   (Player x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23 x'24 x'25 x'26
     x'27 x'28)
   (Player y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18 y'19 y'20 y'21 y'22 y'23 y'24 y'25 y'26
     y'27 y'28)
   = Player (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)
      (P'.mergeAppend x'19 y'19)
      (P'.mergeAppend x'20 y'20)
      (P'.mergeAppend x'21 y'21)
      (P'.mergeAppend x'22 y'22)
      (P'.mergeAppend x'23 y'23)
      (P'.mergeAppend x'24 y'24)
      (P'.mergeAppend x'25 y'25)
      (P'.mergeAppend x'26 y'26)
      (P'.mergeAppend x'27 y'27)
      (P'.mergeAppend x'28 y'28)

instance P'.Default Player where
  defaultValue
   = Player P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire Player where
  wireSize ft'
   self'@(Player x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23 x'24 x'25
           x'26 x'27 x'28)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeRep 1 13 x'2 + P'.wireSizeOpt 1 13 x'3 + P'.wireSizeRep 1 11 x'4 +
             P'.wireSizeRep 1 11 x'5
             + P'.wireSizeRep 1 11 x'6
             + P'.wireSizeOpt 1 13 x'7
             + P'.wireSizeOpt 1 13 x'8
             + P'.wireSizeOpt 1 13 x'9
             + P'.wireSizeOpt 1 13 x'10
             + P'.wireSizeOpt 1 13 x'11
             + P'.wireSizeOpt 1 13 x'12
             + P'.wireSizeOpt 1 13 x'13
             + P'.wireSizeOpt 1 13 x'14
             + P'.wireSizeOpt 1 13 x'15
             + P'.wireSizeOpt 2 13 x'16
             + P'.wireSizeOpt 2 13 x'17
             + P'.wireSizeOpt 2 2 x'18
             + P'.wireSizeOpt 2 2 x'19
             + P'.wireSizeOpt 2 2 x'20
             + P'.wireSizeOpt 2 2 x'21
             + P'.wireSizeRep 2 13 x'22
             + P'.wireSizeRep 2 2 x'23
             + P'.wireSizeRep 2 11 x'24
             + P'.wireSizeOpt 2 8 x'25
             + P'.wireSizeRep 2 11 x'26
             + P'.wireSizeOpt 2 13 x'27
             + P'.wireSizeOpt 2 13 x'28)
  wirePut ft'
   self'@(Player x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19 x'20 x'21 x'22 x'23 x'24 x'25
           x'26 x'27 x'28)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutRep 16 13 x'2
             P'.wirePutOpt 24 13 x'3
             P'.wirePutRep 34 11 x'4
             P'.wirePutRep 42 11 x'5
             P'.wirePutRep 50 11 x'6
             P'.wirePutOpt 56 13 x'7
             P'.wirePutOpt 64 13 x'8
             P'.wirePutOpt 72 13 x'9
             P'.wirePutOpt 80 13 x'10
             P'.wirePutOpt 88 13 x'11
             P'.wirePutOpt 96 13 x'12
             P'.wirePutOpt 104 13 x'13
             P'.wirePutOpt 112 13 x'14
             P'.wirePutOpt 120 13 x'15
             P'.wirePutOpt 128 13 x'16
             P'.wirePutOpt 136 13 x'17
             P'.wirePutOpt 149 2 x'18
             P'.wirePutOpt 157 2 x'19
             P'.wirePutOpt 165 2 x'20
             P'.wirePutOpt 173 2 x'21
             P'.wirePutRep 176 13 x'22
             P'.wirePutRep 189 2 x'23
             P'.wirePutRep 194 11 x'24
             P'.wirePutOpt 200 8 x'25
             P'.wirePutRep 210 11 x'26
             P'.wirePutOpt 216 13 x'27
             P'.wirePutOpt 224 13 x'28
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{account_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{ability_upgrades = P'.append (ability_upgrades old'Self) new'Field})
                    (P'.wireGet 13)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{ability_upgrades = P'.mergeAppend (ability_upgrades old'Self) new'Field})
                    (P'.wireGetPacked 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{player_slot = Prelude'.Just new'Field}) (P'.wireGet 13)
             34 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{equipped_econ_items = P'.append (equipped_econ_items old'Self) new'Field})
                    (P'.wireGet 11)
             42 -> Prelude'.fmap (\ !new'Field -> old'Self{kills = P'.append (kills old'Self) new'Field}) (P'.wireGet 11)
             50 -> Prelude'.fmap (\ !new'Field -> old'Self{items = P'.append (items old'Self) new'Field}) (P'.wireGet 11)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{avg_kills_x16 = Prelude'.Just new'Field}) (P'.wireGet 13)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{avg_deaths_x16 = Prelude'.Just new'Field}) (P'.wireGet 13)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{avg_assists_x16 = Prelude'.Just new'Field}) (P'.wireGet 13)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{avg_gpm_x16 = Prelude'.Just new'Field}) (P'.wireGet 13)
             88 -> Prelude'.fmap (\ !new'Field -> old'Self{avg_xpm_x16 = Prelude'.Just new'Field}) (P'.wireGet 13)
             96 -> Prelude'.fmap (\ !new'Field -> old'Self{best_kills_x16 = Prelude'.Just new'Field}) (P'.wireGet 13)
             104 -> Prelude'.fmap (\ !new'Field -> old'Self{best_assists_x16 = Prelude'.Just new'Field}) (P'.wireGet 13)
             112 -> Prelude'.fmap (\ !new'Field -> old'Self{best_gpm_x16 = Prelude'.Just new'Field}) (P'.wireGet 13)
             120 -> Prelude'.fmap (\ !new'Field -> old'Self{best_xpm_x16 = Prelude'.Just new'Field}) (P'.wireGet 13)
             128 -> Prelude'.fmap (\ !new'Field -> old'Self{win_streak = Prelude'.Just new'Field}) (P'.wireGet 13)
             136 -> Prelude'.fmap (\ !new'Field -> old'Self{best_win_streak = Prelude'.Just new'Field}) (P'.wireGet 13)
             149 -> Prelude'.fmap (\ !new'Field -> old'Self{fight_score = Prelude'.Just new'Field}) (P'.wireGet 2)
             157 -> Prelude'.fmap (\ !new'Field -> old'Self{farm_score = Prelude'.Just new'Field}) (P'.wireGet 2)
             165 -> Prelude'.fmap (\ !new'Field -> old'Self{support_score = Prelude'.Just new'Field}) (P'.wireGet 2)
             173 -> Prelude'.fmap (\ !new'Field -> old'Self{push_score = Prelude'.Just new'Field}) (P'.wireGet 2)
             176 -> Prelude'.fmap (\ !new'Field -> old'Self{level_up_times = P'.append (level_up_times old'Self) new'Field})
                     (P'.wireGet 13)
             178 -> Prelude'.fmap (\ !new'Field -> old'Self{level_up_times = P'.mergeAppend (level_up_times old'Self) new'Field})
                     (P'.wireGetPacked 13)
             189 -> Prelude'.fmap (\ !new'Field -> old'Self{graph_net_worth = P'.append (graph_net_worth old'Self) new'Field})
                     (P'.wireGet 2)
             186 -> Prelude'.fmap (\ !new'Field -> old'Self{graph_net_worth = P'.mergeAppend (graph_net_worth old'Self) new'Field})
                     (P'.wireGetPacked 2)
             194 -> Prelude'.fmap (\ !new'Field -> old'Self{inventory_snapshot = P'.append (inventory_snapshot old'Self) new'Field})
                     (P'.wireGet 11)
             200 -> Prelude'.fmap (\ !new'Field -> old'Self{avg_stats_calibrated = Prelude'.Just new'Field}) (P'.wireGet 8)
             210 -> Prelude'.fmap
                     (\ !new'Field -> old'Self{auto_style_criteria = P'.append (auto_style_criteria old'Self) new'Field})
                     (P'.wireGet 11)
             216 -> Prelude'.fmap (\ !new'Field -> old'Self{event_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             224 -> Prelude'.fmap (\ !new'Field -> old'Self{event_points = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Player) Player where
  getVal m' f' = f' m'

instance P'.GPB Player

instance P'.ReflectDescriptor Player where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList
        [8, 16, 18, 24, 34, 42, 50, 56, 64, 72, 80, 88, 96, 104, 112, 120, 128, 136, 149, 157, 165, 173, 176, 178, 186, 189, 194,
         200, 210, 216, 224])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName = MName \"Player\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaMatchMetadata\",\"CDOTAMatchMetadata\",\"Team\",\"Player.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.account_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"account_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.ability_upgrades\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"ability_upgrades\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Just (WireTag {getWireTag = 16},WireTag {getWireTag = 18}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.player_slot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"player_slot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.equipped_econ_items\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"equipped_econ_items\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 34}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Base_Gcmessages.CSOEconItem\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2BaseGcMessages\"], baseName = MName \"CSOEconItem\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.kills\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"kills\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.PlayerKill\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName = MName \"PlayerKill\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.items\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"items\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 50}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.ItemPurchase\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName = MName \"ItemPurchase\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.avg_kills_x16\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"avg_kills_x16\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.avg_deaths_x16\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"avg_deaths_x16\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.avg_assists_x16\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"avg_assists_x16\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.avg_gpm_x16\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"avg_gpm_x16\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.avg_xpm_x16\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"avg_xpm_x16\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 88}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.best_kills_x16\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"best_kills_x16\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 96}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.best_assists_x16\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"best_assists_x16\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 104}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.best_gpm_x16\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"best_gpm_x16\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 112}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.best_xpm_x16\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"best_xpm_x16\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 120}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.win_streak\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"win_streak\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 128}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.best_win_streak\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"best_win_streak\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 136}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.fight_score\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"fight_score\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 149}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.farm_score\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"farm_score\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 19}, wireTag = WireTag {getWireTag = 157}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.support_score\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"support_score\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 20}, wireTag = WireTag {getWireTag = 165}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.push_score\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"push_score\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 21}, wireTag = WireTag {getWireTag = 173}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.level_up_times\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"level_up_times\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 22}, wireTag = WireTag {getWireTag = 176}, packedTag = Just (WireTag {getWireTag = 176},WireTag {getWireTag = 178}), wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.graph_net_worth\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"graph_net_worth\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 23}, wireTag = WireTag {getWireTag = 189}, packedTag = Just (WireTag {getWireTag = 189},WireTag {getWireTag = 186}), wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.inventory_snapshot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"inventory_snapshot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 24}, wireTag = WireTag {getWireTag = 194}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.InventorySnapshot\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName = MName \"InventorySnapshot\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.avg_stats_calibrated\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"avg_stats_calibrated\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 25}, wireTag = WireTag {getWireTag = 200}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.auto_style_criteria\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"auto_style_criteria\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 26}, wireTag = WireTag {getWireTag = 210}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.AutoStyleCriteria\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName = MName \"AutoStyleCriteria\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.event_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"event_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 27}, wireTag = WireTag {getWireTag = 216}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player.event_points\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"event_points\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 28}, wireTag = WireTag {getWireTag = 224}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Player where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Player where
  textPut msg
   = do
       P'.tellT "account_id" (account_id msg)
       P'.tellT "ability_upgrades" (ability_upgrades msg)
       P'.tellT "player_slot" (player_slot msg)
       P'.tellT "equipped_econ_items" (equipped_econ_items msg)
       P'.tellT "kills" (kills msg)
       P'.tellT "items" (items msg)
       P'.tellT "avg_kills_x16" (avg_kills_x16 msg)
       P'.tellT "avg_deaths_x16" (avg_deaths_x16 msg)
       P'.tellT "avg_assists_x16" (avg_assists_x16 msg)
       P'.tellT "avg_gpm_x16" (avg_gpm_x16 msg)
       P'.tellT "avg_xpm_x16" (avg_xpm_x16 msg)
       P'.tellT "best_kills_x16" (best_kills_x16 msg)
       P'.tellT "best_assists_x16" (best_assists_x16 msg)
       P'.tellT "best_gpm_x16" (best_gpm_x16 msg)
       P'.tellT "best_xpm_x16" (best_xpm_x16 msg)
       P'.tellT "win_streak" (win_streak msg)
       P'.tellT "best_win_streak" (best_win_streak msg)
       P'.tellT "fight_score" (fight_score msg)
       P'.tellT "farm_score" (farm_score msg)
       P'.tellT "support_score" (support_score msg)
       P'.tellT "push_score" (push_score msg)
       P'.tellT "level_up_times" (level_up_times msg)
       P'.tellT "graph_net_worth" (graph_net_worth msg)
       P'.tellT "inventory_snapshot" (inventory_snapshot msg)
       P'.tellT "avg_stats_calibrated" (avg_stats_calibrated msg)
       P'.tellT "auto_style_criteria" (auto_style_criteria msg)
       P'.tellT "event_id" (event_id msg)
       P'.tellT "event_points" (event_points msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'account_id, parse'ability_upgrades, parse'player_slot, parse'equipped_econ_items, parse'kills, parse'items,
                   parse'avg_kills_x16, parse'avg_deaths_x16, parse'avg_assists_x16, parse'avg_gpm_x16, parse'avg_xpm_x16,
                   parse'best_kills_x16, parse'best_assists_x16, parse'best_gpm_x16, parse'best_xpm_x16, parse'win_streak,
                   parse'best_win_streak, parse'fight_score, parse'farm_score, parse'support_score, parse'push_score,
                   parse'level_up_times, parse'graph_net_worth, parse'inventory_snapshot, parse'avg_stats_calibrated,
                   parse'auto_style_criteria, parse'event_id, parse'event_points])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'account_id
         = P'.try
            (do
               v <- P'.getT "account_id"
               Prelude'.return (\ o -> o{account_id = v}))
        parse'ability_upgrades
         = P'.try
            (do
               v <- P'.getT "ability_upgrades"
               Prelude'.return (\ o -> o{ability_upgrades = P'.append (ability_upgrades o) v}))
        parse'player_slot
         = P'.try
            (do
               v <- P'.getT "player_slot"
               Prelude'.return (\ o -> o{player_slot = v}))
        parse'equipped_econ_items
         = P'.try
            (do
               v <- P'.getT "equipped_econ_items"
               Prelude'.return (\ o -> o{equipped_econ_items = P'.append (equipped_econ_items o) v}))
        parse'kills
         = P'.try
            (do
               v <- P'.getT "kills"
               Prelude'.return (\ o -> o{kills = P'.append (kills o) v}))
        parse'items
         = P'.try
            (do
               v <- P'.getT "items"
               Prelude'.return (\ o -> o{items = P'.append (items o) v}))
        parse'avg_kills_x16
         = P'.try
            (do
               v <- P'.getT "avg_kills_x16"
               Prelude'.return (\ o -> o{avg_kills_x16 = v}))
        parse'avg_deaths_x16
         = P'.try
            (do
               v <- P'.getT "avg_deaths_x16"
               Prelude'.return (\ o -> o{avg_deaths_x16 = v}))
        parse'avg_assists_x16
         = P'.try
            (do
               v <- P'.getT "avg_assists_x16"
               Prelude'.return (\ o -> o{avg_assists_x16 = v}))
        parse'avg_gpm_x16
         = P'.try
            (do
               v <- P'.getT "avg_gpm_x16"
               Prelude'.return (\ o -> o{avg_gpm_x16 = v}))
        parse'avg_xpm_x16
         = P'.try
            (do
               v <- P'.getT "avg_xpm_x16"
               Prelude'.return (\ o -> o{avg_xpm_x16 = v}))
        parse'best_kills_x16
         = P'.try
            (do
               v <- P'.getT "best_kills_x16"
               Prelude'.return (\ o -> o{best_kills_x16 = v}))
        parse'best_assists_x16
         = P'.try
            (do
               v <- P'.getT "best_assists_x16"
               Prelude'.return (\ o -> o{best_assists_x16 = v}))
        parse'best_gpm_x16
         = P'.try
            (do
               v <- P'.getT "best_gpm_x16"
               Prelude'.return (\ o -> o{best_gpm_x16 = v}))
        parse'best_xpm_x16
         = P'.try
            (do
               v <- P'.getT "best_xpm_x16"
               Prelude'.return (\ o -> o{best_xpm_x16 = v}))
        parse'win_streak
         = P'.try
            (do
               v <- P'.getT "win_streak"
               Prelude'.return (\ o -> o{win_streak = v}))
        parse'best_win_streak
         = P'.try
            (do
               v <- P'.getT "best_win_streak"
               Prelude'.return (\ o -> o{best_win_streak = v}))
        parse'fight_score
         = P'.try
            (do
               v <- P'.getT "fight_score"
               Prelude'.return (\ o -> o{fight_score = v}))
        parse'farm_score
         = P'.try
            (do
               v <- P'.getT "farm_score"
               Prelude'.return (\ o -> o{farm_score = v}))
        parse'support_score
         = P'.try
            (do
               v <- P'.getT "support_score"
               Prelude'.return (\ o -> o{support_score = v}))
        parse'push_score
         = P'.try
            (do
               v <- P'.getT "push_score"
               Prelude'.return (\ o -> o{push_score = v}))
        parse'level_up_times
         = P'.try
            (do
               v <- P'.getT "level_up_times"
               Prelude'.return (\ o -> o{level_up_times = P'.append (level_up_times o) v}))
        parse'graph_net_worth
         = P'.try
            (do
               v <- P'.getT "graph_net_worth"
               Prelude'.return (\ o -> o{graph_net_worth = P'.append (graph_net_worth o) v}))
        parse'inventory_snapshot
         = P'.try
            (do
               v <- P'.getT "inventory_snapshot"
               Prelude'.return (\ o -> o{inventory_snapshot = P'.append (inventory_snapshot o) v}))
        parse'avg_stats_calibrated
         = P'.try
            (do
               v <- P'.getT "avg_stats_calibrated"
               Prelude'.return (\ o -> o{avg_stats_calibrated = v}))
        parse'auto_style_criteria
         = P'.try
            (do
               v <- P'.getT "auto_style_criteria"
               Prelude'.return (\ o -> o{auto_style_criteria = P'.append (auto_style_criteria o) v}))
        parse'event_id
         = P'.try
            (do
               v <- P'.getT "event_id"
               Prelude'.return (\ o -> o{event_id = v}))
        parse'event_points
         = P'.try
            (do
               v <- P'.getT "event_points"
               Prelude'.return (\ o -> o{event_points = v}))