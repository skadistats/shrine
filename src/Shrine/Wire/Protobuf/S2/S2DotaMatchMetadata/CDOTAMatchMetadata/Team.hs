{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadata.Team (Team(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadata.Team.Player
       as S2DotaMatchMetadata.CDOTAMatchMetadata.Team (Player)

data Team = Team{dota_team :: !(P'.Maybe P'.Word32), players :: !(P'.Seq S2DotaMatchMetadata.CDOTAMatchMetadata.Team.Player),
                 graph_experience :: !(P'.Seq P'.Float), graph_gold_earned :: !(P'.Seq P'.Float),
                 graph_net_worth :: !(P'.Seq P'.Float), cm_first_pick :: !(P'.Maybe P'.Bool),
                 cm_captain_player_id :: !(P'.Maybe P'.Word32), cm_bans :: !(P'.Seq P'.Word32), cm_picks :: !(P'.Seq P'.Word32),
                 cm_penalty :: !(P'.Maybe P'.Word32)}
          deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Team where
  mergeAppend (Team x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10) (Team y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10)
   = Team (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)

instance P'.Default Team where
  defaultValue
   = Team P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire Team where
  wireSize ft' self'@(Team x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeRep 1 11 x'2 + P'.wireSizeRep 1 2 x'3 + P'.wireSizeRep 1 2 x'4 +
             P'.wireSizeRep 1 2 x'5
             + P'.wireSizeOpt 1 8 x'6
             + P'.wireSizeOpt 1 13 x'7
             + P'.wireSizeRep 1 13 x'8
             + P'.wireSizeRep 1 13 x'9
             + P'.wireSizeOpt 1 13 x'10)
  wirePut ft' self'@(Team x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutRep 18 11 x'2
             P'.wirePutRep 29 2 x'3
             P'.wirePutRep 37 2 x'4
             P'.wirePutRep 45 2 x'5
             P'.wirePutOpt 48 8 x'6
             P'.wirePutOpt 56 13 x'7
             P'.wirePutRep 64 13 x'8
             P'.wirePutRep 72 13 x'9
             P'.wirePutOpt 80 13 x'10
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{dota_team = Prelude'.Just new'Field}) (P'.wireGet 13)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{players = P'.append (players old'Self) new'Field}) (P'.wireGet 11)
             29 -> Prelude'.fmap (\ !new'Field -> old'Self{graph_experience = P'.append (graph_experience old'Self) new'Field})
                    (P'.wireGet 2)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{graph_experience = P'.mergeAppend (graph_experience old'Self) new'Field})
                    (P'.wireGetPacked 2)
             37 -> Prelude'.fmap (\ !new'Field -> old'Self{graph_gold_earned = P'.append (graph_gold_earned old'Self) new'Field})
                    (P'.wireGet 2)
             34 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{graph_gold_earned = P'.mergeAppend (graph_gold_earned old'Self) new'Field})
                    (P'.wireGetPacked 2)
             45 -> Prelude'.fmap (\ !new'Field -> old'Self{graph_net_worth = P'.append (graph_net_worth old'Self) new'Field})
                    (P'.wireGet 2)
             42 -> Prelude'.fmap (\ !new'Field -> old'Self{graph_net_worth = P'.mergeAppend (graph_net_worth old'Self) new'Field})
                    (P'.wireGetPacked 2)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{cm_first_pick = Prelude'.Just new'Field}) (P'.wireGet 8)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{cm_captain_player_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{cm_bans = P'.append (cm_bans old'Self) new'Field}) (P'.wireGet 13)
             66 -> Prelude'.fmap (\ !new'Field -> old'Self{cm_bans = P'.mergeAppend (cm_bans old'Self) new'Field})
                    (P'.wireGetPacked 13)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{cm_picks = P'.append (cm_picks old'Self) new'Field}) (P'.wireGet 13)
             74 -> Prelude'.fmap (\ !new'Field -> old'Self{cm_picks = P'.mergeAppend (cm_picks old'Self) new'Field})
                    (P'.wireGetPacked 13)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{cm_penalty = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Team) Team where
  getVal m' f' = f' m'

instance P'.GPB Team

instance P'.ReflectDescriptor Team where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList [8, 18, 26, 29, 34, 37, 42, 45, 48, 56, 64, 66, 72, 74, 80])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\"], baseName = MName \"Team\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaMatchMetadata\",\"CDOTAMatchMetadata\",\"Team.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.dota_team\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"dota_team\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.players\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"players\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.Player\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName = MName \"Player\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.graph_experience\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"graph_experience\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 29}, packedTag = Just (WireTag {getWireTag = 29},WireTag {getWireTag = 26}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.graph_gold_earned\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"graph_gold_earned\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 37}, packedTag = Just (WireTag {getWireTag = 37},WireTag {getWireTag = 34}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.graph_net_worth\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"graph_net_worth\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 45}, packedTag = Just (WireTag {getWireTag = 45},WireTag {getWireTag = 42}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.cm_first_pick\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"cm_first_pick\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.cm_captain_player_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"cm_captain_player_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.cm_bans\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"cm_bans\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Just (WireTag {getWireTag = 64},WireTag {getWireTag = 66}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.cm_picks\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"cm_picks\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Just (WireTag {getWireTag = 72},WireTag {getWireTag = 74}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team.cm_penalty\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\",MName \"Team\"], baseName' = FName \"cm_penalty\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Team where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Team where
  textPut msg
   = do
       P'.tellT "dota_team" (dota_team msg)
       P'.tellT "players" (players msg)
       P'.tellT "graph_experience" (graph_experience msg)
       P'.tellT "graph_gold_earned" (graph_gold_earned msg)
       P'.tellT "graph_net_worth" (graph_net_worth msg)
       P'.tellT "cm_first_pick" (cm_first_pick msg)
       P'.tellT "cm_captain_player_id" (cm_captain_player_id msg)
       P'.tellT "cm_bans" (cm_bans msg)
       P'.tellT "cm_picks" (cm_picks msg)
       P'.tellT "cm_penalty" (cm_penalty msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'dota_team, parse'players, parse'graph_experience, parse'graph_gold_earned, parse'graph_net_worth,
                   parse'cm_first_pick, parse'cm_captain_player_id, parse'cm_bans, parse'cm_picks, parse'cm_penalty])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'dota_team
         = P'.try
            (do
               v <- P'.getT "dota_team"
               Prelude'.return (\ o -> o{dota_team = v}))
        parse'players
         = P'.try
            (do
               v <- P'.getT "players"
               Prelude'.return (\ o -> o{players = P'.append (players o) v}))
        parse'graph_experience
         = P'.try
            (do
               v <- P'.getT "graph_experience"
               Prelude'.return (\ o -> o{graph_experience = P'.append (graph_experience o) v}))
        parse'graph_gold_earned
         = P'.try
            (do
               v <- P'.getT "graph_gold_earned"
               Prelude'.return (\ o -> o{graph_gold_earned = P'.append (graph_gold_earned o) v}))
        parse'graph_net_worth
         = P'.try
            (do
               v <- P'.getT "graph_net_worth"
               Prelude'.return (\ o -> o{graph_net_worth = P'.append (graph_net_worth o) v}))
        parse'cm_first_pick
         = P'.try
            (do
               v <- P'.getT "cm_first_pick"
               Prelude'.return (\ o -> o{cm_first_pick = v}))
        parse'cm_captain_player_id
         = P'.try
            (do
               v <- P'.getT "cm_captain_player_id"
               Prelude'.return (\ o -> o{cm_captain_player_id = v}))
        parse'cm_bans
         = P'.try
            (do
               v <- P'.getT "cm_bans"
               Prelude'.return (\ o -> o{cm_bans = P'.append (cm_bans o) v}))
        parse'cm_picks
         = P'.try
            (do
               v <- P'.getT "cm_picks"
               Prelude'.return (\ o -> o{cm_picks = P'.append (cm_picks o) v}))
        parse'cm_penalty
         = P'.try
            (do
               v <- P'.getT "cm_penalty"
               Prelude'.return (\ o -> o{cm_penalty = v}))