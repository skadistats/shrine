{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadata (CDOTAMatchMetadata(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchMetadata.Team as S2DotaMatchMetadata.CDOTAMatchMetadata
       (Team)
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CLobbyTimedRewardDetails as S2DotaMatchMetadata
       (CLobbyTimedRewardDetails)

data CDOTAMatchMetadata = CDOTAMatchMetadata{teams :: !(P'.Seq S2DotaMatchMetadata.CDOTAMatchMetadata.Team),
                                             item_rewards :: !(P'.Seq S2DotaMatchMetadata.CLobbyTimedRewardDetails),
                                             lobby_id :: !(P'.Maybe P'.Word64), report_until_time :: !(P'.Maybe P'.Word64),
                                             event_game_custom_table :: !(P'.Maybe P'.ByteString)}
                        deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CDOTAMatchMetadata where
  mergeAppend (CDOTAMatchMetadata x'1 x'2 x'3 x'4 x'5) (CDOTAMatchMetadata y'1 y'2 y'3 y'4 y'5)
   = CDOTAMatchMetadata (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)

instance P'.Default CDOTAMatchMetadata where
  defaultValue = CDOTAMatchMetadata P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CDOTAMatchMetadata where
  wireSize ft' self'@(CDOTAMatchMetadata x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeRep 1 11 x'1 + P'.wireSizeRep 1 11 x'2 + P'.wireSizeOpt 1 6 x'3 + P'.wireSizeOpt 1 6 x'4 +
             P'.wireSizeOpt 1 12 x'5)
  wirePut ft' self'@(CDOTAMatchMetadata x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 10 11 x'1
             P'.wirePutRep 18 11 x'2
             P'.wirePutOpt 25 6 x'3
             P'.wirePutOpt 33 6 x'4
             P'.wirePutOpt 42 12 x'5
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{teams = P'.append (teams old'Self) new'Field}) (P'.wireGet 11)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{item_rewards = P'.append (item_rewards old'Self) new'Field})
                    (P'.wireGet 11)
             25 -> Prelude'.fmap (\ !new'Field -> old'Self{lobby_id = Prelude'.Just new'Field}) (P'.wireGet 6)
             33 -> Prelude'.fmap (\ !new'Field -> old'Self{report_until_time = Prelude'.Just new'Field}) (P'.wireGet 6)
             42 -> Prelude'.fmap (\ !new'Field -> old'Self{event_game_custom_table = Prelude'.Just new'Field}) (P'.wireGet 12)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CDOTAMatchMetadata) CDOTAMatchMetadata where
  getVal m' f' = f' m'

instance P'.GPB CDOTAMatchMetadata

instance P'.ReflectDescriptor CDOTAMatchMetadata where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18, 25, 33, 42])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\"], baseName = MName \"CDOTAMatchMetadata\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaMatchMetadata\",\"CDOTAMatchMetadata.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.teams\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\"], baseName' = FName \"teams\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.Team\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\"], baseName = MName \"Team\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.item_rewards\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\"], baseName' = FName \"item_rewards\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CLobbyTimedRewardDetails\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\"], baseName = MName \"CLobbyTimedRewardDetails\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.lobby_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\"], baseName' = FName \"lobby_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 25}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.report_until_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\"], baseName' = FName \"report_until_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 33}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchMetadata.event_game_custom_table\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchMetadata\"], baseName' = FName \"event_game_custom_table\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CDOTAMatchMetadata where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CDOTAMatchMetadata where
  textPut msg
   = do
       P'.tellT "teams" (teams msg)
       P'.tellT "item_rewards" (item_rewards msg)
       P'.tellT "lobby_id" (lobby_id msg)
       P'.tellT "report_until_time" (report_until_time msg)
       P'.tellT "event_game_custom_table" (event_game_custom_table msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'teams, parse'item_rewards, parse'lobby_id, parse'report_until_time, parse'event_game_custom_table])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'teams
         = P'.try
            (do
               v <- P'.getT "teams"
               Prelude'.return (\ o -> o{teams = P'.append (teams o) v}))
        parse'item_rewards
         = P'.try
            (do
               v <- P'.getT "item_rewards"
               Prelude'.return (\ o -> o{item_rewards = P'.append (item_rewards o) v}))
        parse'lobby_id
         = P'.try
            (do
               v <- P'.getT "lobby_id"
               Prelude'.return (\ o -> o{lobby_id = v}))
        parse'report_until_time
         = P'.try
            (do
               v <- P'.getT "report_until_time"
               Prelude'.return (\ o -> o{report_until_time = v}))
        parse'event_game_custom_table
         = P'.try
            (do
               v <- P'.getT "event_game_custom_table"
               Prelude'.return (\ o -> o{event_game_custom_table = v}))