{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team (Team(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team.Building
       as S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team (Building)
import qualified Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team.Player
       as S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team (Player)

data Team = Team{dota_team :: !(P'.Maybe P'.Word32), players :: !(P'.Seq S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team.Player),
                 buildings :: !(P'.Seq S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team.Building)}
          deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Team where
  mergeAppend (Team x'1 x'2 x'3) (Team y'1 y'2 y'3)
   = Team (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default Team where
  defaultValue = Team P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire Team where
  wireSize ft' self'@(Team x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeRep 1 11 x'2 + P'.wireSizeRep 1 11 x'3)
  wirePut ft' self'@(Team x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutRep 18 11 x'2
             P'.wirePutRep 26 11 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{dota_team = Prelude'.Just new'Field}) (P'.wireGet 13)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{players = P'.append (players old'Self) new'Field}) (P'.wireGet 11)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{buildings = P'.append (buildings old'Self) new'Field}) (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Team) Team where
  getVal m' f' = f' m'

instance P'.GPB Team

instance P'.ReflectDescriptor Team where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18, 26])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\"], baseName = MName \"Team\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaMatchMetadata\",\"CDOTAMatchPrivateMetadata\",\"Team.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.dota_team\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\"], baseName' = FName \"dota_team\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.players\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\"], baseName' = FName \"players\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Player\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\"], baseName = MName \"Player\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.buildings\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\"], baseName' = FName \"buildings\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Building\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\"], baseName = MName \"Building\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Team where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Team where
  textPut msg
   = do
       P'.tellT "dota_team" (dota_team msg)
       P'.tellT "players" (players msg)
       P'.tellT "buildings" (buildings msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'dota_team, parse'players, parse'buildings]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'dota_team
         = P'.try
            (do
               v <- P'.getT "dota_team"
               Prelude'.return (\ o -> o{dota_team = v}))
        parse'players
         = P'.try
            (do
               v <- P'.getT "players"
               Prelude'.return (\ o -> o{players = P'.append (players o) v}))
        parse'buildings
         = P'.try
            (do
               v <- P'.getT "buildings"
               Prelude'.return (\ o -> o{buildings = P'.append (buildings o) v}))