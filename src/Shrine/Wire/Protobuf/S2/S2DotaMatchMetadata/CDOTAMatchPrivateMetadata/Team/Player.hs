{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team.Player (Player(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data Player = Player{account_id :: !(P'.Maybe P'.Word32), player_slot :: !(P'.Maybe P'.Word32),
                     position_stream :: !(P'.Maybe P'.ByteString)}
            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Player where
  mergeAppend (Player x'1 x'2 x'3) (Player y'1 y'2 y'3)
   = Player (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default Player where
  defaultValue = Player P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire Player where
  wireSize ft' self'@(Player x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 13 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 12 x'3)
  wirePut ft' self'@(Player x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 13 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 26 12 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{account_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{player_slot = Prelude'.Just new'Field}) (P'.wireGet 13)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{position_stream = Prelude'.Just new'Field}) (P'.wireGet 12)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Player) Player where
  getVal m' f' = f' m'

instance P'.GPB Player

instance P'.ReflectDescriptor Player where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 26])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Player\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\"], baseName = MName \"Player\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaMatchMetadata\",\"CDOTAMatchPrivateMetadata\",\"Team\",\"Player.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Player.account_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"account_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Player.player_slot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"player_slot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Player.position_stream\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\",MName \"Player\"], baseName' = FName \"position_stream\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Player where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Player where
  textPut msg
   = do
       P'.tellT "account_id" (account_id msg)
       P'.tellT "player_slot" (player_slot msg)
       P'.tellT "position_stream" (position_stream msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'account_id, parse'player_slot, parse'position_stream]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'account_id
         = P'.try
            (do
               v <- P'.getT "account_id"
               Prelude'.return (\ o -> o{account_id = v}))
        parse'player_slot
         = P'.try
            (do
               v <- P'.getT "player_slot"
               Prelude'.return (\ o -> o{player_slot = v}))
        parse'position_stream
         = P'.try
            (do
               v <- P'.getT "position_stream"
               Prelude'.return (\ o -> o{position_stream = v}))