{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2DotaMatchMetadata.CDOTAMatchPrivateMetadata.Team.Building (Building(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data Building = Building{unit_name :: !(P'.Maybe P'.Utf8), position_quant_x :: !(P'.Maybe P'.Word32),
                         position_quant_y :: !(P'.Maybe P'.Word32), death_time :: !(P'.Maybe P'.Float)}
              deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable Building where
  mergeAppend (Building x'1 x'2 x'3 x'4) (Building y'1 y'2 y'3 y'4)
   = Building (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)

instance P'.Default Building where
  defaultValue = Building P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire Building where
  wireSize ft' self'@(Building x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 9 x'1 + P'.wireSizeOpt 1 13 x'2 + P'.wireSizeOpt 1 13 x'3 + P'.wireSizeOpt 1 2 x'4)
  wirePut ft' self'@(Building x'1 x'2 x'3 x'4)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
             P'.wirePutOpt 16 13 x'2
             P'.wirePutOpt 24 13 x'3
             P'.wirePutOpt 37 2 x'4
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{unit_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{position_quant_x = Prelude'.Just new'Field}) (P'.wireGet 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{position_quant_y = Prelude'.Just new'Field}) (P'.wireGet 13)
             37 -> Prelude'.fmap (\ !new'Field -> old'Self{death_time = Prelude'.Just new'Field}) (P'.wireGet 2)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> Building) Building where
  getVal m' f' = f' m'

instance P'.GPB Building

instance P'.ReflectDescriptor Building where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 16, 24, 37])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Building\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\"], baseName = MName \"Building\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2DotaMatchMetadata\",\"CDOTAMatchPrivateMetadata\",\"Team\",\"Building.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Building.unit_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\",MName \"Building\"], baseName' = FName \"unit_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Building.position_quant_x\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\",MName \"Building\"], baseName' = FName \"position_quant_x\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Building.position_quant_y\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\",MName \"Building\"], baseName' = FName \"position_quant_y\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Dota_Match_Metadata.CDOTAMatchPrivateMetadata.Team.Building.death_time\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2DotaMatchMetadata\",MName \"CDOTAMatchPrivateMetadata\",MName \"Team\",MName \"Building\"], baseName' = FName \"death_time\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 37}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType Building where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg Building where
  textPut msg
   = do
       P'.tellT "unit_name" (unit_name msg)
       P'.tellT "position_quant_x" (position_quant_x msg)
       P'.tellT "position_quant_y" (position_quant_y msg)
       P'.tellT "death_time" (death_time msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'unit_name, parse'position_quant_x, parse'position_quant_y, parse'death_time]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'unit_name
         = P'.try
            (do
               v <- P'.getT "unit_name"
               Prelude'.return (\ o -> o{unit_name = v}))
        parse'position_quant_x
         = P'.try
            (do
               v <- P'.getT "position_quant_x"
               Prelude'.return (\ o -> o{position_quant_x = v}))
        parse'position_quant_y
         = P'.try
            (do
               v <- P'.getT "position_quant_y"
               Prelude'.return (\ o -> o{position_quant_y = v}))
        parse'death_time
         = P'.try
            (do
               v <- P'.getT "death_time"
               Prelude'.return (\ o -> o{death_time = v}))