{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2TempEntities.CMsgEffectData (CMsgEffectData(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgQAngle as NetworkBaseTypes (CMsgQAngle)
import qualified Shrine.Wire.Protobuf.Common.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CMsgEffectData = CMsgEffectData{origin :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                     start :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                     normal :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                     angles :: !(P'.Maybe NetworkBaseTypes.CMsgQAngle), entity :: !(P'.Maybe P'.Word32),
                                     otherentity :: !(P'.Maybe P'.Word32), scale :: !(P'.Maybe P'.Float),
                                     magnitude :: !(P'.Maybe P'.Float), radius :: !(P'.Maybe P'.Float),
                                     surfaceprop :: !(P'.Maybe P'.Word32), effectindex :: !(P'.Maybe P'.Word64),
                                     damagetype :: !(P'.Maybe P'.Word32), material :: !(P'.Maybe P'.Word32),
                                     hitbox :: !(P'.Maybe P'.Word32), color :: !(P'.Maybe P'.Word32),
                                     flags :: !(P'.Maybe P'.Word32), attachmentindex :: !(P'.Maybe P'.Int32),
                                     effectname :: !(P'.Maybe P'.Word32), attachmentname :: !(P'.Maybe P'.Word32)}
                    deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgEffectData where
  mergeAppend (CMsgEffectData x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19)
   (CMsgEffectData y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15 y'16 y'17 y'18 y'19)
   = CMsgEffectData (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)
      (P'.mergeAppend x'16 y'16)
      (P'.mergeAppend x'17 y'17)
      (P'.mergeAppend x'18 y'18)
      (P'.mergeAppend x'19 y'19)

instance P'.Default CMsgEffectData where
  defaultValue
   = CMsgEffectData P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CMsgEffectData where
  wireSize ft' self'@(CMsgEffectData x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 11 x'1 + P'.wireSizeOpt 1 11 x'2 + P'.wireSizeOpt 1 11 x'3 + P'.wireSizeOpt 1 11 x'4 +
             P'.wireSizeOpt 1 7 x'5
             + P'.wireSizeOpt 1 7 x'6
             + P'.wireSizeOpt 1 2 x'7
             + P'.wireSizeOpt 1 2 x'8
             + P'.wireSizeOpt 1 2 x'9
             + P'.wireSizeOpt 1 7 x'10
             + P'.wireSizeOpt 1 6 x'11
             + P'.wireSizeOpt 1 13 x'12
             + P'.wireSizeOpt 1 13 x'13
             + P'.wireSizeOpt 1 13 x'14
             + P'.wireSizeOpt 1 13 x'15
             + P'.wireSizeOpt 2 13 x'16
             + P'.wireSizeOpt 2 5 x'17
             + P'.wireSizeOpt 2 13 x'18
             + P'.wireSizeOpt 2 13 x'19)
  wirePut ft' self'@(CMsgEffectData x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15 x'16 x'17 x'18 x'19)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 11 x'1
             P'.wirePutOpt 18 11 x'2
             P'.wirePutOpt 26 11 x'3
             P'.wirePutOpt 34 11 x'4
             P'.wirePutOpt 45 7 x'5
             P'.wirePutOpt 53 7 x'6
             P'.wirePutOpt 61 2 x'7
             P'.wirePutOpt 69 2 x'8
             P'.wirePutOpt 77 2 x'9
             P'.wirePutOpt 85 7 x'10
             P'.wirePutOpt 89 6 x'11
             P'.wirePutOpt 96 13 x'12
             P'.wirePutOpt 104 13 x'13
             P'.wirePutOpt 112 13 x'14
             P'.wirePutOpt 120 13 x'15
             P'.wirePutOpt 128 13 x'16
             P'.wirePutOpt 136 5 x'17
             P'.wirePutOpt 144 13 x'18
             P'.wirePutOpt 152 13 x'19
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{origin = P'.mergeAppend (origin old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{start = P'.mergeAppend (start old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{normal = P'.mergeAppend (normal old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             34 -> Prelude'.fmap (\ !new'Field -> old'Self{angles = P'.mergeAppend (angles old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             45 -> Prelude'.fmap (\ !new'Field -> old'Self{entity = Prelude'.Just new'Field}) (P'.wireGet 7)
             53 -> Prelude'.fmap (\ !new'Field -> old'Self{otherentity = Prelude'.Just new'Field}) (P'.wireGet 7)
             61 -> Prelude'.fmap (\ !new'Field -> old'Self{scale = Prelude'.Just new'Field}) (P'.wireGet 2)
             69 -> Prelude'.fmap (\ !new'Field -> old'Self{magnitude = Prelude'.Just new'Field}) (P'.wireGet 2)
             77 -> Prelude'.fmap (\ !new'Field -> old'Self{radius = Prelude'.Just new'Field}) (P'.wireGet 2)
             85 -> Prelude'.fmap (\ !new'Field -> old'Self{surfaceprop = Prelude'.Just new'Field}) (P'.wireGet 7)
             89 -> Prelude'.fmap (\ !new'Field -> old'Self{effectindex = Prelude'.Just new'Field}) (P'.wireGet 6)
             96 -> Prelude'.fmap (\ !new'Field -> old'Self{damagetype = Prelude'.Just new'Field}) (P'.wireGet 13)
             104 -> Prelude'.fmap (\ !new'Field -> old'Self{material = Prelude'.Just new'Field}) (P'.wireGet 13)
             112 -> Prelude'.fmap (\ !new'Field -> old'Self{hitbox = Prelude'.Just new'Field}) (P'.wireGet 13)
             120 -> Prelude'.fmap (\ !new'Field -> old'Self{color = Prelude'.Just new'Field}) (P'.wireGet 13)
             128 -> Prelude'.fmap (\ !new'Field -> old'Self{flags = Prelude'.Just new'Field}) (P'.wireGet 13)
             136 -> Prelude'.fmap (\ !new'Field -> old'Self{attachmentindex = Prelude'.Just new'Field}) (P'.wireGet 5)
             144 -> Prelude'.fmap (\ !new'Field -> old'Self{effectname = Prelude'.Just new'Field}) (P'.wireGet 13)
             152 -> Prelude'.fmap (\ !new'Field -> old'Self{attachmentname = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgEffectData) CMsgEffectData where
  getVal m' f' = f' m'

instance P'.GPB CMsgEffectData

instance P'.ReflectDescriptor CMsgEffectData where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [])
      (P'.fromDistinctAscList [10, 18, 26, 34, 45, 53, 61, 69, 77, 85, 89, 96, 104, 112, 120, 128, 136, 144, 152])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Te.CMsgEffectData\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2TempEntities\"], baseName = MName \"CMsgEffectData\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2TempEntities\",\"CMsgEffectData.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.origin\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"origin\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.start\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"start\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.normal\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"normal\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.angles\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"angles\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 34}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgQAngle\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgQAngle\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.entity\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"entity\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 45}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.otherentity\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"otherentity\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 53}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.scale\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"scale\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 61}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.magnitude\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"magnitude\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 69}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.radius\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"radius\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 77}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.surfaceprop\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"surfaceprop\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 85}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 7}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.effectindex\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"effectindex\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 89}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.damagetype\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"damagetype\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 96}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.material\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"material\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 13}, wireTag = WireTag {getWireTag = 104}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.hitbox\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"hitbox\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 112}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.color\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"color\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 120}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.flags\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"flags\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 128}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.attachmentindex\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"attachmentindex\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 17}, wireTag = WireTag {getWireTag = 136}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.effectname\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"effectname\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 18}, wireTag = WireTag {getWireTag = 144}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgEffectData.attachmentname\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgEffectData\"], baseName' = FName \"attachmentname\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 19}, wireTag = WireTag {getWireTag = 152}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgEffectData where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgEffectData where
  textPut msg
   = do
       P'.tellT "origin" (origin msg)
       P'.tellT "start" (start msg)
       P'.tellT "normal" (normal msg)
       P'.tellT "angles" (angles msg)
       P'.tellT "entity" (entity msg)
       P'.tellT "otherentity" (otherentity msg)
       P'.tellT "scale" (scale msg)
       P'.tellT "magnitude" (magnitude msg)
       P'.tellT "radius" (radius msg)
       P'.tellT "surfaceprop" (surfaceprop msg)
       P'.tellT "effectindex" (effectindex msg)
       P'.tellT "damagetype" (damagetype msg)
       P'.tellT "material" (material msg)
       P'.tellT "hitbox" (hitbox msg)
       P'.tellT "color" (color msg)
       P'.tellT "flags" (flags msg)
       P'.tellT "attachmentindex" (attachmentindex msg)
       P'.tellT "effectname" (effectname msg)
       P'.tellT "attachmentname" (attachmentname msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'origin, parse'start, parse'normal, parse'angles, parse'entity, parse'otherentity, parse'scale,
                   parse'magnitude, parse'radius, parse'surfaceprop, parse'effectindex, parse'damagetype, parse'material,
                   parse'hitbox, parse'color, parse'flags, parse'attachmentindex, parse'effectname, parse'attachmentname])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'origin
         = P'.try
            (do
               v <- P'.getT "origin"
               Prelude'.return (\ o -> o{origin = v}))
        parse'start
         = P'.try
            (do
               v <- P'.getT "start"
               Prelude'.return (\ o -> o{start = v}))
        parse'normal
         = P'.try
            (do
               v <- P'.getT "normal"
               Prelude'.return (\ o -> o{normal = v}))
        parse'angles
         = P'.try
            (do
               v <- P'.getT "angles"
               Prelude'.return (\ o -> o{angles = v}))
        parse'entity
         = P'.try
            (do
               v <- P'.getT "entity"
               Prelude'.return (\ o -> o{entity = v}))
        parse'otherentity
         = P'.try
            (do
               v <- P'.getT "otherentity"
               Prelude'.return (\ o -> o{otherentity = v}))
        parse'scale
         = P'.try
            (do
               v <- P'.getT "scale"
               Prelude'.return (\ o -> o{scale = v}))
        parse'magnitude
         = P'.try
            (do
               v <- P'.getT "magnitude"
               Prelude'.return (\ o -> o{magnitude = v}))
        parse'radius
         = P'.try
            (do
               v <- P'.getT "radius"
               Prelude'.return (\ o -> o{radius = v}))
        parse'surfaceprop
         = P'.try
            (do
               v <- P'.getT "surfaceprop"
               Prelude'.return (\ o -> o{surfaceprop = v}))
        parse'effectindex
         = P'.try
            (do
               v <- P'.getT "effectindex"
               Prelude'.return (\ o -> o{effectindex = v}))
        parse'damagetype
         = P'.try
            (do
               v <- P'.getT "damagetype"
               Prelude'.return (\ o -> o{damagetype = v}))
        parse'material
         = P'.try
            (do
               v <- P'.getT "material"
               Prelude'.return (\ o -> o{material = v}))
        parse'hitbox
         = P'.try
            (do
               v <- P'.getT "hitbox"
               Prelude'.return (\ o -> o{hitbox = v}))
        parse'color
         = P'.try
            (do
               v <- P'.getT "color"
               Prelude'.return (\ o -> o{color = v}))
        parse'flags
         = P'.try
            (do
               v <- P'.getT "flags"
               Prelude'.return (\ o -> o{flags = v}))
        parse'attachmentindex
         = P'.try
            (do
               v <- P'.getT "attachmentindex"
               Prelude'.return (\ o -> o{attachmentindex = v}))
        parse'effectname
         = P'.try
            (do
               v <- P'.getT "effectname"
               Prelude'.return (\ o -> o{effectname = v}))
        parse'attachmentname
         = P'.try
            (do
               v <- P'.getT "attachmentname"
               Prelude'.return (\ o -> o{attachmentname = v}))
