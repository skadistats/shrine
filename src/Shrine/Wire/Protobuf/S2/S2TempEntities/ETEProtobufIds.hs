{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2TempEntities.ETEProtobufIds (ETEProtobufIds(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data ETEProtobufIds = TE_EffectDispatchId
                    | TE_ArmorRicochetId
                    | TE_BeamEntPointId
                    | TE_BeamEntsId
                    | TE_BeamPointsId
                    | TE_BeamRingId
                    | TE_BreakModelId
                    | TE_BSPDecalId
                    | TE_BubblesId
                    | TE_BubbleTrailId
                    | TE_DecalId
                    | TE_WorldDecalId
                    | TE_EnergySplashId
                    | TE_FizzId
                    | TE_ShatterSurfaceId
                    | TE_GlowSpriteId
                    | TE_ImpactId
                    | TE_MuzzleFlashId
                    | TE_BloodStreamId
                    | TE_ExplosionId
                    | TE_DustId
                    | TE_LargeFunnelId
                    | TE_SparksId
                    | TE_PhysicsPropId
                    | TE_PlayerDecalId
                    | TE_ProjectedDecalId
                    | TE_SmokeId
                    deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                              Prelude'.Generic)

instance P'.Mergeable ETEProtobufIds

instance Prelude'.Bounded ETEProtobufIds where
  minBound = TE_EffectDispatchId
  maxBound = TE_SmokeId

instance P'.Default ETEProtobufIds where
  defaultValue = TE_EffectDispatchId

toMaybe'Enum :: Prelude'.Int -> P'.Maybe ETEProtobufIds
toMaybe'Enum 400 = Prelude'.Just TE_EffectDispatchId
toMaybe'Enum 401 = Prelude'.Just TE_ArmorRicochetId
toMaybe'Enum 402 = Prelude'.Just TE_BeamEntPointId
toMaybe'Enum 403 = Prelude'.Just TE_BeamEntsId
toMaybe'Enum 404 = Prelude'.Just TE_BeamPointsId
toMaybe'Enum 405 = Prelude'.Just TE_BeamRingId
toMaybe'Enum 406 = Prelude'.Just TE_BreakModelId
toMaybe'Enum 407 = Prelude'.Just TE_BSPDecalId
toMaybe'Enum 408 = Prelude'.Just TE_BubblesId
toMaybe'Enum 409 = Prelude'.Just TE_BubbleTrailId
toMaybe'Enum 410 = Prelude'.Just TE_DecalId
toMaybe'Enum 411 = Prelude'.Just TE_WorldDecalId
toMaybe'Enum 412 = Prelude'.Just TE_EnergySplashId
toMaybe'Enum 413 = Prelude'.Just TE_FizzId
toMaybe'Enum 414 = Prelude'.Just TE_ShatterSurfaceId
toMaybe'Enum 415 = Prelude'.Just TE_GlowSpriteId
toMaybe'Enum 416 = Prelude'.Just TE_ImpactId
toMaybe'Enum 417 = Prelude'.Just TE_MuzzleFlashId
toMaybe'Enum 418 = Prelude'.Just TE_BloodStreamId
toMaybe'Enum 419 = Prelude'.Just TE_ExplosionId
toMaybe'Enum 420 = Prelude'.Just TE_DustId
toMaybe'Enum 421 = Prelude'.Just TE_LargeFunnelId
toMaybe'Enum 422 = Prelude'.Just TE_SparksId
toMaybe'Enum 423 = Prelude'.Just TE_PhysicsPropId
toMaybe'Enum 424 = Prelude'.Just TE_PlayerDecalId
toMaybe'Enum 425 = Prelude'.Just TE_ProjectedDecalId
toMaybe'Enum 426 = Prelude'.Just TE_SmokeId
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum ETEProtobufIds where
  fromEnum TE_EffectDispatchId = 400
  fromEnum TE_ArmorRicochetId = 401
  fromEnum TE_BeamEntPointId = 402
  fromEnum TE_BeamEntsId = 403
  fromEnum TE_BeamPointsId = 404
  fromEnum TE_BeamRingId = 405
  fromEnum TE_BreakModelId = 406
  fromEnum TE_BSPDecalId = 407
  fromEnum TE_BubblesId = 408
  fromEnum TE_BubbleTrailId = 409
  fromEnum TE_DecalId = 410
  fromEnum TE_WorldDecalId = 411
  fromEnum TE_EnergySplashId = 412
  fromEnum TE_FizzId = 413
  fromEnum TE_ShatterSurfaceId = 414
  fromEnum TE_GlowSpriteId = 415
  fromEnum TE_ImpactId = 416
  fromEnum TE_MuzzleFlashId = 417
  fromEnum TE_BloodStreamId = 418
  fromEnum TE_ExplosionId = 419
  fromEnum TE_DustId = 420
  fromEnum TE_LargeFunnelId = 421
  fromEnum TE_SparksId = 422
  fromEnum TE_PhysicsPropId = 423
  fromEnum TE_PlayerDecalId = 424
  fromEnum TE_ProjectedDecalId = 425
  fromEnum TE_SmokeId = 426
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2TempEntities.ETEProtobufIds")
      . toMaybe'Enum
  succ TE_EffectDispatchId = TE_ArmorRicochetId
  succ TE_ArmorRicochetId = TE_BeamEntPointId
  succ TE_BeamEntPointId = TE_BeamEntsId
  succ TE_BeamEntsId = TE_BeamPointsId
  succ TE_BeamPointsId = TE_BeamRingId
  succ TE_BeamRingId = TE_BreakModelId
  succ TE_BreakModelId = TE_BSPDecalId
  succ TE_BSPDecalId = TE_BubblesId
  succ TE_BubblesId = TE_BubbleTrailId
  succ TE_BubbleTrailId = TE_DecalId
  succ TE_DecalId = TE_WorldDecalId
  succ TE_WorldDecalId = TE_EnergySplashId
  succ TE_EnergySplashId = TE_FizzId
  succ TE_FizzId = TE_ShatterSurfaceId
  succ TE_ShatterSurfaceId = TE_GlowSpriteId
  succ TE_GlowSpriteId = TE_ImpactId
  succ TE_ImpactId = TE_MuzzleFlashId
  succ TE_MuzzleFlashId = TE_BloodStreamId
  succ TE_BloodStreamId = TE_ExplosionId
  succ TE_ExplosionId = TE_DustId
  succ TE_DustId = TE_LargeFunnelId
  succ TE_LargeFunnelId = TE_SparksId
  succ TE_SparksId = TE_PhysicsPropId
  succ TE_PhysicsPropId = TE_PlayerDecalId
  succ TE_PlayerDecalId = TE_ProjectedDecalId
  succ TE_ProjectedDecalId = TE_SmokeId
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2TempEntities.ETEProtobufIds"
  pred TE_ArmorRicochetId = TE_EffectDispatchId
  pred TE_BeamEntPointId = TE_ArmorRicochetId
  pred TE_BeamEntsId = TE_BeamEntPointId
  pred TE_BeamPointsId = TE_BeamEntsId
  pred TE_BeamRingId = TE_BeamPointsId
  pred TE_BreakModelId = TE_BeamRingId
  pred TE_BSPDecalId = TE_BreakModelId
  pred TE_BubblesId = TE_BSPDecalId
  pred TE_BubbleTrailId = TE_BubblesId
  pred TE_DecalId = TE_BubbleTrailId
  pred TE_WorldDecalId = TE_DecalId
  pred TE_EnergySplashId = TE_WorldDecalId
  pred TE_FizzId = TE_EnergySplashId
  pred TE_ShatterSurfaceId = TE_FizzId
  pred TE_GlowSpriteId = TE_ShatterSurfaceId
  pred TE_ImpactId = TE_GlowSpriteId
  pred TE_MuzzleFlashId = TE_ImpactId
  pred TE_BloodStreamId = TE_MuzzleFlashId
  pred TE_ExplosionId = TE_BloodStreamId
  pred TE_DustId = TE_ExplosionId
  pred TE_LargeFunnelId = TE_DustId
  pred TE_SparksId = TE_LargeFunnelId
  pred TE_PhysicsPropId = TE_SparksId
  pred TE_PlayerDecalId = TE_PhysicsPropId
  pred TE_ProjectedDecalId = TE_PlayerDecalId
  pred TE_SmokeId = TE_ProjectedDecalId
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2TempEntities.ETEProtobufIds"

instance P'.Wire ETEProtobufIds where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB ETEProtobufIds

instance P'.MessageAPI msg' (msg' -> ETEProtobufIds) ETEProtobufIds where
  getVal m' f' = f' m'

instance P'.ReflectEnum ETEProtobufIds where
  reflectEnum
   = [(400, "TE_EffectDispatchId", TE_EffectDispatchId), (401, "TE_ArmorRicochetId", TE_ArmorRicochetId),
      (402, "TE_BeamEntPointId", TE_BeamEntPointId), (403, "TE_BeamEntsId", TE_BeamEntsId),
      (404, "TE_BeamPointsId", TE_BeamPointsId), (405, "TE_BeamRingId", TE_BeamRingId), (406, "TE_BreakModelId", TE_BreakModelId),
      (407, "TE_BSPDecalId", TE_BSPDecalId), (408, "TE_BubblesId", TE_BubblesId), (409, "TE_BubbleTrailId", TE_BubbleTrailId),
      (410, "TE_DecalId", TE_DecalId), (411, "TE_WorldDecalId", TE_WorldDecalId), (412, "TE_EnergySplashId", TE_EnergySplashId),
      (413, "TE_FizzId", TE_FizzId), (414, "TE_ShatterSurfaceId", TE_ShatterSurfaceId), (415, "TE_GlowSpriteId", TE_GlowSpriteId),
      (416, "TE_ImpactId", TE_ImpactId), (417, "TE_MuzzleFlashId", TE_MuzzleFlashId), (418, "TE_BloodStreamId", TE_BloodStreamId),
      (419, "TE_ExplosionId", TE_ExplosionId), (420, "TE_DustId", TE_DustId), (421, "TE_LargeFunnelId", TE_LargeFunnelId),
      (422, "TE_SparksId", TE_SparksId), (423, "TE_PhysicsPropId", TE_PhysicsPropId), (424, "TE_PlayerDecalId", TE_PlayerDecalId),
      (425, "TE_ProjectedDecalId", TE_ProjectedDecalId), (426, "TE_SmokeId", TE_SmokeId)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Te.ETEProtobufIds") ["Shrine", "Wire", "Protobuf", "S2"] ["S2TempEntities"] "ETEProtobufIds")
      ["Shrine", "Wire", "Protobuf", "S2", "S2TempEntities", "ETEProtobufIds.hs"]
      [(400, "TE_EffectDispatchId"), (401, "TE_ArmorRicochetId"), (402, "TE_BeamEntPointId"), (403, "TE_BeamEntsId"),
       (404, "TE_BeamPointsId"), (405, "TE_BeamRingId"), (406, "TE_BreakModelId"), (407, "TE_BSPDecalId"), (408, "TE_BubblesId"),
       (409, "TE_BubbleTrailId"), (410, "TE_DecalId"), (411, "TE_WorldDecalId"), (412, "TE_EnergySplashId"), (413, "TE_FizzId"),
       (414, "TE_ShatterSurfaceId"), (415, "TE_GlowSpriteId"), (416, "TE_ImpactId"), (417, "TE_MuzzleFlashId"),
       (418, "TE_BloodStreamId"), (419, "TE_ExplosionId"), (420, "TE_DustId"), (421, "TE_LargeFunnelId"), (422, "TE_SparksId"),
       (423, "TE_PhysicsPropId"), (424, "TE_PlayerDecalId"), (425, "TE_ProjectedDecalId"), (426, "TE_SmokeId")]

instance P'.TextType ETEProtobufIds where
  tellT = P'.tellShow
  getT = P'.getRead