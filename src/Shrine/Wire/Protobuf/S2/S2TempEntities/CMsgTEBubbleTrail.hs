{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2TempEntities.CMsgTEBubbleTrail (CMsgTEBubbleTrail(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CMsgTEBubbleTrail = CMsgTEBubbleTrail{mins :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                           maxs :: !(P'.Maybe NetworkBaseTypes.CMsgVector), waterz :: !(P'.Maybe P'.Float),
                                           count :: !(P'.Maybe P'.Word32), speed :: !(P'.Maybe P'.Float)}
                       deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgTEBubbleTrail where
  mergeAppend (CMsgTEBubbleTrail x'1 x'2 x'3 x'4 x'5) (CMsgTEBubbleTrail y'1 y'2 y'3 y'4 y'5)
   = CMsgTEBubbleTrail (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)

instance P'.Default CMsgTEBubbleTrail where
  defaultValue = CMsgTEBubbleTrail P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CMsgTEBubbleTrail where
  wireSize ft' self'@(CMsgTEBubbleTrail x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 11 x'1 + P'.wireSizeOpt 1 11 x'2 + P'.wireSizeOpt 1 2 x'3 + P'.wireSizeOpt 1 13 x'4 +
             P'.wireSizeOpt 1 2 x'5)
  wirePut ft' self'@(CMsgTEBubbleTrail x'1 x'2 x'3 x'4 x'5)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 11 x'1
             P'.wirePutOpt 18 11 x'2
             P'.wirePutOpt 29 2 x'3
             P'.wirePutOpt 32 13 x'4
             P'.wirePutOpt 45 2 x'5
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{mins = P'.mergeAppend (mins old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{maxs = P'.mergeAppend (maxs old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             29 -> Prelude'.fmap (\ !new'Field -> old'Self{waterz = Prelude'.Just new'Field}) (P'.wireGet 2)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{count = Prelude'.Just new'Field}) (P'.wireGet 13)
             45 -> Prelude'.fmap (\ !new'Field -> old'Self{speed = Prelude'.Just new'Field}) (P'.wireGet 2)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgTEBubbleTrail) CMsgTEBubbleTrail where
  getVal m' f' = f' m'

instance P'.GPB CMsgTEBubbleTrail

instance P'.ReflectDescriptor CMsgTEBubbleTrail where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18, 29, 32, 45])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Te.CMsgTEBubbleTrail\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2TempEntities\"], baseName = MName \"CMsgTEBubbleTrail\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2TempEntities\",\"CMsgTEBubbleTrail.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEBubbleTrail.mins\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEBubbleTrail\"], baseName' = FName \"mins\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEBubbleTrail.maxs\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEBubbleTrail\"], baseName' = FName \"maxs\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEBubbleTrail.waterz\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEBubbleTrail\"], baseName' = FName \"waterz\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 29}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEBubbleTrail.count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEBubbleTrail\"], baseName' = FName \"count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEBubbleTrail.speed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEBubbleTrail\"], baseName' = FName \"speed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 45}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgTEBubbleTrail where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgTEBubbleTrail where
  textPut msg
   = do
       P'.tellT "mins" (mins msg)
       P'.tellT "maxs" (maxs msg)
       P'.tellT "waterz" (waterz msg)
       P'.tellT "count" (count msg)
       P'.tellT "speed" (speed msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'mins, parse'maxs, parse'waterz, parse'count, parse'speed]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'mins
         = P'.try
            (do
               v <- P'.getT "mins"
               Prelude'.return (\ o -> o{mins = v}))
        parse'maxs
         = P'.try
            (do
               v <- P'.getT "maxs"
               Prelude'.return (\ o -> o{maxs = v}))
        parse'waterz
         = P'.try
            (do
               v <- P'.getT "waterz"
               Prelude'.return (\ o -> o{waterz = v}))
        parse'count
         = P'.try
            (do
               v <- P'.getT "count"
               Prelude'.return (\ o -> o{count = v}))
        parse'speed
         = P'.try
            (do
               v <- P'.getT "speed"
               Prelude'.return (\ o -> o{speed = v}))