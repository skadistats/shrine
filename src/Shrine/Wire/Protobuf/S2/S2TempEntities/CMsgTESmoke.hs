{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2TempEntities.CMsgTESmoke (CMsgTESmoke(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CMsgTESmoke = CMsgTESmoke{origin :: !(P'.Maybe NetworkBaseTypes.CMsgVector), scale :: !(P'.Maybe P'.Float)}
                 deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgTESmoke where
  mergeAppend (CMsgTESmoke x'1 x'2) (CMsgTESmoke y'1 y'2) = CMsgTESmoke (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMsgTESmoke where
  defaultValue = CMsgTESmoke P'.defaultValue P'.defaultValue

instance P'.Wire CMsgTESmoke where
  wireSize ft' self'@(CMsgTESmoke x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 11 x'1 + P'.wireSizeOpt 1 2 x'2)
  wirePut ft' self'@(CMsgTESmoke x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 11 x'1
             P'.wirePutOpt 21 2 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{origin = P'.mergeAppend (origin old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             21 -> Prelude'.fmap (\ !new'Field -> old'Self{scale = Prelude'.Just new'Field}) (P'.wireGet 2)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgTESmoke) CMsgTESmoke where
  getVal m' f' = f' m'

instance P'.GPB CMsgTESmoke

instance P'.ReflectDescriptor CMsgTESmoke where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 21])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Te.CMsgTESmoke\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2TempEntities\"], baseName = MName \"CMsgTESmoke\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2TempEntities\",\"CMsgTESmoke.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTESmoke.origin\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTESmoke\"], baseName' = FName \"origin\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTESmoke.scale\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTESmoke\"], baseName' = FName \"scale\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 21}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgTESmoke where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgTESmoke where
  textPut msg
   = do
       P'.tellT "origin" (origin msg)
       P'.tellT "scale" (scale msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'origin, parse'scale]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'origin
         = P'.try
            (do
               v <- P'.getT "origin"
               Prelude'.return (\ o -> o{origin = v}))
        parse'scale
         = P'.try
            (do
               v <- P'.getT "scale"
               Prelude'.return (\ o -> o{scale = v}))