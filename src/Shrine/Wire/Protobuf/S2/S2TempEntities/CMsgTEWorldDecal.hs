{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2TempEntities.CMsgTEWorldDecal (CMsgTEWorldDecal(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CMsgTEWorldDecal = CMsgTEWorldDecal{origin :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                         normal :: !(P'.Maybe NetworkBaseTypes.CMsgVector), index :: !(P'.Maybe P'.Word32)}
                      deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgTEWorldDecal where
  mergeAppend (CMsgTEWorldDecal x'1 x'2 x'3) (CMsgTEWorldDecal y'1 y'2 y'3)
   = CMsgTEWorldDecal (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default CMsgTEWorldDecal where
  defaultValue = CMsgTEWorldDecal P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CMsgTEWorldDecal where
  wireSize ft' self'@(CMsgTEWorldDecal x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 11 x'1 + P'.wireSizeOpt 1 11 x'2 + P'.wireSizeOpt 1 13 x'3)
  wirePut ft' self'@(CMsgTEWorldDecal x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 11 x'1
             P'.wirePutOpt 18 11 x'2
             P'.wirePutOpt 24 13 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{origin = P'.mergeAppend (origin old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{normal = P'.mergeAppend (normal old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{index = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgTEWorldDecal) CMsgTEWorldDecal where
  getVal m' f' = f' m'

instance P'.GPB CMsgTEWorldDecal

instance P'.ReflectDescriptor CMsgTEWorldDecal where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18, 24])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Te.CMsgTEWorldDecal\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2TempEntities\"], baseName = MName \"CMsgTEWorldDecal\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2TempEntities\",\"CMsgTEWorldDecal.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEWorldDecal.origin\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEWorldDecal\"], baseName' = FName \"origin\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEWorldDecal.normal\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEWorldDecal\"], baseName' = FName \"normal\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEWorldDecal.index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEWorldDecal\"], baseName' = FName \"index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgTEWorldDecal where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgTEWorldDecal where
  textPut msg
   = do
       P'.tellT "origin" (origin msg)
       P'.tellT "normal" (normal msg)
       P'.tellT "index" (index msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'origin, parse'normal, parse'index]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'origin
         = P'.try
            (do
               v <- P'.getT "origin"
               Prelude'.return (\ o -> o{origin = v}))
        parse'normal
         = P'.try
            (do
               v <- P'.getT "normal"
               Prelude'.return (\ o -> o{normal = v}))
        parse'index
         = P'.try
            (do
               v <- P'.getT "index"
               Prelude'.return (\ o -> o{index = v}))