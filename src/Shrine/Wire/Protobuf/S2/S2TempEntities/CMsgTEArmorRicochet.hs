{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2TempEntities.CMsgTEArmorRicochet (CMsgTEArmorRicochet(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data CMsgTEArmorRicochet = CMsgTEArmorRicochet{pos :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                               dir :: !(P'.Maybe NetworkBaseTypes.CMsgVector)}
                         deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgTEArmorRicochet where
  mergeAppend (CMsgTEArmorRicochet x'1 x'2) (CMsgTEArmorRicochet y'1 y'2)
   = CMsgTEArmorRicochet (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMsgTEArmorRicochet where
  defaultValue = CMsgTEArmorRicochet P'.defaultValue P'.defaultValue

instance P'.Wire CMsgTEArmorRicochet where
  wireSize ft' self'@(CMsgTEArmorRicochet x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 11 x'1 + P'.wireSizeOpt 1 11 x'2)
  wirePut ft' self'@(CMsgTEArmorRicochet x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 11 x'1
             P'.wirePutOpt 18 11 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{pos = P'.mergeAppend (pos old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{dir = P'.mergeAppend (dir old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgTEArmorRicochet) CMsgTEArmorRicochet where
  getVal m' f' = f' m'

instance P'.GPB CMsgTEArmorRicochet

instance P'.ReflectDescriptor CMsgTEArmorRicochet where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Te.CMsgTEArmorRicochet\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2TempEntities\"], baseName = MName \"CMsgTEArmorRicochet\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2TempEntities\",\"CMsgTEArmorRicochet.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEArmorRicochet.pos\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEArmorRicochet\"], baseName' = FName \"pos\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Te.CMsgTEArmorRicochet.dir\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2TempEntities\",MName \"CMsgTEArmorRicochet\"], baseName' = FName \"dir\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgTEArmorRicochet where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgTEArmorRicochet where
  textPut msg
   = do
       P'.tellT "pos" (pos msg)
       P'.tellT "dir" (dir msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'pos, parse'dir]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'pos
         = P'.try
            (do
               v <- P'.getT "pos"
               Prelude'.return (\ o -> o{pos = v}))
        parse'dir
         = P'.try
            (do
               v <- P'.getT "dir"
               Prelude'.return (\ o -> o{dir = v}))