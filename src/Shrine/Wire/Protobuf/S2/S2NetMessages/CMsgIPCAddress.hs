{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2NetMessages.CMsgIPCAddress (CMsgIPCAddress(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CMsgIPCAddress = CMsgIPCAddress{computer_guid :: !(P'.Maybe P'.Word64), process_id :: !(P'.Maybe P'.Word32)}
                    deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgIPCAddress where
  mergeAppend (CMsgIPCAddress x'1 x'2) (CMsgIPCAddress y'1 y'2) = CMsgIPCAddress (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CMsgIPCAddress where
  defaultValue = CMsgIPCAddress P'.defaultValue P'.defaultValue

instance P'.Wire CMsgIPCAddress where
  wireSize ft' self'@(CMsgIPCAddress x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 6 x'1 + P'.wireSizeOpt 1 13 x'2)
  wirePut ft' self'@(CMsgIPCAddress x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 9 6 x'1
             P'.wirePutOpt 16 13 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             9 -> Prelude'.fmap (\ !new'Field -> old'Self{computer_guid = Prelude'.Just new'Field}) (P'.wireGet 6)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{process_id = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgIPCAddress) CMsgIPCAddress where
  getVal m' f' = f' m'

instance P'.GPB CMsgIPCAddress

instance P'.ReflectDescriptor CMsgIPCAddress where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [9, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Netmessages.CMsgIPCAddress\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"CMsgIPCAddress\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2NetMessages\",\"CMsgIPCAddress.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CMsgIPCAddress.computer_guid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CMsgIPCAddress\"], baseName' = FName \"computer_guid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 9}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CMsgIPCAddress.process_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CMsgIPCAddress\"], baseName' = FName \"process_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgIPCAddress where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgIPCAddress where
  textPut msg
   = do
       P'.tellT "computer_guid" (computer_guid msg)
       P'.tellT "process_id" (process_id msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'computer_guid, parse'process_id]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'computer_guid
         = P'.try
            (do
               v <- P'.getT "computer_guid"
               Prelude'.return (\ o -> o{computer_guid = v}))
        parse'process_id
         = P'.try
            (do
               v <- P'.getT "process_id"
               Prelude'.return (\ o -> o{process_id = v}))