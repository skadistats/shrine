{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_PeerList (CSVCMsg_PeerList(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.CMsgServerPeer as S2NetMessages (CMsgServerPeer)

data CSVCMsg_PeerList = CSVCMsg_PeerList{peer :: !(P'.Seq S2NetMessages.CMsgServerPeer)}
                      deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CSVCMsg_PeerList where
  mergeAppend (CSVCMsg_PeerList x'1) (CSVCMsg_PeerList y'1) = CSVCMsg_PeerList (P'.mergeAppend x'1 y'1)

instance P'.Default CSVCMsg_PeerList where
  defaultValue = CSVCMsg_PeerList P'.defaultValue

instance P'.Wire CSVCMsg_PeerList where
  wireSize ft' self'@(CSVCMsg_PeerList x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 11 x'1)
  wirePut ft' self'@(CSVCMsg_PeerList x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 10 11 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{peer = P'.append (peer old'Self) new'Field}) (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_PeerList) CSVCMsg_PeerList where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_PeerList

instance P'.ReflectDescriptor CSVCMsg_PeerList where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Netmessages.CSVCMsg_PeerList\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"CSVCMsg_PeerList\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2NetMessages\",\"CSVCMsg_PeerList.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_PeerList.peer\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_PeerList\"], baseName' = FName \"peer\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Netmessages.CMsgServerPeer\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"CMsgServerPeer\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_PeerList where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_PeerList where
  textPut msg
   = do
       P'.tellT "peer" (peer msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'peer]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'peer
         = P'.try
            (do
               v <- P'.getT "peer"
               Prelude'.return (\ o -> o{peer = P'.append (peer o) v}))