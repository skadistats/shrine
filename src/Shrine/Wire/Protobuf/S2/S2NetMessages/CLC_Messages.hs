{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2NetMessages.CLC_Messages (CLC_Messages(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CLC_Messages = Clc_ClientInfo
                  | Clc_Move
                  | Clc_VoiceData
                  | Clc_BaselineAck
                  | Clc_ListenEvents
                  | Clc_RespondCvarValue
                  | Clc_FileCRCCheck
                  | Clc_LoadingProgress
                  | Clc_SplitPlayerConnect
                  | Clc_ClientMessage
                  | Clc_SplitPlayerDisconnect
                  | Clc_ServerStatus
                  | Clc_ServerPing
                  | Clc_RequestPause
                  | Clc_CmdKeyValues
                  deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                            Prelude'.Generic)

instance P'.Mergeable CLC_Messages

instance Prelude'.Bounded CLC_Messages where
  minBound = Clc_ClientInfo
  maxBound = Clc_CmdKeyValues

instance P'.Default CLC_Messages where
  defaultValue = Clc_ClientInfo

toMaybe'Enum :: Prelude'.Int -> P'.Maybe CLC_Messages
toMaybe'Enum 20 = Prelude'.Just Clc_ClientInfo
toMaybe'Enum 21 = Prelude'.Just Clc_Move
toMaybe'Enum 22 = Prelude'.Just Clc_VoiceData
toMaybe'Enum 23 = Prelude'.Just Clc_BaselineAck
toMaybe'Enum 24 = Prelude'.Just Clc_ListenEvents
toMaybe'Enum 25 = Prelude'.Just Clc_RespondCvarValue
toMaybe'Enum 26 = Prelude'.Just Clc_FileCRCCheck
toMaybe'Enum 27 = Prelude'.Just Clc_LoadingProgress
toMaybe'Enum 28 = Prelude'.Just Clc_SplitPlayerConnect
toMaybe'Enum 29 = Prelude'.Just Clc_ClientMessage
toMaybe'Enum 30 = Prelude'.Just Clc_SplitPlayerDisconnect
toMaybe'Enum 31 = Prelude'.Just Clc_ServerStatus
toMaybe'Enum 32 = Prelude'.Just Clc_ServerPing
toMaybe'Enum 33 = Prelude'.Just Clc_RequestPause
toMaybe'Enum 34 = Prelude'.Just Clc_CmdKeyValues
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum CLC_Messages where
  fromEnum Clc_ClientInfo = 20
  fromEnum Clc_Move = 21
  fromEnum Clc_VoiceData = 22
  fromEnum Clc_BaselineAck = 23
  fromEnum Clc_ListenEvents = 24
  fromEnum Clc_RespondCvarValue = 25
  fromEnum Clc_FileCRCCheck = 26
  fromEnum Clc_LoadingProgress = 27
  fromEnum Clc_SplitPlayerConnect = 28
  fromEnum Clc_ClientMessage = 29
  fromEnum Clc_SplitPlayerDisconnect = 30
  fromEnum Clc_ServerStatus = 31
  fromEnum Clc_ServerPing = 32
  fromEnum Clc_RequestPause = 33
  fromEnum Clc_CmdKeyValues = 34
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2NetMessages.CLC_Messages")
      . toMaybe'Enum
  succ Clc_ClientInfo = Clc_Move
  succ Clc_Move = Clc_VoiceData
  succ Clc_VoiceData = Clc_BaselineAck
  succ Clc_BaselineAck = Clc_ListenEvents
  succ Clc_ListenEvents = Clc_RespondCvarValue
  succ Clc_RespondCvarValue = Clc_FileCRCCheck
  succ Clc_FileCRCCheck = Clc_LoadingProgress
  succ Clc_LoadingProgress = Clc_SplitPlayerConnect
  succ Clc_SplitPlayerConnect = Clc_ClientMessage
  succ Clc_ClientMessage = Clc_SplitPlayerDisconnect
  succ Clc_SplitPlayerDisconnect = Clc_ServerStatus
  succ Clc_ServerStatus = Clc_ServerPing
  succ Clc_ServerPing = Clc_RequestPause
  succ Clc_RequestPause = Clc_CmdKeyValues
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2NetMessages.CLC_Messages"
  pred Clc_Move = Clc_ClientInfo
  pred Clc_VoiceData = Clc_Move
  pred Clc_BaselineAck = Clc_VoiceData
  pred Clc_ListenEvents = Clc_BaselineAck
  pred Clc_RespondCvarValue = Clc_ListenEvents
  pred Clc_FileCRCCheck = Clc_RespondCvarValue
  pred Clc_LoadingProgress = Clc_FileCRCCheck
  pred Clc_SplitPlayerConnect = Clc_LoadingProgress
  pred Clc_ClientMessage = Clc_SplitPlayerConnect
  pred Clc_SplitPlayerDisconnect = Clc_ClientMessage
  pred Clc_ServerStatus = Clc_SplitPlayerDisconnect
  pred Clc_ServerPing = Clc_ServerStatus
  pred Clc_RequestPause = Clc_ServerPing
  pred Clc_CmdKeyValues = Clc_RequestPause
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2NetMessages.CLC_Messages"

instance P'.Wire CLC_Messages where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB CLC_Messages

instance P'.MessageAPI msg' (msg' -> CLC_Messages) CLC_Messages where
  getVal m' f' = f' m'

instance P'.ReflectEnum CLC_Messages where
  reflectEnum
   = [(20, "Clc_ClientInfo", Clc_ClientInfo), (21, "Clc_Move", Clc_Move), (22, "Clc_VoiceData", Clc_VoiceData),
      (23, "Clc_BaselineAck", Clc_BaselineAck), (24, "Clc_ListenEvents", Clc_ListenEvents),
      (25, "Clc_RespondCvarValue", Clc_RespondCvarValue), (26, "Clc_FileCRCCheck", Clc_FileCRCCheck),
      (27, "Clc_LoadingProgress", Clc_LoadingProgress), (28, "Clc_SplitPlayerConnect", Clc_SplitPlayerConnect),
      (29, "Clc_ClientMessage", Clc_ClientMessage), (30, "Clc_SplitPlayerDisconnect", Clc_SplitPlayerDisconnect),
      (31, "Clc_ServerStatus", Clc_ServerStatus), (32, "Clc_ServerPing", Clc_ServerPing),
      (33, "Clc_RequestPause", Clc_RequestPause), (34, "Clc_CmdKeyValues", Clc_CmdKeyValues)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Netmessages.CLC_Messages") ["Shrine", "Wire", "Protobuf", "S2"] ["S2NetMessages"] "CLC_Messages")
      ["Shrine", "Wire", "Protobuf", "S2", "S2NetMessages", "CLC_Messages.hs"]
      [(20, "Clc_ClientInfo"), (21, "Clc_Move"), (22, "Clc_VoiceData"), (23, "Clc_BaselineAck"), (24, "Clc_ListenEvents"),
       (25, "Clc_RespondCvarValue"), (26, "Clc_FileCRCCheck"), (27, "Clc_LoadingProgress"), (28, "Clc_SplitPlayerConnect"),
       (29, "Clc_ClientMessage"), (30, "Clc_SplitPlayerDisconnect"), (31, "Clc_ServerStatus"), (32, "Clc_ServerPing"),
       (33, "Clc_RequestPause"), (34, "Clc_CmdKeyValues")]

instance P'.TextType CLC_Messages where
  tellT = P'.tellShow
  getT = P'.getRead