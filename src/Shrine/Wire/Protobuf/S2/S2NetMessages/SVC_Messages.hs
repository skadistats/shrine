{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2NetMessages.SVC_Messages (SVC_Messages(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data SVC_Messages = Svc_ServerInfo
                  | Svc_FlattenedSerializer
                  | Svc_ClassInfo
                  | Svc_SetPause
                  | Svc_CreateStringTable
                  | Svc_UpdateStringTable
                  | Svc_VoiceInit
                  | Svc_VoiceData
                  | Svc_Print
                  | Svc_Sounds
                  | Svc_SetView
                  | Svc_ClearAllStringTables
                  | Svc_CmdKeyValues
                  | Svc_BSPDecal
                  | Svc_SplitScreen
                  | Svc_PacketEntities
                  | Svc_Prefetch
                  | Svc_Menu
                  | Svc_GetCvarValue
                  | Svc_StopSound
                  | Svc_PeerList
                  | Svc_PacketReliable
                  | Svc_HLTVStatus
                  | Svc_ServerSteamID
                  | Svc_FullFrameSplit
                  deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                            Prelude'.Generic)

instance P'.Mergeable SVC_Messages

instance Prelude'.Bounded SVC_Messages where
  minBound = Svc_ServerInfo
  maxBound = Svc_FullFrameSplit

instance P'.Default SVC_Messages where
  defaultValue = Svc_ServerInfo

toMaybe'Enum :: Prelude'.Int -> P'.Maybe SVC_Messages
toMaybe'Enum 40 = Prelude'.Just Svc_ServerInfo
toMaybe'Enum 41 = Prelude'.Just Svc_FlattenedSerializer
toMaybe'Enum 42 = Prelude'.Just Svc_ClassInfo
toMaybe'Enum 43 = Prelude'.Just Svc_SetPause
toMaybe'Enum 44 = Prelude'.Just Svc_CreateStringTable
toMaybe'Enum 45 = Prelude'.Just Svc_UpdateStringTable
toMaybe'Enum 46 = Prelude'.Just Svc_VoiceInit
toMaybe'Enum 47 = Prelude'.Just Svc_VoiceData
toMaybe'Enum 48 = Prelude'.Just Svc_Print
toMaybe'Enum 49 = Prelude'.Just Svc_Sounds
toMaybe'Enum 50 = Prelude'.Just Svc_SetView
toMaybe'Enum 51 = Prelude'.Just Svc_ClearAllStringTables
toMaybe'Enum 52 = Prelude'.Just Svc_CmdKeyValues
toMaybe'Enum 53 = Prelude'.Just Svc_BSPDecal
toMaybe'Enum 54 = Prelude'.Just Svc_SplitScreen
toMaybe'Enum 55 = Prelude'.Just Svc_PacketEntities
toMaybe'Enum 56 = Prelude'.Just Svc_Prefetch
toMaybe'Enum 57 = Prelude'.Just Svc_Menu
toMaybe'Enum 58 = Prelude'.Just Svc_GetCvarValue
toMaybe'Enum 59 = Prelude'.Just Svc_StopSound
toMaybe'Enum 60 = Prelude'.Just Svc_PeerList
toMaybe'Enum 61 = Prelude'.Just Svc_PacketReliable
toMaybe'Enum 62 = Prelude'.Just Svc_HLTVStatus
toMaybe'Enum 63 = Prelude'.Just Svc_ServerSteamID
toMaybe'Enum 70 = Prelude'.Just Svc_FullFrameSplit
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum SVC_Messages where
  fromEnum Svc_ServerInfo = 40
  fromEnum Svc_FlattenedSerializer = 41
  fromEnum Svc_ClassInfo = 42
  fromEnum Svc_SetPause = 43
  fromEnum Svc_CreateStringTable = 44
  fromEnum Svc_UpdateStringTable = 45
  fromEnum Svc_VoiceInit = 46
  fromEnum Svc_VoiceData = 47
  fromEnum Svc_Print = 48
  fromEnum Svc_Sounds = 49
  fromEnum Svc_SetView = 50
  fromEnum Svc_ClearAllStringTables = 51
  fromEnum Svc_CmdKeyValues = 52
  fromEnum Svc_BSPDecal = 53
  fromEnum Svc_SplitScreen = 54
  fromEnum Svc_PacketEntities = 55
  fromEnum Svc_Prefetch = 56
  fromEnum Svc_Menu = 57
  fromEnum Svc_GetCvarValue = 58
  fromEnum Svc_StopSound = 59
  fromEnum Svc_PeerList = 60
  fromEnum Svc_PacketReliable = 61
  fromEnum Svc_HLTVStatus = 62
  fromEnum Svc_ServerSteamID = 63
  fromEnum Svc_FullFrameSplit = 70
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2NetMessages.SVC_Messages")
      . toMaybe'Enum
  succ Svc_ServerInfo = Svc_FlattenedSerializer
  succ Svc_FlattenedSerializer = Svc_ClassInfo
  succ Svc_ClassInfo = Svc_SetPause
  succ Svc_SetPause = Svc_CreateStringTable
  succ Svc_CreateStringTable = Svc_UpdateStringTable
  succ Svc_UpdateStringTable = Svc_VoiceInit
  succ Svc_VoiceInit = Svc_VoiceData
  succ Svc_VoiceData = Svc_Print
  succ Svc_Print = Svc_Sounds
  succ Svc_Sounds = Svc_SetView
  succ Svc_SetView = Svc_ClearAllStringTables
  succ Svc_ClearAllStringTables = Svc_CmdKeyValues
  succ Svc_CmdKeyValues = Svc_BSPDecal
  succ Svc_BSPDecal = Svc_SplitScreen
  succ Svc_SplitScreen = Svc_PacketEntities
  succ Svc_PacketEntities = Svc_Prefetch
  succ Svc_Prefetch = Svc_Menu
  succ Svc_Menu = Svc_GetCvarValue
  succ Svc_GetCvarValue = Svc_StopSound
  succ Svc_StopSound = Svc_PeerList
  succ Svc_PeerList = Svc_PacketReliable
  succ Svc_PacketReliable = Svc_HLTVStatus
  succ Svc_HLTVStatus = Svc_ServerSteamID
  succ Svc_ServerSteamID = Svc_FullFrameSplit
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2NetMessages.SVC_Messages"
  pred Svc_FlattenedSerializer = Svc_ServerInfo
  pred Svc_ClassInfo = Svc_FlattenedSerializer
  pred Svc_SetPause = Svc_ClassInfo
  pred Svc_CreateStringTable = Svc_SetPause
  pred Svc_UpdateStringTable = Svc_CreateStringTable
  pred Svc_VoiceInit = Svc_UpdateStringTable
  pred Svc_VoiceData = Svc_VoiceInit
  pred Svc_Print = Svc_VoiceData
  pred Svc_Sounds = Svc_Print
  pred Svc_SetView = Svc_Sounds
  pred Svc_ClearAllStringTables = Svc_SetView
  pred Svc_CmdKeyValues = Svc_ClearAllStringTables
  pred Svc_BSPDecal = Svc_CmdKeyValues
  pred Svc_SplitScreen = Svc_BSPDecal
  pred Svc_PacketEntities = Svc_SplitScreen
  pred Svc_Prefetch = Svc_PacketEntities
  pred Svc_Menu = Svc_Prefetch
  pred Svc_GetCvarValue = Svc_Menu
  pred Svc_StopSound = Svc_GetCvarValue
  pred Svc_PeerList = Svc_StopSound
  pred Svc_PacketReliable = Svc_PeerList
  pred Svc_HLTVStatus = Svc_PacketReliable
  pred Svc_ServerSteamID = Svc_HLTVStatus
  pred Svc_FullFrameSplit = Svc_ServerSteamID
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2NetMessages.SVC_Messages"

instance P'.Wire SVC_Messages where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB SVC_Messages

instance P'.MessageAPI msg' (msg' -> SVC_Messages) SVC_Messages where
  getVal m' f' = f' m'

instance P'.ReflectEnum SVC_Messages where
  reflectEnum
   = [(40, "Svc_ServerInfo", Svc_ServerInfo), (41, "Svc_FlattenedSerializer", Svc_FlattenedSerializer),
      (42, "Svc_ClassInfo", Svc_ClassInfo), (43, "Svc_SetPause", Svc_SetPause),
      (44, "Svc_CreateStringTable", Svc_CreateStringTable), (45, "Svc_UpdateStringTable", Svc_UpdateStringTable),
      (46, "Svc_VoiceInit", Svc_VoiceInit), (47, "Svc_VoiceData", Svc_VoiceData), (48, "Svc_Print", Svc_Print),
      (49, "Svc_Sounds", Svc_Sounds), (50, "Svc_SetView", Svc_SetView), (51, "Svc_ClearAllStringTables", Svc_ClearAllStringTables),
      (52, "Svc_CmdKeyValues", Svc_CmdKeyValues), (53, "Svc_BSPDecal", Svc_BSPDecal), (54, "Svc_SplitScreen", Svc_SplitScreen),
      (55, "Svc_PacketEntities", Svc_PacketEntities), (56, "Svc_Prefetch", Svc_Prefetch), (57, "Svc_Menu", Svc_Menu),
      (58, "Svc_GetCvarValue", Svc_GetCvarValue), (59, "Svc_StopSound", Svc_StopSound), (60, "Svc_PeerList", Svc_PeerList),
      (61, "Svc_PacketReliable", Svc_PacketReliable), (62, "Svc_HLTVStatus", Svc_HLTVStatus),
      (63, "Svc_ServerSteamID", Svc_ServerSteamID), (70, "Svc_FullFrameSplit", Svc_FullFrameSplit)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Netmessages.SVC_Messages") ["Shrine", "Wire", "Protobuf", "S2"] ["S2NetMessages"] "SVC_Messages")
      ["Shrine", "Wire", "Protobuf", "S2", "S2NetMessages", "SVC_Messages.hs"]
      [(40, "Svc_ServerInfo"), (41, "Svc_FlattenedSerializer"), (42, "Svc_ClassInfo"), (43, "Svc_SetPause"),
       (44, "Svc_CreateStringTable"), (45, "Svc_UpdateStringTable"), (46, "Svc_VoiceInit"), (47, "Svc_VoiceData"),
       (48, "Svc_Print"), (49, "Svc_Sounds"), (50, "Svc_SetView"), (51, "Svc_ClearAllStringTables"), (52, "Svc_CmdKeyValues"),
       (53, "Svc_BSPDecal"), (54, "Svc_SplitScreen"), (55, "Svc_PacketEntities"), (56, "Svc_Prefetch"), (57, "Svc_Menu"),
       (58, "Svc_GetCvarValue"), (59, "Svc_StopSound"), (60, "Svc_PeerList"), (61, "Svc_PacketReliable"), (62, "Svc_HLTVStatus"),
       (63, "Svc_ServerSteamID"), (70, "Svc_FullFrameSplit")]

instance P'.TextType SVC_Messages where
  tellT = P'.tellShow
  getT = P'.getRead