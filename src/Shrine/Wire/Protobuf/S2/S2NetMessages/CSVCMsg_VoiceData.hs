{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_VoiceData (CSVCMsg_VoiceData(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.Common.NetMessages.CMsgVoiceAudio as NetMessages (CMsgVoiceAudio)

data CSVCMsg_VoiceData = CSVCMsg_VoiceData{audio :: !(P'.Maybe NetMessages.CMsgVoiceAudio), client :: !(P'.Maybe P'.Int32),
                                           proximity :: !(P'.Maybe P'.Bool), xuid :: !(P'.Maybe P'.Word64),
                                           audible_mask :: !(P'.Maybe P'.Int32), tick :: !(P'.Maybe P'.Word32)}
                       deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CSVCMsg_VoiceData where
  mergeAppend (CSVCMsg_VoiceData x'1 x'2 x'3 x'4 x'5 x'6) (CSVCMsg_VoiceData y'1 y'2 y'3 y'4 y'5 y'6)
   = CSVCMsg_VoiceData (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CSVCMsg_VoiceData where
  defaultValue = CSVCMsg_VoiceData P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CSVCMsg_VoiceData where
  wireSize ft' self'@(CSVCMsg_VoiceData x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 11 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 8 x'3 + P'.wireSizeOpt 1 6 x'4 +
             P'.wireSizeOpt 1 5 x'5
             + P'.wireSizeOpt 1 13 x'6)
  wirePut ft' self'@(CSVCMsg_VoiceData x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 11 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 8 x'3
             P'.wirePutOpt 33 6 x'4
             P'.wirePutOpt 40 5 x'5
             P'.wirePutOpt 48 13 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{audio = P'.mergeAppend (audio old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{client = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{proximity = Prelude'.Just new'Field}) (P'.wireGet 8)
             33 -> Prelude'.fmap (\ !new'Field -> old'Self{xuid = Prelude'.Just new'Field}) (P'.wireGet 6)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{audible_mask = Prelude'.Just new'Field}) (P'.wireGet 5)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{tick = Prelude'.Just new'Field}) (P'.wireGet 13)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_VoiceData) CSVCMsg_VoiceData where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_VoiceData

instance P'.ReflectDescriptor CSVCMsg_VoiceData where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 16, 24, 33, 40, 48])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Netmessages.CSVCMsg_VoiceData\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"CSVCMsg_VoiceData\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2NetMessages\",\"CSVCMsg_VoiceData.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_VoiceData.audio\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_VoiceData\"], baseName' = FName \"audio\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Netmessages.CMsgVoiceAudio\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetMessages\"], baseName = MName \"CMsgVoiceAudio\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_VoiceData.client\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_VoiceData\"], baseName' = FName \"client\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_VoiceData.proximity\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_VoiceData\"], baseName' = FName \"proximity\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_VoiceData.xuid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_VoiceData\"], baseName' = FName \"xuid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 33}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_VoiceData.audible_mask\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_VoiceData\"], baseName' = FName \"audible_mask\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_VoiceData.tick\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_VoiceData\"], baseName' = FName \"tick\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_VoiceData where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_VoiceData where
  textPut msg
   = do
       P'.tellT "audio" (audio msg)
       P'.tellT "client" (client msg)
       P'.tellT "proximity" (proximity msg)
       P'.tellT "xuid" (xuid msg)
       P'.tellT "audible_mask" (audible_mask msg)
       P'.tellT "tick" (tick msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'audio, parse'client, parse'proximity, parse'xuid, parse'audible_mask, parse'tick])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'audio
         = P'.try
            (do
               v <- P'.getT "audio"
               Prelude'.return (\ o -> o{audio = v}))
        parse'client
         = P'.try
            (do
               v <- P'.getT "client"
               Prelude'.return (\ o -> o{client = v}))
        parse'proximity
         = P'.try
            (do
               v <- P'.getT "proximity"
               Prelude'.return (\ o -> o{proximity = v}))
        parse'xuid
         = P'.try
            (do
               v <- P'.getT "xuid"
               Prelude'.return (\ o -> o{xuid = v}))
        parse'audible_mask
         = P'.try
            (do
               v <- P'.getT "audible_mask"
               Prelude'.return (\ o -> o{audible_mask = v}))
        parse'tick
         = P'.try
            (do
               v <- P'.getT "tick"
               Prelude'.return (\ o -> o{tick = v}))
