{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_CreateStringTable (CSVCMsg_CreateStringTable(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CSVCMsg_CreateStringTable = CSVCMsg_CreateStringTable{name :: !(P'.Maybe P'.Utf8), num_entries :: !(P'.Maybe P'.Int32),
                                                           user_data_fixed_size :: !(P'.Maybe P'.Bool),
                                                           user_data_size :: !(P'.Maybe P'.Int32),
                                                           user_data_size_bits :: !(P'.Maybe P'.Int32),
                                                           flags :: !(P'.Maybe P'.Int32), string_data :: !(P'.Maybe P'.ByteString),
                                                           uncompressed_size :: !(P'.Maybe P'.Int32),
                                                           data_compressed :: !(P'.Maybe P'.Bool)}
                               deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                         Prelude'.Generic)

instance P'.Mergeable CSVCMsg_CreateStringTable where
  mergeAppend (CSVCMsg_CreateStringTable x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9)
   (CSVCMsg_CreateStringTable y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9)
   = CSVCMsg_CreateStringTable (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)

instance P'.Default CSVCMsg_CreateStringTable where
  defaultValue
   = CSVCMsg_CreateStringTable P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CSVCMsg_CreateStringTable where
  wireSize ft' self'@(CSVCMsg_CreateStringTable x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 9 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 8 x'3 + P'.wireSizeOpt 1 5 x'4 +
             P'.wireSizeOpt 1 5 x'5
             + P'.wireSizeOpt 1 5 x'6
             + P'.wireSizeOpt 1 12 x'7
             + P'.wireSizeOpt 1 5 x'8
             + P'.wireSizeOpt 1 8 x'9)
  wirePut ft' self'@(CSVCMsg_CreateStringTable x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 8 x'3
             P'.wirePutOpt 32 5 x'4
             P'.wirePutOpt 40 5 x'5
             P'.wirePutOpt 48 5 x'6
             P'.wirePutOpt 58 12 x'7
             P'.wirePutOpt 64 5 x'8
             P'.wirePutOpt 72 8 x'9
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{name = Prelude'.Just new'Field}) (P'.wireGet 9)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{num_entries = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{user_data_fixed_size = Prelude'.Just new'Field}) (P'.wireGet 8)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{user_data_size = Prelude'.Just new'Field}) (P'.wireGet 5)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{user_data_size_bits = Prelude'.Just new'Field}) (P'.wireGet 5)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{flags = Prelude'.Just new'Field}) (P'.wireGet 5)
             58 -> Prelude'.fmap (\ !new'Field -> old'Self{string_data = Prelude'.Just new'Field}) (P'.wireGet 12)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{uncompressed_size = Prelude'.Just new'Field}) (P'.wireGet 5)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{data_compressed = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_CreateStringTable) CSVCMsg_CreateStringTable where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_CreateStringTable

instance P'.ReflectDescriptor CSVCMsg_CreateStringTable where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 16, 24, 32, 40, 48, 58, 64, 72])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"CSVCMsg_CreateStringTable\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2NetMessages\",\"CSVCMsg_CreateStringTable.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable.name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_CreateStringTable\"], baseName' = FName \"name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable.num_entries\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_CreateStringTable\"], baseName' = FName \"num_entries\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable.user_data_fixed_size\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_CreateStringTable\"], baseName' = FName \"user_data_fixed_size\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable.user_data_size\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_CreateStringTable\"], baseName' = FName \"user_data_size\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable.user_data_size_bits\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_CreateStringTable\"], baseName' = FName \"user_data_size_bits\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable.flags\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_CreateStringTable\"], baseName' = FName \"flags\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable.string_data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_CreateStringTable\"], baseName' = FName \"string_data\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 58}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable.uncompressed_size\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_CreateStringTable\"], baseName' = FName \"uncompressed_size\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_CreateStringTable.data_compressed\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_CreateStringTable\"], baseName' = FName \"data_compressed\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_CreateStringTable where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_CreateStringTable where
  textPut msg
   = do
       P'.tellT "name" (name msg)
       P'.tellT "num_entries" (num_entries msg)
       P'.tellT "user_data_fixed_size" (user_data_fixed_size msg)
       P'.tellT "user_data_size" (user_data_size msg)
       P'.tellT "user_data_size_bits" (user_data_size_bits msg)
       P'.tellT "flags" (flags msg)
       P'.tellT "string_data" (string_data msg)
       P'.tellT "uncompressed_size" (uncompressed_size msg)
       P'.tellT "data_compressed" (data_compressed msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'name, parse'num_entries, parse'user_data_fixed_size, parse'user_data_size, parse'user_data_size_bits,
                   parse'flags, parse'string_data, parse'uncompressed_size, parse'data_compressed])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'name
         = P'.try
            (do
               v <- P'.getT "name"
               Prelude'.return (\ o -> o{name = v}))
        parse'num_entries
         = P'.try
            (do
               v <- P'.getT "num_entries"
               Prelude'.return (\ o -> o{num_entries = v}))
        parse'user_data_fixed_size
         = P'.try
            (do
               v <- P'.getT "user_data_fixed_size"
               Prelude'.return (\ o -> o{user_data_fixed_size = v}))
        parse'user_data_size
         = P'.try
            (do
               v <- P'.getT "user_data_size"
               Prelude'.return (\ o -> o{user_data_size = v}))
        parse'user_data_size_bits
         = P'.try
            (do
               v <- P'.getT "user_data_size_bits"
               Prelude'.return (\ o -> o{user_data_size_bits = v}))
        parse'flags
         = P'.try
            (do
               v <- P'.getT "flags"
               Prelude'.return (\ o -> o{flags = v}))
        parse'string_data
         = P'.try
            (do
               v <- P'.getT "string_data"
               Prelude'.return (\ o -> o{string_data = v}))
        parse'uncompressed_size
         = P'.try
            (do
               v <- P'.getT "uncompressed_size"
               Prelude'.return (\ o -> o{uncompressed_size = v}))
        parse'data_compressed
         = P'.try
            (do
               v <- P'.getT "data_compressed"
               Prelude'.return (\ o -> o{data_compressed = v}))