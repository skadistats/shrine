{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2NetMessages.CMsgServerPeer (CMsgServerPeer(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.CMsgIPCAddress as S2NetMessages (CMsgIPCAddress)

data CMsgServerPeer = CMsgServerPeer{player_slot :: !(P'.Maybe P'.Int32), steamid :: !(P'.Maybe P'.Word64),
                                     ipc :: !(P'.Maybe S2NetMessages.CMsgIPCAddress), they_hear_you :: !(P'.Maybe P'.Bool),
                                     you_hear_them :: !(P'.Maybe P'.Bool), is_listenserver_host :: !(P'.Maybe P'.Bool)}
                    deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CMsgServerPeer where
  mergeAppend (CMsgServerPeer x'1 x'2 x'3 x'4 x'5 x'6) (CMsgServerPeer y'1 y'2 y'3 y'4 y'5 y'6)
   = CMsgServerPeer (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default CMsgServerPeer where
  defaultValue = CMsgServerPeer P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CMsgServerPeer where
  wireSize ft' self'@(CMsgServerPeer x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 6 x'2 + P'.wireSizeOpt 1 11 x'3 + P'.wireSizeOpt 1 8 x'4 +
             P'.wireSizeOpt 1 8 x'5
             + P'.wireSizeOpt 1 8 x'6)
  wirePut ft' self'@(CMsgServerPeer x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 17 6 x'2
             P'.wirePutOpt 26 11 x'3
             P'.wirePutOpt 32 8 x'4
             P'.wirePutOpt 40 8 x'5
             P'.wirePutOpt 48 8 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{player_slot = Prelude'.Just new'Field}) (P'.wireGet 5)
             17 -> Prelude'.fmap (\ !new'Field -> old'Self{steamid = Prelude'.Just new'Field}) (P'.wireGet 6)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{ipc = P'.mergeAppend (ipc old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{they_hear_you = Prelude'.Just new'Field}) (P'.wireGet 8)
             40 -> Prelude'.fmap (\ !new'Field -> old'Self{you_hear_them = Prelude'.Just new'Field}) (P'.wireGet 8)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{is_listenserver_host = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CMsgServerPeer) CMsgServerPeer where
  getVal m' f' = f' m'

instance P'.GPB CMsgServerPeer

instance P'.ReflectDescriptor CMsgServerPeer where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 17, 26, 32, 40, 48])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Netmessages.CMsgServerPeer\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"CMsgServerPeer\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2NetMessages\",\"CMsgServerPeer.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CMsgServerPeer.player_slot\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CMsgServerPeer\"], baseName' = FName \"player_slot\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CMsgServerPeer.steamid\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CMsgServerPeer\"], baseName' = FName \"steamid\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 17}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 6}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CMsgServerPeer.ipc\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CMsgServerPeer\"], baseName' = FName \"ipc\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Netmessages.CMsgIPCAddress\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"CMsgIPCAddress\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CMsgServerPeer.they_hear_you\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CMsgServerPeer\"], baseName' = FName \"they_hear_you\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CMsgServerPeer.you_hear_them\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CMsgServerPeer\"], baseName' = FName \"you_hear_them\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 40}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CMsgServerPeer.is_listenserver_host\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CMsgServerPeer\"], baseName' = FName \"is_listenserver_host\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CMsgServerPeer where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CMsgServerPeer where
  textPut msg
   = do
       P'.tellT "player_slot" (player_slot msg)
       P'.tellT "steamid" (steamid msg)
       P'.tellT "ipc" (ipc msg)
       P'.tellT "they_hear_you" (they_hear_you msg)
       P'.tellT "you_hear_them" (you_hear_them msg)
       P'.tellT "is_listenserver_host" (is_listenserver_host msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'player_slot, parse'steamid, parse'ipc, parse'they_hear_you, parse'you_hear_them,
                   parse'is_listenserver_host])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'player_slot
         = P'.try
            (do
               v <- P'.getT "player_slot"
               Prelude'.return (\ o -> o{player_slot = v}))
        parse'steamid
         = P'.try
            (do
               v <- P'.getT "steamid"
               Prelude'.return (\ o -> o{steamid = v}))
        parse'ipc
         = P'.try
            (do
               v <- P'.getT "ipc"
               Prelude'.return (\ o -> o{ipc = v}))
        parse'they_hear_you
         = P'.try
            (do
               v <- P'.getT "they_hear_you"
               Prelude'.return (\ o -> o{they_hear_you = v}))
        parse'you_hear_them
         = P'.try
            (do
               v <- P'.getT "you_hear_them"
               Prelude'.return (\ o -> o{you_hear_them = v}))
        parse'is_listenserver_host
         = P'.try
            (do
               v <- P'.getT "is_listenserver_host"
               Prelude'.return (\ o -> o{is_listenserver_host = v}))