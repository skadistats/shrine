{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2NetMessages.CSVCMsg_FlattenedSerializer (CSVCMsg_FlattenedSerializer(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.ProtoFlattenedSerializerField_t as S2NetMessages
       (ProtoFlattenedSerializerField_t)
import qualified Shrine.Wire.Protobuf.S2.S2NetMessages.ProtoFlattenedSerializer_t as S2NetMessages (ProtoFlattenedSerializer_t)

data CSVCMsg_FlattenedSerializer = CSVCMsg_FlattenedSerializer{serializers :: !(P'.Seq S2NetMessages.ProtoFlattenedSerializer_t),
                                                               symbols :: !(P'.Seq P'.Utf8),
                                                               fields :: !(P'.Seq S2NetMessages.ProtoFlattenedSerializerField_t)}
                                 deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                           Prelude'.Generic)

instance P'.Mergeable CSVCMsg_FlattenedSerializer where
  mergeAppend (CSVCMsg_FlattenedSerializer x'1 x'2 x'3) (CSVCMsg_FlattenedSerializer y'1 y'2 y'3)
   = CSVCMsg_FlattenedSerializer (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default CSVCMsg_FlattenedSerializer where
  defaultValue = CSVCMsg_FlattenedSerializer P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CSVCMsg_FlattenedSerializer where
  wireSize ft' self'@(CSVCMsg_FlattenedSerializer x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 11 x'1 + P'.wireSizeRep 1 9 x'2 + P'.wireSizeRep 1 11 x'3)
  wirePut ft' self'@(CSVCMsg_FlattenedSerializer x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 10 11 x'1
             P'.wirePutRep 18 9 x'2
             P'.wirePutRep 26 11 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{serializers = P'.append (serializers old'Self) new'Field})
                    (P'.wireGet 11)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{symbols = P'.append (symbols old'Self) new'Field}) (P'.wireGet 9)
             26 -> Prelude'.fmap (\ !new'Field -> old'Self{fields = P'.append (fields old'Self) new'Field}) (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CSVCMsg_FlattenedSerializer) CSVCMsg_FlattenedSerializer where
  getVal m' f' = f' m'

instance P'.GPB CSVCMsg_FlattenedSerializer

instance P'.ReflectDescriptor CSVCMsg_FlattenedSerializer where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18, 26])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Netmessages.CSVCMsg_FlattenedSerializer\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"CSVCMsg_FlattenedSerializer\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2NetMessages\",\"CSVCMsg_FlattenedSerializer.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_FlattenedSerializer.serializers\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_FlattenedSerializer\"], baseName' = FName \"serializers\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Netmessages.ProtoFlattenedSerializer_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"ProtoFlattenedSerializer_t\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_FlattenedSerializer.symbols\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_FlattenedSerializer\"], baseName' = FName \"symbols\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.CSVCMsg_FlattenedSerializer.fields\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"CSVCMsg_FlattenedSerializer\"], baseName' = FName \"fields\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"ProtoFlattenedSerializerField_t\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CSVCMsg_FlattenedSerializer where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CSVCMsg_FlattenedSerializer where
  textPut msg
   = do
       P'.tellT "serializers" (serializers msg)
       P'.tellT "symbols" (symbols msg)
       P'.tellT "fields" (fields msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'serializers, parse'symbols, parse'fields]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'serializers
         = P'.try
            (do
               v <- P'.getT "serializers"
               Prelude'.return (\ o -> o{serializers = P'.append (serializers o) v}))
        parse'symbols
         = P'.try
            (do
               v <- P'.getT "symbols"
               Prelude'.return (\ o -> o{symbols = P'.append (symbols o) v}))
        parse'fields
         = P'.try
            (do
               v <- P'.getT "fields"
               Prelude'.return (\ o -> o{fields = P'.append (fields o) v}))