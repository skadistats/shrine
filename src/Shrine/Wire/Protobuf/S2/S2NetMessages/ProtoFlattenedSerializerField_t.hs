{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2NetMessages.ProtoFlattenedSerializerField_t (ProtoFlattenedSerializerField_t(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data ProtoFlattenedSerializerField_t = ProtoFlattenedSerializerField_t{var_type_sym :: !(P'.Maybe P'.Int32),
                                                                       var_name_sym :: !(P'.Maybe P'.Int32),
                                                                       bit_count :: !(P'.Maybe P'.Int32),
                                                                       low_value :: !(P'.Maybe P'.Float),
                                                                       high_value :: !(P'.Maybe P'.Float),
                                                                       encode_flags :: !(P'.Maybe P'.Int32),
                                                                       field_serializer_name_sym :: !(P'.Maybe P'.Int32),
                                                                       field_serializer_version :: !(P'.Maybe P'.Int32),
                                                                       send_node_sym :: !(P'.Maybe P'.Int32),
                                                                       var_encoder_sym :: !(P'.Maybe P'.Int32)}
                                     deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                               Prelude'.Generic)

instance P'.Mergeable ProtoFlattenedSerializerField_t where
  mergeAppend (ProtoFlattenedSerializerField_t x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   (ProtoFlattenedSerializerField_t y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10)
   = ProtoFlattenedSerializerField_t (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)
      (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)

instance P'.Default ProtoFlattenedSerializerField_t where
  defaultValue
   = ProtoFlattenedSerializerField_t P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire ProtoFlattenedSerializerField_t where
  wireSize ft' self'@(ProtoFlattenedSerializerField_t x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeOpt 1 2 x'4 +
             P'.wireSizeOpt 1 2 x'5
             + P'.wireSizeOpt 1 5 x'6
             + P'.wireSizeOpt 1 5 x'7
             + P'.wireSizeOpt 1 5 x'8
             + P'.wireSizeOpt 1 5 x'9
             + P'.wireSizeOpt 1 5 x'10)
  wirePut ft' self'@(ProtoFlattenedSerializerField_t x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutOpt 37 2 x'4
             P'.wirePutOpt 45 2 x'5
             P'.wirePutOpt 48 5 x'6
             P'.wirePutOpt 56 5 x'7
             P'.wirePutOpt 64 5 x'8
             P'.wirePutOpt 72 5 x'9
             P'.wirePutOpt 80 5 x'10
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{var_type_sym = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{var_name_sym = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{bit_count = Prelude'.Just new'Field}) (P'.wireGet 5)
             37 -> Prelude'.fmap (\ !new'Field -> old'Self{low_value = Prelude'.Just new'Field}) (P'.wireGet 2)
             45 -> Prelude'.fmap (\ !new'Field -> old'Self{high_value = Prelude'.Just new'Field}) (P'.wireGet 2)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{encode_flags = Prelude'.Just new'Field}) (P'.wireGet 5)
             56 -> Prelude'.fmap (\ !new'Field -> old'Self{field_serializer_name_sym = Prelude'.Just new'Field}) (P'.wireGet 5)
             64 -> Prelude'.fmap (\ !new'Field -> old'Self{field_serializer_version = Prelude'.Just new'Field}) (P'.wireGet 5)
             72 -> Prelude'.fmap (\ !new'Field -> old'Self{send_node_sym = Prelude'.Just new'Field}) (P'.wireGet 5)
             80 -> Prelude'.fmap (\ !new'Field -> old'Self{var_encoder_sym = Prelude'.Just new'Field}) (P'.wireGet 5)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> ProtoFlattenedSerializerField_t) ProtoFlattenedSerializerField_t where
  getVal m' f' = f' m'

instance P'.GPB ProtoFlattenedSerializerField_t

instance P'.ReflectDescriptor ProtoFlattenedSerializerField_t where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 37, 45, 48, 56, 64, 72, 80])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2NetMessages\"], baseName = MName \"ProtoFlattenedSerializerField_t\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2NetMessages\",\"ProtoFlattenedSerializerField_t.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.var_type_sym\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"var_type_sym\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.var_name_sym\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"var_name_sym\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.bit_count\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"bit_count\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.low_value\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"low_value\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 37}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.high_value\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"high_value\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 45}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 2}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.encode_flags\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"encode_flags\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.field_serializer_name_sym\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"field_serializer_name_sym\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 56}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.field_serializer_version\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"field_serializer_version\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 64}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.send_node_sym\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"send_node_sym\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 72}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Netmessages.ProtoFlattenedSerializerField_t.var_encoder_sym\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2NetMessages\",MName \"ProtoFlattenedSerializerField_t\"], baseName' = FName \"var_encoder_sym\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 80}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType ProtoFlattenedSerializerField_t where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg ProtoFlattenedSerializerField_t where
  textPut msg
   = do
       P'.tellT "var_type_sym" (var_type_sym msg)
       P'.tellT "var_name_sym" (var_name_sym msg)
       P'.tellT "bit_count" (bit_count msg)
       P'.tellT "low_value" (low_value msg)
       P'.tellT "high_value" (high_value msg)
       P'.tellT "encode_flags" (encode_flags msg)
       P'.tellT "field_serializer_name_sym" (field_serializer_name_sym msg)
       P'.tellT "field_serializer_version" (field_serializer_version msg)
       P'.tellT "send_node_sym" (send_node_sym msg)
       P'.tellT "var_encoder_sym" (var_encoder_sym msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'var_type_sym, parse'var_name_sym, parse'bit_count, parse'low_value, parse'high_value, parse'encode_flags,
                   parse'field_serializer_name_sym, parse'field_serializer_version, parse'send_node_sym, parse'var_encoder_sym])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'var_type_sym
         = P'.try
            (do
               v <- P'.getT "var_type_sym"
               Prelude'.return (\ o -> o{var_type_sym = v}))
        parse'var_name_sym
         = P'.try
            (do
               v <- P'.getT "var_name_sym"
               Prelude'.return (\ o -> o{var_name_sym = v}))
        parse'bit_count
         = P'.try
            (do
               v <- P'.getT "bit_count"
               Prelude'.return (\ o -> o{bit_count = v}))
        parse'low_value
         = P'.try
            (do
               v <- P'.getT "low_value"
               Prelude'.return (\ o -> o{low_value = v}))
        parse'high_value
         = P'.try
            (do
               v <- P'.getT "high_value"
               Prelude'.return (\ o -> o{high_value = v}))
        parse'encode_flags
         = P'.try
            (do
               v <- P'.getT "encode_flags"
               Prelude'.return (\ o -> o{encode_flags = v}))
        parse'field_serializer_name_sym
         = P'.try
            (do
               v <- P'.getT "field_serializer_name_sym"
               Prelude'.return (\ o -> o{field_serializer_name_sym = v}))
        parse'field_serializer_version
         = P'.try
            (do
               v <- P'.getT "field_serializer_version"
               Prelude'.return (\ o -> o{field_serializer_version = v}))
        parse'send_node_sym
         = P'.try
            (do
               v <- P'.getT "send_node_sym"
               Prelude'.return (\ o -> o{send_node_sym = v}))
        parse'var_encoder_sym
         = P'.try
            (do
               v <- P'.getT "var_encoder_sym"
               Prelude'.return (\ o -> o{var_encoder_sym = v}))