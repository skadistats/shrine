{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.EBaseUserMessages (EBaseUserMessages(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EBaseUserMessages = UM_AchievementEvent
                       | UM_CloseCaption
                       | UM_CloseCaptionDirect
                       | UM_CurrentTimescale
                       | UM_DesiredTimescale
                       | UM_Fade
                       | UM_GameTitle
                       | UM_HintText
                       | UM_HudMsg
                       | UM_HudText
                       | UM_KeyHintText
                       | UM_ColoredText
                       | UM_RequestState
                       | UM_ResetHUD
                       | UM_Rumble
                       | UM_SayText
                       | UM_SayText2
                       | UM_SayTextChannel
                       | UM_Shake
                       | UM_ShakeDir
                       | UM_TextMsg
                       | UM_ScreenTilt
                       | UM_Train
                       | UM_VGUIMenu
                       | UM_VoiceMask
                       | UM_VoiceSubtitle
                       | UM_SendAudio
                       | UM_ItemPickup
                       | UM_AmmoDenied
                       | UM_CrosshairAngle
                       | UM_ShowMenu
                       | UM_CreditsMsg
                       | UM_CloseCaptionPlaceholder
                       | UM_CameraTransition
                       | UM_AudioParameter
                       | UM_ParticleManager
                       | UM_HudError
                       | UM_CustomGameEvent
                       | UM_HandHapticPulse
                       | UM_AnimGraphUpdate
                       | UM_HandHapticPulsePrecise
                       | UM_MAX_BASE
                       deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                 Prelude'.Generic)

instance P'.Mergeable EBaseUserMessages

instance Prelude'.Bounded EBaseUserMessages where
  minBound = UM_AchievementEvent
  maxBound = UM_MAX_BASE

instance P'.Default EBaseUserMessages where
  defaultValue = UM_AchievementEvent

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EBaseUserMessages
toMaybe'Enum 101 = Prelude'.Just UM_AchievementEvent
toMaybe'Enum 102 = Prelude'.Just UM_CloseCaption
toMaybe'Enum 103 = Prelude'.Just UM_CloseCaptionDirect
toMaybe'Enum 104 = Prelude'.Just UM_CurrentTimescale
toMaybe'Enum 105 = Prelude'.Just UM_DesiredTimescale
toMaybe'Enum 106 = Prelude'.Just UM_Fade
toMaybe'Enum 107 = Prelude'.Just UM_GameTitle
toMaybe'Enum 109 = Prelude'.Just UM_HintText
toMaybe'Enum 110 = Prelude'.Just UM_HudMsg
toMaybe'Enum 111 = Prelude'.Just UM_HudText
toMaybe'Enum 112 = Prelude'.Just UM_KeyHintText
toMaybe'Enum 113 = Prelude'.Just UM_ColoredText
toMaybe'Enum 114 = Prelude'.Just UM_RequestState
toMaybe'Enum 115 = Prelude'.Just UM_ResetHUD
toMaybe'Enum 116 = Prelude'.Just UM_Rumble
toMaybe'Enum 117 = Prelude'.Just UM_SayText
toMaybe'Enum 118 = Prelude'.Just UM_SayText2
toMaybe'Enum 119 = Prelude'.Just UM_SayTextChannel
toMaybe'Enum 120 = Prelude'.Just UM_Shake
toMaybe'Enum 121 = Prelude'.Just UM_ShakeDir
toMaybe'Enum 124 = Prelude'.Just UM_TextMsg
toMaybe'Enum 125 = Prelude'.Just UM_ScreenTilt
toMaybe'Enum 126 = Prelude'.Just UM_Train
toMaybe'Enum 127 = Prelude'.Just UM_VGUIMenu
toMaybe'Enum 128 = Prelude'.Just UM_VoiceMask
toMaybe'Enum 129 = Prelude'.Just UM_VoiceSubtitle
toMaybe'Enum 130 = Prelude'.Just UM_SendAudio
toMaybe'Enum 131 = Prelude'.Just UM_ItemPickup
toMaybe'Enum 132 = Prelude'.Just UM_AmmoDenied
toMaybe'Enum 133 = Prelude'.Just UM_CrosshairAngle
toMaybe'Enum 134 = Prelude'.Just UM_ShowMenu
toMaybe'Enum 135 = Prelude'.Just UM_CreditsMsg
toMaybe'Enum 142 = Prelude'.Just UM_CloseCaptionPlaceholder
toMaybe'Enum 143 = Prelude'.Just UM_CameraTransition
toMaybe'Enum 144 = Prelude'.Just UM_AudioParameter
toMaybe'Enum 145 = Prelude'.Just UM_ParticleManager
toMaybe'Enum 146 = Prelude'.Just UM_HudError
toMaybe'Enum 148 = Prelude'.Just UM_CustomGameEvent
toMaybe'Enum 149 = Prelude'.Just UM_HandHapticPulse
toMaybe'Enum 150 = Prelude'.Just UM_AnimGraphUpdate
toMaybe'Enum 151 = Prelude'.Just UM_HandHapticPulsePrecise
toMaybe'Enum 200 = Prelude'.Just UM_MAX_BASE
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EBaseUserMessages where
  fromEnum UM_AchievementEvent = 101
  fromEnum UM_CloseCaption = 102
  fromEnum UM_CloseCaptionDirect = 103
  fromEnum UM_CurrentTimescale = 104
  fromEnum UM_DesiredTimescale = 105
  fromEnum UM_Fade = 106
  fromEnum UM_GameTitle = 107
  fromEnum UM_HintText = 109
  fromEnum UM_HudMsg = 110
  fromEnum UM_HudText = 111
  fromEnum UM_KeyHintText = 112
  fromEnum UM_ColoredText = 113
  fromEnum UM_RequestState = 114
  fromEnum UM_ResetHUD = 115
  fromEnum UM_Rumble = 116
  fromEnum UM_SayText = 117
  fromEnum UM_SayText2 = 118
  fromEnum UM_SayTextChannel = 119
  fromEnum UM_Shake = 120
  fromEnum UM_ShakeDir = 121
  fromEnum UM_TextMsg = 124
  fromEnum UM_ScreenTilt = 125
  fromEnum UM_Train = 126
  fromEnum UM_VGUIMenu = 127
  fromEnum UM_VoiceMask = 128
  fromEnum UM_VoiceSubtitle = 129
  fromEnum UM_SendAudio = 130
  fromEnum UM_ItemPickup = 131
  fromEnum UM_AmmoDenied = 132
  fromEnum UM_CrosshairAngle = 133
  fromEnum UM_ShowMenu = 134
  fromEnum UM_CreditsMsg = 135
  fromEnum UM_CloseCaptionPlaceholder = 142
  fromEnum UM_CameraTransition = 143
  fromEnum UM_AudioParameter = 144
  fromEnum UM_ParticleManager = 145
  fromEnum UM_HudError = 146
  fromEnum UM_CustomGameEvent = 148
  fromEnum UM_HandHapticPulse = 149
  fromEnum UM_AnimGraphUpdate = 150
  fromEnum UM_HandHapticPulsePrecise = 151
  fromEnum UM_MAX_BASE = 200
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2UserMessages.EBaseUserMessages")
      . toMaybe'Enum
  succ UM_AchievementEvent = UM_CloseCaption
  succ UM_CloseCaption = UM_CloseCaptionDirect
  succ UM_CloseCaptionDirect = UM_CurrentTimescale
  succ UM_CurrentTimescale = UM_DesiredTimescale
  succ UM_DesiredTimescale = UM_Fade
  succ UM_Fade = UM_GameTitle
  succ UM_GameTitle = UM_HintText
  succ UM_HintText = UM_HudMsg
  succ UM_HudMsg = UM_HudText
  succ UM_HudText = UM_KeyHintText
  succ UM_KeyHintText = UM_ColoredText
  succ UM_ColoredText = UM_RequestState
  succ UM_RequestState = UM_ResetHUD
  succ UM_ResetHUD = UM_Rumble
  succ UM_Rumble = UM_SayText
  succ UM_SayText = UM_SayText2
  succ UM_SayText2 = UM_SayTextChannel
  succ UM_SayTextChannel = UM_Shake
  succ UM_Shake = UM_ShakeDir
  succ UM_ShakeDir = UM_TextMsg
  succ UM_TextMsg = UM_ScreenTilt
  succ UM_ScreenTilt = UM_Train
  succ UM_Train = UM_VGUIMenu
  succ UM_VGUIMenu = UM_VoiceMask
  succ UM_VoiceMask = UM_VoiceSubtitle
  succ UM_VoiceSubtitle = UM_SendAudio
  succ UM_SendAudio = UM_ItemPickup
  succ UM_ItemPickup = UM_AmmoDenied
  succ UM_AmmoDenied = UM_CrosshairAngle
  succ UM_CrosshairAngle = UM_ShowMenu
  succ UM_ShowMenu = UM_CreditsMsg
  succ UM_CreditsMsg = UM_CloseCaptionPlaceholder
  succ UM_CloseCaptionPlaceholder = UM_CameraTransition
  succ UM_CameraTransition = UM_AudioParameter
  succ UM_AudioParameter = UM_ParticleManager
  succ UM_ParticleManager = UM_HudError
  succ UM_HudError = UM_CustomGameEvent
  succ UM_CustomGameEvent = UM_HandHapticPulse
  succ UM_HandHapticPulse = UM_AnimGraphUpdate
  succ UM_AnimGraphUpdate = UM_HandHapticPulsePrecise
  succ UM_HandHapticPulsePrecise = UM_MAX_BASE
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2UserMessages.EBaseUserMessages"
  pred UM_CloseCaption = UM_AchievementEvent
  pred UM_CloseCaptionDirect = UM_CloseCaption
  pred UM_CurrentTimescale = UM_CloseCaptionDirect
  pred UM_DesiredTimescale = UM_CurrentTimescale
  pred UM_Fade = UM_DesiredTimescale
  pred UM_GameTitle = UM_Fade
  pred UM_HintText = UM_GameTitle
  pred UM_HudMsg = UM_HintText
  pred UM_HudText = UM_HudMsg
  pred UM_KeyHintText = UM_HudText
  pred UM_ColoredText = UM_KeyHintText
  pred UM_RequestState = UM_ColoredText
  pred UM_ResetHUD = UM_RequestState
  pred UM_Rumble = UM_ResetHUD
  pred UM_SayText = UM_Rumble
  pred UM_SayText2 = UM_SayText
  pred UM_SayTextChannel = UM_SayText2
  pred UM_Shake = UM_SayTextChannel
  pred UM_ShakeDir = UM_Shake
  pred UM_TextMsg = UM_ShakeDir
  pred UM_ScreenTilt = UM_TextMsg
  pred UM_Train = UM_ScreenTilt
  pred UM_VGUIMenu = UM_Train
  pred UM_VoiceMask = UM_VGUIMenu
  pred UM_VoiceSubtitle = UM_VoiceMask
  pred UM_SendAudio = UM_VoiceSubtitle
  pred UM_ItemPickup = UM_SendAudio
  pred UM_AmmoDenied = UM_ItemPickup
  pred UM_CrosshairAngle = UM_AmmoDenied
  pred UM_ShowMenu = UM_CrosshairAngle
  pred UM_CreditsMsg = UM_ShowMenu
  pred UM_CloseCaptionPlaceholder = UM_CreditsMsg
  pred UM_CameraTransition = UM_CloseCaptionPlaceholder
  pred UM_AudioParameter = UM_CameraTransition
  pred UM_ParticleManager = UM_AudioParameter
  pred UM_HudError = UM_ParticleManager
  pred UM_CustomGameEvent = UM_HudError
  pred UM_HandHapticPulse = UM_CustomGameEvent
  pred UM_AnimGraphUpdate = UM_HandHapticPulse
  pred UM_HandHapticPulsePrecise = UM_AnimGraphUpdate
  pred UM_MAX_BASE = UM_HandHapticPulsePrecise
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2UserMessages.EBaseUserMessages"

instance P'.Wire EBaseUserMessages where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EBaseUserMessages

instance P'.MessageAPI msg' (msg' -> EBaseUserMessages) EBaseUserMessages where
  getVal m' f' = f' m'

instance P'.ReflectEnum EBaseUserMessages where
  reflectEnum
   = [(101, "UM_AchievementEvent", UM_AchievementEvent), (102, "UM_CloseCaption", UM_CloseCaption),
      (103, "UM_CloseCaptionDirect", UM_CloseCaptionDirect), (104, "UM_CurrentTimescale", UM_CurrentTimescale),
      (105, "UM_DesiredTimescale", UM_DesiredTimescale), (106, "UM_Fade", UM_Fade), (107, "UM_GameTitle", UM_GameTitle),
      (109, "UM_HintText", UM_HintText), (110, "UM_HudMsg", UM_HudMsg), (111, "UM_HudText", UM_HudText),
      (112, "UM_KeyHintText", UM_KeyHintText), (113, "UM_ColoredText", UM_ColoredText), (114, "UM_RequestState", UM_RequestState),
      (115, "UM_ResetHUD", UM_ResetHUD), (116, "UM_Rumble", UM_Rumble), (117, "UM_SayText", UM_SayText),
      (118, "UM_SayText2", UM_SayText2), (119, "UM_SayTextChannel", UM_SayTextChannel), (120, "UM_Shake", UM_Shake),
      (121, "UM_ShakeDir", UM_ShakeDir), (124, "UM_TextMsg", UM_TextMsg), (125, "UM_ScreenTilt", UM_ScreenTilt),
      (126, "UM_Train", UM_Train), (127, "UM_VGUIMenu", UM_VGUIMenu), (128, "UM_VoiceMask", UM_VoiceMask),
      (129, "UM_VoiceSubtitle", UM_VoiceSubtitle), (130, "UM_SendAudio", UM_SendAudio), (131, "UM_ItemPickup", UM_ItemPickup),
      (132, "UM_AmmoDenied", UM_AmmoDenied), (133, "UM_CrosshairAngle", UM_CrosshairAngle), (134, "UM_ShowMenu", UM_ShowMenu),
      (135, "UM_CreditsMsg", UM_CreditsMsg), (142, "UM_CloseCaptionPlaceholder", UM_CloseCaptionPlaceholder),
      (143, "UM_CameraTransition", UM_CameraTransition), (144, "UM_AudioParameter", UM_AudioParameter),
      (145, "UM_ParticleManager", UM_ParticleManager), (146, "UM_HudError", UM_HudError),
      (148, "UM_CustomGameEvent", UM_CustomGameEvent), (149, "UM_HandHapticPulse", UM_HandHapticPulse),
      (150, "UM_AnimGraphUpdate", UM_AnimGraphUpdate), (151, "UM_HandHapticPulsePrecise", UM_HandHapticPulsePrecise),
      (200, "UM_MAX_BASE", UM_MAX_BASE)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Usermessages.EBaseUserMessages") ["Shrine", "Wire", "Protobuf", "S2"] ["S2UserMessages"]
        "EBaseUserMessages")
      ["Shrine", "Wire", "Protobuf", "S2", "S2UserMessages", "EBaseUserMessages.hs"]
      [(101, "UM_AchievementEvent"), (102, "UM_CloseCaption"), (103, "UM_CloseCaptionDirect"), (104, "UM_CurrentTimescale"),
       (105, "UM_DesiredTimescale"), (106, "UM_Fade"), (107, "UM_GameTitle"), (109, "UM_HintText"), (110, "UM_HudMsg"),
       (111, "UM_HudText"), (112, "UM_KeyHintText"), (113, "UM_ColoredText"), (114, "UM_RequestState"), (115, "UM_ResetHUD"),
       (116, "UM_Rumble"), (117, "UM_SayText"), (118, "UM_SayText2"), (119, "UM_SayTextChannel"), (120, "UM_Shake"),
       (121, "UM_ShakeDir"), (124, "UM_TextMsg"), (125, "UM_ScreenTilt"), (126, "UM_Train"), (127, "UM_VGUIMenu"),
       (128, "UM_VoiceMask"), (129, "UM_VoiceSubtitle"), (130, "UM_SendAudio"), (131, "UM_ItemPickup"), (132, "UM_AmmoDenied"),
       (133, "UM_CrosshairAngle"), (134, "UM_ShowMenu"), (135, "UM_CreditsMsg"), (142, "UM_CloseCaptionPlaceholder"),
       (143, "UM_CameraTransition"), (144, "UM_AudioParameter"), (145, "UM_ParticleManager"), (146, "UM_HudError"),
       (148, "UM_CustomGameEvent"), (149, "UM_HandHapticPulse"), (150, "UM_AnimGraphUpdate"), (151, "UM_HandHapticPulsePrecise"),
       (200, "UM_MAX_BASE")]

instance P'.TextType EBaseUserMessages where
  tellT = P'.tellShow
  getT = P'.getRead