{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_CustomGameEvent_ServerToClient (CUserMsg_CustomGameEvent_ServerToClient(..))
       where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CUserMsg_CustomGameEvent_ServerToClient = CUserMsg_CustomGameEvent_ServerToClient{event_name :: !(P'.Maybe P'.Utf8),
                                                                                       data' :: !(P'.Maybe P'.ByteString)}
                                             deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                                       Prelude'.Generic)

instance P'.Mergeable CUserMsg_CustomGameEvent_ServerToClient where
  mergeAppend (CUserMsg_CustomGameEvent_ServerToClient x'1 x'2) (CUserMsg_CustomGameEvent_ServerToClient y'1 y'2)
   = CUserMsg_CustomGameEvent_ServerToClient (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CUserMsg_CustomGameEvent_ServerToClient where
  defaultValue = CUserMsg_CustomGameEvent_ServerToClient P'.defaultValue P'.defaultValue

instance P'.Wire CUserMsg_CustomGameEvent_ServerToClient where
  wireSize ft' self'@(CUserMsg_CustomGameEvent_ServerToClient x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 9 x'1 + P'.wireSizeOpt 1 12 x'2)
  wirePut ft' self'@(CUserMsg_CustomGameEvent_ServerToClient x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 9 x'1
             P'.wirePutOpt 18 12 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{event_name = Prelude'.Just new'Field}) (P'.wireGet 9)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{data' = Prelude'.Just new'Field}) (P'.wireGet 12)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CUserMsg_CustomGameEvent_ServerToClient) CUserMsg_CustomGameEvent_ServerToClient where
  getVal m' f' = f' m'

instance P'.GPB CUserMsg_CustomGameEvent_ServerToClient

instance P'.ReflectDescriptor CUserMsg_CustomGameEvent_ServerToClient where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_CustomGameEvent_ServerToClient\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"CUserMsg_CustomGameEvent_ServerToClient\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CUserMsg_CustomGameEvent_ServerToClient.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_CustomGameEvent_ServerToClient.event_name\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_CustomGameEvent_ServerToClient\"], baseName' = FName \"event_name\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 9}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_CustomGameEvent_ServerToClient.data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_CustomGameEvent_ServerToClient\"], baseName' = FName \"data'\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CUserMsg_CustomGameEvent_ServerToClient where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CUserMsg_CustomGameEvent_ServerToClient where
  textPut msg
   = do
       P'.tellT "event_name" (event_name msg)
       P'.tellT "data" (data' msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'event_name, parse'data']) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'event_name
         = P'.try
            (do
               v <- P'.getT "event_name"
               Prelude'.return (\ o -> o{event_name = v}))
        parse'data'
         = P'.try
            (do
               v <- P'.getT "data"
               Prelude'.return (\ o -> o{data' = v}))