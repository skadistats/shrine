{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMessageResetHUD (CUserMessageResetHUD(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CUserMessageResetHUD = CUserMessageResetHUD{}
                          deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CUserMessageResetHUD where
  mergeAppend CUserMessageResetHUD CUserMessageResetHUD = CUserMessageResetHUD

instance P'.Default CUserMessageResetHUD where
  defaultValue = CUserMessageResetHUD

instance P'.Wire CUserMessageResetHUD where
  wireSize ft' self'@(CUserMessageResetHUD)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = 0
  wirePut ft' self'@(CUserMessageResetHUD)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             Prelude'.return ()
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CUserMessageResetHUD) CUserMessageResetHUD where
  getVal m' f' = f' m'

instance P'.GPB CUserMessageResetHUD

instance P'.ReflectDescriptor CUserMessageResetHUD where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CUserMessageResetHUD\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"CUserMessageResetHUD\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CUserMessageResetHUD.hs\"], isGroup = False, fields = fromList [], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CUserMessageResetHUD where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CUserMessageResetHUD where
  textPut msg = Prelude'.return ()
  textGet = Prelude'.return P'.defaultValue
    where