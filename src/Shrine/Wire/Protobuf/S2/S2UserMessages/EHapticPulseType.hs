{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.EHapticPulseType (EHapticPulseType(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EHapticPulseType = VR_HAND_HAPTIC_PULSE_LIGHT
                      | VR_HAND_HAPTIC_PULSE_MEDIUM
                      | VR_HAND_HAPTIC_PULSE_STRONG
                      deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                Prelude'.Generic)

instance P'.Mergeable EHapticPulseType

instance Prelude'.Bounded EHapticPulseType where
  minBound = VR_HAND_HAPTIC_PULSE_LIGHT
  maxBound = VR_HAND_HAPTIC_PULSE_STRONG

instance P'.Default EHapticPulseType where
  defaultValue = VR_HAND_HAPTIC_PULSE_LIGHT

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EHapticPulseType
toMaybe'Enum 0 = Prelude'.Just VR_HAND_HAPTIC_PULSE_LIGHT
toMaybe'Enum 1 = Prelude'.Just VR_HAND_HAPTIC_PULSE_MEDIUM
toMaybe'Enum 2 = Prelude'.Just VR_HAND_HAPTIC_PULSE_STRONG
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EHapticPulseType where
  fromEnum VR_HAND_HAPTIC_PULSE_LIGHT = 0
  fromEnum VR_HAND_HAPTIC_PULSE_MEDIUM = 1
  fromEnum VR_HAND_HAPTIC_PULSE_STRONG = 2
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2UserMessages.EHapticPulseType")
      . toMaybe'Enum
  succ VR_HAND_HAPTIC_PULSE_LIGHT = VR_HAND_HAPTIC_PULSE_MEDIUM
  succ VR_HAND_HAPTIC_PULSE_MEDIUM = VR_HAND_HAPTIC_PULSE_STRONG
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2UserMessages.EHapticPulseType"
  pred VR_HAND_HAPTIC_PULSE_MEDIUM = VR_HAND_HAPTIC_PULSE_LIGHT
  pred VR_HAND_HAPTIC_PULSE_STRONG = VR_HAND_HAPTIC_PULSE_MEDIUM
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2UserMessages.EHapticPulseType"

instance P'.Wire EHapticPulseType where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EHapticPulseType

instance P'.MessageAPI msg' (msg' -> EHapticPulseType) EHapticPulseType where
  getVal m' f' = f' m'

instance P'.ReflectEnum EHapticPulseType where
  reflectEnum
   = [(0, "VR_HAND_HAPTIC_PULSE_LIGHT", VR_HAND_HAPTIC_PULSE_LIGHT),
      (1, "VR_HAND_HAPTIC_PULSE_MEDIUM", VR_HAND_HAPTIC_PULSE_MEDIUM),
      (2, "VR_HAND_HAPTIC_PULSE_STRONG", VR_HAND_HAPTIC_PULSE_STRONG)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Usermessages.EHapticPulseType") ["Shrine", "Wire", "Protobuf", "S2"] ["S2UserMessages"]
        "EHapticPulseType")
      ["Shrine", "Wire", "Protobuf", "S2", "S2UserMessages", "EHapticPulseType.hs"]
      [(0, "VR_HAND_HAPTIC_PULSE_LIGHT"), (1, "VR_HAND_HAPTIC_PULSE_MEDIUM"), (2, "VR_HAND_HAPTIC_PULSE_STRONG")]

instance P'.TextType EHapticPulseType where
  tellT = P'.tellShow
  getT = P'.getRead