{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CEntityMessageScreenOverlay (CEntityMessageScreenOverlay(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CEntityMessageScreenOverlay = CEntityMessageScreenOverlay{start_effect :: !(P'.Maybe P'.Bool)}
                                 deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                           Prelude'.Generic)

instance P'.Mergeable CEntityMessageScreenOverlay where
  mergeAppend (CEntityMessageScreenOverlay x'1) (CEntityMessageScreenOverlay y'1)
   = CEntityMessageScreenOverlay (P'.mergeAppend x'1 y'1)

instance P'.Default CEntityMessageScreenOverlay where
  defaultValue = CEntityMessageScreenOverlay P'.defaultValue

instance P'.Wire CEntityMessageScreenOverlay where
  wireSize ft' self'@(CEntityMessageScreenOverlay x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 8 x'1)
  wirePut ft' self'@(CEntityMessageScreenOverlay x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 8 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{start_effect = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CEntityMessageScreenOverlay) CEntityMessageScreenOverlay where
  getVal m' f' = f' m'

instance P'.GPB CEntityMessageScreenOverlay

instance P'.ReflectDescriptor CEntityMessageScreenOverlay where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CEntityMessageScreenOverlay\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"CEntityMessageScreenOverlay\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CEntityMessageScreenOverlay.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CEntityMessageScreenOverlay.start_effect\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CEntityMessageScreenOverlay\"], baseName' = FName \"start_effect\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CEntityMessageScreenOverlay where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CEntityMessageScreenOverlay where
  textPut msg
   = do
       P'.tellT "start_effect" (start_effect msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'start_effect]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'start_effect
         = P'.try
            (do
               v <- P'.getT "start_effect"
               Prelude'.return (\ o -> o{start_effect = v}))