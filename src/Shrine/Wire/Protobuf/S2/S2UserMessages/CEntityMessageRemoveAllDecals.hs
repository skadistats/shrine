{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CEntityMessageRemoveAllDecals (CEntityMessageRemoveAllDecals(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CEntityMessageRemoveAllDecals = CEntityMessageRemoveAllDecals{remove_decals :: !(P'.Maybe P'.Bool)}
                                   deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                             Prelude'.Generic)

instance P'.Mergeable CEntityMessageRemoveAllDecals where
  mergeAppend (CEntityMessageRemoveAllDecals x'1) (CEntityMessageRemoveAllDecals y'1)
   = CEntityMessageRemoveAllDecals (P'.mergeAppend x'1 y'1)

instance P'.Default CEntityMessageRemoveAllDecals where
  defaultValue = CEntityMessageRemoveAllDecals P'.defaultValue

instance P'.Wire CEntityMessageRemoveAllDecals where
  wireSize ft' self'@(CEntityMessageRemoveAllDecals x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 8 x'1)
  wirePut ft' self'@(CEntityMessageRemoveAllDecals x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 8 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{remove_decals = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CEntityMessageRemoveAllDecals) CEntityMessageRemoveAllDecals where
  getVal m' f' = f' m'

instance P'.GPB CEntityMessageRemoveAllDecals

instance P'.ReflectDescriptor CEntityMessageRemoveAllDecals where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CEntityMessageRemoveAllDecals\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"CEntityMessageRemoveAllDecals\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CEntityMessageRemoveAllDecals.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CEntityMessageRemoveAllDecals.remove_decals\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CEntityMessageRemoveAllDecals\"], baseName' = FName \"remove_decals\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CEntityMessageRemoveAllDecals where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CEntityMessageRemoveAllDecals where
  textPut msg
   = do
       P'.tellT "remove_decals" (remove_decals msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'remove_decals]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'remove_decals
         = P'.try
            (do
               v <- P'.getT "remove_decals"
               Prelude'.return (\ o -> o{remove_decals = v}))