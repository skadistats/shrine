{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_TrackedControllerInput_ClientToServer
       (CUserMsg_TrackedControllerInput_ClientToServer(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CUserMsg_TrackedControllerInput_ClientToServer = CUserMsg_TrackedControllerInput_ClientToServer{data' ::
                                                                                                     !(P'.Maybe P'.ByteString)}
                                                    deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable,
                                                              Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CUserMsg_TrackedControllerInput_ClientToServer where
  mergeAppend (CUserMsg_TrackedControllerInput_ClientToServer x'1) (CUserMsg_TrackedControllerInput_ClientToServer y'1)
   = CUserMsg_TrackedControllerInput_ClientToServer (P'.mergeAppend x'1 y'1)

instance P'.Default CUserMsg_TrackedControllerInput_ClientToServer where
  defaultValue = CUserMsg_TrackedControllerInput_ClientToServer P'.defaultValue

instance P'.Wire CUserMsg_TrackedControllerInput_ClientToServer where
  wireSize ft' self'@(CUserMsg_TrackedControllerInput_ClientToServer x'1)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 12 x'1)
  wirePut ft' self'@(CUserMsg_TrackedControllerInput_ClientToServer x'1)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 10 12 x'1
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{data' = Prelude'.Just new'Field}) (P'.wireGet 12)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CUserMsg_TrackedControllerInput_ClientToServer) CUserMsg_TrackedControllerInput_ClientToServer
         where
  getVal m' f' = f' m'

instance P'.GPB CUserMsg_TrackedControllerInput_ClientToServer

instance P'.ReflectDescriptor CUserMsg_TrackedControllerInput_ClientToServer where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [10])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_TrackedControllerInput_ClientToServer\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"CUserMsg_TrackedControllerInput_ClientToServer\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CUserMsg_TrackedControllerInput_ClientToServer.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_TrackedControllerInput_ClientToServer.data\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_TrackedControllerInput_ClientToServer\"], baseName' = FName \"data'\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 10}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 12}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CUserMsg_TrackedControllerInput_ClientToServer where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CUserMsg_TrackedControllerInput_ClientToServer where
  textPut msg
   = do
       P'.tellT "data" (data' msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'data']) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'data'
         = P'.try
            (do
               v <- P'.getT "data"
               Prelude'.return (\ o -> o{data' = v}))