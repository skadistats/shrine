{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.EBaseEntityMessages (EBaseEntityMessages(..)) where
import Prelude ((+), (/), (.))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data EBaseEntityMessages = EM_PlayJingle
                         | EM_ScreenOverlay
                         | EM_RemoveAllDecals
                         | EM_PropagateForce
                         | EM_DoSpark
                         | EM_FixAngle
                         deriving (Prelude'.Read, Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                   Prelude'.Generic)

instance P'.Mergeable EBaseEntityMessages

instance Prelude'.Bounded EBaseEntityMessages where
  minBound = EM_PlayJingle
  maxBound = EM_FixAngle

instance P'.Default EBaseEntityMessages where
  defaultValue = EM_PlayJingle

toMaybe'Enum :: Prelude'.Int -> P'.Maybe EBaseEntityMessages
toMaybe'Enum 136 = Prelude'.Just EM_PlayJingle
toMaybe'Enum 137 = Prelude'.Just EM_ScreenOverlay
toMaybe'Enum 138 = Prelude'.Just EM_RemoveAllDecals
toMaybe'Enum 139 = Prelude'.Just EM_PropagateForce
toMaybe'Enum 140 = Prelude'.Just EM_DoSpark
toMaybe'Enum 141 = Prelude'.Just EM_FixAngle
toMaybe'Enum _ = Prelude'.Nothing

instance Prelude'.Enum EBaseEntityMessages where
  fromEnum EM_PlayJingle = 136
  fromEnum EM_ScreenOverlay = 137
  fromEnum EM_RemoveAllDecals = 138
  fromEnum EM_PropagateForce = 139
  fromEnum EM_DoSpark = 140
  fromEnum EM_FixAngle = 141
  toEnum
   = P'.fromMaybe
      (Prelude'.error "hprotoc generated code: toEnum failure for type Shrine.Wire.Protobuf.S2.S2UserMessages.EBaseEntityMessages")
      . toMaybe'Enum
  succ EM_PlayJingle = EM_ScreenOverlay
  succ EM_ScreenOverlay = EM_RemoveAllDecals
  succ EM_RemoveAllDecals = EM_PropagateForce
  succ EM_PropagateForce = EM_DoSpark
  succ EM_DoSpark = EM_FixAngle
  succ _ = Prelude'.error "hprotoc generated code: succ failure for type Shrine.Wire.Protobuf.S2.S2UserMessages.EBaseEntityMessages"
  pred EM_ScreenOverlay = EM_PlayJingle
  pred EM_RemoveAllDecals = EM_ScreenOverlay
  pred EM_PropagateForce = EM_RemoveAllDecals
  pred EM_DoSpark = EM_PropagateForce
  pred EM_FixAngle = EM_DoSpark
  pred _ = Prelude'.error "hprotoc generated code: pred failure for type Shrine.Wire.Protobuf.S2.S2UserMessages.EBaseEntityMessages"

instance P'.Wire EBaseEntityMessages where
  wireSize ft' enum = P'.wireSize ft' (Prelude'.fromEnum enum)
  wirePut ft' enum = P'.wirePut ft' (Prelude'.fromEnum enum)
  wireGet 14 = P'.wireGetEnum toMaybe'Enum
  wireGet ft' = P'.wireGetErr ft'
  wireGetPacked 14 = P'.wireGetPackedEnum toMaybe'Enum
  wireGetPacked ft' = P'.wireGetErr ft'

instance P'.GPB EBaseEntityMessages

instance P'.MessageAPI msg' (msg' -> EBaseEntityMessages) EBaseEntityMessages where
  getVal m' f' = f' m'

instance P'.ReflectEnum EBaseEntityMessages where
  reflectEnum
   = [(136, "EM_PlayJingle", EM_PlayJingle), (137, "EM_ScreenOverlay", EM_ScreenOverlay),
      (138, "EM_RemoveAllDecals", EM_RemoveAllDecals), (139, "EM_PropagateForce", EM_PropagateForce),
      (140, "EM_DoSpark", EM_DoSpark), (141, "EM_FixAngle", EM_FixAngle)]
  reflectEnumInfo _
   = P'.EnumInfo
      (P'.makePNF (P'.pack ".S2_Usermessages.EBaseEntityMessages") ["Shrine", "Wire", "Protobuf", "S2"] ["S2UserMessages"]
        "EBaseEntityMessages")
      ["Shrine", "Wire", "Protobuf", "S2", "S2UserMessages", "EBaseEntityMessages.hs"]
      [(136, "EM_PlayJingle"), (137, "EM_ScreenOverlay"), (138, "EM_RemoveAllDecals"), (139, "EM_PropagateForce"),
       (140, "EM_DoSpark"), (141, "EM_FixAngle")]

instance P'.TextType EBaseEntityMessages where
  tellT = P'.tellShow
  getT = P'.getRead