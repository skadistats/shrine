{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticleOffset (UpdateParticleOffset(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data UpdateParticleOffset = UpdateParticleOffset{control_point :: !(P'.Maybe P'.Int32),
                                                 origin_offset :: !(P'.Maybe NetworkBaseTypes.CMsgVector)}
                          deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable UpdateParticleOffset where
  mergeAppend (UpdateParticleOffset x'1 x'2) (UpdateParticleOffset y'1 y'2)
   = UpdateParticleOffset (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default UpdateParticleOffset where
  defaultValue = UpdateParticleOffset P'.defaultValue P'.defaultValue

instance P'.Wire UpdateParticleOffset where
  wireSize ft' self'@(UpdateParticleOffset x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 11 x'2)
  wirePut ft' self'@(UpdateParticleOffset x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 18 11 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{control_point = Prelude'.Just new'Field}) (P'.wireGet 5)
             18 -> Prelude'.fmap
                    (\ !new'Field -> old'Self{origin_offset = P'.mergeAppend (origin_offset old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> UpdateParticleOffset) UpdateParticleOffset where
  getVal m' f' = f' m'

instance P'.GPB UpdateParticleOffset

instance P'.ReflectDescriptor UpdateParticleOffset where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleOffset\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleOffset\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CUserMsg_ParticleManager\",\"UpdateParticleOffset.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleOffset.control_point\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\",MName \"UpdateParticleOffset\"], baseName' = FName \"control_point\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleOffset.origin_offset\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\",MName \"UpdateParticleOffset\"], baseName' = FName \"origin_offset\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType UpdateParticleOffset where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg UpdateParticleOffset where
  textPut msg
   = do
       P'.tellT "control_point" (control_point msg)
       P'.tellT "origin_offset" (origin_offset msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'control_point, parse'origin_offset]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'control_point
         = P'.try
            (do
               v <- P'.getT "control_point"
               Prelude'.return (\ o -> o{control_point = v}))
        parse'origin_offset
         = P'.try
            (do
               v <- P'.getT "origin_offset"
               Prelude'.return (\ o -> o{origin_offset = v}))