{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticleEnt (UpdateParticleEnt(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.NetworkBaseTypes.CMsgVector as NetworkBaseTypes (CMsgVector)

data UpdateParticleEnt = UpdateParticleEnt{control_point :: !(P'.Maybe P'.Int32), entity_handle :: !(P'.Maybe P'.Int32),
                                           attach_type :: !(P'.Maybe P'.Int32), attachment :: !(P'.Maybe P'.Int32),
                                           fallback_position :: !(P'.Maybe NetworkBaseTypes.CMsgVector),
                                           include_wearables :: !(P'.Maybe P'.Bool)}
                       deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable UpdateParticleEnt where
  mergeAppend (UpdateParticleEnt x'1 x'2 x'3 x'4 x'5 x'6) (UpdateParticleEnt y'1 y'2 y'3 y'4 y'5 y'6)
   = UpdateParticleEnt (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)

instance P'.Default UpdateParticleEnt where
  defaultValue = UpdateParticleEnt P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire UpdateParticleEnt where
  wireSize ft' self'@(UpdateParticleEnt x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 5 x'2 + P'.wireSizeOpt 1 5 x'3 + P'.wireSizeOpt 1 5 x'4 +
             P'.wireSizeOpt 1 11 x'5
             + P'.wireSizeOpt 1 8 x'6)
  wirePut ft' self'@(UpdateParticleEnt x'1 x'2 x'3 x'4 x'5 x'6)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 5 x'2
             P'.wirePutOpt 24 5 x'3
             P'.wirePutOpt 32 5 x'4
             P'.wirePutOpt 42 11 x'5
             P'.wirePutOpt 48 8 x'6
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{control_point = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{entity_handle = Prelude'.Just new'Field}) (P'.wireGet 5)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{attach_type = Prelude'.Just new'Field}) (P'.wireGet 5)
             32 -> Prelude'.fmap (\ !new'Field -> old'Self{attachment = Prelude'.Just new'Field}) (P'.wireGet 5)
             42 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{fallback_position = P'.mergeAppend (fallback_position old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             48 -> Prelude'.fmap (\ !new'Field -> old'Self{include_wearables = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> UpdateParticleEnt) UpdateParticleEnt where
  getVal m' f' = f' m'

instance P'.GPB UpdateParticleEnt

instance P'.ReflectDescriptor UpdateParticleEnt where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16, 24, 32, 42, 48])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleEnt\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleEnt\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CUserMsg_ParticleManager\",\"UpdateParticleEnt.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleEnt.control_point\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\",MName \"UpdateParticleEnt\"], baseName' = FName \"control_point\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleEnt.entity_handle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\",MName \"UpdateParticleEnt\"], baseName' = FName \"entity_handle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleEnt.attach_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\",MName \"UpdateParticleEnt\"], baseName' = FName \"attach_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleEnt.attachment\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\",MName \"UpdateParticleEnt\"], baseName' = FName \"attachment\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 32}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleEnt.fallback_position\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\",MName \"UpdateParticleEnt\"], baseName' = FName \"fallback_position\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgVector\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgVector\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleEnt.include_wearables\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\",MName \"UpdateParticleEnt\"], baseName' = FName \"include_wearables\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 48}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType UpdateParticleEnt where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg UpdateParticleEnt where
  textPut msg
   = do
       P'.tellT "control_point" (control_point msg)
       P'.tellT "entity_handle" (entity_handle msg)
       P'.tellT "attach_type" (attach_type msg)
       P'.tellT "attachment" (attachment msg)
       P'.tellT "fallback_position" (fallback_position msg)
       P'.tellT "include_wearables" (include_wearables msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'control_point, parse'entity_handle, parse'attach_type, parse'attachment, parse'fallback_position,
                   parse'include_wearables])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'control_point
         = P'.try
            (do
               v <- P'.getT "control_point"
               Prelude'.return (\ o -> o{control_point = v}))
        parse'entity_handle
         = P'.try
            (do
               v <- P'.getT "entity_handle"
               Prelude'.return (\ o -> o{entity_handle = v}))
        parse'attach_type
         = P'.try
            (do
               v <- P'.getT "attach_type"
               Prelude'.return (\ o -> o{attach_type = v}))
        parse'attachment
         = P'.try
            (do
               v <- P'.getT "attachment"
               Prelude'.return (\ o -> o{attachment = v}))
        parse'fallback_position
         = P'.try
            (do
               v <- P'.getT "fallback_position"
               Prelude'.return (\ o -> o{fallback_position = v}))
        parse'include_wearables
         = P'.try
            (do
               v <- P'.getT "include_wearables"
               Prelude'.return (\ o -> o{include_wearables = v}))