{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMessageVoiceMask (CUserMessageVoiceMask(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'

data CUserMessageVoiceMask = CUserMessageVoiceMask{gamerules_masks :: !(P'.Seq P'.Word32), ban_masks :: !(P'.Seq P'.Word32),
                                                   mod_enable :: !(P'.Maybe P'.Bool)}
                           deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CUserMessageVoiceMask where
  mergeAppend (CUserMessageVoiceMask x'1 x'2 x'3) (CUserMessageVoiceMask y'1 y'2 y'3)
   = CUserMessageVoiceMask (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3)

instance P'.Default CUserMessageVoiceMask where
  defaultValue = CUserMessageVoiceMask P'.defaultValue P'.defaultValue P'.defaultValue

instance P'.Wire CUserMessageVoiceMask where
  wireSize ft' self'@(CUserMessageVoiceMask x'1 x'2 x'3)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeRep 1 13 x'1 + P'.wireSizeRep 1 13 x'2 + P'.wireSizeOpt 1 8 x'3)
  wirePut ft' self'@(CUserMessageVoiceMask x'1 x'2 x'3)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutRep 8 13 x'1
             P'.wirePutRep 16 13 x'2
             P'.wirePutOpt 24 8 x'3
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{gamerules_masks = P'.append (gamerules_masks old'Self) new'Field})
                   (P'.wireGet 13)
             10 -> Prelude'.fmap (\ !new'Field -> old'Self{gamerules_masks = P'.mergeAppend (gamerules_masks old'Self) new'Field})
                    (P'.wireGetPacked 13)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{ban_masks = P'.append (ban_masks old'Self) new'Field}) (P'.wireGet 13)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{ban_masks = P'.mergeAppend (ban_masks old'Self) new'Field})
                    (P'.wireGetPacked 13)
             24 -> Prelude'.fmap (\ !new'Field -> old'Self{mod_enable = Prelude'.Just new'Field}) (P'.wireGet 8)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CUserMessageVoiceMask) CUserMessageVoiceMask where
  getVal m' f' = f' m'

instance P'.GPB CUserMessageVoiceMask

instance P'.ReflectDescriptor CUserMessageVoiceMask where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 10, 16, 18, 24])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CUserMessageVoiceMask\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"CUserMessageVoiceMask\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CUserMessageVoiceMask.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMessageVoiceMask.gamerules_masks\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMessageVoiceMask\"], baseName' = FName \"gamerules_masks\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Just (WireTag {getWireTag = 8},WireTag {getWireTag = 10}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMessageVoiceMask.ban_masks\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMessageVoiceMask\"], baseName' = FName \"ban_masks\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Just (WireTag {getWireTag = 16},WireTag {getWireTag = 18}), wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = True, mightPack = True, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMessageVoiceMask.mod_enable\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMessageVoiceMask\"], baseName' = FName \"mod_enable\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 24}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CUserMessageVoiceMask where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CUserMessageVoiceMask where
  textPut msg
   = do
       P'.tellT "gamerules_masks" (gamerules_masks msg)
       P'.tellT "ban_masks" (ban_masks msg)
       P'.tellT "mod_enable" (mod_enable msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'gamerules_masks, parse'ban_masks, parse'mod_enable]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'gamerules_masks
         = P'.try
            (do
               v <- P'.getT "gamerules_masks"
               Prelude'.return (\ o -> o{gamerules_masks = P'.append (gamerules_masks o) v}))
        parse'ban_masks
         = P'.try
            (do
               v <- P'.getT "ban_masks"
               Prelude'.return (\ o -> o{ban_masks = P'.append (ban_masks o) v}))
        parse'mod_enable
         = P'.try
            (do
               v <- P'.getT "mod_enable"
               Prelude'.return (\ o -> o{mod_enable = v}))