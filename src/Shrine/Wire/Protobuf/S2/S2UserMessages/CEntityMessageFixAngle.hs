{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CEntityMessageFixAngle (CEntityMessageFixAngle(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.NetworkBaseTypes.CMsgQAngle as NetworkBaseTypes (CMsgQAngle)

data CEntityMessageFixAngle = CEntityMessageFixAngle{relative :: !(P'.Maybe P'.Bool),
                                                     angle :: !(P'.Maybe NetworkBaseTypes.CMsgQAngle)}
                            deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CEntityMessageFixAngle where
  mergeAppend (CEntityMessageFixAngle x'1 x'2) (CEntityMessageFixAngle y'1 y'2)
   = CEntityMessageFixAngle (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CEntityMessageFixAngle where
  defaultValue = CEntityMessageFixAngle P'.defaultValue P'.defaultValue

instance P'.Wire CEntityMessageFixAngle where
  wireSize ft' self'@(CEntityMessageFixAngle x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 8 x'1 + P'.wireSizeOpt 1 11 x'2)
  wirePut ft' self'@(CEntityMessageFixAngle x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 8 x'1
             P'.wirePutOpt 18 11 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{relative = Prelude'.Just new'Field}) (P'.wireGet 8)
             18 -> Prelude'.fmap (\ !new'Field -> old'Self{angle = P'.mergeAppend (angle old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CEntityMessageFixAngle) CEntityMessageFixAngle where
  getVal m' f' = f' m'

instance P'.GPB CEntityMessageFixAngle

instance P'.ReflectDescriptor CEntityMessageFixAngle where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 18])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CEntityMessageFixAngle\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"CEntityMessageFixAngle\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CEntityMessageFixAngle.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CEntityMessageFixAngle.relative\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CEntityMessageFixAngle\"], baseName' = FName \"relative\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 8}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CEntityMessageFixAngle.angle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CEntityMessageFixAngle\"], baseName' = FName \"angle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 18}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".Networkbasetypes.CMsgQAngle\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"NetworkBaseTypes\"], baseName = MName \"CMsgQAngle\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CEntityMessageFixAngle where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CEntityMessageFixAngle where
  textPut msg
   = do
       P'.tellT "relative" (relative msg)
       P'.tellT "angle" (angle msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'relative, parse'angle]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'relative
         = P'.try
            (do
               v <- P'.getT "relative"
               Prelude'.return (\ o -> o{relative = v}))
        parse'angle
         = P'.try
            (do
               v <- P'.getT "angle"
               Prelude'.return (\ o -> o{angle = v}))