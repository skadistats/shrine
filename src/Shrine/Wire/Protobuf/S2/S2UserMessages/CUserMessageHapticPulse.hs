{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMessageHapticPulse (CUserMessageHapticPulse(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.EHapticPulseType as S2UserMessages (EHapticPulseType)

data CUserMessageHapticPulse = CUserMessageHapticPulse{hand_id :: !(P'.Maybe P'.Int32),
                                                       pulse_type :: !(P'.Maybe S2UserMessages.EHapticPulseType)}
                             deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data, Prelude'.Generic)

instance P'.Mergeable CUserMessageHapticPulse where
  mergeAppend (CUserMessageHapticPulse x'1 x'2) (CUserMessageHapticPulse y'1 y'2)
   = CUserMessageHapticPulse (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2)

instance P'.Default CUserMessageHapticPulse where
  defaultValue = CUserMessageHapticPulse P'.defaultValue (Prelude'.Just (Prelude'.read "VR_HAND_HAPTIC_PULSE_LIGHT"))

instance P'.Wire CUserMessageHapticPulse where
  wireSize ft' self'@(CUserMessageHapticPulse x'1 x'2)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size = (P'.wireSizeOpt 1 5 x'1 + P'.wireSizeOpt 1 14 x'2)
  wirePut ft' self'@(CUserMessageHapticPulse x'1 x'2)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutOpt 8 5 x'1
             P'.wirePutOpt 16 14 x'2
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{hand_id = Prelude'.Just new'Field}) (P'.wireGet 5)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{pulse_type = Prelude'.Just new'Field}) (P'.wireGet 14)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CUserMessageHapticPulse) CUserMessageHapticPulse where
  getVal m' f' = f' m'

instance P'.GPB CUserMessageHapticPulse

instance P'.ReflectDescriptor CUserMessageHapticPulse where
  getMessageInfo _ = P'.GetMessageInfo (P'.fromDistinctAscList []) (P'.fromDistinctAscList [8, 16])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CUserMessageHapticPulse\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"CUserMessageHapticPulse\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CUserMessageHapticPulse.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMessageHapticPulse.hand_id\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMessageHapticPulse\"], baseName' = FName \"hand_id\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 5}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMessageHapticPulse.pulse_type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMessageHapticPulse\"], baseName' = FName \"pulse_type\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.EHapticPulseType\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"EHapticPulseType\"}), hsRawDefault = Just \"VR_HAND_HAPTIC_PULSE_LIGHT\", hsDefault = Just (HsDef'Enum \"VR_HAND_HAPTIC_PULSE_LIGHT\")}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CUserMessageHapticPulse where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CUserMessageHapticPulse where
  textPut msg
   = do
       P'.tellT "hand_id" (hand_id msg)
       P'.tellT "pulse_type" (pulse_type msg)
  textGet
   = do
       mods <- P'.sepEndBy (P'.choice [parse'hand_id, parse'pulse_type]) P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'hand_id
         = P'.try
            (do
               v <- P'.getT "hand_id"
               Prelude'.return (\ o -> o{hand_id = v}))
        parse'pulse_type
         = P'.try
            (do
               v <- P'.getT "pulse_type"
               Prelude'.return (\ o -> o{pulse_type = v}))