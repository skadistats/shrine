{-# LANGUAGE BangPatterns, DeriveDataTypeable, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC  -fno-warn-unused-imports #-}
module Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager (CUserMsg_ParticleManager(..)) where
import Prelude ((+), (/))
import qualified Prelude as Prelude'
import qualified Data.Typeable as Prelude'
import qualified GHC.Generics as Prelude'
import qualified Data.Data as Prelude'
import qualified Text.ProtocolBuffers.Header as P'
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.ChangeControlPointAttachment
       as S2UserMessages.CUserMsg_ParticleManager (ChangeControlPointAttachment)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.CreateParticle
       as S2UserMessages.CUserMsg_ParticleManager (CreateParticle)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.DestroyParticle
       as S2UserMessages.CUserMsg_ParticleManager (DestroyParticle)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.DestroyParticleInvolving
       as S2UserMessages.CUserMsg_ParticleManager (DestroyParticleInvolving)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.ReleaseParticleIndex
       as S2UserMessages.CUserMsg_ParticleManager (ReleaseParticleIndex)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticle
       as S2UserMessages.CUserMsg_ParticleManager (UpdateParticle)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticleEnt
       as S2UserMessages.CUserMsg_ParticleManager (UpdateParticleEnt)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticleFallback
       as S2UserMessages.CUserMsg_ParticleManager (UpdateParticleFallback)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticleFwd
       as S2UserMessages.CUserMsg_ParticleManager (UpdateParticleFwd)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticleOffset
       as S2UserMessages.CUserMsg_ParticleManager (UpdateParticleOffset)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticleOrient
       as S2UserMessages.CUserMsg_ParticleManager (UpdateParticleOrient)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticleSetFrozen
       as S2UserMessages.CUserMsg_ParticleManager (UpdateParticleSetFrozen)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.CUserMsg_ParticleManager.UpdateParticleShouldDraw
       as S2UserMessages.CUserMsg_ParticleManager (UpdateParticleShouldDraw)
import qualified Shrine.Wire.Protobuf.S2.S2UserMessages.PARTICLE_MESSAGE as S2UserMessages (PARTICLE_MESSAGE)

data CUserMsg_ParticleManager = CUserMsg_ParticleManager{type' :: !(S2UserMessages.PARTICLE_MESSAGE), index :: !(P'.Word32),
                                                         release_particle_index ::
                                                         !(P'.Maybe S2UserMessages.CUserMsg_ParticleManager.ReleaseParticleIndex),
                                                         create_particle ::
                                                         !(P'.Maybe S2UserMessages.CUserMsg_ParticleManager.CreateParticle),
                                                         destroy_particle ::
                                                         !(P'.Maybe S2UserMessages.CUserMsg_ParticleManager.DestroyParticle),
                                                         destroy_particle_involving ::
                                                         !(P'.Maybe
                                                            S2UserMessages.CUserMsg_ParticleManager.DestroyParticleInvolving),
                                                         update_particle ::
                                                         !(P'.Maybe S2UserMessages.CUserMsg_ParticleManager.UpdateParticle),
                                                         update_particle_fwd ::
                                                         !(P'.Maybe S2UserMessages.CUserMsg_ParticleManager.UpdateParticleFwd),
                                                         update_particle_orient ::
                                                         !(P'.Maybe S2UserMessages.CUserMsg_ParticleManager.UpdateParticleOrient),
                                                         update_particle_fallback ::
                                                         !(P'.Maybe S2UserMessages.CUserMsg_ParticleManager.UpdateParticleFallback),
                                                         update_particle_offset ::
                                                         !(P'.Maybe S2UserMessages.CUserMsg_ParticleManager.UpdateParticleOffset),
                                                         update_particle_ent ::
                                                         !(P'.Maybe S2UserMessages.CUserMsg_ParticleManager.UpdateParticleEnt),
                                                         update_particle_should_draw ::
                                                         !(P'.Maybe
                                                            S2UserMessages.CUserMsg_ParticleManager.UpdateParticleShouldDraw),
                                                         update_particle_set_frozen ::
                                                         !(P'.Maybe
                                                            S2UserMessages.CUserMsg_ParticleManager.UpdateParticleSetFrozen),
                                                         change_control_point_attachment ::
                                                         !(P'.Maybe
                                                            S2UserMessages.CUserMsg_ParticleManager.ChangeControlPointAttachment)}
                              deriving (Prelude'.Show, Prelude'.Eq, Prelude'.Ord, Prelude'.Typeable, Prelude'.Data,
                                        Prelude'.Generic)

instance P'.Mergeable CUserMsg_ParticleManager where
  mergeAppend (CUserMsg_ParticleManager x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15)
   (CUserMsg_ParticleManager y'1 y'2 y'3 y'4 y'5 y'6 y'7 y'8 y'9 y'10 y'11 y'12 y'13 y'14 y'15)
   = CUserMsg_ParticleManager (P'.mergeAppend x'1 y'1) (P'.mergeAppend x'2 y'2) (P'.mergeAppend x'3 y'3) (P'.mergeAppend x'4 y'4)
      (P'.mergeAppend x'5 y'5)
      (P'.mergeAppend x'6 y'6)
      (P'.mergeAppend x'7 y'7)
      (P'.mergeAppend x'8 y'8)
      (P'.mergeAppend x'9 y'9)
      (P'.mergeAppend x'10 y'10)
      (P'.mergeAppend x'11 y'11)
      (P'.mergeAppend x'12 y'12)
      (P'.mergeAppend x'13 y'13)
      (P'.mergeAppend x'14 y'14)
      (P'.mergeAppend x'15 y'15)

instance P'.Default CUserMsg_ParticleManager where
  defaultValue
   = CUserMsg_ParticleManager (Prelude'.read "GAME_PARTICLE_MANAGER_EVENT_CREATE") P'.defaultValue P'.defaultValue P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue
      P'.defaultValue

instance P'.Wire CUserMsg_ParticleManager where
  wireSize ft' self'@(CUserMsg_ParticleManager x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15)
   = case ft' of
       10 -> calc'Size
       11 -> P'.prependMessageSize calc'Size
       _ -> P'.wireSizeErr ft' self'
    where
        calc'Size
         = (P'.wireSizeReq 1 14 x'1 + P'.wireSizeReq 1 13 x'2 + P'.wireSizeOpt 1 11 x'3 + P'.wireSizeOpt 1 11 x'4 +
             P'.wireSizeOpt 1 11 x'5
             + P'.wireSizeOpt 1 11 x'6
             + P'.wireSizeOpt 1 11 x'7
             + P'.wireSizeOpt 1 11 x'8
             + P'.wireSizeOpt 1 11 x'9
             + P'.wireSizeOpt 1 11 x'10
             + P'.wireSizeOpt 1 11 x'11
             + P'.wireSizeOpt 1 11 x'12
             + P'.wireSizeOpt 1 11 x'13
             + P'.wireSizeOpt 1 11 x'14
             + P'.wireSizeOpt 2 11 x'15)
  wirePut ft' self'@(CUserMsg_ParticleManager x'1 x'2 x'3 x'4 x'5 x'6 x'7 x'8 x'9 x'10 x'11 x'12 x'13 x'14 x'15)
   = case ft' of
       10 -> put'Fields
       11 -> do
               P'.putSize (P'.wireSize 10 self')
               put'Fields
       _ -> P'.wirePutErr ft' self'
    where
        put'Fields
         = do
             P'.wirePutReq 8 14 x'1
             P'.wirePutReq 16 13 x'2
             P'.wirePutOpt 26 11 x'3
             P'.wirePutOpt 34 11 x'4
             P'.wirePutOpt 42 11 x'5
             P'.wirePutOpt 50 11 x'6
             P'.wirePutOpt 58 11 x'7
             P'.wirePutOpt 66 11 x'8
             P'.wirePutOpt 74 11 x'9
             P'.wirePutOpt 82 11 x'10
             P'.wirePutOpt 90 11 x'11
             P'.wirePutOpt 98 11 x'12
             P'.wirePutOpt 114 11 x'13
             P'.wirePutOpt 122 11 x'14
             P'.wirePutOpt 130 11 x'15
  wireGet ft'
   = case ft' of
       10 -> P'.getBareMessageWith update'Self
       11 -> P'.getMessageWith update'Self
       _ -> P'.wireGetErr ft'
    where
        update'Self wire'Tag old'Self
         = case wire'Tag of
             8 -> Prelude'.fmap (\ !new'Field -> old'Self{type' = new'Field}) (P'.wireGet 14)
             16 -> Prelude'.fmap (\ !new'Field -> old'Self{index = new'Field}) (P'.wireGet 13)
             26 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{release_particle_index = P'.mergeAppend (release_particle_index old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             34 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{create_particle = P'.mergeAppend (create_particle old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             42 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{destroy_particle = P'.mergeAppend (destroy_particle old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             50 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{destroy_particle_involving =
                                P'.mergeAppend (destroy_particle_involving old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             58 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{update_particle = P'.mergeAppend (update_particle old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             66 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{update_particle_fwd = P'.mergeAppend (update_particle_fwd old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             74 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{update_particle_orient = P'.mergeAppend (update_particle_orient old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             82 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{update_particle_fallback =
                                P'.mergeAppend (update_particle_fallback old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             90 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{update_particle_offset = P'.mergeAppend (update_particle_offset old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             98 -> Prelude'.fmap
                    (\ !new'Field ->
                      old'Self{update_particle_ent = P'.mergeAppend (update_particle_ent old'Self) (Prelude'.Just new'Field)})
                    (P'.wireGet 11)
             114 -> Prelude'.fmap
                     (\ !new'Field ->
                       old'Self{update_particle_should_draw =
                                 P'.mergeAppend (update_particle_should_draw old'Self) (Prelude'.Just new'Field)})
                     (P'.wireGet 11)
             122 -> Prelude'.fmap
                     (\ !new'Field ->
                       old'Self{update_particle_set_frozen =
                                 P'.mergeAppend (update_particle_set_frozen old'Self) (Prelude'.Just new'Field)})
                     (P'.wireGet 11)
             130 -> Prelude'.fmap
                     (\ !new'Field ->
                       old'Self{change_control_point_attachment =
                                 P'.mergeAppend (change_control_point_attachment old'Self) (Prelude'.Just new'Field)})
                     (P'.wireGet 11)
             _ -> let (field'Number, wire'Type) = P'.splitWireTag wire'Tag in P'.unknown field'Number wire'Type old'Self

instance P'.MessageAPI msg' (msg' -> CUserMsg_ParticleManager) CUserMsg_ParticleManager where
  getVal m' f' = f' m'

instance P'.GPB CUserMsg_ParticleManager

instance P'.ReflectDescriptor CUserMsg_ParticleManager where
  getMessageInfo _
   = P'.GetMessageInfo (P'.fromDistinctAscList [8, 16])
      (P'.fromDistinctAscList [8, 16, 26, 34, 42, 50, 58, 66, 74, 82, 90, 98, 114, 122, 130])
  reflectDescriptorInfo _
   = Prelude'.read
      "DescriptorInfo {descName = ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"CUserMsg_ParticleManager\"}, descFilePath = [\"Shrine\",\"Wire\",\"Protobuf\",\"S2\",\"S2UserMessages\",\"CUserMsg_ParticleManager.hs\"], isGroup = False, fields = fromList [FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.type\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"type'\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 1}, wireTag = WireTag {getWireTag = 8}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 14}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.PARTICLE_MESSAGE\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\"], baseName = MName \"PARTICLE_MESSAGE\"}), hsRawDefault = Just \"GAME_PARTICLE_MANAGER_EVENT_CREATE\", hsDefault = Just (HsDef'Enum \"GAME_PARTICLE_MANAGER_EVENT_CREATE\")},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 2}, wireTag = WireTag {getWireTag = 16}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = True, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 13}, typeName = Nothing, hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.release_particle_index\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"release_particle_index\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 3}, wireTag = WireTag {getWireTag = 26}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.ReleaseParticleIndex\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"ReleaseParticleIndex\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.create_particle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"create_particle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 4}, wireTag = WireTag {getWireTag = 34}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.CreateParticle\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"CreateParticle\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.destroy_particle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"destroy_particle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 5}, wireTag = WireTag {getWireTag = 42}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.DestroyParticle\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"DestroyParticle\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.destroy_particle_involving\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"destroy_particle_involving\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 6}, wireTag = WireTag {getWireTag = 50}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.DestroyParticleInvolving\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"DestroyParticleInvolving\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.update_particle\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"update_particle\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 7}, wireTag = WireTag {getWireTag = 58}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticle\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticle\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.update_particle_fwd\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"update_particle_fwd\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 8}, wireTag = WireTag {getWireTag = 66}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleFwd\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleFwd\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.update_particle_orient\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"update_particle_orient\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 9}, wireTag = WireTag {getWireTag = 74}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleOrient\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleOrient\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.update_particle_fallback\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"update_particle_fallback\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 10}, wireTag = WireTag {getWireTag = 82}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleFallback\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleFallback\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.update_particle_offset\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"update_particle_offset\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 11}, wireTag = WireTag {getWireTag = 90}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleOffset\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleOffset\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.update_particle_ent\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"update_particle_ent\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 12}, wireTag = WireTag {getWireTag = 98}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleEnt\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleEnt\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.update_particle_should_draw\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"update_particle_should_draw\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 14}, wireTag = WireTag {getWireTag = 114}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleShouldDraw\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleShouldDraw\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.update_particle_set_frozen\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"update_particle_set_frozen\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 15}, wireTag = WireTag {getWireTag = 122}, packedTag = Nothing, wireTagLength = 1, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.UpdateParticleSetFrozen\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"UpdateParticleSetFrozen\"}), hsRawDefault = Nothing, hsDefault = Nothing},FieldInfo {fieldName = ProtoFName {protobufName' = FIName \".S2_Usermessages.CUserMsg_ParticleManager.change_control_point_attachment\", haskellPrefix' = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule' = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName' = FName \"change_control_point_attachment\", baseNamePrefix' = \"\"}, fieldNumber = FieldId {getFieldId = 16}, wireTag = WireTag {getWireTag = 130}, packedTag = Nothing, wireTagLength = 2, isPacked = False, isRequired = False, canRepeat = False, mightPack = False, typeCode = FieldType {getFieldType = 11}, typeName = Just (ProtoName {protobufName = FIName \".S2_Usermessages.CUserMsg_ParticleManager.ChangeControlPointAttachment\", haskellPrefix = [MName \"Shrine\",MName \"Wire\",MName \"Protobuf\",MName \"S2\"], parentModule = [MName \"S2UserMessages\",MName \"CUserMsg_ParticleManager\"], baseName = MName \"ChangeControlPointAttachment\"}), hsRawDefault = Nothing, hsDefault = Nothing}], descOneofs = fromList [], keys = fromList [], extRanges = [], knownKeys = fromList [], storeUnknown = False, lazyFields = False, makeLenses = False}"

instance P'.TextType CUserMsg_ParticleManager where
  tellT = P'.tellSubMessage
  getT = P'.getSubMessage

instance P'.TextMsg CUserMsg_ParticleManager where
  textPut msg
   = do
       P'.tellT "type" (type' msg)
       P'.tellT "index" (index msg)
       P'.tellT "release_particle_index" (release_particle_index msg)
       P'.tellT "create_particle" (create_particle msg)
       P'.tellT "destroy_particle" (destroy_particle msg)
       P'.tellT "destroy_particle_involving" (destroy_particle_involving msg)
       P'.tellT "update_particle" (update_particle msg)
       P'.tellT "update_particle_fwd" (update_particle_fwd msg)
       P'.tellT "update_particle_orient" (update_particle_orient msg)
       P'.tellT "update_particle_fallback" (update_particle_fallback msg)
       P'.tellT "update_particle_offset" (update_particle_offset msg)
       P'.tellT "update_particle_ent" (update_particle_ent msg)
       P'.tellT "update_particle_should_draw" (update_particle_should_draw msg)
       P'.tellT "update_particle_set_frozen" (update_particle_set_frozen msg)
       P'.tellT "change_control_point_attachment" (change_control_point_attachment msg)
  textGet
   = do
       mods <- P'.sepEndBy
                (P'.choice
                  [parse'type', parse'index, parse'release_particle_index, parse'create_particle, parse'destroy_particle,
                   parse'destroy_particle_involving, parse'update_particle, parse'update_particle_fwd, parse'update_particle_orient,
                   parse'update_particle_fallback, parse'update_particle_offset, parse'update_particle_ent,
                   parse'update_particle_should_draw, parse'update_particle_set_frozen, parse'change_control_point_attachment])
                P'.spaces
       Prelude'.return (Prelude'.foldl (\ v f -> f v) P'.defaultValue mods)
    where
        parse'type'
         = P'.try
            (do
               v <- P'.getT "type"
               Prelude'.return (\ o -> o{type' = v}))
        parse'index
         = P'.try
            (do
               v <- P'.getT "index"
               Prelude'.return (\ o -> o{index = v}))
        parse'release_particle_index
         = P'.try
            (do
               v <- P'.getT "release_particle_index"
               Prelude'.return (\ o -> o{release_particle_index = v}))
        parse'create_particle
         = P'.try
            (do
               v <- P'.getT "create_particle"
               Prelude'.return (\ o -> o{create_particle = v}))
        parse'destroy_particle
         = P'.try
            (do
               v <- P'.getT "destroy_particle"
               Prelude'.return (\ o -> o{destroy_particle = v}))
        parse'destroy_particle_involving
         = P'.try
            (do
               v <- P'.getT "destroy_particle_involving"
               Prelude'.return (\ o -> o{destroy_particle_involving = v}))
        parse'update_particle
         = P'.try
            (do
               v <- P'.getT "update_particle"
               Prelude'.return (\ o -> o{update_particle = v}))
        parse'update_particle_fwd
         = P'.try
            (do
               v <- P'.getT "update_particle_fwd"
               Prelude'.return (\ o -> o{update_particle_fwd = v}))
        parse'update_particle_orient
         = P'.try
            (do
               v <- P'.getT "update_particle_orient"
               Prelude'.return (\ o -> o{update_particle_orient = v}))
        parse'update_particle_fallback
         = P'.try
            (do
               v <- P'.getT "update_particle_fallback"
               Prelude'.return (\ o -> o{update_particle_fallback = v}))
        parse'update_particle_offset
         = P'.try
            (do
               v <- P'.getT "update_particle_offset"
               Prelude'.return (\ o -> o{update_particle_offset = v}))
        parse'update_particle_ent
         = P'.try
            (do
               v <- P'.getT "update_particle_ent"
               Prelude'.return (\ o -> o{update_particle_ent = v}))
        parse'update_particle_should_draw
         = P'.try
            (do
               v <- P'.getT "update_particle_should_draw"
               Prelude'.return (\ o -> o{update_particle_should_draw = v}))
        parse'update_particle_set_frozen
         = P'.try
            (do
               v <- P'.getT "update_particle_set_frozen"
               Prelude'.return (\ o -> o{update_particle_set_frozen = v}))
        parse'change_control_point_attachment
         = P'.try
            (do
               v <- P'.getT "change_control_point_attachment"
               Prelude'.return (\ o -> o{change_control_point_attachment = v}))