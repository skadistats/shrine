{-# LANGUAGE NoImplicitPrelude #-}

module Shrine.Replay ( Location
                     , getBytes
                     , getUBitVar
                     , getVarint32
                     , getVarUInt32
                     , reduceVarint32
                     , mkInitState
                     ) where

import Debug.Trace

import Control.Monad.Trans.State (State, state, runState, get, put)
import qualified Data.Binary.Get as G
import Data.Bits
import Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import GHC.Int
import GHC.Word
import Prelude as P' hiding (drop, fst, head, null, snd, take)

import qualified Shrine.Wire.Embed as SWE

type BufSrc = ByteString
type BufPos = Int
type BufCurRem = Int
type BufCurAcc = Word64

type Location = ( BufSrc    -- INVARIANT: ByteString with buffer data
                , BufPos    -- position of current byte
                , BufCurRem -- count of unread bits in working byte
                , BufCurAcc -- value of remaining bits in working byte
                )

gatherVarint32Bytes :: G.Get [Word8]
gatherVarint32Bytes =
  let
    stripHighBit = \b -> b .&. 0x7f
  in do
    b1 <- G.getWord8
    if not $ testBit b1 7
    then return [stripHighBit b1]
    else do
      restOfVarint32Bytes <- gatherVarint32Bytes
      if (P'.length restOfVarint32Bytes) > 4
      then
        error "malformed varint detected"
      else
        return ([stripHighBit b1] ++ restOfVarint32Bytes)

reduceVarint32 :: Int -> Int -> [Word8] -> Int
reduceVarint32 acc _ []     = acc
reduceVarint32 acc n (x:xs) =
  let
    x' = fromIntegral x :: Int
  in
    reduceVarint32 (acc .|. (shift x' n)) (n + 7) xs

getVarint32 :: G.Get Int
getVarint32 = do
  bytes <- gatherVarint32Bytes
  return $ reduceVarint32 0 0 bytes

mkInitState :: ByteString -> Location
mkInitState src = (src, 0, 0, 0)

bytesRemaining :: Location -> Int
bytesRemaining (bufSrc, bufPos, _, _) = (B.length bufSrc) - bufPos

ckAccess :: Int -> Int -> BufPos -> State Location ()
ckAccess offset bufLen bufPos
  | bufPos + offset > bufLen = error $ "bad access: pos=" ++ show bufPos
  | otherwise = do return ()

getNextByte :: State Location Word8
getNextByte = do
  (bufSrc, bufPos, bufCurRem, bufCurAcc) <- get
  let byte = head (drop bufPos bufSrc)
  ckAccess 1 (B.length bufSrc) bufPos >> put (bufSrc, bufPos + 1, bufCurRem, bufCurAcc)
  return byte

getBits' :: Int -> BufCurRem -> BufCurAcc -> State Location (BufCurRem, BufCurAcc)
getBits' len curRem curAcc
  | len > curRem = do byte <- getNextByte
                      let asWord64 = fromIntegral byte :: Word64
                          curAcc' = curAcc .|. (asWord64 `shiftL` curRem)
                      getBits' len (curRem + 8) curAcc'
  | otherwise = do return (curRem, curAcc)

getBits :: Int -> State Location Word32
getBits len =
  let mask = (1 `shiftL` len) - 1 :: Word64
  in do (bufSrc, _, bufCurRem, bufCurAcc) <- get
        (curRem, curAcc) <- getBits' len bufCurRem bufCurAcc
        (_, bufPos, _, _) <- get -- bufPos mutated since last `get`
        put (bufSrc, bufPos, (curRem - len), (curAcc `shiftR` len))
        let value = curAcc .&. mask
        return $ fromIntegral value

getBitwiseByte :: State Location Word8
getBitwiseByte = do
  (_, _, bufCurRem, _) <- get
  if bufCurRem == 0
  then do
    byte <- getNextByte
    return byte
  else do
    byte <- getBits 8
    return $ fromIntegral byte

getBytes' :: Int -> [Word8] -> State Location ByteString
getBytes' rem acc
  | rem > 0 = do byte <- getBitwiseByte
                 getBytes' (rem - 1) (acc ++ [byte])
  | otherwise = do return $ pack acc

getBytes :: Int -> State Location ByteString
getBytes len = do
  (bufSrc, bufPos, bufCurRem, bufCurAcc) <- get
  if bufCurRem == 0
  then do
    ckAccess len (B.length bufSrc) bufPos >> put (bufSrc, bufPos + len, bufCurRem, bufCurAcc)
    return $ take len (drop bufPos bufSrc)
  else do
    getBytes' len []

getUBitVar :: State Location Word32
getUBitVar = do
  head' <- getBits 6
  let op = head' .&. 0x30
      lo = head' .&. 0x0F
  case op of
    0 -> return lo
    16 -> do rem <- getBits 4
             return $ lo .|. (rem `shiftL` 4)
    32 -> do rem <- getBits 8
             return $ lo .|. (rem `shiftL` 4)
    48 -> do rem <- getBits 28
             return $ lo .|. (rem `shiftL` 4)
    otherwise -> error $ "congratulations, you've won the lottery"

getVarMaxBits :: Int -> Int
getVarMaxBits requested = ((requested + 6) `div` 7) * 7

getVarU :: Int -> Int -> Word64 -> State Location Word64
getVarU maxBits runningBits acc = do
    byte <- getBitwiseByte
    let asWord64 = fromIntegral byte :: Word64
        acc' = acc .|. ((asWord64 .&. 0x7F) `shiftL` runningBits)
        runningBits' = runningBits + 7
    if byte .&. 0x80 == 0 || runningBits' == maxBits
    then
      return acc'
    else
      getVarU maxBits runningBits' acc'

getVarUInt32 :: State Location Word32
getVarUInt32 = do
  value <- getVarU (getVarMaxBits 32) 0 0
  return $ fromIntegral value

-- // readVarInt64 reads a signed 32-bit varint
-- func (r *reader) readVarInt32() int32 {
-- 	ux := r.readVarUint32()
-- 	x := int32(ux >> 1)
-- 	if ux&1 != 0 {
-- 		x = ^x
-- 	}
-- 	return x
-- }
-- 
-- // readVarUint64 reads an unsigned 64-bit varint
-- func (r *reader) readVarUint64() uint64 {
-- 	var x, s uint64
-- 	for i := 0; ; i++ {
-- 		b := r.readByte()
-- 		if b < 0x80 {
-- 			if i > 9 || i == 9 && b > 1 {
-- 				_panicf("read overflow: varint overflows uint64")
-- 			}
-- 			return x | uint64(b)<<s
-- 		}
-- 		x |= uint64(b&0x7f) << s
-- 		s += 7
-- 	}
-- }
-- 
-- // readVarInt64 reads a signed 64-bit varint
-- func (r *reader) readVarInt64() int64 {
-- 	ux := r.readVarUint64()
-- 	x := int64(ux >> 1)
-- 	if ux&1 != 0 {
-- 		x = ^x
-- 	}
-- 	return x
-- }
-- 
-- // readBoolean reads and interprets single bit as true or false
-- func (r *reader) readBoolean() bool {
-- 	return r.readBits(1) == 1
-- }
-- 
-- // readFloat reads an IEEE 754 float
-- func (r *reader) readFloat() float32 {
-- 	return math.Float32frombits(r.readLeUint32())
-- }
-- 
-- // readUBitVarFP reads a variable length uint32 encoded using fieldpath encoding
-- func (r *reader) readUBitVarFP() uint32 {
-- 	if r.readBoolean() {
-- 		return r.readBits(2)
-- 	}
-- 	if r.readBoolean() {
-- 		return r.readBits(4)
-- 	}
-- 	if r.readBoolean() {
-- 		return r.readBits(10)
-- 	}
-- 	if r.readBoolean() {
-- 		return r.readBits(17)
-- 	}
-- 	return r.readBits(31)
-- }
-- 
-- // readStringN reads a string of a given length
-- func (r *reader) readStringN(n uint32) string {
-- 	return string(r.readBytes(n))
-- }
-- 
-- // readString reads a null terminated string
-- func (r *reader) readString() string {
-- 	buf := make([]byte, 0)
-- 	for {
-- 		b := r.readByte()
-- 		if b == 0 {
-- 			break
-- 		}
-- 		buf = append(buf, b)
-- 	}
-- 
-- 	return string(buf)
-- }
-- 
-- // readCoord reads a coord as a float32
-- func (r *reader) readCoord() float32 {
-- 	value := float32(0.0)
-- 
-- 	intval := r.readBits(1)
-- 	fractval := r.readBits(1)
-- 	signbit := false
-- 
-- 	if intval != 0 || fractval != 0 {
-- 		signbit = r.readBoolean()
-- 
-- 		if intval != 0 {
-- 			intval = r.readBits(14) + 1
-- 		}
-- 
-- 		if fractval != 0 {
-- 			fractval = r.readBits(5)
-- 		}
-- 
-- 		value = float32(intval) + float32(fractval)*(1.0/(1<<5))
-- 
-- 		// Fixup the sign if negative.
-- 		if signbit {
-- 			value = -value
-- 		}
-- 	}
-- 
-- 	return value
-- }
-- 
-- // readAngle reads a bit angle of the given size
-- func (r *reader) readAngle(n uint32) float32 {
-- 	return float32(r.readBits(n)) * 360.0 / float32(int(1<<n))
-- }
-- 
-- // readNormal reads a normalized float vector
-- func (r *reader) readNormal() float32 {
-- 	isNeg := r.readBoolean()
-- 	len := r.readBits(11)
-- 	ret := float32(len) * float32(1.0/(float32(1<<11)-1.0))
-- 
-- 	if isNeg {
-- 		return -ret
-- 	} else {
-- 		return ret
-- 	}
-- }
-- 
-- // read3BitNormal reads a normalized float vector
-- func (r *reader) read3BitNormal() []float32 {
-- 	ret := []float32{0.0, 0.0, 0.0}
-- 
-- 	hasX := r.readBoolean()
-- 	haxY := r.readBoolean()
-- 
-- 	if hasX {
-- 		ret[0] = r.readNormal()
-- 	}
-- 
-- 	if haxY {
-- 		ret[1] = r.readNormal()
-- 	}
-- 
-- 	negZ := r.readBoolean()
-- 	prodsum := ret[0]*ret[0] + ret[1]*ret[1]
-- 
-- 	if prodsum < 1.0 {
-- 		ret[2] = float32(math.Sqrt(float64(1.0 - prodsum)))
-- 	} else {
-- 		ret[2] = 0.0
-- 	}
-- 
-- 	if negZ {
-- 		ret[2] = -ret[2]
-- 	}
-- 
-- 	return ret
-- }
-- 
-- // readBitsAsBytes reads the given number of bits in groups of bytes
-- func (r *reader) readBitsAsBytes(n uint32) []byte {
-- 	tmp := make([]byte, 0)
-- 	for n >= 8 {
-- 		tmp = append(tmp, r.readByte())
-- 		n -= 8
-- 	}
-- 	if n > 0 {
-- 		tmp = append(tmp, byte(r.readBits(n)))
-- 	}
-- 	return tmp
-- }
-- 
-- // readLeUint32 reads an little-endian uint32
-- func (r *reader) readLeUint32() uint32 {
-- 	return binary.LittleEndian.Uint32(r.readBytes(4))
-- }
-- 
-- // readLeUint64 reads a little-endian uint64
-- func (r *reader) readLeUint64() uint64 {
-- 	return binary.LittleEndian.Uint64(r.readBytes(8))
-- }
