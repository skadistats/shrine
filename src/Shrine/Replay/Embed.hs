module Shrine.Replay.Embed ( getEmbedMessage
                           , getEmbeds
                           ) where

-- REMOVE ME:
import Debug.Trace
-- REMOVE ME^

import Data.ByteString as B
import Data.ByteString.Lazy as L
import Control.Monad.Trans.State

import Shrine.Replay as R
import Shrine.Wire.Embed as SWE

getEmbedMessage :: Int -> State R.Location SWE.EmbedMessage
getEmbedMessage tick = do
  kind <- getUBitVar
  size <- getVarUInt32
  data' <- getBytes $ fromIntegral size
  (_, bufPos, _, _) <- get
  trace ("kind: " ++ show kind ++ "; size: " ++ show size ++ "; buf pos: " ++ show bufPos) $ (return $ SWE.deserializeEmbedMessage (fromIntegral kind) tick (L.fromStrict data'))

getEmbeds :: Int -> [SWE.EmbedMessage] -> State Location [SWE.EmbedMessage]
getEmbeds tick acc = do
  (bufSrc, bufPos, _, _) <- get
  if (B.length bufSrc) == bufPos
  then
    trace ("bufSrc len: " ++ show (B.length bufSrc)) $ (return acc)
  else do
    message <- getEmbedMessage tick
    getEmbeds tick (acc ++ [message])
