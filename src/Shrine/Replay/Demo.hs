module Shrine.Replay.Demo ( DemoMessage(..)
                          , readAllDemoMessages
                          , ToDemoMessage
                          , toDemoMessage
                          ) where

import qualified Codec.Compression.Snappy as S
import Data.Binary.Get as G
import Data.Bits
import qualified Data.ByteString.Lazy as L hiding(map, putStrLn)
import Text.ProtocolBuffers

import qualified Shrine.Replay as R
import Shrine.Wire.Demo

getDemoMessage :: G.Get DemoMessage
getDemoMessage =
 let
   reduceVarint32' bytes = R.reduceVarint32 0 0 bytes
 in do
  rawCmnd <- R.getVarint32
  tick <- R.getVarint32
  size <- R.getVarint32
  rawData <- getByteString(size)
  let wasCompressed = (rawCmnd .&. 64) == 64 :: Bool
  let cmnd = rawCmnd .&. (complement 64)
  let data' = if wasCompressed then (S.decompress rawData) else rawData
  return $ deserializeDemoMessage cmnd tick (L.fromStrict data')

readDemoMessage :: L.ByteString -> Maybe (L.ByteString, ByteOffset, DemoMessage)
readDemoMessage bs =
  let
    message = G.runGetOrFail getDemoMessage bs
  in
    case message of
      Left  (_, _, s)    -> Nothing
      Right (bs, bo, dm) -> Just (bs, bo, dm)

readAllDemoMessages :: L.ByteString -> [DemoMessage] -> [DemoMessage]
readAllDemoMessages bs acc =
  let
    message = readDemoMessage bs
  in
    case message of
      Just (bs, bo, message) -> readAllDemoMessages bs (message : acc)
      Nothing                -> reverse acc
