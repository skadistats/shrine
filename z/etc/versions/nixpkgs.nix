{
  url = "git://github.com/onethirtyfive/nixpkgs.git";
  rev = "f2ea080e785be2876b094744415c7ad843b9524c";
  sha256 = "11l3qksmb7h06bg8hs62s7nrvyiph00k32xn9h8m28hhb6zi55bl";
}
