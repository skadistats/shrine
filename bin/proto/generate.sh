rm -rf src/Shrine/Wire/Protobuf
hprotoc -I $PWD/clarity-protobuf/src/main/proto/common \
        -d $PWD/src \
        -p "shrine.wire.protobuf.common" \
        $PWD/clarity-protobuf/src/main/proto/common/*.proto
hprotoc -I $PWD/clarity-protobuf/src/main/proto/common \
        -I $PWD/clarity-protobuf/src/main/proto/s2 \
        -d $PWD/src \
        -p "shrine.wire.protobuf.s2" \
        $PWD/clarity-protobuf/src/main/proto/s2/*.proto
